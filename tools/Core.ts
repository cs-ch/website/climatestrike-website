import {IHttpService} from "../core/@def";
import {log} from "../server/core/Logger";
import {IStrapiAuth} from "./@def";

export const getAllRoute = (route: string) => {
    return `${route}?_limit=-1`;
};

export const authenticate = async (http: IHttpService, credentials: IStrapiAuth = {}) => {
    if (credentials.identifier && credentials.password) {
        try {
            const data = await http.postd<{jwt: string}>({url: "auth/local/", body: credentials});
            http.setAuthHeader(`Bearer ${data.jwt}`);
        }
        catch (err) {
            log.error("Error authenticating: " + err);
        }
    }
};