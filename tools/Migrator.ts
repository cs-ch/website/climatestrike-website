import dotenv from "dotenv";
dotenv.config();
import {forEach, map, cloneDeep, camelCase} from "lodash";
import nodefetch from "node-fetch";

import {log} from "../server/core/Logger";

import {IMigratorModel, IMigrationCtx, EStrapiRoute, ISeedModel} from "./@def";
import {HttpService} from "../core/HttpService";
import {getAllRoute, authenticate} from "./Core";
import {Seeder} from "./Seeder";

if (!globalThis.fetch) {
    globalThis.fetch = nodefetch as any;
}

class Migrator {

    constructor(private model: IMigratorModel) {
    }

    public async migrate() {
        const values = await this.getSourceValues();
        this.write(values);
    }

    private async getSourceValues() {
        const {source, routes} = this.model;
        const http = HttpService.instance(source);
        await authenticate(http, source);
        const values: any = {};
        for (const route of routes) {
            try {
                values[route] = await http.getd({url: "/" + getAllRoute(route)});
            }
            catch (err) {
                log.error(`Migrator: Error retrieving route '${route}': ${err}`);
            }
        }
        return values;
    }

    private async write(ctx: IMigrationCtx) {
        const {target, migrators} = this.model;
        const seeder = new Seeder(target);
        const seedModel: ISeedModel = {} as any;
        forEach(migrators, migrator => {
            const {type, migrate} = migrator;

            seedModel[camelCase(migrator.type)] = map(ctx[type], d => migrate(ctx, d));
        });
        await seeder.seed(seedModel);
    }
}

if (require.main === module) {
    const migrator = new Migrator({
        source: {
            baseURL: process.env.SOURCE_URL,
            identifier: process.env.SOURCE_IDENTIFIER,
            password: process.env.SOURCE_PASSWORD
        },
        target: {
            baseURL: process.env.TARGET_URL,
            identifier: process.env.TARGET_IDENTIFIER,
            password: process.env.TARGET_PASSWORD
        },
        routes: [
            EStrapiRoute.contacts,
            EStrapiRoute.contactGroups,
            EStrapiRoute.contents,
            EStrapiRoute.events,
            EStrapiRoute.eventGroups,
            EStrapiRoute.pages,
            EStrapiRoute.posts,
        ],
        migrators: [
            {
                type: EStrapiRoute.contactGroups,
                migrate: (_ctx, item) => item // already resolved
            },
            {
                type: EStrapiRoute.eventGroups,
                migrate: (_ctx, item) => item // already resolved
            },
            {
                type: EStrapiRoute.pages,
                migrate: (_ctx, item) => item // already resolved
            },
            {
                type: EStrapiRoute.posts,
                migrate: (_ctx, item: any) => {
                    const insertItem = cloneDeep(item);
                    delete insertItem.image;
                    insertItem.content = map(insertItem.content, c => {
                        const v = {...c.config, __component: "component." + c.type};
                        return v;
                    });
                    return insertItem;
                }
            }
        ]
    });
    migrator.migrate()
        .then(() => log.debug("Done"))
        .catch((err) => log.debug(err));
}