import {TStrapiPost} from "../../@def";
import {EPostCategory} from "../../../components/@def";
import {ELanguages} from "../../../core/@def";
import {refProvider} from "./Utils";


const getRefDe = refProvider();
const getRefFr = refProvider();
const getRefIt = refProvider();

export const factsDoYouReallyWantToKnowPost: TStrapiPost = {
    "identifier": "facts-do-you-really-want-to-know",
    date: "2020-04-22T10:00:00.000Z",
    "author": [
        {
            "language": ELanguages.all,
            "text": ""
        }
    ],
    "title": [
        {
            "language": ELanguages.de,
            "text": "Willst du es wirklich wissen?"
        },
        {
            "language": ELanguages.fr,
            "text": "Voulez-vous vraiment savoir?"
        },
        {
            "language": ELanguages.it,
            "text": "Vuoi davvero saperlo?"
        }
    ],
    "link": [],
    "image": [], // {"image": [{"language": ELanguages.all,"text": "/static/images/do-you-really-want-to-know-title.png"}],"caption": [{"language": ELanguages.de,"text": "Bild: Schmelzendes arktisches Eis"}]},
    "lead": [
        {
            "language": ELanguages.de,
            "text": "Die Wissenschaft ist sich einig: Wir gefährden mit unserem Treibhausgas-Ausstoss die menschliche Zivilisation. Zweifel daran werden aber gezielt geschürt."
        },
        {
            "language": ELanguages.fr,
            "text": "La science est certaine: nous mettons en danger la civilisation humaine avec nos émissions de gaz à effet de serre. Mais les doutes à ce sujet sont délibérément alimentés."
        },
        {
            "language": ELanguages.it,
            "text": "La scienza ne è certa: stiamo mettendo in pericolo la civiltà umana con le nostre emissioni di gas serra. Ma i dubbi su questo sono volutamente alimentati."
        }
    ],
    "category": EPostCategory.fact,
    "content": [
        {
            "__component": "component.markdown",
            "text": [
                {
                    "language": ELanguages.de,
                    "text": `Graphen wie diesen hast Du sicher schon oft gesehen:
                    
![Graph, der den CO2 Anstieg zeigt](/static/images/do-you-really-want-to-know-graph.jpg)

Der darin sichtbare Zusammenhang zwischen dem menschlichen Treibhausgas-Ausstoss und dem immer heisseren Klima ist wissenschaftlich absolut unbestritten. Der ihm zugrunde liegende Treibhauseffekt beruht auf Grundlagenphysik.${getRefDe()} Tatsächlich ist sich die Wissenschaft unterdessen sicherer, dass unser CO2, Methan & co. für die beobachtete Erhitzung verantwortlich sind, als dass Rauchen zu Lungenkrebs führt.${getRefDe()}
                    
“Ich habe da aber schon anderes gehört!”, denkst Du jetzt vielleicht, und tatsächlich, es kursieren extrem viele Falschinformationen: Am Stammtisch, im Internet, in einigen Zeitungen, auf dem Twitteraccount eines gewissen amerikanischen Präsidenten.
                    
Warum halten sich aber diese offensichtlich falschen Aussagen so hartnäckig? Die Antwort darauf besteht aus zwei Teilen: Viele von uns wollen schlicht nicht an diese **unangenehme Wahrheit** glauben – und eine regelrechte Fake-News-Maschinerie liefert uns die dafür willkommenen "**alternativen Fakten**" (‘Vulkane! Sonnenaktivität!’) und andere Ausreden fürs Nichtstun (‘Die in China machen ja auch nix!ʼ). \n\n`
                },
                {
                    "language": ELanguages.fr,
                    "text": `Vous avez probablement déjà vu des graphiques de ce type à de nombreuses reprises.
                    
[Graph showing the CO2 increase](static/images/do-you-really-want-to-know-graph.jpg)

Le lien visible entre les émissions humaines de gaz à effet de serre et le climat toujours plus chaud est scientifiquement incontesté. L'effet de serre sous-jacent est basé sur la physique fondamentale.${getRefFr()} En fait, la science est maintenant plus certaine du fait que notre CO2, méthane et autres sont responsables du réchauffement observé plutôt que le tabagisme cause le cancer du poumon.${getRefFr()}

"J'ai entendu différentes choses", penses-tu peut-être maintenant, et en fait, il y a beaucoup de désinformation qui circule: Autour d’une table, sur Internet, dans certains journaux, sur le compte Twitter d'un certain président américain.

Mais pourquoi ces fausses nouvelles et pseudo-vérités persistent-elles autant? La réponse à cette question se compose de deux parties: Premièrement, beaucoup d'entre nous refusent tout simplement d'accepter cette **vérité désagréable**. D'autre part, une véritable usine à fausses informations nous fournit les "**faits alternatifs**" ("Volcans! Activité solaire!") et d'autres excuses pour ne rien faire ("Ils ne font rien en Chine non plus!"). \n`
                },
                {
                    "language": ELanguages.it,
                    "text": `Probabilmente avete già visto più volte grafici come questo.
                    
![Graph showing the CO2 increase](static/images/do-you-really-want-to-know-graph.jpg)

La connessione visibile tra le emissioni di gas serra dell'uomo e il clima sempre più caldo è scientificamente indiscussa. L'effetto serra sottostante si basa sulla fisica di base.${getRefIt()}

In realtà, la scienza è ora più certa che la nostra CO2, il metano e le altre sostanze sono responsabili del riscaldamento osservato, piuttosto che il fumo provoca il cancro ai polmoni.${getRefIt()}

"Ho sentito cose diverse", si potrebbe pensare ora, e in effetti, circolano molte informazioni errate: Al pub, su internet, su alcuni giornali, sull'account Twitter di un certo presidente americano.

Ma perché queste notizie false e queste pseudo-verità persistono così tanto? La risposta a questa domanda si compone di due parti: in primo luogo, molti di noi semplicemente non vogliono affrontare questa **spiacevole verità**. D'altra parte, una vera e propria macchina di notizie fasulle ci fornisce i gradevoli "**fatti alternativi**" ("Vulcani! Attività solare!") e altre scuse per non fare nulla ("Non stanno facendo nulla neanche in Cina!"). \n`
                }
            ]
        },
        {
            "__component": "component.collapsible-text",
            "title": [
                {
                    "language": ELanguages.de,
                    "text": "Mehr zu dieser Fake-News-Maschinerie"
                },
                {
                    "language": ELanguages.fr,
                    "text": "En savoir plus sur cette usine à fausses informations"
                },
                {
                    "language": ELanguages.it,
                    "text": "Di più su questa macchina di notizie false"
                }
            ],
            "text": [
                {
                    "language": ELanguages.de,
                    "text": `Insbesondere in den USA haben einige wenige sehr reiche Männer${getRefDe()} und einige der Unternehmen${getRefDe()}, die am meisten an der CO2-Produktion verdienen${getRefDe()}, jahrzehntelang mit viel Aufwand eine sehr erfolgreiche Desinformations- und Verwirrungskampagne betrieben.${getRefDe()} Sie haben Wissenschaftler bezahlt${getRefDe()} und bedroht${getRefDe()}, Thinktanks gegründet${getRefDe(3)}, Studien in Auftrag gegeben, und "Experten" in Talkshows geschickt, um immer wieder Zweifel an dem zu säen, was längst als gesicherte wissenschaftliche Erkenntnis galt: Dass wir selbst es sind, die mit unserer Sucht nach fossilen Brennstoffen und Rindfleisch dafür sorgen, dass die Erde immer heißer wird, die Ozeane immer saurer.${getRefDe()} Es ist gut dokumentiert, dass einige der grössten Erdölkonzerne bereits um 1970 herum ziemlich genau wussten, was sie mit ihrem Geschäftsmodell anrichten: Die von ihren eigenen Wissenschaftler*innen prognostizierte Erhitzung bis 2019 traf ziemlich genau ein.${getRefDe()}\n`
                },
                {
                    "language": ELanguages.fr,
                    "text": `Aux États-Unis en particulier, quelques hommes très riches${getRefFr()} et certaines des entreprises${getRefFr()} qui le plus d'argent grâce à la production de CO2${getRefFr()} mènent depuis des décennies une campagne de désinformation et de confusion${getRefFr()} très réussie. Ils ont payé${getRefFr()} et menacé${getRefFr()} des scientifiques, fondé des des think tanks${getRefFr(3)}, commandé des études et envoyé des "experts" dans des talk-shows pour semer le doute encore et encore sur ce qui a longtemps été considéré comme des connaissances scientifiques: que c'est nous qui, avec notre dépendance aux combustibles fossiles et au bœuf, rendons la terre de plus en plus chaude et les océans de plus en plus acides.${getRefFr()}  \nIl est bien documenté que certaines des plus grandes compagnies pétrolières savaient à peu près ce qu'elles faisaient de leur modèle d'entreprise dès 1970 environ: Le réchauffement prévu par leurs propres scientifiques* d'ici 2019 était assez précis.${getRefFr()}\n`
                },
                {
                    "language": ELanguages.it,
                    "text": `Soprattutto negli Stati Uniti, alcuni uomini molto ricchi${getRefIt()} e alcune delle aziende${getRefIt()} che guadagnano${getRefIt()} di più con la produzione di CO2 hanno condotto per decenni una campagna di disinformazione e confusione di grande successo.${getRefIt()} Hanno pagato${getRefIt()} e minacciato${getRefIt()} scienziati, fondato think tank${getRefIt(3)}, commissionato studi e inviato "esperti" in talk show per seminare sempre più dubbi su ciò che da tempo era considerato un sapere scientifico: che siamo noi stessi che, con la nostra dipendenza dai combustibili fossili e dalla carne bovina, stiamo rendendo la terra sempre più calda e gli oceani sempre più acidi.${getRefIt()} \nÈ ben documentato che alcune delle maggiori compagnie petrolifere sapevano più o meno cosa stavano facendo con il loro modello di business già intorno al 1970: il riscaldamento previsto dai loro stessi scienziati* entro il 2019 era praticamente esatto.${getRefIt()}\n`
                }
            ]
        },
        {
            "__component": "component.collapsible-text",
            "title": [
                {
                    "language": ELanguages.de,
                    "text": "Mehr zur Klimapsychologie"
                },
                {
                    "language": ELanguages.fr,
                    "text": "Plus d'informations sur la psychologie du climat"
                },
                {
                    "language": ELanguages.it,
                    "text": "Di più sulla psicologia del clima"
                }
            ],
            "text": [
                {
                    "language": ELanguages.de,
                    "text": `Dem Klimapsychologen Per Espen Stoknes zufolge lassen sich folgende fünf psychologischen Barrieren ausmachen${getRefDe()}:\n \n**1. Distanz**\n«Das geht mich doch nichts (mehr) an.»\n\nDie Klima-Angelegenheit bleibt einer Mehrheit von uns auf verschiedenste Arten fremd.\nWir können die Klimaerhitzung nicht sehen.\nWir können CO2 weder anfassen, sehen, fühlen, hören oder riechen.\nUnd abgesehen von den schmelzenden Gletschern: Die Orte, wo heute schon die Meeresspiegel deutlich steigen, wo die Überflutungen immer schlimmer werden, wo Feuer, Trockenheit und andere Folgen der Klimakrise jetzt schon so verdammt deutlich sind: Sie sind weit weg.\nEs trifft Unbekannte. Nicht mich oder meine Sippe.\nUnd die schlimmsten Folgen kommen sowieso erst noch, sie warten in der Zukunft, im nächsten Jahrhundert erst.\nDiese Gefahr ist vollkommen gegen die menschliche Natur, sie löst einfach nicht die nötigen Abwehr-/Flucht-Mechanismen aus.\n\n**2. Verhängnis:**\n«Ja, ja, die Welt geht unter. Hatten wir schon.»\n\nWenn die Klimakrise immer und überall, in all ihrer potentiellen Zerstörungskraft, als desaströse Krise dargestellt wird, verlieren viele Leute den Mut. Und wenn die Auswege aus dieser Krise nur aus Verzicht, Aufopferung und Kosten zu bestehen scheinen, hat eine Mehrheit von uns die Tendenz, einfach nicht mehr zuhören zu wollen. Und wegen Punkt 1 ist das leider allzu einfach.\nSowieso: wir fürchten uns viel stärker von Verlusten, als dass wir potentielle Gewinne sehen.\nOhne genug praktische Lösungen wachsen Frustration und Hilflosigkeit, wir hören nicht mehr zu. «Das Ende ist nah» haben wir irgendwann so oft gehört, dass wir gar nicht mehr hinschauen wollen.\n \n**3. Dissonanz:**\n«Das wird wohl nicht so schlimm sein…»\n \nWenn, das, was wir wissen (Hier: CO2 = Klimakrise) mit dem in Konflikt steht, was wir tun (Fliegen, Fleisch essen, Auto fahren, etc.) erleben wir eine sogenannte Kognitive Dissonanz.\nDas funktioniert sogar, wenn Menschen, die uns wichtig sind, anders leben, als wir es für nötig halten.\nIn beiden Fällen schwächen das Fehlen von richtigem Handeln (Auf dem Boden bleiben! Pflanzliche(re) Ernährung!) und die Abwesenheit von sozialer Unterstützung unsere Einstellung zur Klimakrise.\nWir Menschen wollen ja zwar das Richtige tun. Wir sind soziale Wesen. Wir wollen ein gutes Gewissen.\nAlso suchen wir einen Ausweg, und davon gibt es zwei: 1. Möglichst vollständige Konsequenz im eigenen Verhalten (Handeln) – oder aber 2. ein Zurechtbiegen der Wahrheit (Denken), ein Herunterspielen der Fakten, ein Anzweifeln der Aussagen von Expert*innen.\n \n**4. Leugnen:**\n«Willst Du mir etwa wegen einem Hirngespinst vorschreiben, wie ich zu leben habe?! »\n \nWenn wir ablehnen, ignorieren oder uns auf andere Arten weigern, die unangenehme Wahrheit über die Klimakrise anzuerkennen, finden wir Schutz vor Angst und Schuld. Wenn wir zu offener Ablehnung oder Spott übergehen, können wir uns an denjenigen ‘rächen’, die uns ein schlechtes Gefühl geben: indem sie unseren inkonsequenten Lebensstil kritisieren, indem sie denken, dass sie es besser wissen, indem sie uns vorschreiben wollen, wie wir zu leben haben!\n **Leugnung ist in Selbstverteidigung verwurzelt, nicht Ignoranz, Intelligenz oder Mangel an Informationen.**\n \n**5. Identität:**\n«Ja, diese Ökos halt wieder…»\n \nWir filtern Neuigkeiten durch unsere berufliche und kulturelle Identität. Wir halten Ausschau nach Informationen, die unsere bestehenden Einstellungen bestätigen und filtern raus, was diese in Frage stellt.\nWenn Menschen mit konservativer Einstellung von einem ‘Öko’ hören, dass das Klima sich ändert, ist es weniger wahrscheinlich, dass sie ihm Glauben schenken. Unsere kulturelle Identität überschreibt die Fakten. Wenn neue Information verlangt, dass wir uns ändern, wird die Information wahrscheinlich verlieren. Wir erfahren Widerstand gegen Aufrufe, uns zu ändern.\n`
                },
                {
                    "language": ELanguages.fr,
                    "text": `Selon le psychologue du climat Per Espen Stoknes, les cinq barrières psychologiques suivantes peuvent être identifiées${getRefFr()}:\n\n**1. La distance**\n"Ce ne sont plus mes affaires."\n \nLa question du climat reste étrangère à la majorité d'entre nous, à bien des égards.\nNous ne voyons pas le réchauffement du climat.\nNous ne pouvons pas toucher, voir, sentir, entendre ou sentir le CO2.\nEt mis à part la fonte des glaciers: les endroits où le niveau des mers augmente déjà de manière significative, où les inondations s'aggravent, où les incendies, la sécheresse et les autres conséquences de la crise climatique sont si évidentes en ce moment: ils sont loin.\nIl touche les inconnus. Ni moi, ni mes proches.\nEt les pires conséquences sont encore à venir de toute façon, ils attendent dans l'avenir, au siècle prochain.\nCe danger est tout à fait contraire à la nature humaine, il ne déclenche tout simplement pas les mécanismes de défense/évasion nécessaires.\n \n**2. Le destin**\n"Oui, oui, le monde arrive à sa fin. Nous savions déjà cela. »\n \nLorsque la crise climatique est présentée comme une crise désastreuse toujours et partout, dans toute sa puissance destructrice potentielle, beaucoup de gens se découragent. Et lorsque la sortie de cette crise semble ne consister qu'en renoncement, sacrifice et coût, une majorité d'entre nous a tendance à ne plus vouloir écouter. Et à cause du point 1, c'est malheureusement trop facile.\nQuoi qu'il en soit: nous avons beaucoup plus peur des pertes que des gains potentiels.\nSans suffisamment de solutions pratiques, la frustration et l'impuissance grandissent, et nous n'écoutons plus. À un moment donné, nous avons entendu si souvent "La fin est proche" que nous ne voulons plus regarder.\n \n**3. La dissonance**\n"Je ne pense pas que ce sera si mauvais..."\n \nSi ce que nous savons (ici: CO2 = crise climatique) est en conflit avec ce que nous faisons (voler, manger de la viande, conduire une voiture, etc.), nous connaissons une dissonance dite cognitive.\nCela fonctionne même lorsque des personnes qui sont importantes pour nous vivent différemment de ce que nous pensons être nécessaire.\nDans les deux cas, l'absence de mesures appropriées (Rester sur le sol, alimentation à base de plantes) et l'absence de soutien social affaiblissent notre attitude face à la crise climatique.\nAprès tout, nous, les humains, voulons faire ce qui est juste. Nous sommes des êtres sociaux. Nous voulons avoir la conscience tranquille.\nNous cherchons donc une issue, et il y en a deux: 1. la conséquence la plus complète possible dans notre propre comportement (agir) - ou 2. déformer la vérité (penser), minimiser les faits, mettre en doute les déclarations des experts.\n \n**4. Le déni**\n"Essayez-vous de me dire comment vivre à cause d'un fantasme? »\n \nSi nous refusons, ignorons ou ne reconnaissons pas la désagréable vérité sur la crise climatique, nous trouverons une protection contre la peur et la culpabilité. Si nous allons jusqu'au rejet ouvert ou au ridicule, nous pouvons nous "venger" de ceux qui nous font du tort: en critiquant notre style de vie incohérent, en pensant qu'ils savent mieux que nous, en nous disant comment vivre!\n **Le déni est ancré dans la légitime défense, et non dans l'ignorance, l'intelligence ou le manque d'information.** \n\n **5. L'identité**\n" Oui, encore ces éco-gens... "\n \nNous filtrons les informations à travers notre identité professionnelle et culturelle. Nous recherchons des informations qui confirment nos attitudes actuelles et filtrent ce qui les remet en question.\nLorsque des personnes ayant une attitude conservatrice entendent un "écolo" dire que le climat change, elles ont moins de chances de le croire. Notre identité culturelle l'emporte sur les faits. Si de nouvelles informations exigent que nous changions, il est probable que l'information soit perdue. Nous rencontrons des résistances aux appels au changement.\n`
                },
                {
                    "language": ELanguages.it,
                    "text": `Secondo lo psicologo del clima Per Espen Stoknes si possono identificare le seguenti cinque barriere psicologiche${getRefIt()}:\n\n**1. La distanza**\n"Non sono più affari miei."\n \nLa questione del clima rimane estranea alla maggior parte di noi in molti modi diversi.\nNon possiamo vedere il riscaldamento del clima.\n\nNon possiamo toccare, vedere, sentire, sentire o annusare la CO2.\nE a parte lo scioglimento dei ghiacciai...i luoghi dove il livello del mare si sta già innalzando in modo significativo, dove le inondazioni stanno peggiorando, dove gli incendi, la siccità e le altre conseguenze della crisi climatica sono così dannatamente ovvie in questo momento: sono lontani.\nColpisce gli sconosciuti. Non me o i miei parenti.\nE le conseguenze peggiori devono comunque ancora arrivare, sono in attesa nel futuro, nel prossimo secolo.\nQuesto pericolo è completamente in contrasto con la natura umana, semplicemente non innesca i necessari meccanismi di difesa/fuga.\n \n**2. Il destino**\n"Sì, sì, il mondo sta per finire. Lo sapevamo già. »\n \nQuando la crisi climatica viene dipinta come una crisi disastrosa sempre e ovunque, in tutto il suo potenziale potere distruttivo, molte persone si perdono d'animo. E quando la via d'uscita da questa crisi sembra consistere solo nella rinuncia, nel sacrificio e nel costo, la maggioranza di noi ha la tendenza a non voler più ascoltare. E a causa del punto 1 questo è purtroppo fin troppo facile.\nIn ogni caso: abbiamo molta più paura delle perdite che dei potenziali guadagni.\nSenza sufficienti soluzioni pratiche la frustrazione e l'impotenza crescono, non ascoltiamo più. A un certo punto abbiamo sentito dire "La fine è vicina" così spesso che non vogliamo più guardare.\n \n**3. La dissonanza**\n"Non credo che sarà così male..."\n \nSe ciò che sappiamo (qui: CO2 = crisi climatica) è in conflitto con ciò che facciamo (volare, mangiare carne, guidare un'auto, ecc.) avvertiamo una cosiddetta dissonanza cognitiva.\nQuesto funziona anche quando le persone che sono importanti per noi vivono in modo diverso da quanto pensiamo sia necessario.\nIn entrambi i casi, la mancanza di un'azione corretta (Restare a terra, nutrirsi di piante) e l'assenza di sostegno sociale indeboliscono il nostro atteggiamento nei confronti della crisi climatica.\nDopo tutto, noi umani vogliamo fare la cosa giusta. Siamo esseri sociali. Vogliamo avere la coscienza pulita.\nQuindi stiamo cercando una via d'uscita, e ce ne sono due: 1. una conseguenza più completa possibile nel nostro comportamento (agire) - oppure 2. piegare la verità (pensare), minimizzare i fatti, mettere in dubbio le dichiarazioni degli esperti.\n \n**4. La negazione**\n"Stai cercando di dirmi come vivere a causa di una fantasia? »\n \nSe ci rifiutiamo, ignoriamo o ci rifiutiamo di riconoscere la spiacevole verità sulla crisi climatica, troveremo protezione dalla paura e dal senso di colpa. Se passiamo al rifiuto aperto o al ridicolo, possiamo 'vendicarci' di chi ci fa sentire male: criticando il nostro stile di vita incoerente, pensando di saperne di più, dicendoci come vivere!\n **La negazione è radicata nell'autodifesa, non nell'ignoranza, nell'intelligenza o nella mancanza di informazioni.** \n\n**5. L’identità:**\n"Sì, di nuovo queste persone ecologiste..."\n \nFiltriamo le notizie attraverso la nostra identità professionale e culturale. Cerchiamo informazioni che confermino i nostri atteggiamenti esistenti e filtrino ciò che li mette in discussione.\nQuando le persone con un atteggiamento conservatore sentono da un "eco" che il clima sta cambiando, è meno probabile che gli credano. La nostra identità culturale prevale sui fatti. Se le nuove informazioni richiedono un cambiamento, è probabile che esse vadano perse. Sperimentiamo la resistenza alle chiamate al cambiamento.\n`
                }
            ]
        },
        {
            "__component": "component.markdown",
            "text": [
                {
                    "language": ELanguages.de,
                    "text": `“Unangenehme Wahrheit?”, denkst Du jetzt vielleicht, “Dann wird es eben wärmer. Aber ist denn das so schlimm?” \n \nDas ist eine berechtigte Frage, und die kurze Antwort darauf lautet **Ja – und zwar schlimmer, als die allermeisten denken.** Die zunehmenden Extremunwetter, das rapide Artensterben${getRefDe()} und die verheerenden Wald- und Buschbränden auf der ganzen Welt sind nur die Vorboten einer so katastrophalen Entwicklung, dass sie für die meisten Menschen bis heute unvorstellbar ist - oder aktiv ausgeblendet wird. (s. „Klimapsychologie“) Das Gefährliche daran: Wenn Monsterstürme${getRefDe()}, mörderische Hitzewellen${getRefDe()}, Dürreperioden und sterbende Ökosysteme${getRefDe()} sowie die daraus resultierenden Hungersnöte${getRefDe()}, Massenmigration${getRefDe()} und Ressourcenkriege${getRefDe()} auch das ‚sichere Europa‘ erreichen, **wird es zu spät sein**. Es gibt bei der Klimaerhitzung klar definierte Kipppunkte, und wenn die überschritten werden, gibt es kein Zurück mehr.\n\n Und hier kommt auch der eigentliche Grund, warum wir die Erhitzung so dringend auf 1.5 Grad beschränken müssen. Dies liegt nämlich nicht nur daran, dass die Schäden bei 2 Grad Erhitzung bereits **massiv** schlimmer werden als bei 1.5 Grad.${getRefDe(2)}\n **Vielmehr sind 1.5 Grad die Grenze, die als sicher betrachtet wird, um die Erhitzung zu stabilisieren, bevor sie außer Kontrolle gerät.**${getRefDe()}`
                },
                {
                    "language": ELanguages.fr,
                    "text": `"Une vérité désagréable?", vous pourriez penser maintenant, "Alors ça va juste se réchauffer. Mais est-ce si grave?" \n\nC'est une question légitime, et la réponse courte est **oui - et pire que ce que la plupart des gens pensent**. Le nombre croissant de phénomènes météorologiques extrêmes, l'extinction rapide des espèces${getRefFr()} et les incendies de forêt et de brousse dévastateurs dans le monde entier ne sont que les signes avant-coureurs d'une évolution si catastrophique que pour la plupart des gens, elle est encore inimaginable aujourd'hui - ou est activement ignorée (voir "Psychologie du climat"). Le danger: lorsque des tempêtes monstrueuses${getRefFr()}, des vagues de chaleur meurtrières${getRefFr()}, des sécheresses et des écosystèmes mourants${getRefFr()}, ainsi que les famines${getRefFr()}, les migrations de masse${getRefFr()} et les guerres de ressources${getRefFr()} qui en résultent, atteindront l'"Europe sûre", **il sera trop tard**. Il existe des points de basculement clairement définis dans le réchauffement climatique, et une fois qu'ils sont dépassés, il n'y a plus de retour en arrière.\n\nC'est aussi la véritable raison pour laquelle nous devons, de toute urgence, limiter le réchauffement climatique à 1,5 degré. Cela n'est pas seulement dû au fait que les dommages causés par 2 degrés sont déjà **massivement** pires que ceux causés par 1,5 degré.${getRefFr(2)} **Au contraire, 1,5 degré est la limite ultime pour stopper le réchauffement avant qu'il ne devienne incontrôlable.**${getRefFr()}\n`
                },
                {
                    "language": ELanguages.it,
                    "text": `“La spiacevole verità?", si potrebbe pensare ora: "Allora farà solo più caldo. Ma è così grave?" \n\nQuesta è una domanda legittima, e la risposta breve è sì - **e peggio di quanto molti pensino**. Il numero crescente di eventi meteorologici estremi, la rapida estinzione delle specie${getRefIt()} e i devastanti incendi di foreste e cespugli in tutto il mondo sono solo i forieri di uno sviluppo così catastrofico che per la maggior parte delle persone è ancora oggi inimmaginabile - o viene attivamente ignorato. (vedi "Psicologia del clima") La parte pericolosa: quando tempeste mostruose${getRefIt()}, ondate di calore assassine${getRefIt()}, siccità ed ecosistemi morenti${getRefIt()} e le carestie${getRefIt()}, migrazioni di massa${getRefIt()} e guerre per le risorse${getRefIt()} che ne derivano raggiungeranno "l'Europa sicura", **sarà troppo tardi**. Ci sono punti di svolta chiaramente definiti nel riscaldamento climatico, e una volta superati questi, non si può tornare indietro.\n\nQuesto è anche il vero motivo per cui dobbiamo limitare con urgenza il riscaldamento globale a 1,5 gradi. Questo non solo perché i danni causati da 2 gradi sono già **molto più** gravi di quelli causati da 1,5 gradi.${getRefIt(2)} **Piuttosto, 1,5 gradi è il limite che è considerato sicuro per stabilizzare il riscaldamento prima che vada fuori controllo.**${getRefIt()}`
                }
            ]
        },
        {
            __component: "component.image-with-caption",
            "image": [
                {
                    "language": ELanguages.all,
                    "text": "/static/images/do-you-really-want-to-know-end.JPG"
                }
            ],
            "caption": [
                {
                    "language": ELanguages.de,
                    "text": ""
                },
                {
                    "language": ELanguages.fr,
                    "text": ""
                },
                {
                    "language": ELanguages.it,
                    "text": ""
                }
            ]
        },
        {
            "__component": "component.markdown",
            "text": [
                {
                    "language": ELanguages.de,
                    "text": `Es geht darum, einen ** Dominoeffekt ** verschiedener ** Klima-Kippelemente ** zu vermeiden. 

Von diesen Kippelementen sind mehr als ein Dutzend untersucht. Dazu zählen der auftauende Permafrost, die schmelzenden Pole und das schon jetzt beobachtete außerordentliche Abbrennen riesiger Flächen Wald und Buschland. Dabei heizt das bei den Waldbränden ausgestossene CO2 schon heute das Schmelzen der Arktis weiter an, * gleichzeitig verstärken sich auch andere Prozesse gegenseitig * **– ein Teufelskreis **.

![Weltkarte mit Klima - Kipppunkten](/static/images/do-you-really-want-to-know-tipping.png)

Was in einem solchen ungebremsten Szenario auf uns zukommen kann, berechneten Forscher des Potsdam- Institut für Klimaforschung: 10 bis 60 Meter Meeresspiegelanstieg, 4 bis 5 Grad Erhitzung.${getRefDe()}
 
Achtung: Wir sprechen hier über ein Szenario, in dem diese Erhitzung ** trotz ** unserer Klimaschutz - Anstrengungen eintritt, sollten wir den Kipppunkt überschreiten. Fakt ist aber, dass wir in einem „Business as Usual“-Szenario auch ganz ohne Kipppunkte auf eine ähnliche Erhitzung kommen. Das harmloseste Szenario für den heutigen Pfad (kein echter Klimaschutz) rechnet mit einer Erhitzung von 3.7°C bis Ende Jahrhundert. Die Kosten der Klimakrise werden in diesem Szenario auf 550 Billionen Dollar geschätzt.${getRefDe()} ** Das ist mehr, als alle heute vorhandenen Vermögen zusammengenommen.** ${getRefDe()} Neuste Modelle gehen aber davon aus, dass die Erhitzung bis zum Ende des Jahrhunderts sogar bei etwa 5°C liegen könnte.${getRefDe()}
 
Einige der renommiertesten Klimawissenschaftler\\*innen warnen davor, **dass bei einer ungebremsten Erhitzung nur etwa eine Milliarde Menschen auf dem Planeten überleben könnten.** ${getRefDe()}
 
So dramatisch das klingt: ** Das Ende der menschlichen Zivilisation liegt im Bereich des Möglichen.**
        `
                },
                {
                    "language": ELanguages.fr,
                    "text": `Le but est d'éviter un **effet domino** des différents **éléments de basculement du climat**. 

Plus d'une douzaine de ces éléments de basculement ont été étudiés. Il s'agit notamment du dégel du pergélisol (permafrost), de la fonte des pôles et de l'extrême combustion de vastes zones de forêt et de maquis déjà observés. Le CO2 émis par les feux de forêt réchauffe déjà la fonte de l'Arctique${getRefFr()}, * ainsi que d'autres processus, qui se renforcent aussi mutuellement* **- un cercle vicieux**. 

![Éléments de basculement du climat dans la monde](/static/images/do-you-really-want-to-know-tipping.png)

Les chercheurs de l'Institut de recherche sur le climat de Potsdam ont calculé ce à quoi nous pouvons nous attendre dans un tel scénario non maîtrisé: une élévation du niveau de la mer de 10 à 60 mètres, une hausse de la température de 4 à 5 degrés.  ${getRefFr()}

Attention: nous parlons d'un scénario dans lequel ce réchauffement se produira **malgré** nos efforts de protection du climat si nous dépassons le point de basculement. Le fait est cependant que dans un scénario "Business as Usual", nous pouvons obtenir un réchauffement similaire même sans point de basculement. Le scénario le plus inoffensif pour la trajectoire actuelle (pas de réelle protection du climat) prévoit un réchauffement de 3,7°C d'ici la fin du siècle. Le coût de la crise climatique dans ce scénario est estimé à 550 000 milliards de dollars.${getRefFr()}  ** C'est plus que tous les actifs disponibles aujourd'hui réunis.** ${getRefFr()} Cependant, les derniers modèles supposent que le réchauffement pourrait même être d'environ 5°C d'ici la fin du siècle.${getRefFr()}

Certain.e.x.s des climatologues les plus renommé.e.x.s avertissent que ** seulement un milliard de personnes environ sur la planète pourraient survivre si le réchauffement se poursuit sans relâche.** ${getRefFr()}

Aussi dramatique que cela puisse paraître: ** la fin de la civilisation humaine est du domaine du possible.**
    `
                },
                {
                    "language": ELanguages.it,
                    "text": `Più di una ** dozzina ** di questi ** elementi di ribaltamento ** sono stati studiati.

Tra questi, lo scongelamento del permafrost, lo scioglimento dei poli e la straordinaria combustione di vaste aree di foresta e di macchia già osservate. La CO2 emessa dagli incendi boschivi sta già accelerando lo scioglimento dell'Artico, *mentre altri processi si stanno rafforzando a vicenda* **- un circolo vizioso**. 

![Elementi di ribaltamento nel mondo](/static/images/do-you-really-want-to-know-tipping.png)

I ricercatori del Potsdam Institute for Climate Research hanno calcolato quello che ci si può aspettare in uno scenario così incontrollato: un innalzamento del livello del mare da 10 a 60 metri, un aumento della temperatura da 4 a 5 gradi.${getRefIt()}

Attenzione: stiamo parlando di uno scenario in cui questo riscaldamento si verificherà nonostante i nostri sforzi per la protezione del clima, se superiamo il punto di non ritorno. Il fatto è, tuttavia, che in uno scenario "Business as Usual" possiamo ottenere un riscaldamento simile anche senza punti di ribaltamento. Lo scenario più innocuo per il percorso odierno (nessuna vera protezione del clima) prevede un riscaldamento di 3, 7°C entro la fine del secolo. Il costo della crisi climatica in questo scenario è stimato in 550’000 miliardi di dollari.${getRefIt()}  ** Questo è più di tutti i beni disponibili oggi messi insieme.** ${getRefIt()} Tuttavia, gli ultimi modelli ipotizzano che il riscaldamento potrebbe arrivare a circa 5°C entro la fine del secolo.${getRefIt()}

Alcuni / e dei / delle più rinomati / e scienziati / e del clima avvertono ** che solo un miliardo di persone sul pianeta potrebbe sopravvivere se il riscaldamento continua ad aumentare.** ${getRefIt()}

Per quanto possa sembrare drammatico: ** la fine della civiltà umana è nel regno delle possibilità.**
    `
                }
            ]
        },
        {
            "__component": "component.collapsible-text",
            "title": [
                {
                    "language": ELanguages.de,
                    "text": "Mehr zu den Klima-Kipppunkten"
                },
                {
                    "language": ELanguages.fr,
                    "text": "En savoir plus sur les points de basculement du climat"
                },
                {
                    "language": ELanguages.it,
                    "text": "Per saperne di più sui punti di non ritorno climatici"
                }
            ],
            "text": [
                {
                    "language": ELanguages.de,
                    "text": `Wenn sich ein Prozess selber verstärkt, spricht die Wissenschaft von einer „positiven Rückkopplung“. Im Klimasystem gibt es einen Punkt, an dem sich dieser Prozess so stark verselbstständigt, dass er durch menschlichen Einfluss kaum oder gar nicht mehr gestoppt werden kann. Das Klima erhitzt sich unkontrolliert von selber, es kippt in einen neuen, viel heisseren Zustand: Unser Planet wird zur „Hothouse Earth“, zum Treibhaus Erde.${getRefDe()}

Wie laufen diese Prozesse ab? Ein einfach zu verstehendes Beispiel ist die abtauende Arktis: Helles Eis reflektiert, schwarzes Wasser hingegen absorbiert einfallendes Licht. Je weniger Eis, desto mehr Erhitzung.
** Und da das arktische Meereis unterdessen um beinahe die Hälfte abgenommen hat, macht dieser Effekt alleine bereits ein Fünftel der globalen Erhitzung aus.**

Ein noch beunruhigenderes Beispiel ist aber der auftauende Permafrost. Im gefrorenen Boden der nördlichen Breiten sind gigantische Mengen (1700 Mia t) Kohlenstoff gespeichert. Bei steigenden Temperaturen taut dieser Boden auf. Beim Überschreiten der 1.5 - Grad - Grenze rechnen wir so mit zusätzlichen 68 bis 508 Milliarden Tonnen Kohlenstoff, die in die Atmosphäre entweichen.${getRefDe()} Vergleich: ** Bis jetzt sind wir bei ca. 500 Milliarden Tonnen menschlichen Emissionen.**

Kipppunkte können sogar ganze Ökosysteme auslöschen. So wird erwartet, dass unglaubliche 99 % der tropischen Korallen verloren gehen, wenn die globale Durchschnittstemperatur um 2 °C steigt. Dies geschieht wegen Wechselwirkungen zwischen Erhitzung, Versauerung der Ozeane und Verschmutzung. Dies würde einen massiven Verlust an mariner Biodiversität und menschlichen Lebensgrundlagen bedeuten: In Korallenriffen leben ein Viertel aller marinen Arten und 500 Millionen Menschen sind direkt oder indirekt abhängig von Dingen wie Nahrung, Tourismus und Schutz vor Stürmen.${getRefDe()}

Auch unsere Regenwälder könnten schon bald kollabieren. Ein Kipppunkt im Amazonasgebiet könnte zwischen 40 % Entwaldung bis zu nur 20 % Verlust an Waldfläche liegen. ** Seit 1970 sind davon bereits 17 % verloren gegangen.** Die Kombination von globaler Erhitzung und Rodungen könnten uns also schon sehr bald in die gefährliche Zone bringen, in welcher der Regenwald durch Verdunstung nicht mehr genug Niederschlag generieren kann, um sich selber zu erhalten. In diesem Fall würde der Amazonas zur Savanne degradieren.${getRefDe()}

Auch die nördlichen Wälder sind in Gefahr. Die Erhitzung hat bereits großflächigen Befall durch Insekten und eine Zunahme von Bränden ausgelöst. So brannte letztes Jahr in Sibirien eine Waldfläche von der Grösse der Schweiz ab. Dabei beschleunigt der schwarze Russ noch das Tauen der Arktis, wenn er sich auf dem Eis ablagert – ein weiteres Beispiel dafür, wie sich diese Prozesse gegenseitig verstärken.${getRefDe()}

Alle diese Rückkopplungsprozesse schmälern unsere Chance, die 1, 5 - Grad Grenze zu erreichen, nochmals erheblich. Das verbleibende Emissionsbudget der Welt für eine 50: 50 - Chance, innerhalb der 1, 5 °C Erwärmung zu bleiben, beträgt nur noch etwa 500 Gigatonnen (Gt) CO2. Die Emissionen aus dem Permafrost könnten dieses Budget um schätzungsweise 20 % (100 Gt CO2) verringern, und zwar ohne Berücksichtigung von Methan aus tiefem Permafrost oder unterseeischen Hydraten. Wenn die Wälder nahe an den Kipppunkten liegen, könnte das Amazonas - Sterben weitere 90 Gt CO2 freisetzen und die borealen Wälder weitere 110 Gt CO2. **Da die globalen CO2-Gesamtemissionen immer noch mehr als 40 Gt pro Jahr betragen, könnte das verbleibende Budget bereits fast vollständig aufgebraucht sein.**

Das atmosphärische CO2 hat bereits ein Niveau erreicht, das zuletzt vor etwa vier Millionen Jahren, im Pliozän, beobachtet wurde. Es nähert sich rasch den Werten, die zuletzt vor etwa 50 Millionen Jahren - im Eozän - beobachtet wurden, als die Temperaturen bis zu 14 °C höher waren als in der vorindustriellen Zeit.
Eine mögliche Erklärung für diese nochmals signifikant höheren Temperaturen ist, dass die Modelle einen wichtigen Kipppunkt übersehen haben: Ein in diesem Jahr veröffentlichtes wolkenauflösendes Modell legt nahe, dass das abrupte Aufbrechen der Stratocumulus - Wolke über etwa 1.200 Teile pro Million CO2 zu einer globalen Erwärmung von etwa 8 °C geführt haben könnte.

All diese Fakten legen nahe, dass wir uns in einer extrem gefährlichen Situation mit massiven Risiken befinden. Wir verlangen von unserer Regierung eine diesem Notstand angemessene Reaktion.
`
                },
                {
                    "language": ELanguages.fr,
                    "text": `Lorsqu'un processus se renforce, la science parle de "feed-back positif". Il y a un moment dans le système climatique où ce processus devient si autonome qu'il ne peut plus ou à peine être arrêté par l'influence humaine. Le climat se réchauffe de façon incontrôlée, il bascule dans un nouvel état, beaucoup plus chaud: notre planète devient une "planète étuve".${getRefFr()}  

Comment ces processus se déroulent - ils? Un exemple facile à comprendre est l'Arctique, qui est en train de fondre: la glace légère réfléchit, mais les eaux noires absorbent la lumière incidente. Moins il y a de glace, plus il y a de réchauffement. 
** Et comme la glace de mer arctique a entre - temps diminué de près de moitié, cet effet représente à lui seul un cinquième du réchauffement climatique.**

Mais un exemple encore plus inquiétant est le dégel du pergélisol. Le sol gelé du Nord stocke des quantités gigantesques (1700 milliards de tonnes) de carbone. Avec l'augmentation des températures, ce sol dégèle. Si la limite de 1,5 degré est dépassée, nous prévoyons que 68 à 508 milliards de tonnes de carbone supplémentaires seront libérées dans l'atmosphère.${getRefFr()} Comparaison: Jusqu'à présent, nous en sommes à environ **500 milliards de tonnes d'émissions humaines.**

Les points de basculement peuvent même anéantir des écosystèmes entiers. Par exemple, on s'attend à ce que 99% des coraux tropicaux disparaissent lorsque la température moyenne mondiale augmentera de 2°C. Cela se produit en raison des interactions entre le réchauffement, l'acidification des océans et la pollution. Cela signifierait une perte massive de la biodiversité marine et des moyens de subsistance de l'homme: Les récifs coralliens abritent un quart de toutes les espèces marines et 500 millions de personnes dépendent directement ou indirectement de choses comme la nourriture, le tourisme et la protection contre les tempêtes.${getRefFr()}

Nos forêts tropicales pourraient également s'effondrer très bientôt. Un point de basculement dans la région amazonienne pourrait aller de 40% de déforestation à seulement 20% de perte de surface forestière. **Depuis 1970, 17% de cette somme a déjà été perdue.** La combinaison du réchauffement climatique et de la déforestation pourrait bientôt nous amener dans la zone dangereuse où la forêt tropicale humide ne peut pas générer suffisamment de précipitations pour se maintenir par évapo-transpiration. Dans ce cas, l'Amazonie se dégraderait en savane.${getRefFr()}

Les forêts du Nord sont également en danger. Le réchauffement a déjà provoqué une infestation importante d'insectes et une augmentation des incendies. L'année dernière, par exemple, une zone forestière de la taille de la Suisse a brûlé en Sibérie. La suie noire accélère le dégel de l'Arctique lorsqu'elle se dépose sur la glace - un autre exemple de la façon dont ces processus se renforcent mutuellement.${getRefFr()}

Tous ces processus de retour d'information réduisent encore une fois de manière significative nos chances d'atteindre la note de 1, 5 degré. Le budget d'émissions restant pour une chance sur deux de rester dans les limites du réchauffement de 1,5 degré n'est que d'environ 500 gigatonnes (Gt) de CO2. Les émissions du pergélisol pourraient réduire ce budget d'environ 20 % (100 Gt CO2), sans compter le méthane provenant du pergélisol profond ou des hydrates sous - marins. Si les forêts sont proches des points de basculement, la mort de l'Amazonie pourrait libérer 90 Gt de CO2 supplémentaires et les forêts boréales 110 Gt de CO2 supplémentaires. **Comme les émissions totales de CO2 dans le monde dépassent encore 40 Gt par an, le budget restant pourrait déjà être presque entièrement épuisé.**

Le CO2 atmosphérique a déjà atteint un niveau observé pour la dernière fois il y a environ quatre millions d'années, dans le Pliocène. Il se rapproche rapidement des derniers niveaux observés il y a environ 50 millions d'années - à l'Éocène - lorsque les températures étaient jusqu'à 14°C plus élevées qu'à l'époque préindustrielle.
Une explication possible de ces températures à nouveau nettement plus élevées est que les modèles ont négligé un point de basculement important: Un modèle de résolution des nuages publié cette année suggère que la rupture brutale du nuage de stratocumulus au - dessus d'environ 1’200 parties par million de CO2 pourrait avoir entraîné un réchauffement de la planète d'environ 8°C.

Tous ces faits suggèrent que nous sommes dans une situation extrêmement dangereuse avec des risques massifs. Nous exigeons de notre gouvernement une réponse adaptée à cette urgence. 
`
                },
                {
                    "language": ELanguages.it,
                    "text": `Quando un processo si rafforza, la scienza parla di "feedback positivo". C'è un punto nel processo del cambiamento climatico dopo il quale questo processo diventa così autonomo che non può più o quasi essere fermato tramite influenza umana. Il clima si riscalda da solo senza controllo, si ribalta in un nuovo stato molto più caldo: il nostro pianeta diventa una "Terra calda".${getRefIt()}

Come si svolgono questi processi? Un esempio di facile comprensione è l'Artico, che si sta sciogliendo: il ghiaccio leggero riflette, ma l'acqua nera assorbe la luce incidente. Meno ghiaccio c'è, più si riscalda. 
**E poiché il ghiaccio del mare Artico è nel frattempo diminuito di quasi la metà, questo effetto da solo rappresenta un quinto del riscaldamento globale.**

Ma un esempio ancora più preoccupante è lo scioglimento del permafrost. Il suolo ghiacciato delle latitudini settentrionali immagazzina quantità gigantesche (1700 miliardi di tonnellate) di carbonio. Con l'aumento delle temperature, questo terreno si scongela. Se il limite di 1,5 gradi viene superato, ci aspettiamo che vengano rilasciati nell'atmosfera altri 68 - 508 miliardi di tonnellate di carbonio.${getRefIt()}
Confronto: **finora siamo a circa 500 miliardi di tonnellate di emissioni umane.**

I punti di ribaltamento possono anche spazzare via interi ecosistemi. Ad esempio, si prevede che un incredibile 99 % dei coralli tropicali andrà perduto quando la temperatura media globale aumenterà di 2°C. Ciò avviene a causa delle interazioni tra riscaldamento, acidificazione degli oceani e inquinamento. Ciò significherebbe una massiccia perdita di biodiversità marina e di mezzi di sussistenza umana: Le barriere coralline ospitano un quarto di tutte le specie marine e 500 milioni di persone dipendono direttamente o indirettamente da cose come il cibo, il turismo e la protezione dalle tempeste.${getRefIt()}

Anche le nostre foreste pluviali potrebbero crollare molto presto. Un punto critico nella regione amazzonica potrebbe variare dal 40 % di deforestazione a solo il 20 % di perdita di superficie forestale. Dal **1970, il 17 % di questo è già andato perduto.** La combinazione del riscaldamento globale e della deforestazione potrebbe presto portarci nella zona pericolosa dove la foresta pluviale non può generare precipitazioni sufficienti a sostenersi da sola attraverso l'evaporazione. In questo caso, l'Amazzonia si degraderebbe a savana.${getRefIt()}

Anche le foreste del nord sono in pericolo. Il riscaldamento ha già causato un'estesa infestazione di insetti e un aumento degli incendi. L'anno scorso, ad esempio, in Siberia è bruciata un'area di foresta grande quanto la Svizzera. La fuliggine nera accelera lo scongelamento dell'Artico quando si deposita sul ghiaccio - un altro esempio di come questi processi si rafforzano a vicenda.${getRefIt()}

Tutti questi processi di feedback riducono ancora una volta in modo significativo le nostre possibilità di raggiungere la soglia di 1, 5 gradi. Il bilancio mondiale delle emissioni rimanente per una probabilità di rimanere entro il riscaldamento di 1, 5 gradi è di soli 500 gigatoni (Gt) di CO2. Le emissioni da permafrost potrebbero ridurre questo budget di circa il 20 % (100 Gt di CO2), escluso il metano proveniente dal permafrost profondo o dagli idrati sottomarini. Se le foreste sono vicine ai punti di ribaltamento, la morte dell'Amazzonia potrebbe rilasciare altre 90 Gt di CO2 e le foreste boreali altre 110 Gt di CO2. **Poiché le emissioni globali totali di CO2 superano ancora i 40 Gt all'anno, il budget rimanente potrebbe già essere quasi completamente esaurito.**

La CO2 atmosferica ha già raggiunto un livello osservato l'ultima volta circa quattro milioni di anni fa, nel Pliocene. Si sta rapidamente avvicinando ai livelli osservati l'ultima volta circa 50 milioni di anni fa - nell'Eocene - quando le temperature erano fino a 14°C superiori a quelle dell'era preindustriale.
Una possibile spiegazione per queste temperature ancora una volta significativamente più elevate è che i modelli hanno trascurato un importante punto di ribaltamento: Un modello di risoluzione delle nubi pubblicato quest'anno suggerisce che la brusca rottura della nube di stratocumulo al di sopra di circa 1.200 parti per milione di CO2 avrebbe potuto portare a un riscaldamento globale di circa 8 °C.

Tutti questi fatti suggeriscono che ci troviamo in una situazione estremamente pericolosa con rischi enormi. Chiediamo al nostro governo una risposta adeguata a questa emergenza. 
`
                }
            ]
        },
        {
            "__component": "component.collapsible-text",
            "title": [
                {
                    "language": ELanguages.de,
                    "text": "Das ist doch völlig übertrieben!?"
                },
                {
                    "language": ELanguages.fr,
                    "text": "C'est totalement exagéré!?"
                },
                {
                    "language": ELanguages.it,
                    "text": "Questo è totalmente esagerato!?"
                }
            ],
            "text": [
                {
                    "language": ELanguages.de,
                    "text": `So krass wird das jetzt wohl nicht werden, denkst Du jetzt wahrscheinlich – und das völlig zu Recht, schliesslich hörst Du in unseren Medien kaum davon.

Die Gründe, dass es diese Szenarien so wenig in die Medien schaffen, sind vielschichtig:
 
* Zum einen sind viele Journalist\\*innen einfach auch ‚nur Menschen‘ und haben entsprechend ihre eigenen ** kognitiven Abwehrmechanismen ** (s.„Klimapsychologie“).
* Zum anderen ist der Untergang der menschlichen Zivilisation schlicht ein sehr ** undankbares Nachrichtenthema ** \\- die Leute haben irgendwann genug vom Weltuntergang.
* Nicht zu unterschätzen ist auch ** der Faktor Werbung **. Ein von der ETH herausgegebener Bericht hält fest, dass der Einfluss der Werbefinanzierung auf die redaktionellen Inhalte „unbestritten“ ist.${getRefDe()} Nicht von der Hand zu weisen ist zum Beispiel, dass fossile Wirtschaftszweige, allen voran die Autokonzerne, einen substanziellen Teil der Werbeeinnahmen ausmachen. Historikerin Ariane Tanner verweist in diesem Zusammenhang auf zahllose Beispiele, in denen die Thematik verzerrt dargestellt wurde, so zum Beispiel die Berichterstattung über die nationale Klimademo letzten Herbst: Viel Platz für Klimaschutz - oder Velogegner, wenig bis sehr wenig für die Klimademo selber.${getRefDe(2)}

Zu all dem kommt, dass die Prognosen des Weltklimarats IPCC (dem UNO - Ausschuss von Klimawissenschaftler\\*innen) die tatsächliche Entwicklung systematisch und teils massiv **unter**schätzen:
 
* Im Jahr 2001 schätzte der IPCC, dass der Meeresspiegel um weniger als 2 mm pro Jahr steigen würde. Tatsächlich betrug der Anstieg des Meeresspiegels 3, 3 mm pro Jahr.
* Im Jahr 2007 berichtete der IPCC, dass die Arktis ihr Sommereis nicht vor 2070 verlieren würde. Nun erwarten wir, dass die Arktis bereits 2030 im Sommer frei von Meereis sein wird.${getRefDe()}
* Das Auftauen des Permafrosts (ein gefährliches Kippelement, s.„Mehr zu Klima - Kipppunkten“) war 2019 an einigen Messstationen bereits so weit fortgeschritten wie ursprünglich vom IPCC für 2090 prognostiziert.${getRefDe()}

Warum das? Der IPCC ist gezwungen, einen Konsens zwischen vielen Parteien zu erreichen, was seine Prognosen verzerrt. Es herrscht dementsprechend eine Tendenz zu „least drama“-Szenarien. Sein Bericht muss von allen Regierungen, einschließlich Ölstaaten wie Saudi - Arabien, gebilligt werden. Deshalb hinkt der IPCC hinter der aktuellen Klimawissenschaft her, auch wenn er sich mit seinen neusten Berichten ungewohnt deutlich geäussert hat.${getRefDe()} `
                },
                {
                    "language": ELanguages.fr,
                    "text": `Je ne pense pas que ce sera si grave, c’est probablement ce que tu penses maintenant - et à juste titre, après tout tu n'en entends presque jamais parler dans nos médias. 

Les raisons pour lesquelles ces scénarios font si peu parler d'eux dans les médias sont complexes: 

* D'une part, de nombreux journalistes sont simplement des "êtres humains" et ont leurs propres mécanismes de **défense cognitive** (voir "Psychologie du climat").
* D'autre part, la chute de la civilisation humaine est tout simplement un **sujet d'actualité très peu agréable ** à traiter \\- à un moment donné, les gens en ont assez de la fin du monde.
* Le facteur publicité ne doit pas non plus être sous - estimé. Un rapport publié par l'ETH indique que l'influence du financement de la publicité sur le contenu éditorial est "incontestée".${getRefFr()} On ne peut pas nier, par exemple, que les industries fossiles, surtout les constructeurs automobiles, représentent une part importante des recettes publicitaires. L'historienne Ariane Tanner renvoie dans ce contexte à d'innombrables exemples où le sujet a été présenté de manière déformée, comme la couverture de la manifestation nationale pour le climat  l'automne dernier: beaucoup de place pour les opposants à la protection du climat ou au cyclisme, peu ou très peu pour la manifestation elle-même.${getRefFr(2)}

En outre, les prévisions du Groupe d'experts intergouvernemental sur l'évolution du climat (GIEC) (le comité des scientifiques du climat des Nations unies *) sous - estiment systématiquement et parfois massivement les développements réels:

* En 2001, le GIEC a estimé que le niveau de la mer augmenterait de moins de 2 mm par an. En fait, l'élévation du niveau de la mer était de 3,3 mm par an. 
* En 2007, le GIEC a indiqué que l'Arctique ne perdrait pas sa glace d'été avant 2070. Aujourd'hui, nous prévoyons que l'Arctique sera libre de glace de mer en été dès 2030.  ${getRefFr()}
* Le dégel du permafrost (un élément de basculement dangereux, voir "En savoir plus sur les points de basculement climatiques") était déjà aussi avancé en 2019 dans certaines stations de surveillance que ce que le GIEC avait prévu à l'origine pour 2090.${getRefFr()}

Et pourquoi donc? Le GIEC est obligé de parvenir à un consensus entre de nombreux partis, ce qui fausse ses prévisions. Comme conséquence, on observe une tendance à privilégier les scénarios "les moins dramatiques". Son rapport doit être approuvé par tous les gouvernements, y compris les États pétroliers comme l'Arabie saoudite. C'est pourquoi le GIEC est en retard sur la science climatique actuelle, même si ses derniers rapports sont exceptionnellement clairs.${getRefFr()}
`
                },
                {
                    "language": ELanguages.it,
                    "text": `Non credo che sarà così male, probabilmente lo pensi ora - e giustamente, dopo tutto non ne sentirete quasi mai parlare nei nostri media.

Le ragioni per le quali questi scenari rendono così poco mediatici sono complesse: 

* Per prima cosa, molti giornalisti sono semplicemente "solo persone" e hanno i loro meccanismi di **difesa cognitiva** (vedi "Psicologia del clima").
* D'altra parte, la caduta della civiltà umana è semplicemente una **notizia molto ingrata** \\- a un certo punto la gente ne ha abbastanza della fine del mondo.
* Anche il **fattore pubblicità** non va sottovalutato. Secondo un rapporto pubblicato dal PF, l'influenza del finanziamento pubblicitario sui contenuti editoriali è "indiscussa".${getRefIt()}  Non si può negare, ad esempio, che le industrie fossili, soprattutto le case automobilistiche, rappresentano una parte sostanziale delle entrate pubblicitarie. Lo storico Ariane Tanner fa riferimento in questo contesto a innumerevoli esempi in cui l'argomento è stato presentato in modo distorto, come la copertura del Klimademo nazionale dello scorso autunno: tanto spazio per la protezione del clima o per gli avversari del ciclismo, poco o molto poco per il Klimademo stesso.${getRefIt(2)}

Oltre a tutto questo, le previsioni dell'Intergovernmental Panel on Climate Change (IPCC) (il comitato degli scienziati del clima delle Nazioni Unite) **sotto**valutano sistematicamente e a volte massicciamente gli sviluppi reali:

* Nel 2001, l'IPCC ha stimato che il livello del mare sarebbe aumentato di meno di 2 mm all'anno. In effetti, l'innalzamento del livello del mare è stato di 3,3 mm all'anno. 
* Nel 2007, l'IPCC ha riferito che l'Artico non avrebbe perso i suoi ghiacci estivi fino al 2070. Ora ci aspettiamo che l'Artico sia libero dal ghiaccio marino in estate già nel 2030.${getRefIt()}
* Lo scioglimento del permafrost (un pericoloso elemento di ribaltamento, vedi "Ulteriori informazioni sui punti di ribaltamento climatici") era già avanzato al massimo nel 2019 in alcune stazioni di monitoraggio, come originariamente previsto dall'IPCC per il 2090.${getRefIt()}

Perché questo? L'IPCC è costretto a raggiungere un consenso tra molte parti, il che distorce le sue previsioni. Di conseguenza, c'è una tendenza verso scenari "meno drammatici". Il suo rapporto deve essere approvato da tutti i governi, compresi gli Stati petroliferi come l'Arabia Saudita. Per questo motivo l'IPCC è in ritardo rispetto all'attuale scienza del clima, anche se i suoi ultimi rapporti sono insolitamente chiari.${getRefIt()}
    `
                }
            ]
        },
        {
            "__component": "component.markdown",
            "text": [
                {
                    "language": ELanguages.de,
                    "text": `*Zu den Autoren: Matthias & Nicola studieren im 3. Jahr Umweltnaturwissenschaften an der ETH und sind beide seit Tag 1 im Klimastreik aktiv.*
                    `
                },
                {
                    "language": ELanguages.fr,
                    "text": `*À propos des auteurs : Matthias & Nicola sont en troisième année d'études en sciences de l'environnement à l'ETH et ont tous deux participé activement à la grève du climat depuis le premier jour.*
                    `
                },
                {
                    "language": ELanguages.it,
                    "text": `*A proposito degli autori: Matthias e Nicola sono al terzo anno di studi di scienze ambientali al Politecnico federale e sono entrambi attivi nello sciopero del clima dal primo giorno.*
                    `
                }
            ]
        },
        {
            "__component": "component.references",
            "references": [
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "IPCC 2018: “Summary for Policymakers”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.ipcc.ch/site/assets/uploads/sites/2/2019/05/SR15_SPM_version_report_LR.pdf"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Scientific American (2014): “Climate Risks as Conclusive as Link between Smoking and Lung Cancer”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.scientificamerican.com/article/climate-risks-as-conclusive-as-link-between-smoking-and-lung-cancer/"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "The New Yorker (2019): ““Kochland” Examines the Koch Brothers’ Early, Crucial Role in Climate-Change Denial”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.newyorker.com/news/daily-comment/kochland-examines-how-the-koch-brothers-made-their-fortune-and-the-influence-it-bought"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Der SPIEGEL (2007): «10.000 Dollar für Widerlegung der Klimastudie ausgelobt»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.spiegel.de/wissenschaft/natur/oelindustrie-10-000-dollar-fuer-widerlegung-der-klimastudie-ausgelobt-a-463887.html"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Welches leider auch gleich einige der reichsten und mächtigsten Konzerne der Welt sind. The Guardian (2017): “Just 100 companies responsible for 71% of global emissions, study says”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.theguardian.com/sustainable-business/2017/jul/10/100-fossil-fuel-companies-investors-responsible-71-global-emissions-cdp-study-climate-change"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Wikipedia (2020): “Global Warming Controversy”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://en.wikipedia.org/wiki/Global_warming_controversy"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Tagesanzeiger (2020): “«Shell Papers»: Dutzende Multis finanzierten Klima-Skeptiker»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.tagesanzeiger.ch/wirtschaft/standardshell-papers-dutzende-multis-finanzierten-klimaskeptiker/story/21591385"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "NZZ (2019): “Der Klimakrieg: Ein internationales Netz von Klimaskeptikern greift Forscher an»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://nzzas.nzz.ch/hintergrund/klimawandel-wissenschaft-wird-von-leugnern-weltweit-diffamiert-ld.1465989?reduced=true"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "The Guardian (2018): “How the Koch brothers built the most powerful rightwing group you've never heard of”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.theguardian.com/us-news/2018/sep/26/koch-brothers-americans-for-prosperity-rightwing-political-group"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Desmog (2020): “Americans For Prosperity”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.desmogblog.com/americans-for-prosperity"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Correctiv (2020): “Die Heartland-Lobby”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://correctiv.org/top-stories/2020/02/04/die-heartland-lobby/"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Ganzer Abschnitt mit leichten Ergänzungen aus Christian Stöckers Artikel im SPIEGEL (2019): «Gemeinsam gegen den Golem»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.spiegel.de/wissenschaft/mensch/klimakrise-gemeinsam-gegen-den-golem-a-1286896.html"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "The Guardian (2019): “Exxon sowed doubt about climate crisis, House Democrats hear in testimony”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.theguardian.com/business/2019/oct/23/exxon-climate-crisis-house-democrats-hearing"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Per Espen Stoknes: “What We Think About When We Try Not To Think About Global Warming”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.researchgate.net/publication/280683963_What_We_Think_About_When_We_Try_Not_To_Think_About_Global_Warming"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Seit 1970 sind die weltweiten Wirbeltierpopulationen um mehr als 60% geschwunden. WWF (2018): “Living Planet Report»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.wwf.ch/sites/default/files/doc-2018-10/LPR2018_Full%20Report%20Pages_22.10.2018_0.pdf"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "The Guardian (2019): «Are hurricanes getting stronger – and is climate breakdown to blame?»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.theguardian.com/world/2019/may/20/are-hurricanes-getting-stronger-and-is-the-climate-crisis-to-blame"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Hitzesommer wie 2003 oder 2018 werden zum Normalfall. Bundesamt für Umwelt (2018): «Hitze und Trockenheit im Sommer 2018»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.bafu.admin.ch/bafu/de/home/themen/klima/publikationen-studien/publikationen/hitze-und-trockenheit.html"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Zum Beispiel das durch Hitzewellen ausgelöste Sterben von 98% aller Bodeninsekten in Puerto Rico. The Guardian (2019): “Insect collapse: ‘We are destroying our life support systems’”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.theguardian.com/environment/2019/jan/15/insect-collapse-we-are-destroying-our-life-support-systems?fbclid=IwAR02PMolwt1UYuGYdc1szbYvcgWkPDQZRvXpQAerTKbsx4eNNrcyILg_zpQ"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Der SPIEGEL (2008): «Forscher warnen vor Klima-Hungersnöten»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.spiegel.de/wissenschaft/mensch/globale-erwaermung-forscher-warnen-vor-klima-hungersnoeten-a-532408.html"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Schätzungen gehen bis zu 2 Milliarden Menschen in einem ungebremsten Szenario. Charles Geisler & Ben Currens (2017): «Impediments to inland resettlement under conditions of accelerated sea level rise»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.sciencedirect.com/science/article/abs/pii/S0264837715301812"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "The Guardian (2015): «Tackle climate change or face resource wars, Lord Ashdown warns»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.theguardian.com/environment/2015/sep/09/tackle-climate-change-or-face-resource-wars-lord-ashdown-warns"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "wie ausgeführt im IPCC-Sonderbericht von 2018. Kurz gefasst ist die 2-Grad Grenze der Moment, an dem es auch für den reichen globalen Norden so richtig unangenehm wird - viele Regionen des globalen Südens sind dann aber schon unbewohnbar. IPCC 2018: “Summary for Policymakers”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.ipcc.ch/site/assets/uploads/sites/2/2019/05/SR15_SPM_version_report_LR.pdf"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "oder, für eine übersichtliche Zusammenfassung:"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.wri.org/blog/2018/10/half-degree-and-world-apart-difference-climate-impacts-between-15-c-and-2-c-warming"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Lenton et al. (2019): «Climate tipping points — too risky to bet against»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.nature.com/articles/d41586-019-03595-0"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Steffen et al. (2018): “Trajectories of the Earth System in the Anthropocene”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.pnas.org/content/115/33/8252"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Die Frage, ob man Menschenleben einfach in Dollar ausdrücken kann, lassen wir hier mal offen. Tyndall Center for Climate Change Research (2018): “Risks associated with global warming of 1.5°C or 2°C”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://tyndall.ac.uk/sites/default/files/publications/briefing_note_risks_warren_r1-1.pdf"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "317 Billionen Dollar, aus Credit Suisse (2018): “Global Wealth Report 2018”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.credit-suisse.com/media/assets/corporate/docs/publications/research-institute/global-wealth-report-2018-en.pdf"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "American Association for the Advancement of Science (2019): “New Climate Models Predict a Warming Surge”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.sciencemag.org/news/2019/04/new-climate-models-predict-warming-surge"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Selbstverständlich sind solche Prognosen umstritten. Sie beinhalten viele kaum abzuschätzende Elemente wie Hungersnöte, Wasserknappheit und vor allem die menschlichen Reaktionen darauf (Massenmigration, sozialer Kollaps, Ressourcenkriege, potenziell der Einsatz von Atomwaffen). Der springende Punkt: Genau so, wie wir solche Szenarien nicht richtig vorhersagen können, können wir sie auch nicht mit Sicherheit ausschliessen. Mehr dazu in einem Artikel des renommierten Professors William E. Rees (2019): «Yes, the Climate Crisis May Wipe out Six Billion People”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://thetyee.ca/Analysis/2019/09/18/Climate-Crisis-Wipe-Out/"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Die Fakten aus diesem Abschnitt stammen grösstenteils aus diesem Artikel im renommierten Wissenschaftsjournal Nature (2019): «Climate tipping points — too risky to bet against»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.nature.com/articles/d41586-019-03595-0"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "MacDougall et al. (2012): “Significant contribution to climate warming from the permafrost carbon feedback”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.nature.com/articles/ngeo1573"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "WWF (2018): «Korallen. Was unter der Meeresoberfläche abgeht»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.wwf.ch/sites/default/files/doc-2018-09/2018_Faktenblatt%20Korallen_d.pdf"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "The Guardian (2019): «Amazon rainforest 'close to irreversible tipping point'”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.theguardian.com/environment/2019/oct/23/amazon-rainforest-close-to-irreversible-tipping-point"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Greenpeace (2019): «Massive Waldbrände in Sibirien sind eine Klimakatastrophe»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.greenpeace.ch/de/story/33835/massive-waldbraende-in-sibirien-sind-eine-klimakatastrophe/"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "ETH Zürich Research Collection (2017): «Medien und Meinungsmacht»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.research-collection.ethz.ch/bitstream/handle/20.500.11850/125191/eth-50273-01.pdf"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Ariane Tanner (2019): «Kontertext: Vereint im Untergang»"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.infosperber.ch/Medien/Werbung-Okologie"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Zur Illustration die Titelseiten und Berichte der drei grössten Tages- und Sonntagszeitungen:"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://twitter.com/hansi_voigt/status/1179290148833505280"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Scientific American (2012): “Climate Science Predictions Prove Too Conservative”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.scientificamerican.com/article/climate-science-predictions-prove-too-conservative/"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "Louise M. Farquharson (2019): “Climate Change Drives Widespread and Rapid Thermokarst Development in Very Cold Permafrost in the Canadian High Arctic”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2019GL082187"
                        }
                    ]
                },
                {
                    "description": [
                        {
                            "language": ELanguages.all,
                            "text": "David Spratt & Ian Dunlop (2018): «What LIES beneath. The Understatement of Existential Climate Risk”"
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://www.breakthroughonline.org.au/whatliesbeneath"
                        }
                    ]
                }
            ]
        }
    ]
};