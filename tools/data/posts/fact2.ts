import {TStrapiPost} from "../../@def";
import {ELanguages} from "../../../core/@def";
import {EPostCategory} from "../../../components/@def";

export const factsYouAreAffectedPost: TStrapiPost = {
    identifier: "facts-you-are-affected",
    date: "",
    title: [{language: ELanguages.de, text: "Es betrifft auch dich"}],
    lead: [{language: ELanguages.de, text: "Die Krise betrifft uns in der Schweiz schon heute und in der nahen Zukunft ganz konkret, vor allem aber auch die junge Generation."}],
    author: [{language: ELanguages.all, text: "Nicola Bosshard"}],
    category: EPostCategory.fact,
    image: [], // {image: [], caption: []},
    content: []
};