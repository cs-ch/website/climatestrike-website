export const refProvider = () => {
    let ref = 0;
    return (n = 1) => {
        const refs = [];
        for (let i = 0; i < n; i++) {
            ref++;
            refs.push(ref);
        }
        return `<sup>${refs.join(", ")}</sup>`;
    };
};