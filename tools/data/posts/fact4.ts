import {TStrapiPost} from "../../@def";
import {ELanguages} from "../../../core/@def";
import {EPostCategory} from "../../../components/@def";

export const factsYourInvolvementMattersPost: TStrapiPost = {
    identifier: "facts-your-involvement-matters",
    date: "",
    title: [{language: ELanguages.de, text: "Du kannst etwas bewirken"}],
    lead: [{language: ELanguages.de, text: "Die grösste Verantwortung liegt, so wichtig diese sind, nicht in persönlichen Konsumentscheiden, sondern in politischem Engagement: Massenmobilisierungen sind die beste Möglichkeit, diese politische Veränderung zu bewirken."}],
    author: [{language: ELanguages.all, text: "Nicola Bosshard"}],
    category: EPostCategory.fact,
    image: [], // {image: [], caption: []},
    content: []
};