import {TStrapiPost} from "../../@def";
import {ELanguages} from "../../../core/@def";
import {EPostCategory} from "../../../components/@def";
import {refProvider} from "./Utils";


export const climateActionPlanAnnouncement: TStrapiPost = {
    identifier: "climate-action-plan-announcement",
    date: "2019-11-23",
    title: [
        {language: ELanguages.de, text: "Der Klima-Aktionsplan"},
    ],
    lead: [
        {language: ELanguages.de, text: "Das vom Ständerat verabschiedetete CO2-Gesetz ist absolut ungenügend und reicht nicht netto null bis 2050 zu erreichen, geschweige denn bis 2030. Der Klimastreik hat sich dazu entschlossen, selbst einen Klima-Aktionsplan zu erarbeiten."},
    ],
    content: [
        {
            __component: "component.markdown",
            text: [
                {
                    language: ELanguages.de, text: `Am 28. September 2019 versammelten sich in Bern 100'000 Menschen aus der ganzen Schweiz zur grössten Klimademo, die die Schweiz je gesehen hat. An den internationalen Streiktagen am 20. und 27. September waren global mehr als sieben Millionen Menschen auf der Strasse. Diese aussergewöhnlichen Entwicklungen zeigen, dass die Menschen in der Schweiz und auf der ganzen Welt es wirklich ernst meinen mit dem griffigen Klimaschutz. Die Klimastreik-Bewegung steht nun seit bald einem Jahr auf der Strasse, doch die institutionelle Schweizer Politik ist bis heute unserer Forderung kein bisschen nachgekommen. Das vom Ständerat in der Herbstsession verabschiedete CO2-Gesetz ist absolut ungenügend und erfüllt die Hauptforderung des Klimastreiks, die THG-Emissionen bis 2030 auf netto null im Inland zu reduzieren, nicht einmal annähernd. Tatsächlich genügt es nicht einmal, um das an sich schon verheerend tief gesteckte Ziel des Bundesrates von netto null THG-Emissionen bis 2050 zu erreichen. Die [Medienmitteilung der nationalrätlichen Umweltkommission vom 29. Oktober](https://www.parlament.ch/press-releases/Pages/mm-urek-n-2019-10-29.aspx?lang=1031) zeigt klar, dass auch der Nationalrat dieser Forderung nicht nachkommen wird. Um Klartext zu sprechen: Auch nach einem Jahr Klimastreiks hat die Politik keinen Plan für die Lösung der Klimakrise und versagt darin, die Bevölkerung vor dem drohenden ökologischen Kollaps zu schützen. 
                    
Unterdessen werden die fatalen Auswirkungen der Klimakrise immer klarer sichtbar und spürbar. Aus diesem Grund müssen *jetzt* drastische Massnahmen ergriffen werden. Der Klimastreik Schweiz fordert deshalb **eine Reduktion der THG-Emissionen von 13 Prozent im Inland  im Jahr 2020.** Diese Forderung steht auch im Mittelpunkt der nächsten Klimastreiks vom 29. November.

In den letzten Monaten wurde uns jedoch zunehmend klar, dass wir uns nicht länger auf die institutionelle Politik verlassen können und dass diese höchstwahrscheinlich auch die notwendige Reduktion im Jahr 2020 deutlich verfehlen wird. Unser Ziel ist es -- nach wie vor unverändert -- die Klimakrise einzudämmen und die institutionelle Politik ist derzeit offensichtlich nicht imstande, diese Verantwortung wahrzunehmen. Diese Feststellung machen wir nicht leichtfertig, sondern aufgrund der Erfahrungen des Klimastreiks in den letzten Monaten. Besonders einprägsam war für uns die monatelange, ermüdende Mitarbeit am Klimaplan der kantonalen Regierung in Waadt.  Auch unser Treffen mit Bundesrätin Simonetta Sommaruga vom 17. Juni hat diese Tatsache nur bestätigt. Sie und das Bundesamt für Umwelt wurden von den Klimastreikenden dazu aufgefordert, einen Aktionsplan auszuarbeiten, wie die Schweiz bis 2030 ihre THG-Emissionen im Inland auf netto null reduzieren kann. Ein solcher Plan wurde bis heute nicht präsentiert.

Der Klimastreik hat sich deshalb dazu entschlossen, **selbst einen Klima-Aktionsplan zu erarbeiten. Dieser Schritt ist ein klares Misstrauensvotum an die institutionelle Politik, die nach wie vor versagt, diese Krise als solche zu behandeln.**

Das Ziel des Klima-Aktionsplans ist es, aufzuzeigen, wie eine emissionsneutrale Gesellschaft aussehen kann und mit welchen Massnahmen diese erreicht wird. Um das 1.5°-Ziel einzuhalten, muss die Schweiz bis 2030 netto null im Inland erreichen und sämtliche internationalen Hebel in Gang setzen, um Emissionen weltweit zu reduzieren. Die Schweiz soll so ihrer Verantwortung gerecht werden und eine bedeutende internationale Rolle spielen. 

Dieser Plan wird auch auf dem Prinzip der **Klimagerechtigkeit** basieren, was bedeutet, dass alle Massnahmen sozial gerecht und nachhaltig sein müssen.

**Unser Klima-Aktionsplan soll somit mehr sein, als nur ein gewöhnlicher Massnahmenplan.** Er soll eine realistische Utopie, eine konkrete Vision einer Gesellschaft ohne fossile Energien werden.

Für die Erarbeitung des Plans haben wir aus der ganzen Schweiz Expert\\*innen aus allen Bereichen der Wissenschaft angefragt. Mehr als 60 Personen haben sich unterdessen bereit erklärt, uns bei diesem Projekt zu unterstützen. Die Arbeit am Klima-Aktionsplan erfolgt in sektorspezifischen Arbeitsgruppen wie z.B. Mobilität, Industrie und Landwirtschaft. Dabei werden alle relevanten Aspekte behandelt. **Eine Arbeitsgruppe setzt sich aus Wissenschaftlerinnen, Experten, Klimastreikenden und direkt betroffenen Personen zusammen.**  

Die Arbeit am Klima-Aktionsplan hat bereits begonnen. Der Plan wird voraussichtlich im **April 2020** fertiggestellt und präsentiert. Er wird konkrete Lösungen aufzeigen und erklären, mit welchen politischen Massnahmen diese erreicht werden können.
                    
Uns ist es auch sehr wichtig, dass der Klima-Aktionsplan ein **offenes, transparentes und partizipatives Projekt der Schweizer Bevölkerung** ist. Wir laden deshalb alle Menschen herzlich ein, sich bei der Erarbeitung des Plans einzubringen.

Nebst der Arbeit in den Arbeitsgruppen werden wir dazu lokale **Klimaversammlungen** zu spezifischen Fragestellungen, Themen und Sektoren durchführen. Damit sollen auch insbesondere Betroffene in den einzelnen Sektoren miteinbezogen werden. Auf [climateactionplan.ch ](https://www.climateactionplan.ch/en/)findet man demnächst eine Übersicht aller anstehenden Klimaversammlungen und eine genaue Anleitung zur Durchführung einer eigenen Klimaversammlung. An die Klimaversammlungen sind alle Menschen eingeladen, um dort ihre Vorschläge für den Klima-Aktionsplan einzubringen und mit uns zu sprechen. Eine solche öffentliche Diskussion soll erstmals auch im Rahmen des Streiks am 29. November stattfinden. Zusätzlich wird es online auf [climateactionplan.ch](https://www.climateactionplan.ch/en/) eine offene Plattform geben, auf der Inputs eingereicht, diskutiert und bewertet werden können. Auch werden die Arbeitsdokumente der Arbeitsgruppen regelmässig auf der Website zur offenen Einsicht und zur öffentlichen Diskussion aufgeschaltet.`},
            ]
        },
    ],
    image: [], // {image: [{language: ELanguages.de, text: "/static/images/climate-action-plan-title.png"}], caption: []},
    author: [],
    link: [],
    category: EPostCategory.otherNews
};


export const oneYearClimatestrike: TStrapiPost = {
    identifier: "one-year-climatestrike",
    date: "2019-12-23",
    title: [
        {language: ELanguages.de, text: "Ein Jahr Klimastreik"}
    ],
    lead: [
        {language: ELanguages.de, text: "Die Bewegung hat im letzten Jahr grossflächig zu mobilisieren und zu polarisieren vermocht und konnte dabei vor allem einen grossen Erfolg verzeichnen: Das Klima wurde zum wichtigsten Thema der öffentlichen Debatte. Konkrete politische Massnahmen bleiben aber nach wie vor aus, die Klimastreikbewegung macht also weiter!"},
    ],
    content: [
        {
            __component: "component.markdown",
            text: [
                {
                    language: ELanguages.de, text: `
## Rückblick auf ein bewegtes Jahr
Angefangen hat es in verschiedenen schweizer Städten im Dezember letzten Jahres, als mehrere hundert Schüler\\*innen einem über Whatsapp-Chats verbreiteten Aufruf gefolgt und zum ersten Mal auf die Strasse gegangen. Sie haben erkannt, dass ihre Zukunft auf dem Spiel steht: Die sich zunehmend verschärfende Klimakrise bedroht die Lebensgrundlagen von uns allen, doch aufgrund persönlicher Interessen verschliessen Machtträger\\*innen die Augen und ergreifen kaum Massnahmen, um das Problem zu lösen. Zwar sollte es nicht Aufgabe der Schüler\\*innen sein, die Klimakrise zu lösen, doch durch die Proteste sollten Gesellschaft und Politik wachgerüttelt werden.

Von Dezember bis Mai fanden neben kleineren lokalen Aktionen fast monatlich schweizweite oder globale Streiks oder Demos statt, die für jeweils für viel Aufruhr sorgten. Im Hintergrund hat sich die Bewegung formiert und eine Struktur gegeben. An mittlerweile fünf nationalen Treffen haben sich die Aktivist\\*innen auf ihre grundlegenden Forderungen geeinigt:

Wir fordern, dass die Schweiz den nationalen Klimanotstand ausruft.\
Wir fordern, dass die Schweiz bis 2030 im Inland netto 0 Treibhausgasemissionen ohne Einplanung von Kompensationstechnologien verursacht. Die netto Treibhausgasemissionen müssen zwischen 1.1.2020 und 1.1.2024 um mindestens 13% pro Jahr sinken, und danach um mindestens 8% pro Jahr sinken bis 1.1.2030. Alle Anteile verstehen sich relativ zu den Emissionen von 2018.\
Wir fordern Klimagerechtigkeit.\
Falls diese Forderungen im bestehenden System nicht umgesetzt werden können, braucht es einen Systemwandel.

Der Klimastreikbewegung hat im vergangenen Jahr zu mobilisieren vermocht wie kaum eine zuvor in der Schweiz. An der "Klimademo des Wandels", einer zentralen Klimademo am 28. September 2019 in Bern, haben fast 100 000 Menschen teilgenommen.

## Erfolge und Enttäuschungen
Die Bewegung hat das Jahr 2019 in der Schweiz geprägt wie kein anderes Thema. Das Klima rückte in der Fokus der öffentlichen Debatte und sowohl Medien wie auch Politik waren gezwungen, Stellung zu nehmen. Die grosse Aufmerksamkeit, die der Bewegung zuteil wurde, ist wohl ihr grösster Erfolg. Dazu kommen einige politische Errungenschaften; etwa haben zahlreiche Städte den Klimanotstand ausgerufen. Aber wirkungsvolle Klimaschutzmassnahmen auf nationaler Ebene lassen auf sich warten.

## Noch lange nicht am Ziel
Für die Klimaaktivist*innen ist klar: Solange ihre Forderungen nicht erreicht sind, bleiben sie dran. Noch ist die Politik weit davon entfernt, Massnahmen umzusetzen, die zu netto null Treibhausgasen bis 2030 führen würde. Dabei gäbe es viele Bereiche, wo angesetzt werden könnte, zum Beispiel beim Schweizer Finanzplatz. Der Klimastreik hat diesbezüglich Forderungen an die Politik und an die Finanzinstitute erarbeitet: Finanzflüsse sollen transparent und klimafreundlich werden, dazu braucht es auch Regulierungen, um so grosse Mengen an CO2 einzusparen und weitere Umweltschäden zu verhindern.

Auch nächstes Jahr geht es weiter: Am **15. Mai 2020 findet der Strike for Future statt,** das bisher wichtigste und grösste Projekt der Klimastreikbewegung. Menschen aus allen Bevölkerungsschichten sollen sich beteiligen an diesem nationalen Aktionstag, ähnlich dem Frauen*streik. So soll ein zusätzliches starkes Zeichen gesetzt und der Druck nochmals erhöht werden. Bereits jetzt ist die Zusammenarbeit mit anderen Akteur*innen und der Aufbau von Lokalgruppen in vollem Gang.`},
            ]
        },
    ],
    image: [],// {image: [{language: ELanguages.de, text: "/static/images/one-year-climatestrike-title.jpg"},], caption: []},
    author: [],
    link: [],
    category: EPostCategory.goodNews
};

export const tansparencyReport2019: TStrapiPost = {
    identifier: "finance-transparency-report-2019",
    date: "2020-02-27",
    title: [
        {language: ELanguages.de, text: "Transparenzbericht Finanzen 2019"},
        {language: ELanguages.fr, text: "Rapport sur la transparence financière 2019"}
    ],
    lead: [
        {language: ELanguages.de, text: "Im vergangenen Jahr haben wir viele Spenden erhalten. In unserem Finanztransparenzbericht 2019 veröffentlichen wir, wie dieses Geld eingesetzt wurde."},
        {language: ELanguages.fr, text: "En 2019, nous avons reçu de nombreux dons. C’est pourquoi nous rendons publique notre gestion de l’argent dans le rapport sur la transparence financière de 2019."}
    ],
    content: [
        {
            __component: "component.markdown",
            text: [
                {
                    language: ELanguages.de, text: `Die Klimastreik Bewegung ist unabhängig von Organisationen und Parteien, auch finanziell. Wir akzeptieren nur Spenden von Spender\\*innen welche die Prinzipien und Werte von Klimastreik unterstützen. So haben wir letztes Jahr vor allem von vielen privaten Einzelspender\\*innen Geld erhalten. Rund 82.7 % unser Einnahmen sind von privaten Einzelspender\\*innen mit einem durchschnittlichen Beitrag von 92.5 CHF pro Spende.

Wir haben im Jahr 2019 Spenden in der Summe von insgesamt 447 000 CHF erhalten und einen Aufwand in der Höhe von 364 000 CHF getätigt. Somit blieb uns Ende Jahr 83 000 CHF an Spende-Geldern übrig.

Unsere Grösste Ausgabe war das Klimablatt mit 213 000 CHF sowie der Extra Zug an den Internationalen Streik in Aachen, welcher 45 000 CHF gekostet hat. Weitere Ausgaben für Bewilligungen, Flyer, Plakate und Zug-Tickets beliefen sich auf insgesamt auf 105 000 CHF (mit einer Rückstellung von 20 000 CHF für schlechte Zeiten). 

Mehr Informationen in unserem Transparenzbericht Finanzen 2019 als [PDF Dokument](https://drive.google.com/file/d/1skjN0q9gMntPKqVbs2q8O4gHxLe5DBEO/view).

Zusatzinformation: Wir sind junge Menschen, ohne grosse Erfahrung mit Spendentransparenz sowie mit wenig Zeitressourcen, da wir alle in unserer Freizeit für die Finanzen Arbeitsgruppe arbeiten, darum bitten wir dich um Entschuldigung, wenn wir nicht alle deine Fragen in diesem Transparenzbericht beantworten konnten.

Nationale Arbeitsgruppe Finanzen

Klimastreik Schweiz

*Falls du Menschen kennst, welche im Raum Bern wohnen und buchhalterische Erfahrung haben, würden wir uns über Hilfe bei der Arbeitsgruppe freuen. Melde dich dazu bei finance@climatestrike.ch*`},
                {
                    language: ELanguages.fr, text: `En 2019, nous avons reçu de nombreux dons. C'est pourquoi nous rendons publique notre gestion de l'argent dans le rapport sur la transparence financière de 2019.

Le mouvement de grève du climat est indépendant des organisations et des partis, y compris au niveau des finances. Nous n'acceptons que les dons de donateurs qui soutiennent les principes et les valeurs de Klimastreik. L'année dernière, nous avons reçu de l'argent de nombreux donateurs privés. 82,7 % de nos revenus proviennent de donateurs individuels privés, avec une contribution moyenne de 92,5 CHF par don.

En 2019, nous avons reçu des dons d'un montant total de 447 000 francs suisses et dépensé 364 000 francs suisses. Cela nous a laissé 83 000 CHF de dons à la fin de l'année.

Notre plus gros dépense a été la Feuille du Climat avec 213 000 CHF et le train supplémentaire pour la grève internationale à Aix-la-Chapelle, qui a coûté 45 000 CHF. Les autres dépenses pour les permis, les flyers, les affiches et les billets de train se sont élevées à 105 000 CHF (avec une provision de 20 000 CHF pour les mauvais jours).

De plus amples informations sont disponibles dans notre rapport de transparence financière 2019 en [document PDF (seulement en allemand).](https://drive.google.com/file/d/1skjN0q9gMntPKqVbs2q8O4gHxLe5DBEO/view)

Informations complémentaires : Nous sommes des jeunes, sans grande expérience en matière de transparence des dons et avec peu de ressources en temps, car nous travaillons tous pour le groupe de travail sur les finances pendant notre temps libre. Nous nous excusons donc de ne pas pouvoir répondre à toutes vos questions dans ce rapport de transparence.

Groupe de travail sur les finances national

Grève du Climat Suisse`}
            ]
        },
    ],
    image: [],// {image: [{language: ELanguages.de, text: "/static/images/transparency-report-title.jpg"},{language: ELanguages.fr, text: "transparency-report-title.jpg"}], caption: []},
    author: [],
    link: [],
    category: EPostCategory.goodNews
};



let getRefDe = refProvider();
let getRefFr = refProvider();

export const aviationOpenLetter: TStrapiPost = {
    identifier: "aviation-open-letter-to-the-government",
    date: "2020-04-07",
    title: [
        {language: ELanguages.de, text: "Flugverkehr – Offener Brief an den Bundesrat"},
        {language: ELanguages.fr, text: "Aviation – Lettre ouverte au Conseil fédéral"}
    ],
    lead: [
        {language: ELanguages.de, text: "Gemeinsam mit zahlreichen weiteren Organisationen fordert der Klimastreik den Bundesrat auf die Bevorzugung des Flugverkehrs zu stoppen."},
        {language: ELanguages.fr, text: "Avec de nombreuses autres organisations, la grève du climat demande au Conseil fédéral de ne plus favoriser le transport aérien."}
    ],
    content: [
        {
            __component: "component.markdown",
            text: [
                {
                    language: ELanguages.de, text: `Bern, 7. April 2020

### Kein Sonderstatus für den Flugverkehr!

Sehr geehrte Bundesrätinnen und Bundesräte

Die Schweiz steht wegen der Corona-Krise still und die Flugzeuge bleiben weitgehend am Boden. Fluggesellschaften wie die Swiss und EasyJet bitten deshalb um Rettung durch Steuergelder. Jedoch geniesst der internationale Flugverkehr im Vergleich zu anderen Sektoren bereits sehr hohe Steuerprivilegien, da er weder Treibstoffsteuer noch Mehrwertsteuer zahlt. Dies ist einer der Gründe, warum z.B. die Swiss in den letzten 15 Jahren 5 Mia. Franken Gewinn${getRefDe()} erzielen konnte. Trotzdem verlangt die Swiss jetzt Unterstützung vom Bund.

Der Luftverkehr ist schon heute für 19% des menschengemachten Klimaeffekts in der Schweiz verantwortlich. Gemäss prä-Corona Wachstumsprognosen wird die Luftfahrt bis 2030 der grösste Treiber des Klimaeffekts in der Schweiz sein, wenn keine Massnahmen ergriffen werden.${getRefDe()} Die Luftfahrtindustrie hat sich in den letzten Jahren nachdrücklich gewehrt, einen sinnvollen Beitrag zu den globalen Emissionsreduktionszielen zu leisten. Denn dies würde Massnahmen zur deutlichen Reduktion des Umfangs der Luftfahrt erfordern.

Rettungsaktionen dürfen keinesfalls dazu führen, ein weiteres ungebremstes Wachstum des Flugverkehrs zu unterstützen. Stattdessen muss die Flugbranche auf ein klimaverträgliches Niveau zurückgebaut werden. Deshalb fordern wir Folgendes:

* **Keine neue Bevorzugung der Flugbranche:** Kulturschaffende, Selbständige, KMUs – alle sind von der Corona-Krise betroffen und erhalten bisher maximal Staatshilfe in Form von Lohnfortzahlung oder Überbrückungskrediten. Der Luftverkehr, der von der Kerosin- und Mehrwertsteuer befreit ist, darf keine neue Vorzugsbehandlung gegenüber anderen Wirtschaftsbereichen erhalten. Jegliche staatliche Unterstützung sollte den Schutz der Arbeitnehmenden und des Klimas in den Vordergrund stellen und nicht dazu verwendet werden, das Wachstum der Luftfahrt zu fördern. Da der Flugverkehr enorm klimaschädlich ist, muss staatliche Hilfe für die Luftfahrt an wirksame und verpflichtende klimapolitische Bedingungen geknüpft werden:

* **Reduktion für den Klimaschutz:** Der Flugverkehr hat sich in den letzten 20 Jahren verdoppelt. Damit Netto-Null-CO2 und somit das 1.5 Grad-Ziel erreicht werden können, braucht es umgehend eine massive Reduktion der Treibhausgasemissionen aus dem Flugverkehr.${getRefDe()} Die Emissionsreduktionen müssen direkt im Luftfahrtsektor erfolgen und dürfen nicht auf Kompensationssystemen, dem Handel mit Zertifikaten oder auf Massnahmen mit negativen Auswirkungen auf die Umwelt und die Menschenrechte beruhen.

* **Einführung einer Kerosinsteuer:** Der Flugverkehr verursacht in der Schweiz jährlich externe Kosten in Form von Klima- und Gesundheitsschäden von über 1 Mia. Franken, die zurzeit von der Bevölkerung getragen werden.${getRefDe()} Zusätzlich profitiert die Luftfahrt von der Steuerbefreiung für Kerosin, wodurch der Schweizer Staatskasse jährlich rund 1.7 Mia. Franken entgehen.${getRefDe()} Wer in schwierigen Zeiten den Staat um Hilfe bittet, sollte in guten Zeiten auch Steuern zahlen. Der Bundesrat soll in Zusammenarbeit mit den anderen Staaten die Privilegien für die Flugbranche abschaffen und umgehend eine Kerosinsteuer einführen.

* **Verlagerung auf klimafreundlichere Transportmittel:** Rund 80% der Flugdestinationen aus der Schweiz liegen in Europa und können mit dem klimafreundlicheren Zug erreicht werden.${getRefDe()} Staatsgelder sollen deshalb auch in den Ausbau des internationalen Bahn- und Nachtzugverkehrs investiert werden. 

Die unterzeichnenden Bewegungen, Organisationen und Parteien fordern Sie, die Bundesrätinnen und Bundesräte, auf, die oben genannten Massnahmen zu unterstützen, voranzutreiben und aktiv zu kommunizieren.

Freundliche Grüsse

ACG Association Climat Genève

AEFU Ärztinnen und Ärzte für Umweltschutz

AgF Aktion gegen Fluglärm Altenrhein

ARAG Association des Riverains de l’Aéroport de Genève

ATCR-AIG Association transfrontalière des communes riveraines de l’aéroport internationale de Genève

CARPE Coordination régionale pour un aéroport urbain, respectueux de la population et de l’environnement

Casafair

Christliche Klima Aktion

DVFS Dachverband Fluglärmschutz

Eltern fürs Klima

Ensemble à Gauche

Extinction Rebellion Schweiz

Fair in Air

FLS Fluglärmsolidarität

Flugstreik

Fossil Free

Greenpeace Schweiz

Grüne Schweiz

Junge Evangelische Volkspartei

Junge Grüne

Jungsozialist\\*innen JUSO Schweiz

Klima-Allianz Schweiz

Klima-Grosseltern Schweiz

KlimaSeniorinnen

Klimastreik Schweiz

Klimastadt Zürich

KLUG Koalition Luftverkehr Umwelt und Gesundheit

Mountain Wilderness Schweiz

myblueplanet

Noe21

Ökostadt Basel oeku Kirche und Umwelt

Pro Natura

Protect Our Winters Schweiz

Schutzverband der Bevölkerung um den Flughafen Basel Mülhausen

SES Schweizerische Energie-Stiftung

SP Schweiz

SSF Schweizerischer Schutzverband gegen Flugemissionen

Stiftung gegen Fluglärm

umverkehR

VCS Verkehrs-Club der Schweiz

VeFeF Vereinigung für erträglichen Flugverkehr

VFSN Verein Flugschneise Süd – NEIN

VgF Vereinigung gegen schädliche Auswirkungen des Flugverkehrs

Wettstein21

WWF Schweiz`},
                {
                    language: ELanguages.fr, text: `Berne, 7 avril 2020

### Pas de traitement préférentiel pour le transport aérien!

Mesdames les conseillères fédérales, Messieurs les conseillers fédéraux,

Suite à la crise liée au coronavirus, la Suisse tourne au ralenti et les avions sont cloués au sol. Les compagnies aériennes telles que Swiss et Easyjet réclament un soutien public. Pourtant, le transport aérien international bénéficie déjà de privilèges fiscaux importants par rapport aux autres secteurs économiques: il n’est soumis ni à un impôt sur le kérosène, ni à la TVA. C’est une des raisons pour lesquelles la compagnie Swiss a réussi à engranger ces 15 dernières années des bénéfices à hauteur de 5 milliards de francs.${getRefFr()} Malgré cela, Swiss revendique désormais une aide publique de la Suisse.

Le transport aérien est à l’origine de 19% de l’impact climatique de la Suisse. Selon les prévisions effectuées avant la crise du coronavirus, le transport aérien deviendra le secteur pesant le plus lourd dans le bilan climatique de la Suisse d’ici 2030.${getRefFr()} Jusqu’à présent, le transport aérien a refusé d’apporter une contribution significative aux objectifs mondiaux de réduction des émissions. Car cela implique une réduction importante du volume du trafic aérien.

Les plans de sauvetage ne doivent pas permettre au secteur aérien de retrouver son niveau de croissance, mais de s’adapter aux objectifs climatiques. C’est pour cette raison que nous exigeons les conditions suivantes:

* **Pas de nouveaux privilèges pour le transport aérien:** toutes les branches économiques – que ce soit le secteur culturel, les indépendants ou les PME – sont affectées par la crise du coronavirus et doivent bénéficier de soutiens publics sous la forme de garanties de salaire ou de crédits-relais. Dans ce contexte, le transport aérien international qui n’est pas soumis à l’impôt sur le kérosène et à la TVA, ne doit pas profiter d’un nouveau traitement préférentiel par rapport aux autres branches économiques. Le soutien public doit garantir la protection des travailleuses et travailleurs et en aucun cas n’être utilisé pour relancer la croissance du trafic aérien. Vu le fort impact climatique du transport aérien, tout soutien public doit être lié à des conditions claires et efficaces en matière de politique climatique.

* **Réduction des émissions pour la protection du climat:** Le trafic aérien a doublé ces 20 dernières années. Afin d’atteindre la neutralité carbone et de maintenir le réchauffement climatique en dessous de 1.5 degré, les émissions de gaz à effet de serre du transport aérien devront significativement baisser.${getRefFr()} Cette réduction doit avoir lieu directement dans le secteur aérien et non à travers des systèmes de compensation, d’échange de certificats ou des mesures qui ont des répercussions négatives sur l’environnement et les droits humains.

* **Introduction d’un impôt sur le kérosène:** chaque année le transport aérien est à l’origine de plus de 1 milliard de francs de coûts externes qui sont supportés par la collectivité, sous forme de nuisances pour la santé et pour le climat.${getRefFr()} A cela s’ajoute l’exonération à l’impôt sur le kérosène dont jouit le trafic aérien international et qui représente un manque à gagner d’environ 1,7 milliard de francs par an dans les caisses de l’Etat.${getRefFr()} Les compagnies qui réclament un soutien public durant les périodes difficiles devraient accepter de payer des impôts durant les périodes plus fastes. Le Conseil fédéral doit s’engager au niveau international pour abolir les privilèges du transport aérien et introduire rapidement un impôt sur le kérosène.

* **Transfert vers des moyens de transport plus durables:** environ 80% des passagers qui décollent de la Suisse ont une destination européenne.${getRefFr()} Ces trajets peuvent être effectués avec des moyens de transport plus durables tels que le train. Un soutien public doit donc aussi être accordé pour le développement du trafic ferroviaire international, notamment les trains de nuit. 

Les organisations suivantes vous demandent, Mesdames les conseillères fédérales, Messieurs les conseillers fédéraux, de promouvoir et de communiquer activement les mesures susmentionnées.

Avec nos meilleures salutations

ACG Association Climat Genève

actif-trafiC

Action Chrétienne Pour Le Climat

AgF Aktion gegen Fluglärm Altenrhein

Aînées pour la protection du climat

Alliance Climatique Suisse

ARAG Association des Riverains de l’Aéroport de Genève

ATCR-AIG Association transfrontalière des communes riveraines de l’aéroport internationale de Genève

ATE Association transports et environnement

CARPE Coordination régionale pour un aéroport urbain, respectueux de la population et de l’environnement

CESAR Coalition environnement et santé pour un transport aérien responsable

DVFS Dachverband Fluglärmschutz Eltern fürs Klima

Ensemble à Gauche

Extinction Rebellion Suisse

Fair in Air

FLS Fluglärmsolidarität

Fondation Suisse de l’Énergie Fossil Free

Grands-parents pour le climat

Greenpeace Suisse

Grève de l’avion

Grève du Climat – Suisse

HabitatDurable

Jeune Parti Évangélique

Jeunesse Socialiste Suisse

Jeunes Vert-e-s

Klimastadt Zürich

Les VERTS

Médecins en faveur de l’Environnement

Mountain Wilderness Suisse

myblueplanet

Noe21

Ökostadt Basel

oeco Eglise et environnement

Pro Natura

Protect Our Winters Suisse

PS Suisse

Schutzverband der Bevölkerung um den Flughafen Basel Mülhausen

SSF Schweizerischer Schutzverband gegen Flugemissionen

Stiftung gegen Fluglärm

VeFeF Vereinigung für erträglichen Flugverkehr

VFSN Verein Flugschneise Süd – NEIN

VgF Vereinigung gegen schädliche Auswirkungen des Flugverkehrs

Wettstein21

WWF Suisse
`}
            ]
        },
        {
            __component: "component.references",
            "references": [
                {
                    "description": [
                        {
                            "language": ELanguages.all, "text": "Tribune de Genève, Le grounding a commencé, Berne doit sauver Swiss, 25.03.2020"
                        }
                    ]
                },
                {
                    "link": [
                        {"language": ELanguages.de, "text": "https://www.flugfacts.ch"},
                        {"language": ELanguages.fr, "text": "https://www.aviation-verite.ch "}

                    ]
                },
                {
                    "description": [
                        {"language": ELanguages.all, "text": "IPCC special report – Global Warming of 1.5 °C"}
                    ]
                },
                {
                    "description": [
                        {"language": ELanguages.de, "text": "ARE, Kosten und Nutzen des Verkehrs"},
                        {"language": ELanguages.fr, "text": "ARE, Coûts et bénéfices des transports"}

                    ]
                },
                {
                    "description": [
                        {"language": ELanguages.de, "text": "EZV, Lieferungen von Flugtreibstoffen 2019 (Mineralölsteuer in Höhe von CHF 739.50/1000 L)"},
                        {"language": ELanguages.fr, "text": "AFD, Livraisons de carburants pour avions 2019 (taxe sur les huiles minérales: 739,50 CHF/1000 L)"}

                    ]
                },
                {
                    "description": [
                        {"language": ELanguages.de, "text": "OFS, Transport aérien – Trafic de lignes et charter. Résultats annuels 2019"},
                        {"language": ELanguages.fr, "text": "OFS, Transport aérien – Trafic de lignes et charter. Résultats annuels 2019"}

                    ]
                }
            ]
        }
    ],
    image: [],// {image: [{language: ELanguages.de, text: "/static/images/aviation-open-letter-to-the-government-de.jpg"},{language: ELanguages.fr, text: "/static/images/aviation-open-letter-to-the-government-fr.jpg"}], caption: []},
    author: [],
    link: [],
    category: EPostCategory.badNews
};


getRefDe = refProvider();
getRefFr = refProvider();
const getRefIt = refProvider();


export const coronaCommunication: TStrapiPost = {
    identifier: "corona-communication",
    date: "2020-04-03T10:00:00.000Z",
    title: [
        {language: ELanguages.de, text: "Bericht zur Coronavirus-Krise"},
        {language: ELanguages.fr, text: "Communiqué sur la crise du coronavirus"},
        {language: ELanguages.it, text: "Comunicato stampa coronavirus"}
    ],
    lead: [
        {language: ELanguages.de, text: "Die durch COVID-19 verursachte Krise sowie die Klimakrise stellen die Menschheit vor riesige Herausforderungen und bedrohen unsere Gesellschaft. Beide Krisen bedrohen uns alle, doch nicht alle sind gleich stark davon betroffen."},
        {language: ELanguages.fr, text: "La crise du COVID-19 place l’humanité devant un immense défi et menace nos sociétés, tout comme c’est le cas pour la crise climatique. Les deux crises nous touchent tou·te·x·s, mais tout le monde n’est pas impacté de la même manière."},
        {language: ELanguages.it, text: "La crisi del COVID-19 presenta all’umanità una sfida immensa e minaccia le nostre società, come nel caso della crisi climatica. Entrambe le crisi ci riguardano, ma non tutti sono colpiti allo stesso modo."}
    ],
    content: [
        {
            __component: "component.markdown",
            text: [
                {
                    language: ELanguages.de, text: `Seit über einem Jahr fordert der Klimastreik, dass Krisen ernst genommen werden, wir auf die Wissenschaft hören und dementsprechend reagieren. Die Coronakrise hat gezeigt, dass dies möglich ist. Wir haben gesehen, wie essentiell rechtzeitiges Handeln ist. Was vor einigen Monaten noch als unmöglich galt, wurde plötzlich innerhalb weniger Tage umgesetzt. Wir können angemessen auf Krisen reagieren, wenn der Wille da ist. Für die Überwindung der Coronakrise braucht es Solidarität und entsprechende Massnahmen. Genauso braucht es dies für die Lösung der Klimakrise. Im Falle der Coronakrise bedeutet Solidarität zurzeit einerseits zuhause zu bleiben, andererseits aber auch, den von der Krise besonders betroffenen Menschen zu helfen. Einkäufe tätigen, Kinder hüten, Briefe zur Post bringen sind Beispiele davon. Die Coronakrise sowie die Klimakrise erfordern Generationensolidarität. Deswegen rufen wir zur Solidarität auf und unterstützen Plattformen wie [hilf-jetzt.ch](https://www.hilf-jetzt.ch). Da viele Hilfsbedürftige nicht online unterwegs sind, sind wir auf kreative Lösungen angewiesen, um alle erreichen zu können. Wir unterstützen auch das Solidaritätsnetzwerk von “Landwirtschaft mit Zukunft”. Bäuerinnen und Bauern können in der gleichnamigen Facebook-Gruppe ihren Bedarf an Arbeitskräften melden und werden dann mit Menschen, die in der Nähe der Höfe leben und helfen wollen, vernetzt.

Es steht ausser Frage, dass die Eindämmung der COVID-19-Pandemie zurzeit unsere oberste Priorität sein muss. Dabei darf der Weg aus der Coronakrise heraus aber nicht die Klimakrise anheizen. Um die Erderhitzung auf +1.5° C im Vergleich zur vorindustriellen Zeit zu begrenzen, sind die nächsten Jahre entscheidend.${getRefDe()} **Deshalb dürfen Umweltauflagen keinesfalls aufgehoben werden** – insbesondere die Klima- und Lärmschutzbestimmungen der Luftfahrt und anderer Branchen wie des Energiesektors, dürfen nicht aufgegeben werden. Wir unterstützen die bisher ausgesprochenen Lohnfortzahlungen, jedoch müssen wir uns als Gesellschaft fragen, in welchen Bereichen wir zur vorherigen Situation zurückkehren wollen. Sollen Unternehmen, die der Umwelt schaden, nun durch staatliche Unterstützung getragen werden?

Jede Krise trifft benachteiligte Menschen am härtesten. Heute führt die Quarantäne zu einer Zunahme der häuslichen Gewalt${getRefDe()}, ohne dass die betroffenen Personen, vor allem Kinder und Frauen\\*, einen sicheren Zufluchtsort haben. Obdachlose, Personen ohne anerkannte Papiere, unbegleitete Minderjährige, Flüchtlinge und viele andere sind mittellos und sich selbst überlassen, ohne Schlafplatz, manchmal ohne Nahrung. Wir wollen eine solidarische Gesellschaft, **die schwierige Situationen berücksichtigen und verletzliche Menschen bei der Bewältigung künftiger Krisen schützen kann.**

In der Schweiz entsprechen die sanitäre Situation der Asylsuchende nicht dem humanitären Völkerrecht und den geltenden Gesundheitsempfehlungen. Es müssen menschenwürdige Lebensbedingungen und angemessene Gesundheitsdienste gewährleistet werden, damit die gesamte Bevölkerung Zugang zu menschenwürdigen Lebensbedingungen und zu, auch den am meisten gefährdeten Menschen, hat.

Zurzeit werden über 20’000 Menschen in Flüchtlingslagern auf griechischen Inseln wie Moria festgehalten.${getRefDe()} Viele sind vor dem Bürgerkrieg in Syrien geflohen. Eine der Ursachen des Krieges war eine schreckliche Dürre im Jahr 2007.${getRefDe()} Die sanitären Bedingungen in den Lagern sind prekär. Es gibt nur während wenigen Stunden am Tag fliessendes Wasser und die Anzahl der Wasserhähne ist nichtig im Vergleich der Zahl der Menschen. Physische Distanz zu anderen Menschen in den Flüchtlingslagern kann unmöglich eingehalten werden. Der Zugang zu den Lagern ist für viele NGO-Mitglieder zu gefährlich, unter anderem wegen wiederholter Angriffe faschistischer Gruppen auf sie.${getRefDe()} Unzählige Jugendliche und Kinder begehen Suizid, weil sie keinen Ausweg mehr sehen.${getRefDe()} Die Flüchtlingslager sind nicht auf einen Ausbruch von COVID-19 vorbereitet, weshalb ohne klare Paradigmenwechsel die erwartete Infektionsrate sehr hoch ist.${getRefDe()} Doch wegen Covid-19 und der politischen Lage wird ihnen kein Asylrecht gewährt, so dass sie weiterhin in unzumutbaren Bedingungen dem Virus schutzlos ausgeliefert sind.

In 30 Jahren werden bis zu 250 Millionen Menschen aufgrund von Dürre, Naturkatastrophen oder daraus resultierenden Konflikten aus ihrer momentanen Heimat vertrieben werden.${getRefDe()} Schon heute kommen einige Regierungen ihren Verpflichtungen im Hinblick auf die Menschenrechte von Flüchtlingen nicht nach. Zudem werden Klimakatastrophen nach schweizerischem oder internationalem Recht nicht als Asylgrund anerkannt. Da die Klimakrise bereits im Gange ist, müssen wir bereit sein, allen Flüchtlingen, auch den durch das Klima Vertriebenen, ein menschenwürdiges Leben zu garantieren, um unsere humanitären Pflichten zu erfüllen.

Aktuell kämpfen unter anderem Krankenhauspersonal, Verkaufspersonal, Reinigungs- und Lieferpersonal an der Front gegen COVID-19. Dennoch werden diese Berufe nicht genug wertgeschätzt. Der Gesundheitssektor ist seit einigen Jahren sogar von massiven Sparmassnahmen betroffen.${getRefDe()} Darüber hinaus werden die oben genannten Arbeitsplätze, die meist von Frauen\\* besetzt sind, oft sehr schlecht bezahlt. Vielen Arbeitnehmer*Innen wie unter anderem Selbständigen, Bereitschaftsarbeiter*Innen, grau oder schwarz Arbeitenden oder ”uberisierten” Arbeiter\\*Innen ist die Möglichkeit, Kurzarbeit zu beantragen, nicht gegeben.

Die Klimakrise hat bereits und wird noch Auswirkungen auf die Arbeitswelt haben, sei es mit steigenden Temperaturen auf Baustellen oder mit Unwettern, die die Ernten zerstören. Krisenzeiten zeigen daher die Notwendigkeit für das Intaktbleiben unserer Gesellschaft, **jedem Mensch das Recht auf einen fairen Lohn in einer gesunden Umwelt** zu ermöglichen.


Wegen Lohnkürzungen oder Kündigungen können zurzeit sehr viele Menschen trotz des vom Bundesrat ausgesprochenen finanziellen Rettungspakets ihre Miete nicht mehr bezahlen. **Ein Dach über dem Kopf zu haben ist ebenfalls ein Menschenrecht und muss auch in der Coronakrise garantiert werden.** Der Mietstreik ist ein seit über einem Jahrhundert bewährtes Mittel zur Bekämpfung der Prekarität und der Verschuldung der Haushalte.${getRefDe()} Eine Ursache für die Klimakrise ist Land Grabbing. Besonders in Zeiten von tiefem Wirtschaftswachstum gewinnt die Gewinnung von Profiten durch Miete an Bedeutung.${getRefDe()} Mit Boden darf nicht weiterer Profit erzielt werden, dies kann Mensch und Umwelt schaden.

**Ernährungssouveränität ist in Krisenzeiten besonders wichtig.** In Zeiten COVID-19s steht der Agrarsektor vor neuen Problemen. Durch die vom Bundesrat beschlossene Aufhebung der Wochenmärkte werden den Kleinproduzent\\*Innen direkte Vertriebswege vorenthalten. Ausserdem waren die Produzent”Innen bisher auf billige ausländische Arbeitskräfte angewiesen, die heute nicht mehr zur Verfügung stehen. Daher sind die Bauern auf Solidarität angewiesen und Klimastreikende helfen deshalb direkt mit auf gewissen betroffenen Höfen. Es muss eine langfristige Lösung gefunden werden, da die Schweiz im Falle eines Importmangels kaum in der Lage wäre, sich selbst zu ernähren. Die Ernährungssouveränität, die in den Westschweizer Kantonen (Genf, Vaud, Jura, Neuchâtel) eine Zustimmungsvotum erhalten hat, stellt eine Lösung sowohl für die Produzenten als auch für die KonsumentInnen in der Schweiz dar. **Der Umstieg auf lokale und agro-ökologische Produktionsweisen** würde die Treibhausgasemissionen reduzieren und unsere Abhängigkeit von Lebensmittelimporten verringern.

***Jede Krise verstärkt bereits existierende Ungleichheiten. Die aus dem COVID-19 resultierende Krise gibt uns in vielerlei Hinsicht einen Vorgeschmack auf das, was eine ungebremste Klimakrise zur Folge haben könnte. Klimagerechtigkeit heisst unter anderem, dass Massnahmen gegen die Klimakrise darauf abzielen soziale und ökonomische globale Ungerechtigkeiten zu beseitigen, und dass nach dem Verursacherprinzip die Länder und Akteure, die am meisten zur Klimakrise beitragen auch die grösste Verantwortung dafür tragen. Das bedingt unter anderem das Ernstnehmen der Wissenschaft, rechtzeitiges Handeln und damit netto null Emissionen bis 2030 in der Schweiz. Die Erderhitzung bedroht nicht nur die Umwelt, sondern auch die Menschenrechte der heutigen und zukünftigen Generationen. Der Klimastreik setzt sich für die Verhinderung von Leid durch die Klimakrise, Klimagerechtigkeit und das 1.5° C Ziel ein.

Doch wir stellen uns auch gegen das durch bestehende ökonomische und politische Verhältnisse verursachte Leid, welches nun wegen der Coronakrise noch deutlicher zum Vorschein kommt. Solidarität, wie wir sie während der Covid-19 Krise sehen, ist grundlegend, um alle Formen der Unterdrückung zu beseitigen und eine Welt der sozialen Gerechtigkeit und Klimagerechtigkeit zu erreichen.***`},
                {
                    language: ELanguages.fr, text: `Depuis plus d’un an, la Grève du Climat demande que les crises soient prises au sérieux, que l’on écoute la science et que l’on agisse en conséquence. La crise du COVID-19 a montré que cela est possible. Nous avons vu à quel point il est nécessaire d’agir à temps. Ce qui semblait impossible il y a quelque mois encore, a soudainement été réalisé en quelques jours. Lorsque la volonté est là, nous pouvons réagir de manière adaptée aux crises! La crise du coronavirus, à l’instar de la crise climatique, ne peut être surmontée que **grâce à la solidarité et à la prise de mesures adéquates.** Dans le cadre de la crise du COVID-19, être solidaire signifie non seulement rester chez soi mais aussi aider les personnes les plus fortement impactées par la crise, que ce soit en faisant des achats, en gardant des enfants ou en amenant des lettres à la poste. La crise du COVID-19 tout comme la crise climatique nécessite une solidarité intergénérationnelle. C’est pourquoi nous appelons à la solidarité et soutenons des plateformes comme [aide-maintenant.ch/](https://www.de-maintenant.ch). Comme de nombreuses personnes ayant besoin de soutien ne sont pas actives sur internet, nous sommes poussé·e·x·s à trouver des solutions créatives pour pouvoir toutes les atteindre. Nous soutenons également le réseau de solidarité d’Agriculture pour le futur”. Les paysan.ne.x.s peuvent annoncer dans le groupe Facebook éponyme leur besoin de force de travail et sont ainsi mis.e.x.s en lien avec des personnes qui vivent dans la région et souhaitent les aider.

Il est clair que notre priorité absolue doit être l’endiguement du virus. Toutefois, au sortir de la crise du virus, il ne faudra pas aggraver la crise climatique en prenant de mauvaises décisions. Les prochaines années seront essentielles pour limiter le réchauffement de la terre à +1.5° C par rapport aux niveaux pré-industriels.${getRefFr()} Pour cette raison, **les législations environnementales ne doivent pas être assouplies**, en particulier le règlement concernant l’environnement et les bruits de l’aviation doit être maintenu. Nous soutenons le maintien des salaires dans ces domaines afin de ne pas péjorer les salarié·e·x·s, mais la question de savoir dans quels secteurs nous voulons retourner “à la normale” doit être posée. Les entreprises qui nuisent à l’environnement doivent-elles être sauvées grâce à l’aide de l’Etat?Toute crise donne lieu à un accroissement des inégalités et touche plus durement les personnes les plus vulnérables socialement. Aujourd’hui, la situation de confinement donne lieu à une hausse des violences domestiques${getRefFr()}, sans que les personnes touchées n’aient de lieu sûr où se réfugier. Les personnes sans-domicile fixe, précaires, sans-papiers reconnus, mineur.e.x.s non-accompagné.e.x.s, réfugié·e·x·s et bien d’autres encore se retrouvent démunies et livrées à elles-mêmes, sans lieu où dormir, parfois sans nourriture. Nous voulons construire une **société solidaire, qui prenne en compte les situations précaires et protège les personnes vulnérables lors de la gestion des crises à venir.**

En Suisse les conditions d’accueil des demandeurs d’asile ne sont respectueuses ni du droit humanitaire international ni des recommandations sanitaires en vigueur. Des conditions de vie dignes et des services de santé adéquats doivent être garanties pour toute la population, y compris les personnes les plus marginalisées.

En ce moment, plus de 20’000 personnes sont également bloqué·e·x·s dans des camps pour réfugié·e·x·s sur des îles grecques telles que Moria.${getRefFr()} Une grande partie d’entre elle·eux·s ont fui la guerre civile en Syrie, dont l’une des causes est une terrible sécheresse survenue en 2007.${getRefFr()} Dans ces camps les conditions sanitaires sont précaires. L’eau courante n’est par exemple que disponible durant quelques heures et le nombre de robinets est négligeable par rapport aux nombre de personnes présentes dans ces camps. Une distanciation physique y est totalement inconcevable. L’accès aux camps est rendu impossible pour de nombreuses ONG, notamment à cause d’attaques récurrentes menées envers elleux par des groupes fascistes.${getRefFr()} De nombreux jeunes et enfants se suicident, faute de trouver une autre solution.${getRefFr()} **L’arrivée du COVID-19 dans ces camps est synonyme d’affreuses souffrances pour les personnes qui s’y trouvent.**${getRefFr()} Néanmoins, à cause du Covid-19 et de la situation politique, le droit d’asile ne leur est pas garanti, si bien qu’iels se retrouvent livré.e.x.s sans défense au virus, dans des conditions inacceptables.

Dans 30 ans, jusqu’à 250 millions de personnes auront été chassées de chez elles à cause de sécheresses, de catastrophes naturelles ou des conflits qui en résultent.${getRefFr()} Déjà aujourd’hui, certains gouvernements ne remplissent pas leurs devoirs quant aux droits humains des réfugié.e.x.s. De plus aujourd’hui, les catastrophes climatiques ne sont pas reconnues comme motifs pour demander l’asile, dans le droit suisse ou international. Etant donné que la crise climatique est déjà en cours, **il est nécessaire, pour remplir nos devoirs humanitaires, être prêt.e.x.s à garantir une vie digne à touxtes les réfugié.e.x.s y compris celleux déplacé.e.x.s pour des raisons climatiques** et mettre tout en oeuvre pour lutter contre l’aggravation du changement climatique.

Aujourd’hui, de nombreuses personnes se trouvent en première ligne face au COVID-19: personnel hospitalier, de vente, de nettoyage, de livraison etc. Pourtant, ces emplois ne sont pas suffisamment valorisés. Ainsi, le domaine de la santé fait face à d’importantes mesures d’austérité depuis plusieurs années.${getRefFr()} De plus, les emplois précités, exercés en grande majorité par des femmes*, sont souvent très mal rémunérés. Par ailleurs, de nombreux·ses travailleurs·x·euses (pseudo-indépendant·e·x·s, travail à l’appel, travail gris/noir/ubérisé.e.x.s) n’ont pas la possibilité de demander le chômage partiel.

La crise climatique impacte et impactera elle aussi lourdement le monde du travail, que ce soit avec – par exemple – des températures caniculaires sur les chantiers ou des intempéries détruisant les cultures. **Les temps de crise démontrent donc la nécessité de permettre à tout le monde d’avoir droit à un salaire juste, dans un environnement sain.**

De nombreuses personnes sont actuellement dans l’incapacité de payer leur loyer en raison de la réduction de leur salaire ou de leur licenciement et ce, malgré l’aide financière promise par le Conseil fédéral. **Avoir un toit est un droit humain et doit de ce fait être garanti en tout temps, même en temps de crise.** La grève des loyers, un moyen utilisé depuis plus d’un siècle, est un moyen éprouvé de lutte contre la précarité et l’endettement des ménages.${getRefFr()} Notons que la crise climatique a elle aussi été entre autres causée par l’accaparement de terres. Surtout en période de faible croissance économique, l’extraction de profits par le biais de la rente gagne en importance.${getRefFr()} **Faire de la terre une source de profits peut être profondément nuisible**, à la fois pour les gens et l’environnement.

**La souveraineté alimentaire est particulièrement importante en temps de crise.** Pendant la crise du COVID-19 le secteur agricole rencontre de nouveaux problèmes. En effet, l’annulation des marchés hebdomadaires décidée par le conseil fédéral prive les petits producteur.rices de moyens de distribution directs et la population de produits de qualité via des circuits courts. De plus, les producteur.trices ont compté jusqu’ici sur une main-d’œuvre étrangère bon marché qui n’est plus disponibles aujourd’hui. Par conséquent, les paysan.ne.x.s sont tributaires de la solidarité. C’est pourquoi des grévistes pour le climat aident directement dans des fermes.
Une solution à long terme doit être adoptée puisque la Suisse ne serait guère en mesure de se nourrir elle-même en cas de pénurie d’importations. La souveraineté alimentaire qui a reçu un vote positif dans les cantons romands, (Genève, Vaud, Jura, Neuchâtel) est porteuse de nombreuses solutions tant pour les producteur.ices que pour les mangeur.euse.s en Suisse. **Favoriser un mode de production local et agro-écologique** réduirait les émissions de gaz à effet de serre, et grâce à un meilleur contrôle de l’importation, permettrait de diminuer notre dépendance alimentaire.

***Chaque crise renforce les inégalités déjà existantes. La crise du COVID-19 nous donne un avant-goût de ce qui pourrait résulter d’une crise climatique non maîtrisée. La justice climatique signifie notamment que les mesures contre la crise climatique doit viser une élimination des inégalités sociales et économiques mondiales, et que, selon le principe du pollueur-payeur, les pays et les acteurs qui contribuent le plus à la crise climatique en portent la plus grande responsabilité. Pour cela, il faut entre autres que la science soit prise au sérieux, que des mesures soient prises en temps utile et que la Suisse atteigne ainsi un niveau d’émissions zéro net d’ici 2030. La Grève du Climat se bat pour éviter les conséquences dévastatrices de la crise climatique, la justice climatique et l’objectif des 1,5 degrés.

Cela signifie que nous luttons également contre les souffrances causées par les conditions économiques et politiques actuelles, portées aux nues par la crise du COVID-19. La solidarité, telle que nous l’observons durant la crise du COVID 19 est fondamentale pour éliminer toute forme d’oppression et atteindre un monde où règne la justice sociale et environnementale.***
`},
                {
                    language: ELanguages.it, text: `Da più di un anno ormai, Sciopero per il Clima chiede che le crisi siano prese sul serio, che la scienza venga ascoltata e che si agisca di conseguenza. La crisi del COVID-19 ha dimostrato che ciò è possibile. Abbiamo visto quanto sia necessario agire in tempo. Ciò che sembrava ancora impossibile pochi mesi fa è stato improvvisamente realizzato in pochissimi giorni. Quando c’è la volontà, possiamo reagire in modo appropriato alle crisi! La crisi del COVID-19 presenta all’umanità una sfida immensa e minaccia le nostre società, come nel caso della crisi climatica. Entrambe le crisi ci riguardano, ma non tutti sono colpiti allo stesso modo. Da più di un anno ormai, Sciopero per il Clima chiede che le crisi siano prese sul serio, che la scienza venga ascoltata e che si agisca di conseguenza. La crisi del COVID-19 ha dimostrato che ciò è possibile. Abbiamo visto quanto sia necessario agire in tempo. Ciò che sembrava ancora impossibile pochi mesi fa è stato improvvisamente realizzato in pochissimi giorni. **Quando c’è la volontà, possiamo reagire in modo appropriato alle crisi!**

La crisi del coronavirus, come quella del clima, può essere superata **solo attraverso la solidarietà e misure adeguate.** Nel contesto della crisi del COVID-19, solidarietà che significa non solo stare a casa, ma anche aiutare le persone più colpite dalla crisi, sia che si tratti di fare la spesa, di fare da babysitter o di portare le lettere all’ufficio postale. La crisi del COVID-19, come quella del clima, richiede una solidarietà intergenerazionale. Per questo motivo, chiediamo solidarietà e supportiamo piattaforme come [aide-maintenant.ch/](https://aide-maintenant.ch/) (francese e tedesco) o [redcross.ch/it/fiveup](https://www.redcross.ch/it/fiveup). Poiché molte persone che hanno bisogno di supporto non sono attive su Internet, siamo spinti a trovare soluzioni creative per raggiungerle tutte. Sosteniamo anche la rete di solidarietà di «Agricoltura per il futuro». Gli agricoltori possono annunciare il loro bisogno di manodopera nell’omonimo gruppo Facebook, e sono collegati alle persone che vivono nella regione e vogliono e possono aiutarli.

Non c’è dubbio che contenere la pandemia COVID-19 debba essere la nostra massima priorità al momento. Tuttavia, la via d’uscita dalla crisi del coronavirus non deve alimentare la crisi climatica. I prossimi anni saranno cruciali per limitare il riscaldamento globale a + 1,5 ° C rispetto ai tempi preindustriali.${getRefIt()} Pertanto, le normative ambientali non devono essere revocate in nessuna circostanza in particolare, le normative sulla protezione del clima e sul rumore del trasporto aereo e su altri settori come quello dell’energia non devono essere abbandonate. Sosteniamo i continui salari pagati finora, ma come società dobbiamo chiederci in quali aree vogliamo far tornare alla situazione precedente. Le aziende che danneggiano l’ambiente dovrebbero ora essere salvate con aiuto di aiuti dello Stato?

Ogni crisi porta un aumento delle disuguaglianze e colpisce più duramente le persone socialmente più vulnerabili. Oggi, la situazione di reclusione sta portando a un aumento della violenza domestica${getRefIt()}, senza un luogo sicuro dove le persone colpite possano rifugiarsi. I senzatetto, i precari, i migranti senza documenti riconosciuti, i minori non accompagnati, i rifugiati e molti altri si ritrovano indigenti e abbandonati a loro stessi, senza un posto dove dormire, a volte senza cibo. Vogliamo costruire **una società basata sulla solidarietà, che tenga conto delle situazioni precarie e protegga le persone vulnerabili nella gestione delle crisi future.**

In Svizzera, le condizioni di accoglienza dei richiedenti asilo non sono conformi al diritto internazionale umanitario o alle raccomandazioni sanitarie in vigore. Devono essere garantite condizioni di vita dignitose e servizi sanitari adeguati per l’intera popolazione, anche per i più emarginati.

Al momento, più di 20’000 persone sono bloccate nei campi di rifugiati delle isole greche come Moria.${getRefIt()} Molti di loro sono fuggiti dalla guerra civile in Siria, che ha avuto origine da una terribile siccità nel 2007.${getRefIt()} Le condizioni sanitarie in questi campi sono precarie. Ad esempio, l’acqua corrente è disponibile solo per poche ore e il numero di rubinetti è insignificante rispetto al numero di persone nei campi. Una distanza fisica è assolutamente impossibile. L’accesso ai campi è reso impossibile a molte ONG, anche a causa dei ricorrenti attacchi da parte di gruppi fascisti.${getRefIt()} Molti giovani e bambini si suicidano, per mancanza di un’altra soluzione.${getRefIt()} L’arrivo del COVID-19 in questi campi è sinonimo di terribili sofferenze per la gente che ci si trova.${getRefIt()}  Tuttavia, a causa del COVID-19 e della situazione politica, il diritto d’asilo non è garantito, cosicché si trovano esposti al virus in condizioni inaccettabili.

In 30 anni, fino a 250 milioni di persone saranno state cacciate dalle loro case da siccità, disastri naturali o conflitti che ne derivano.${getRefIt()} Già oggi alcuni governi non adempiono ai loro doveri in materia di diritti umani dei rifugiati. Inoltre, oggi le catastrofi climatiche non sono riconosciute come motivo per chiedere asilo in base al diritto svizzero o internazionale. Dato che la crisi climatica è già in corso, per adempiere ai nostri doveri umanitari dobbiamo essere pronti a garantire una vita dignitosa a tutti i rifugiati, compresi gli sfollati per motivi climatici, e fare tutto il possibile per combattere l’aggravarsi del cambiamento climatico.

Oggi, molte persone sono in prima linea contro il COVID-19: personale ospedaliero, personale di vendita, personale addetto alle pulizie, personale addetto alle consegne, ecc. Tuttavia, il settore sanitario si trova da diversi anni ad affrontare importanti misure di austerità.${getRefIt()} Inoltre, i suddetti posti di lavoro, per lo più occupati da donne*, sono spesso scarsamente retribuiti. Poi molti di questi lavoratori (pseudo-indipendenti, lavoro di guardia, lavoro grigio/nero/uberizzato, ecc.) non hanno la possibilità di presentare domande di disoccupazione parziale.

La crisi climatica ha e avrà un forte impatto anche sul mondo del lavoro, sia che si tratti – ad esempio – di temperature elevate nei cantieri edili, sia che il maltempo distrugga i raccolti. **I tempi di crisi dimostrano quindi la necessità di permettere a tutti di avere diritto a un salario equo in un ambiente sano.**

Attualmente, molte persone non sono in grado di pagare l’affitto a causa di riduzioni di salario o di licenziamenti, questo nonostante l’aiuto finanziario del Consiglio federale. **Avere un tetto sopra la testa è un diritto umano e quindi deve essere garantito in ogni momento, anche in tempi di crisi.** Lo sciopero degli affitti, utilizzato da oltre un secolo, è un mezzo collaudato per combattere la precarietà e l’indebitamento delle famiglie.${getRefIt()} Va notato che la crisi climatica è stata causata anche dall’accaparramento della terra, tra le altre cose. Soprattutto in tempi di debole crescita economica, l’estrazione dei profitti attraverso gli affitti sta acquisendo sempre più importanza.${getRefIt()} **Trasformare la terra in una fonte di profitto può essere profondamente dannoso**, sia per la gente che per l’ambiente.

**La sovranità alimentare è particolarmente importante in tempi di crisi.** Durante la crisi del COVID-19 il settore agricolo sta affrontando nuovi problemi. In effetti, la cancellazione dei mercati settimanali decisa dal Consiglio federale priva i piccoli produttori di mezzi di distribuzione diretta e la popolazione di prodotti di qualità attraverso il corto circuito. Inoltre, i produttori si affidavano finora a manodopera straniera a basso costo che non è più disponibile oggi. Di conseguenza, gli agricoltori dipendono della solidarietà. Ecco perché che gli scioperanti climatici aiutano direttamente nelle fattorie. Occorre trovare una soluzione a lungo termine, poiché la Svizzera non sarebbe capace di alimentarsi da sola in caso di carenza di importazioni. La sovranità alimentare, che ha ricevuto un voto positivo nei cantoni francofoni (Ginevra, Vaud, Giura Neuchâtel), offre molte soluzioni sia per i produttori che per i mangiatori in Svizzera. **Favorire un metodo di produzione locale e agro-ecologico** ridurrebbe le emissioni di gas a effetto serra e, grazie a un migliore controllo delle importazioni, ridurrebbe la nostra dipendenza alimentare.

Ogni crisi rafforza le disuguaglianze esistenti. La crisi di COVID-19 ci dà un assaggio di ciò che potrebbe derivare da una crisi climatica incontrollata. Giustizia climatica significa, tra l’altro, che l’azione contro la crisi climatica deve mirare ad eliminare le disuguaglianze sociali ed economiche globali e che, secondo il principio “chi inquina paga”, i paesi e gli attori che contribuiscono maggiormente alla crisi climatica hanno la maggiore responsabilità. Ciò presuppone, tra l’altro, che la scienza venga presa sul serio e che le misure necessarie vengano adottate in tempo utile affinché la Svizzera raggiunga l’obiettivo di emissioni nette pari a zero entro il 2030. Lo Sciopero per il Clima sta combattendo per evitare le conseguenze devastanti della crisi climatica, la giustizia climatica e l’obiettivo di 1,5 gradi.

***Questo significa che stiamo lottando anche contro le sofferenze causate dalle attuali condizioni economiche e politiche, messe in luce dalla crisi del COVID-19. La solidarietà come la vediamo durante questa crisi del COVID-19 è fondamentale per eliminare ogni forma di oppressione e per raggiungere un mondo di giustizia sociale e ambientale.***
                    `}
            ]
        },
        {
            __component: "component.references",
            "references": [
                {
                    "description": [{"language": ELanguages.all, "text": "31.03.2020"}],
                    "link": [{"language": ELanguages.all, "text": "https://www.sciencedaily.com/releases/2017/04/170413084647.htm"}]
                },
                {
                    "description": [{"language": ELanguages.all, "text": "31.03.2020"}],
                    "link": [{"language": ELanguages.all, "text": "https://www.srf.ch/news/schweiz/wegen-corona-massnahmen-es-wird-vermehrt-zu-haeuslicher-gewalt-kommen"}]
                },
                {
                    "description": [{"language": ELanguages.all, "text": "31.03.2020"}],
                    "link": [{"language": ELanguages.all, "text": "https://www.srf.ch/news/international/fluechtlingslager-auf-lesbos-die-kraetze-frisst-die-menschen-in-moria-lebendig-auf"}]
                },
                {
                    "description": [{"language": ELanguages.all, "text": "31.03.2020"}],
                    "link": [{"language": ELanguages.all, "text": "https://www.pnas.org/content/early/2015/02/23/1421533112.abstract"}]
                },
                {
                    "description": [{"language": ELanguages.all, "text": "31.03.2020"}],
                    "link": [{"language": ELanguages.all, "text": "https://www.zeit.de/gesellschaft/zeitgeschehen/2020-03/griechenland-lesbos-tuerkei-fluechtlinge-rechtsextremismus-fluechtlingshelfer-attacken"}]
                },
                {
                    "description": [{"language": ELanguages.all, "text": "31.03.2020"}],
                    "link": [{"language": ELanguages.all, "text": "https://www.theguardian.com/global-development/2018/oct/03/trauma-runs-deep-for-children-at-dire-lesbos-camp-moria"}]
                },
                {
                    "description": [{"language": ELanguages.all, "text": "31.03.2020"}],
                    "link": [{"language": ELanguages.all, "text": "https://www.nouvelobs.com/monde/20191209.OBS22109/des-enfants-refugies-tentent-de-se-suicider-dans-les-camps-en-grece-alerte-msf.html"}]
                },
                {
                    "description": [{"language": ELanguages.all, "text": "31.03.2020"}],
                    "link": [{"language": ELanguages.all, "text": "https://www.zeit.de/politik/ausland/2020-03/corona-lesbos-fluechtlinge-moria-medizinische-versorgung"}]
                },
                {
                    "description": [{"language": ELanguages.all, "text": "31.03.2020"}],
                    "link": [{"language": ELanguages.all, "text": "https://www.theguardian.com/environment/2007/may/14/climatechange.climatechangeenvironment"}]
                },
                {
                    "description": [{"language": ELanguages.all, "text": "31.03.2020"}],
                    "link": [{"language": ELanguages.all, "text": "https://www.rts.ch/info/suisse/11046858-reduire-ou-non-le-nombre-d-hopitaux-en-suisse-la-question-divise.html"}]
                },
                {
                    "description": [
                        {"language": ELanguages.de, "text": "Siehe Mietstreik 1932 in Zürich (31.03.2020)"},
                        {"language": ELanguages.fr, "text": "Voir grève des loyers en 1932 à Zurich (31.03.2020)"},
                        {"language": ELanguages.it, "text": "Vedere lo sciopero degli affitti nel 1932 a Zurigo (31.03.2020)"}
                    ],
                    "link": [{"language": ELanguages.all, "text": "https://tsri.ch/zh/das-rote-zurich-und-der-mieterstreik/ "}]
                },
            ]
        }
    ],
    image: [], // {image: [{language: ELanguages.all, text: "/static/images/corona-teaser.jpg"}],caption: []},
    author: [],
    link: [],
    category: EPostCategory.badNews
};

export const onlineClimatestrikeApril: TStrapiPost = {
    identifier: "climatestrike-online-24-april",
    date: "2020-04-23",
    title: [
        {language: ELanguages.de, text: "Netzstreik am 24. April"}
    ],
    lead: [
        {language: ELanguages.de, text: "Der 24. April ist ein globaler Streiktag. Trotz Lockdown und Versammlungsverbot macht die Klimakrise keine Pause. Deswegen wird der Klimastreik ins Internet verlegt. Am 24.04 finden auf der ganzen Welt verschiedene Aktionen statt."}
    ],
    content: [
        {
            __component: "component.markdown",
            text: [
                {
                    language: ELanguages.de, text: `
## \\#climatestrikeonline

Der 24. April ist ein globaler Streiktag. Trotz Lockdown und Versammlungsverbot macht die Klimakrise keine Pause: Wir erleben gerade eine Dürre, die mehr als alarmierend ist! Dazu droht die Schweiz, die Klimaziele für 2020 zu verfehlen und klimaschädliche Unternehmen wie die Swiss wollen Milliarden von staatlichem Geld beziehen. Weltweit werden die Umweltschutzmassnahmen gelockert und mit Unterstützung der Wirtschaft wegen Corona-Krise gerechtfertigt. Die Klimakrise wird schwere und langfristige Folgen für die Menschen und die Wirtschaft haben. Wir müssen aus der jetzigen Krise lernen anstatt ein Schritt zurück zu machen! 

Deswegen wird der Klimastreik ins Internet verlegt. Am 24.04 finden auf der ganzen Welt verschiedene Aktionen statt. Es ist Zeit, dass wir uns mit denjenigen vernetzen, denen unser Klima und unsere Zukunft am Herzen liegt. Zusammen teilen wir unsere Visionen, Slogans, Forderungen und sorgen damit für Aufmerksamkeit. Zusammen können wir für aktiven Klimaschutz mobilisieren und auf die momentanen Probleme aufmerksam machen. Mach mit! Es geht ganz einfach: 

1.  Nimm ein Plakat oder ein Banner -- falls du keins hast, dann bastle eins. 
2.  Mach ein Foto von dir und deinem Plakat oder Banner. 
3.  Poste das Bild auf Facebook/Instagram mit dem Hashtag #climatestrikeonline und #DigitalStrike Teile dein Engagement für das Klima und deine Wünsche für die Zukunft mit der Welt!
4.  Tag @klimastreikschweiz oder deine Regionalgruppe und teile die Posts von anderen! In wenigen Stunden werden wir die Bekanntesten auf Social Media!
5.  Mach dich sichtbar! Stell dein Plakat/Banner aus dem Fenster/auf den Balkon!

## 24 Stunden Zoom Call

Wir haben einen ganztägigen Call auf Deutsch vorbereitet, von 8 Uhr morgens bis 8 Uhr morgens!  Über den ganzen Tag bietet er Raum für Diskussionen übers Klima, aber auch eine Gelegenheit um Menschen kennenzulernen, sich weiterzubilden und dabei Spass zu haben! Natürlich muss mensch nicht den ganzen Tag anwesend sein: mensch kann kommen und  gehen wann er will. 

Der Call ist offen für alle, egal ob aktiv oder nicht, jung oder alt, Fan von Sojamilch oder nicht...

Zum Call: <https://zoom.us/j/94608627187?pwd=SVJwNDd2QlNCelVDV2ZRbjF0RUVyZz09>`}
            ]
        },
    ],
    image: [],// {image: [{language: ELanguages.de, text: "/static/images/netzstreik-banner.jpg"}], caption: []},
    author: [],
    link: [],
    category: EPostCategory.goodNews
};