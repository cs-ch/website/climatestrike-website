import {TStrapiPost} from "../../@def";
import {ELanguages} from "../../../core/@def";
import {EPostCategory} from "../../../components/@def";

export const factsChangeIsPossiblePost: TStrapiPost = {
    identifier: "facts-change-is-possible",
    date: "",
    title: [{language: ELanguages.de, text: "Wir können es ändern"}],
    lead: [{language: ELanguages.de, text: "Wir verursachen in unserer aktuellen gesllschaftlichen Organisationsform die Klimakrise. Wir können sie also auch lösen, es ist eine Frage des Wollens."}],
    author: [{language: ELanguages.all, text: "Nicola Bosshard"}],
    category: EPostCategory.fact,
    image: [], // {image: [], caption: []},
    content: []
};