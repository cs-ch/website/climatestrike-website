import {TStrapiPost} from "../../@def";
import {ELanguages} from "../../../core/@def";
import {EPostCategory} from "../../../components/@def";


export const biodiversityForumDavos: TStrapiPost = {
    identifier: "biodiversity-forum-davos",
    date: "2020-01-01T10:00:00.000Z",
    title: [{language: ELanguages.de, text: "Erstes Biodiversitätsforum"}],
    lead: [{language: ELanguages.de, text: "In Davos findet zur vom 23.02.2020 und dem 28.02.2020 das erste Welt-Biodiversitätsforum statt. Das Ziel: Die Biodiversität zu retten"}],
    content: [],
    image: [],// {image: [{language: ELanguages.all, text: "/static/images/home-background.png"}], caption: []},
    author: [{language: ELanguages.all, text: "umweltnetz-schweiz.ch"}],
    link: [{language: ELanguages.all, text: "https://www.umweltnetz-schweiz.ch/themen/naturschutz/3413-auf-die-zukunft-der-biologischen-vielfalt.html"}],
    category: EPostCategory.goodNews
};

export const freePublicTransportInLuxemburg: TStrapiPost = {
    identifier: "free-public-transport-in-luxemburg",
    date: "2020-01-01T10:00:00.000Z",
    title: [{language: ELanguages.de, text: "Luxemburg fördert den öffentlichen Verkehr"}],
    lead: [{language: ELanguages.de, text: "In Luxemburg wird der öffentliche Verkehr ab 2020 kostenlos. Grund genug um das Auto zu Hause zu lassen."}],
    content: [],
    image: [],// {image: [{language: ELanguages.all, text: "/static/images/home-background.png"}], caption: []},
    author: [{language: ELanguages.all, text: "Kathrin Klette, NZZ"}],
    link: [{language: ELanguages.all, text: "https://www.nzz.ch/panorama/oeffentlicher-nahverkehr-in-luxemburg-ab-2020-gratis-ld.1453189"}],
    category: EPostCategory.goodNews
};

export const postFoodwasteDok: TStrapiPost = {
    identifier: "srf-foodwaste",
    date: "2020-01-01T10:00:00.000Z",
    title: [{language: ELanguages.de, text: "Foodwaste - Was tun?"}],
    lead: [{language: ELanguages.de, text: "Für das Abwenden der Klimakrise ist ein verantwortungsvoller Umgang mit Lebensmittel notwending. SRF zeigt in ihrem DOK, wie wir Foodwaste verhindern können."}],
    content: [],
    image: [],// {image: [{language: ELanguages.all, text: "/static/images/home-background.png"}], caption: []},
    author: [{language: ELanguages.all, text: "SRF Dok"}],
    link: [{language: ELanguages.all, text: "https://www.srf.ch/play/tv/dok/video/foodwaste---was-tun?id=3dcbca29-2fba-4d73-92da-a8bbac96d3a5"}],
    category: EPostCategory.otherNews
};

export const postCo2Law: TStrapiPost = {
    identifier: "klimanews-co2-law",
    date: "2020-01-01T10:00:00.000Z",
    title: [{language: ELanguages.de, text: "Das CO2-Gesetz ist ein Papiertiger"}],
    lead: [{language: ELanguages.de, text: "„Ausgewogenes CO2-Gesetz“ heisst es im Titel der Medienmitteilung, welche die Umweltkommission des Nationalrates (Urek-NR) am vergangenen Mittwoch (12. Februar) nach dem Abschluss der Kommissionsarbeit veröffentlicht hat: Mit 18 zu 7 Stimmen hat die nationalrätliche Umweltkommission die Totalrevision des CO2-Gesetzes in der Gesamtabstimmung angenommen. Ein Täuschungsmanöver."}],
    content: [],
    image: [],// {image: [{language: ELanguages.all, text: "/static/images/home-background.png"}], caption: []},
    author: [{language: ELanguages.all, text: "Christian Rentsch, Klimalandschweiz"}],
    link: [{language: ELanguages.all, text: "https://klimalandschweiz.ch/2020/02/16/15-februar-2020-das-co2-gesetz-ist-ein-papiertiger/"}],
    category: EPostCategory.badNews
};

export const postOneYearClimatestrike: TStrapiPost = {
    identifier: "one-year-climatestrike",
    date: "2020-01-01T10:00:00.000Z",
    title: [{language: ELanguages.de, text: "Ein Jahr Klimastreik"}],
    lead: [{language: ELanguages.de, text: "Die Bewegung hat im letzten Jahr grossflächig zu mobilisieren und zu polarisieren vermocht und konnte dabei vor allem einen grossen Erfolg verzeichnen: Das Klima wurde zum wichtigsten Thema der öffentlichen Debatte. Konkrete politische Massnahmen bleiben aber nach wie vor aus, die Klimastreikbewegung macht also weiter!"}],
    content: [{
        __component: "component.markdown",
        text: [{
            language: ELanguages.de, text: `** Rückblick auf ein bewegtes Jahr **

Angefangen hat es in verschiedenen schweizer Städten im Dezember letzten Jahres, als mehrere hundert Schüler\*innen einem über Whatsapp - Chats verbreiteten Aufruf gefolgt und zum ersten Mal auf die Strasse gegangen. Sie haben erkannt, dass ihre Zukunft auf dem Spiel steht: Die sich zunehmend verschärfende Klimakrise bedroht die Lebensgrundlagen von uns allen, doch aufgrund persönlicher Interessen verschliessen Machtträger\*innen die Augen und ergreifen kaum Massnahmen, um das Problem zu lösen. Zwar sollte es nicht Aufgabe der Schüler\*innen sein, die Klimakrise zu lösen, doch durch die Proteste sollten Gesellschaft und Politik wachgerüttelt werden.

Von Dezember bis Mai fanden neben kleineren lokalen Aktionen fast monatlich schweizweite oder globale Streiks oder Demos statt, die für jeweils für viel Aufruhr sorgten. Im Hintergrund hat sich die Bewegung formiert und eine Struktur gegeben. An mittlerweile fünf nationalen Treffen haben sich die Aktivist\*innen auf ihre grundlegenden Forderungen geeinigt:

Wir fordern, dass die Schweiz den nationalen Klimanotstand ausruft.

Wir fordern, dass die Schweiz bis 2030 im Inland netto 0 Treibhausgasemissionen ohne Einplanung von Kompensationstechnologien verursacht. Die netto Treibhausgasemissionen müssen zwischen 1.1.2020 und 1.1.2024 um mindestens 13 % pro Jahr sinken, und danach um mindestens 8 % pro Jahr sinken bis 1.1.2030. Alle Anteile verstehen sich relativ zu den Emissionen von 2018.

Wir fordern Klimagerechtigkeit.

Falls diese Forderungen im bestehenden System nicht umgesetzt werden können, braucht es einen Systemwandel.

Die Klimastreikbewegung hat im vergangenen Jahr zu mobilisieren vermocht wie kaum eine zuvor in der Schweiz. An der “Klimademo des Wandels”, einer zentralen Klimademo am 28. September 2019 in Bern, haben fast 100 000 Menschen teilgenommen.

** Erfolge und Enttäuschungen **

    Die Bewegung hat das Jahr 2019 in der Schweiz geprägt wie kein anderes Thema. Das Klima rückte in der Fokus der öffentlichen Debatte und sowohl Medien wie auch Politik waren gezwungen, Stellung zu nehmen. Die grosse Aufmerksamkeit, die der Bewegung zuteil wurde, ist wohl ihr grösster Erfolg. Dazu kommen einige politische Errungenschaften; etwa haben zahlreiche Städte den Klimanotstand ausgerufen. Aber wirkungsvolle Klimaschutzmassnahmen auf nationaler Ebene lassen auf sich warten.

** Noch lange nicht am Ziel **

    Für die Klimaaktivist\*innen ist klar: Solange ihre Forderungen nicht erreicht sind, bleiben sie dran. Noch ist die Politik weit davon entfernt, Massnahmen umzusetzen, die zu netto null Treibhausgasen bis 2030 führen würde. Dabei gäbe es viele Bereiche, wo angesetzt werden könnte, zum Beispiel beim Schweizer Finanzplatz. Der Klimastreik hat diesbezüglich Forderungen an die Politik und an die Finanzinstitute erarbeitet: Finanzflüsse sollen transparent und klimafreundlich werden, dazu braucht es auch Regulierungen, um so grosse Mengen an CO2 einzusparen und weitere Umweltschäden zu verhindern.

Auch nächstes Jahr geht es weiter: ** Am 15. Mai 2020 findet der Strike for Future ** statt, das bisher wichtigste und grösste Projekt der Klimastreikbewegung. Menschen aus allen Bevölkerungsschichten sollen sich beteiligen an diesem nationalen Aktionstag, ähnlich dem Frauen * streik. So soll ein zusätzliches starkes Zeichen gesetzt und der Druck nochmals erhöht werden. Bereits jetzt ist die Zusammenarbeit mit anderen Akteur\*innen und der Aufbau von Lokalgruppen in vollem Gang.`}]
    }],
    category: EPostCategory.goodNews,
    image: [],// {image: [{language: ELanguages.all, text: "/static/images/home-background.png"}], caption: []},
    author: [{language: ELanguages.all, text: "Anonym"}],
    link: []
};

export const postCo2LawGermany: TStrapiPost = {
    identifier: "klimanews-co2-law-germany",
    date: "2020-01-01T10:00:00.000Z",
    title: [{language: ELanguages.de, text: "Das deutsche Klimapaket wird verschärft"}],
    lead: [{language: ELanguages.de, text: "Nachdem der Weltklimagipfel in Madrid trotz Abschlusserklärung faktisch gescheitert ist, werden nationale Klimapläne wieder wichtiger. Im frommen Glauben, dass ein ambitiöses „Klimapaket“ andere Länder animieren könnte, ihre nationalen Ziele ebenfalls zu verschärfen, hat sich die Grosse Koalition in Deutschland zu Massnahmen durchgerungen, die zwar immer noch nicht ausreichen, um die Pariser Klimaziele zu erreichen, aber wenigstens in die richtige Richtung weisen."}],
    content: [],
    image: [],// {image: [{language: ELanguages.all, text: "/static/images/home-background.png"}], caption: []},
    author: [{language: ELanguages.all, text: "Christian Rentsch, Klimalandschweiz"}],
    link: [{language: ELanguages.all, text: "https://klimalandschweiz.ch/2019/12/20/19-dezember-2019-das-deutsche-klimapaket-wird-verschaerft/"}],
    category: EPostCategory.goodNews
};