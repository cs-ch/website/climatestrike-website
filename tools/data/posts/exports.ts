import {TStrapiPost} from "../../@def";
import {factsDoYouReallyWantToKnowPost} from "./fact1";
import {aviationOpenLetter, coronaCommunication, climateActionPlanAnnouncement, oneYearClimatestrike, tansparencyReport2019, onlineClimatestrikeApril} from "./migratedPosts";
// import {
//     biodiversityForumDavos, freePublicTransportInLuxemburg, postFoodwasteDok,
//     postCo2Law, postOneYearClimatestrike, postCo2LawGermany
// } from "./testPosts";
import {newWebsiteLaunchedPost} from "./newPosts";

export const posts: TStrapiPost[] = [];
posts.push(
    factsDoYouReallyWantToKnowPost,
    climateActionPlanAnnouncement,
    oneYearClimatestrike,
    tansparencyReport2019,
    coronaCommunication,
    aviationOpenLetter,
    onlineClimatestrikeApril,
    newWebsiteLaunchedPost,
    // factsYouAreAffected,
    // factsChangeIsPossible,
    // factsYourInvolvementMatters,
    // biodiversityForumDavos,
    // freePublicTransportInLuxemburg,
    // postFoodwasteDok,
    // postCo2Law,
    // postOneYearClimatestrike,
    // postCo2LawGermany
);