import {TStrapiPost} from "../../@def";
import {ELanguages} from "../../../core/@def";
import {EPostCategory} from "../../../components/@def";

export const newWebsiteLaunchedPost: TStrapiPost = {
    identifier: "new-website-launched",
    date: "2020-04-25T10:00:00.000Z",
    title: [
        {language: ELanguages.de, text: "Neue Website"},
        {language: ELanguages.fr, text: "Nouveau site web"},
        {language: ELanguages.it, text: "Nuovo sito web"}
    ],
    lead: [{
        language: ELanguages.de, text: "\"Hey, lass uns unserer Website einen neuen Anstrich verpassen!\" - so oder ähnlich wurde die Idee einer neuen Website ins Leben gerufen. Eine kurzer Abriss über die Entstehung der neuen Website."
    },
    {
        language: ELanguages.fr, text: "\"Et si on refaisait une beauté à notre site internet?\" C’est d’une proposition comme celle-ci que l'idée d'un nouveau site web est née. Voici un bref aperçu de sa création."
    },
    {
        language: ELanguages.it, text: "\"Ehi, e se reinventassimo il nostro sito web!?\" - è da una proposta del genere che è nata l'idea di un nuovo sito web. Ecco una breve panoramica sul suo percorso di creazione."
    }],
    author: [
        {language: ELanguages.de, text: "AG Neue Website"},
        {language: ELanguages.fr, text: "GT Nouveau site web"},
        {language: ELanguages.it, text: "WG Nuovo sito web"}],
    category: EPostCategory.goodNews,
    image: [], // {image: [{"language": ELanguages.all,"text": "/static/images/new-website-launch-title.jpg"}], caption: []},
    content: [
        {
            "__component": "component.markdown",
            "text": [
                {
                    "language": ELanguages.de, text: `## Das Design
Eine neue Website zu entwickeln braucht eine Vision. Wie soll die Website aufgebaut werden, welche Funktionen sind wichtig, was für eine Farbpalette und Designsprache passt zum Klimastreik? Mit diesen Fragen und damit dem Design hat sich ein kleines Team aus Klimastreikenden und Studenten des Studienganges Digital Ideation der Hochschule Luzern gewidmet. Im Rahmen eines Studienprojekts haben sie sich Designerhüte angezogen, Websiten von ähnlichen Organisationen verglichen, Zielgrupppen analysiert, Befragungen und Benutzertests durchgeführt und schlussendlich einen Prototypen erarbeitet.

## Die Entwicklung
Ein erstes Design stand also gegen Ende 2019, welches dann mit dem Task der Umsetzung an die AG IT weitergereicht wurde. Motivierte Entwickler\\*innen waren bald gefunden - jedoch: Keine\\*r von uns hatte einen Plan von WordPress - das Framework, mit welchem die alte Website aufgebaut war.

Also entschieden wir uns für einen Technologiewechsel (mehr dazu in der Box unten) und danach lautete die Devise *coden, coden, coden*.`
                },
                {
                    language: ELanguages.fr, text: `## Le design
Pour développer un nouveau site web, il faut un concept. Comment doit-il être structuré, quelles fonctions sont importantes, quelle palette de couleurs et quelle identité graphique conviendraient à la Grève du climat? Une petite équipe de climatomanes et d'étudiant.e.s du cursus Digital Ideation de la Haute école spécialisée de Lucerne s'est penchée sur ces questions et a donc travaillé sur le design. Dans le cadre d'un projet d'étude, il a fallu se mettre dans la peau de professionnel.le.s et comparer les sites web d'organisations similaires, analyser les groupes cibles, mener des enquêtes et des tests d'utilisateurs pour finalement développer un prototype.

## Le développement
Une première conception était prête à la fin de 2019, qui a ensuite été transmise à AG IT avec la tâche de réaliser le site. Des développeurs motivés ont rapidement été trouvés - mais aucun d'entre eux n'avait de plan de WordPress - le cadre avec lequel l'ancien site web était construit.

Nous avons donc décidé de changer de technologie (plus d'informations à ce sujet dans l'encadré ci-dessous) et la devise était alors *coder, coder, coder*.
`},
                {
                    language: ELanguages.it, text: `## Il design
Per sviluppare un nuovo sito web in primo luogo è necessario avere un concetto, una visione d’insieme. Come deve essere strutturato? Quali funzioni deve avere, quali sono invece tralasciabili? Quali colori e identità grafica sono i più adatti ad un sito riguardante Sciopero per il Clima?
Sono queste le domande che si è posto il piccolo team, composto di scioperanti del clima e studenti del corso di Digital Ideation della Scuola universitaria professionale di Lucerna. Nell'ambito di un progetto di studio, si sono calati nei panni di designer professionisti/e: hanno confrontato siti web di organizzazioni simili; analizzato i diversi target; condotto indagini e test sugli utenti; e infine, hanno sviluppato un prototipo.

## Lo sviluppo
Un primo progetto, poi passato ad AG IT per la realizzazione, era pronto a fine 2019. Presto sono stati/e trovati/e dei programmatori e delle programmatrici motivati/e - ma nessuno era in possesso del codice sorgente del vecchio sito,che era stato sviluppato sul framework di WordPress. 

Così, abbiamo deciso di passare a una tecnologia diversa (maggiori informazioni a riguardo sono fornite nel riquadro sottostante) e il motto è diventato *coden, coden, coden*.`}
            ]
        },
        {
            "__component": "component.collapsible-text",
            "title": [
                {language: ELanguages.de, text: "Die Technologien (Nerd Alert!)"},
                {language: ELanguages.fr, text: "Les technologies (Nerd Altert!)"},
                {language: ELanguages.it, text: "Le tecnologie (Nerd Alert!)"}
            ],
            "text": [
                {
                    "language": ELanguages.de, text: `Unser Entwicklerteam kannte sich vor allem mit TypeScript, NodeJS, React und MongoDb aus. Nicht die besten Voraussetzungen, falls eine WordPress Lösung umgesetzt werden sollte.

Da wir die Website nicht nur modern designet umsetzen, sondern auch technologisch aufrüsten wollten, entschieden wir uns für folgende Rezeptur:

* Ein Starter von Server-Side-Rendering mit *NextJS*
* einen *Express*-Server darunterziehen
* eine Prise *next-i18next* für die i18n hinzufügen
* mit *Material-UI* Komponenten garnieren
* servieren mit Inhalten aus einer frischen *Strapi*-Installation

Das Rezept wird natürlich in *GitLab*-Rezeptories ([Website](https://gitlab.com/omg_me/climatestrike-website/), [Strapi CMS](https://gitlab.com/omg_me/climatestrike-backend)) sicher aufbewahrt - open source und bereit für weitere Verbesserungen in Form von Issues, Commits und Code Reviews.`
                },
                {
                    "language": ELanguages.fr, text: `Notre équipe de développement était particulièrement familiarisée avec TypeScript, NodeJS, React et MongoDb. Ce ne sont pas les meilleures conditions préalables, si une solution WordPress doit être mise en œuvre.

Comme nous voulions non seulement mettre en œuvre le site web avec un design moderne, mais aussi l'améliorer sur le plan technologique, nous avons décidé de suivre la recette suivante:

* Un début de rendu côté serveur avec *NextJS*
* mettre un serveur *express* en dessous
* ajouter une pincée de *next-i18next* pour l'i18n
* Garnir avec des composants *matériels de l'assurance-chômage*.
* servir avec du contenu provenant d'une installation *Strapi* fraîche

La recette est, bien entendu, stockée en toute sécurité dans les recettes *GitLab* ([Website](https://gitlab.com/omg_me/climatestrike-website/), [Strapi CMS](https://gitlab.com/omg_me/climatestrike-backend)) - open source et prête à être améliorée sous forme de problèmes, d'engagements et de révisions de code.
`
                },
                {
                    "language": ELanguages.it, text: `Il nostro team di sviluppo conosce particolarmente bene TypeScript, NodeJS, React e MongoDb, che non rappresentano le soluzioni migliori nel caso si voglia implementare una soluzione sviluppata su WordPress.
Poiché non volevamo rinnovare il sito web solo visivamente, ma anche tecnologicamente, abbiamo optato per la seguente “ricetta”:

* Iniziare con un Rendering Server-Side costruito con *NextJS*
* Condire con un Server-*Express*
* Aggiungere un pizzico di *next-i18next* per l’i18n
* Guarnire con componenti *Material UI*
* Servire con il contenuto proveniente da un’installazione *Strapi* fresca

La ricetta è, naturalmente, conservata in modo sicuro in Repository *GitLab* ([Website](https://gitlab.com/omg_me/climatestrike-website/), [Strapi CMS](https://gitlab.com/omg_me/climatestrike-backend)) - open source e pronta per ulteriori miglioramenti sotto forma di Issues, Commits e Code Reviews.
`
                }
            ]
        },
        {
            "__component": "component.markdown",
            "text": [
                {
                    "language": ELanguages.de, text: `## Der Inhalt
Auf der inhaltlichen Seite musste auch einiges getan werden. Die französische als auch die italienische Schweiz mussten viel zu lange auf korrekte und vollständige Übersetzungen warten. Dies sollte mit der neuen Version korrigiert werden.

Zudem sah das neue Design eine eigene Fakten-Seite vor - für welche sich unsere Expert\\*innen wissenschaftlichen Paper wälzten, Inhalte zusammentrugen sowie wertvolle Inputs zur Gestaltung lieferten.

## Das Ergebnis
Damit sind wir knapp fünf Monate später am heutigen Zeitpunkt angelangt und endlich können wir euch eine Website präsentieren, die

* ein modernes und konsistentes Design hat,
* schneller und effizienter ist,
* fast alle Schweizer Sprachregionen abholt,
* wichtige Inhalte wie unsere Forderungen, Fakten zur Klimakrise und Neuigkeiten übersichtlich darstellt,
* Besucher\\*innen erlaubt, Events nach Kategorie und Ort zu filtern und selbst neue Events zu erfassen
* und vieles mehr...

Wir wünschen euch viel Spass beim Stöbern. Feedback, Kritik und Verbesserungsvorschläge gerne an [website@climatestrike.ch](mailto:website@climatestrike.ch) (...oder für Nerds auch direkt in unserem [GitLab-Repository](https://gitlab.com/omg_me/climatestrike-website) als Issues 😄)


## Danke!
Ein riesengrosses **Merci** an alle, die das Projekt *Neue Website* vorangetrieben haben! Merci allen Designer\\*innen für alles von Skribbel bis zu Prototyp, allen Entwickler\\*innen für alles von *{* bis zum *}*, allen Übersetzer\\*innen für alles von *Accords* bis zu *zero*, allen Tester\\*innen für alles von *da ist ein Leerschlag zu viel* bis zu *die Website funktionert nicht, wenn xyz*.`
                },
                {
                    "language": ELanguages.fr, text: `## Le contenu
Il y avait aussi beaucoup à faire du côté du contenu. Les parties francophone et italophone de la Suisse ont dû attendre beaucoup trop longtemps pour obtenir des traductions correctes et complètes. Cela devrait être corrigé avec la nouvelle version.

De plus, le nouveau design comprenait une page séparée de faits - pour laquelle nos experts ont lu des articles scientifiques, compilé le contenu et fourni des informations précieuses pour le design.

## Le résultat
Avec cela, nous y sommes arrivés presque cinq mois plus tard et nous pouvons enfin vous présenter un site web qui
* a une conception moderne et cohérente
* est plus rapide et plus efficace,
* reprend presque toutes les régions linguistiques suisses,
* présente clairement des contenus importants tels que nos revendications, des faits sur la crise climatique et des actualités,
* Les visiteurs vous permettent de filtrer les événements par catégorie et par lieu et de créer vous-même de nouveaux événements
* et bien plus encore...

Nous vous souhaitons beaucoup de plaisir en naviguant. Vos commentaires, critiques et suggestions d'amélioration sont les bienvenus à [website@climatestrike.ch](mailto:website@climatestrike.ch) (...ou pour les nerds, directement dans notre [GitLab-Repository](https://gitlab.com/omg_me/climatestrike-website) sous forme de numéros ;))


## Merci !
Un énorme **Merci** à tous ceux qui ont poussé le projet *Nouveau site web* ! Merci à tous les concepteurs, des gribouillages aux prototypes, à tous les développeurs, à tous les traducteurs, à tous les *angewiesen* à *zero*, à tous les testeurs, de *il y a un espace de trop* à *le site ne fonctionnera pas si xyz*.
`
                },
                {
                    "language": ELanguages.it, text: `## Il contenuto
Anche dal punto di vista dei contenuti c’è stato molto da fare. Le parti francofone e italofone della Svizzera hanno dovuto aspettare molto per avere a disposizione traduzioni corrette e complete. Ciò non dovrebbe accadere con alla nuova versione.

Inoltre, il nuovo design include una pagina di “Facts” a sé stante: per realizzarla le nostre esperte e i nostri esperti hanno letto documenti scientifici, catalogato contenuti e fornito preziosi input per la progettazione


## Il risultato
Quasi cinque mesi dopo l’inizio del progetto, oggi possiamo finalmente presentarvi un sito web che
* dispone di un design moderno e coerente
* è più veloce e più efficiente,
* raccoglie quasi tutte le regioni linguistiche svizzere,
* presenta in modo chiaro i contenuti importanti, come per esempio le nostre rivendicazioni, fatti sulla crisi climatica e di attualità,
* consente ai visitatori e alle visitatrici di filtrare gli eventi per categoria e località e di crearne di nuovi
* e molto altro ancora...

Vi auguriamo buon divertimento durante la navigazione. Feedback, critiche e suggerimenti per migliorare sono ben accetti all’indirizzo [website@climatestrike.ch](mailto:website@climatestrike.ch) (...o per i nerd anche direttamente nella nostra [Repository GitLab](https://gitlab.com/omg_me/climatestrike-website) sotto forma di numeri ;))


## Grazie!
Un enorme *GRAZIE* a tutti coloro che hanno sostenuto il progetto del nuovo sito web! Grazie a tutti i designer e tutte le designer per quello che hanno fatto, dagli scarabocchi ai prototipi, grazie a tutti i programmatori e a tutte le programmatrici per tutto il lavoro, da*{* a *}*, grazie a tutti i traduttori e a tutte le traduttrici per tutto, da *angewiesen* fino a  *zèro*, grazie a tutti i tester e a tutte le tester per tutto, da *qui c’è uno spazio di troppo* fino a *il sito web non funziona quando xyz*.
`
                }
            ]
        },
        {
            "__component": "component.collapsible-text",
            "title": [
                {language: ELanguages.de, text: "Fakten: Die Website in Zahlen"},
                {language: ELanguages.fr, text: "Faits: Le site en chiffres"},
                {language: ELanguages.it, text: "Facts: Il sito web in numeri"}
            ],
            "text": [
                {
                    "language": ELanguages.de, text: `- 7919 Zeilen Programiercode<sup>1</sup>
- 606 Commits im Repository<sup>2</sup>
- 94 von 100: Google Page Speed für die Desktop-Seite (alte Website: 33)<sup>3</sup> 
- 81 von 100: Google Page Speed für die Mobile Seite (alte Website: 6)<sup>4</sup>
- 44%: Website Carbon Test (alte Website: 11%)<sup>5</sup>. Das ist zwar noch nicht perfekt, aber wir bleiben dran!
- 3 Sprachen
- 0 Tracker`
                },
                {
                    "language": ELanguages.fr, text: `- 7919 lignes de code de programmation<sup>1</sup>
- 606 engagements dans le dépôt<sup>2</sup>
- 94 sur 100: Google Page Speed pour la page de bureau (ancien site web: 33)<sup>3</sup>
- 81 sur 100: Google Page Speed pour le site mobile (ancien site web: 6)<sup>4</sup>
- 44%: Test carbone du site web (ancien site web : 11%)<sup>5</sup>. Ce n'est pas encore parfait, mais nous continuerons à le faire !
- 3 langues
- 0 tracker
`
                },
                {
                    "language": ELanguages.it, text: `- 7919 linee di codice<sup>1</sup>1</sup
- 606  Commits nella Repository<sup>2</sup>
- 94 su 100: Google Page Speed test per il sito desktop (vecchio sito web: 33)<sup>3</sup>3</sup
- 81 di 100: Google Page Speed test per il sito mobile (vecchio sito web: 6)<sup>4</sup>4</sup
- 44%: Carbon test del sito web (vecchio sito web: 11%)<sup>5</sup>. Non è ancora perfetto, ma continueremo migliorare!
- 3 lingue
- 0 tracker
`
                }
            ]
        },
        {
            "__component": "component.references",
            "references": [
                {
                    "description": [
                        {
                            "language": ELanguages.de,
                            "text": "Stand 23.04.2020, gezählt mit \"find ./components ./core ./factories ./pages ./server ./styles | xargs wc -l\""
                        },
                        {
                            "language": ELanguages.fr,
                            "text": " daté du 23.04.2020, compté avec \"find ./components ./core ./factories ./pages ./server ./styles | xargs wc -l\""
                        },
                        {
                            "language": ELanguages.it,
                            "text": "Datato 23.04.2020, contato con \"find ./components ./core ./factories ./pages ./server ./styles | xargs wc -l\""
                        }
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://gitlab.com/omg_me/climatestrike-website"
                        }
                    ]
                },
                {
                    "description": [
                        {"language": ELanguages.de, "text": "Stand 23.04.2020"},
                        {"language": ELanguages.de, "text": "Daté du 23.04.2020"},
                        {"language": ELanguages.it, "text": "Dal 23.04.2020"}
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://gitlab.com/omg_me/climatestrike-website"
                        }
                    ]
                },
                {
                    "description": [
                        {"language": ELanguages.de, "text": "Getestet am 23.04.2020"},
                        {"language": ELanguages.fr, "text": "Testé le 23.04.2020"},
                        {"language": ELanguages.de, "text": "Testato il am 23.04.2020"}
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://developers.google.com/speed/pagespeed/insights/"
                        }
                    ]
                },
                {
                    "description": [
                        {"language": ELanguages.de, "text": "Getestet am 23.04.2020"},
                        {"language": ELanguages.fr, "text": "Testé le 23.04.2020"},
                        {"language": ELanguages.de, "text": "Testato il am 23.04.2020"}
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://developers.google.com/speed/pagespeed/insights/"
                        }
                    ]
                },
                {
                    "description": [
                        {"language": ELanguages.de, "text": "Getestet am 23.04.2020"},
                        {"language": ELanguages.fr, "text": "Testé le 23.04.2020"},
                        {"language": ELanguages.de, "text": "Testato il am 23.04.2020"}
                    ],
                    "link": [
                        {
                            "language": ELanguages.all,
                            "text": "https://websitecarbon.com"
                        }
                    ]
                }
            ]
        }
    ]
};