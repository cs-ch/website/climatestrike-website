import {ELanguages} from "../../core/@def";
import {IContactGroup} from "../../components/@def";

export const contactGroups: IContactGroup[] = [
    {
        identifier: "ch",
        identifiers: ["CH"],
        selected: true,
        contacts: [
            {
                identifier: "ch-tg-info",
                type: "telegram",
                link: "https://t.me/klimastreikschweizdeinfo",
                linkName: [{language: ELanguages.all, text: "Telegram Infokanal"}]
            },
            {
                identifier: "people-for-future-web",
                type: "website",
                link: "https://peopleforfuture.ch/",
                linkName: [{language: ELanguages.all, text: "Website Menschen fürs Klima"}]
            },
            {
                identifier: "climate-gp-web",
                type: "website",
                link: "https://www.gpclimat.ch/",
                linkName: [{language: ELanguages.all, text: "Website Klima-Grosseltern"}]
            },
            {
                identifier: "people-for-future-wa",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/E2t6erk8VEd594AM3l24uZ",
                linkName: [{language: ELanguages.all, text: "WhatsApp Menschen Fürs Klima"}]
            },
            {
                identifier: "ch-insta-de",
                type: "instagram",
                link: "https://www.instagram.com/klimastreikschweiz/",
                linkName: [{language: ELanguages.all, text: "Instagram deutsch"}]
            },
            {
                identifier: "ch-insta-fr",
                type: "instagram",
                link: "https://www.instagram.com/climatestrike_suisse",
                linkName: [{language: ELanguages.all, text: "Instagram français"}]
            },
            {
                identifier: "ch-insta-it",
                type: "instagram",
                link: "https://www.instagram.com/scioperoperilclimasvizzera",
                linkName: [{language: ELanguages.all, text: "Instagram italiano"}]
            },
            {
                identifier: "ch-fb-de",
                type: "facebook",
                link: "https://www.facebook.com/klimastreikschweiz/",
                linkName: [{language: ELanguages.all, text: "Facebook deutsch"}]
            },
            {
                identifier: "ch-fb-fr",
                type: "facebook",
                link: "https://www.facebook.com/pg/GreveDuClimatSuisse/",
                linkName: [{language: ELanguages.all, text: "Facebook français"}]
            },
            {
                identifier: "ch-fb-it",
                type: "facebook",
                link: "https://www.facebook.com/scioperoperilclimaCH/",
                linkName: [{language: ELanguages.all, text: "Facebook italiano"}]
            }
        ]
    },
    {
        identifier: "ag",
        identifiers: ["AG"],
        contacts: [
            {
                identifier: "ag-mail",
                type: "email",
                link: "aargau@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "ag-wa",
                type: "whatsapp",
                link: "https://tinyurl.com/KlimastreikAargau",
                linkName: [{language: ELanguages.all, text: "WhatsApp"}]

            },
            {
                identifier: "ag-wa-info",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/LoPYgEXiGxTIIQZJRsy7xd",
                linkName: [{language: ELanguages.all, text: "WhatsApp Infokanal"}]
            },
            {
                identifier: "ag-dc",
                type: "discord",
                link: "https://discord.gg/4EWfS8S",
                linkName: [{language: ELanguages.all, text: "Discord"}]
            },
            {
                identifier: "ag-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimastreik_ag/",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            }
        ]
    },
    {
        identifier: "ai-ar",
        identifiers: ["AI", "AR"],
        contacts: [
            {
                identifier: "ai-ar-wa",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/GHoKBNrBeUGDYAN8ncIyvD",
                linkName: [{language: ELanguages.all, text: "WhatsApp"}]

            },
            {
                identifier: "ar-mail",
                type: "email",
                link: "klimagruppear@gmail.com",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]

            },
            {
                identifier: "ar-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimagruppe_ar/",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            }
        ]
    },
    {
        identifier: "bl-bs",
        identifiers: ["BL", "BS"],
        contacts: [
            {
                identifier: "bl-bs-mail",
                type: "email",
                link: "basel@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail Klimastreik"}]
            },
            {
                identifier: "bl-bs-mail-erwachsene",
                type: "email",
                link: "klimabewegungbasel@gmail.com",
                linkName: [{language: ELanguages.all, text: "E-Mail Klimabewegung Basel"}]
            },
            {
                identifier: "bl-bs-wa",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/CbN2A3U9Utw0Pv0gw0c47K",
                linkName: [{language: ELanguages.all, text: "WhatsApp"}]

            },
            {
                identifier: "bs-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimastreikbasel/",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            },
            {
                identifier: "bl-bs-fb",
                type: "facebook",
                link: "https://www.facebook.com/klimastreikbasel/",
                linkName: [{language: ELanguages.all, text: "Facebook"}]
            },
            {
                identifier: "bl-bs-web",
                type: "website",
                link: "https://www.klimastreik-bs.ch/",
                linkName: [{language: ELanguages.all, text: "Website"}]
            }
        ]
    },
    {
        identifier: "be-general",
        identifiers: ["BE"],
        description: [{language: ELanguages.all, text: "Bern allgemein"}],
        contacts: [
            {
                identifier: "be-general-mail",
                type: "email",
                link: "bern@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail Allgemein"}]
            },
            {
                identifier: "be-general-media-mail",
                type: "email",
                link: "medien-bern@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail Medien"}]
            },
            {
                identifier: "be-general-finance-mail",
                type: "email",
                link: "finanzen-bern@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail Finanzen"}]
            },
            {
                identifier: "be-general-web",
                type: "website",
                link: "https://bern.climatestrike.ch/",
                linkName: [{language: ELanguages.all, text: "Website"}]
            },
            {
                identifier: "be-general-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimastreikbern",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            }
        ]
    },
    {
        identifier: "be-local",
        identifiers: ["BE"],
        description: [{language: ELanguages.all, text: "Bern Lokalgruppen"}],
        contacts: [
            {
                identifier: "be-local-uni",
                type: "email",
                link: "unibe@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail Uni Bern"}]
            },
            {
                identifier: "be-local-koeniz",
                type: "email",
                link: "klimastreik.koeniz@protonmail.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail Köniz"}]
            },
            {
                identifier: "be-local-oberaargau",
                type: "email",
                link: "oberaargau@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail Oberaargau (Langenthal)"}]
            },
            {
                identifier: "be-local-muensingen",
                type: "email",
                link: "muensingen@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail Münsingen"}]
            }
        ]
    },
    {
        identifier: "be-wa-tg-sm",
        identifiers: ["BE"],
        description: [{language: ELanguages.all, text: "Bern WhatsApp / Telegram / Social Media"}],
        contacts: [
            {
                identifier: "be-koeniz-tg",
                type: "telegram",
                link: "https://t.me/klimastreik_koeniz",
                linkName: [{language: ELanguages.all, text: "Telegram Köniz Infokanal"}]
            },
            {
                identifier: "be-wa",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/invite/F2pSfrl53Kh90J6v9fm2cz",
                linkName: [{language: ELanguages.all, text: "WhatsApp Köniz Infokanal"}]
            },
            {
                identifier: "be-tg-info",
                type: "telegram",
                link: "https://t.me/klimastreikberninfokanal",
                linkName: [{language: ELanguages.all, text: "Telegram Infokanal"}]
            },
            {
                identifier: "be-wa-general",
                type: "whatsapp",
                link: "https://tinyurl.com/KlimastreikBern2",
                linkName: [{language: ELanguages.all, text: "WhatsApp Bern"}] //2.0
            },
            {
                identifier: "be-wa-info",
                type: "whatsapp",
                link: "https://tinyurl.com/y3466p4h",
                linkName: [{language: ELanguages.all, text: "WhatsApp Bern Infokanal"}]
            },
            {
                identifier: "be-wa-uni-general",
                type: "whatsapp",
                link: "https://tinyurl.com/KlimastreikUniBern",
                linkName: [{language: ELanguages.all, text: "WhatsApp UniBe"}]
            },
            {
                identifier: "be-tg-uni-info",
                type: "telegram",
                link: "https://tinyurl.com/telegramstudisbern",
                linkName: [{language: ELanguages.all, text: "Telegram UniBe Infokanal"}]
            },
            {
                identifier: "be-ig-oberaargau",
                type: "instagram",
                link: "https://www.instagram.com/klimastreikoberaargau",
                linkName: [{language: ELanguages.all, text: "Instagram Oberaargau"}]
            }
        ]
    },
    {
        identifier: "be-biel",
        identifiers: ["BE"],
        description: [{language: ELanguages.all, text: "Biel"}],
        contacts: [
            {
                identifier: "biel-mail",
                type: "email",
                link: "biel-bienne@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "biel-wa",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/IhC0mC1F6I84XjWcXWtZCW",
                linkName: [{language: ELanguages.all, text: "WhatsApp"}]
            }
        ]
    },
    {
        identifier: "fr",
        identifiers: ["FR"],
        contacts: [
            {
                identifier: "fr-mail",
                type: "email",
                link: "fribourg@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "fr-wa-1",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/KEigzZGKv8F1TD2DfrNBND",
                linkName: [{language: ELanguages.all, text: "WhatsApp 1"}]
            },
            {
                identifier: "fr-wa-2",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/GlJsguKOhWO0zoAq4QRtrl",
                linkName: [{language: ELanguages.all, text: "WhatsApp 2"}]
            },
            {
                identifier: "fr-dc",
                type: "discord",
                link: "https://discord.gg/hCrp7WU",
                linkName: [{language: ELanguages.all, text: "Discord"}]
            },
            {
                identifier: "fr-tg",
                type: "telegram",
                link: "https://t.me/GdCFribourg",
                linkName: [{language: ELanguages.all, text: "Telegram"}]
            },
            {
                identifier: "fr-web",
                type: "website",
                link: "https://fribourg.climatestrike.ch/",
                linkName: [{language: ELanguages.all, text: "Website"}]
            }
        ]
    },
    {
        identifier: "ge",
        identifiers: ["GE"],
        contacts: [
            {
                identifier: "ge-mail",
                type: "email",
                link: "climatestrike.ge@gmail.com",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "ge-wa-1",
                type: "whatsapp",
                link: "https://tinyurl.com/GreveduclimatGeneve",
                linkName: [{language: ELanguages.all, text: "WhatsApp 1"}]
            },
            {
                identifier: "ge-wa-2",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/HgddT5PqL6hLscLGhftr5G",
                linkName: [{language: ELanguages.all, text: "WhatsApp 2"}]
            },
            {
                identifier: "ge-dc",
                type: "discord",
                link: "https://discord.gg/Ptt2XCb",
                linkName: [{language: ELanguages.all, text: "Discord"}]
            },
            {
                identifier: "ge-tg",
                type: "telegram",
                link: "https://t.me/joinchat/EZApvEkmlGexkeVQNN7JOw",
                linkName: [{language: ELanguages.all, text: "Telegram"}]
            }
        ]
    },
    {
        identifier: "gl",
        identifiers: ["GL"],
        contacts: [
            {
                identifier: "gl-wa",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/Ehlho2sI6fsJG0pWbFYq0K",
                linkName: [{language: ELanguages.all, text: "WhatsApp"}]
            },
            {
                identifier: "gl-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimastreik_glarus",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            },
            {
                identifier: "gl-fb",
                type: "facebook",
                link: "https://www.facebook.com/klimastrikeglarus/",
                linkName: [{language: ELanguages.all, text: "Facebook"}]
            },
            {
                identifier: "gl-web",
                type: "other",
                link: "https://www.klima-glarus.ch",
                linkName: [{language: ELanguages.all, text: "Website"}]
            },
            {
                identifier: "gl-agenda",
                type: "other",
                link: "https://www.glarneragenda.ch/de/suche?#resultsPerPage=&search=KlimaGlarus",
                linkName: [{language: ELanguages.all, text: "Glarner Agenda"}]
            },
            {
                identifier: "gl-email",
                type: "email",
                link: "glarus@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
        ]
    },
    {
        identifier: "gr",
        identifiers: ["GR"],
        contacts: [
            {
                identifier: "gr-mail",
                type: "email",
                link: "klimastreik.graubuenden@gmail.com",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "gr-wa",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/HFksn3k5BUkFAnqzbyLIDH",
                linkName: [{language: ELanguages.all, text: "WhatsApp"}]
            },
            {
                identifier: "gr-tg",
                type: "telegram",
                link: "https://t.me/joinchat/Aw45uhbPsjRylPiOSmJfKQ",
                linkName: [{language: ELanguages.all, text: "Telegram"}]
            },
            {
                identifier: "gr-ig",
                type: "instagram",
                link: "https://www.instagram.com/buendnerklimastreik",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            },
            {
                identifier: "gr-web",
                type: "website",
                link: "http://www.klimastreik-graubuenden.ch",
                linkName: [{language: ELanguages.all, text: "Website"}]
            }
        ]
    },
    {
        identifier: "ju",
        identifiers: ["JU"],
        contacts: [
            {
                identifier: "ju-mail",
                type: "email",
                link: "jura@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "ju-wa",
                type: "whatsapp",
                link: "mailto:jura@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "Pour intégrer le groupe Whatsapp, veuillez écrire un mail à jura@climatestrike.ch"}]
            },
            {
                identifier: "ju-tg",
                type: "telegram",
                link: "https://t.me/gdcjura",
                linkName: [{language: ELanguages.all, text: "Telegram Canal"}]
            },
            {
                identifier: "ju-ig",
                type: "instagram",
                link: "https://www.instagram.com/climatestrike_jura",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            },
            {
                identifier: "ju-web",
                type: "website",
                link: "https://jura.climatestrike.ch/",
                linkName: [{language: ELanguages.all, text: "Website"}]
            }
        ]
    },
    {
        identifier: "ne",
        identifiers: ["NE"],
        contacts: [
            {
                identifier: "ne-mail",
                type: "email",
                link: "neuchatel@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "ne-web",
                type: "website",
                link: "http://neuchatel.climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "Website"}]
            },
            {
                identifier: "ne-wa",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/Dcus2XqVsfDEfeF9IEE388",
                linkName: [{language: ELanguages.all, text: "WhatsApp"}]
            }
        ]
    },
    {
        identifier: "ow",
        identifiers: ["OW"],
        contacts: [
            {
                identifier: "ow-mail",
                type: "email",
                link: "obwalden@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "ow-wa",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/L73jhVELRGaGDPunVNbokX",
                linkName: [{language: ELanguages.all, text: "WhatsApp"}]
            },
            {
                identifier: "ow-tg",
                type: "telegram",
                link: "https://t.me/joinchat/L-CXw1amYF6JZPHqAbOQPA",
                linkName: [{language: ELanguages.all, text: "Telegram"}]
            }
        ]
    },
    {
        identifier: "so-olten",
        identifiers: ["SO"],
        description: [{language: ELanguages.all, text: "Olten"}],
        contacts: [
            {
                identifier: "so-olten-mail",
                type: "email",
                link: "olten@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "so-olten-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimastreik_olten",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            },
            {
                identifier: "so-olten-fb",
                type: "facebook",
                link: "https://www.facebook.com/Klimastreik-Olten-409495953166255/",
                linkName: [{language: ELanguages.all, text: "Facebook"}]
            }
        ]
    },
    {
        identifier: "sh",
        identifiers: ["SH"],
        contacts: [
            {
                identifier: "sh-mail",
                type: "email",
                link: "schaffhausen@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "sh-wa",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/LjAYsfk9ZQaIONfDH7WTUo",
                linkName: [{language: ELanguages.all, text: "WhatsApp"}]
            },
            {
                identifier: "sh-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimastreik_sh",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            }
        ]
    },
    {
        identifier: "so",
        identifiers: ["SO"],
        contacts: [
            {
                identifier: "so-wa",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/IwZlEAm5PNyLyFiUPelVKM",
                linkName: [{language: ELanguages.all, text: "WhatsApp"}]
            },
            {
                identifier: "so-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimastreik_solothurn",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            }
        ]
    },
    {
        identifier: "sg",
        identifiers: ["SG"],
        description: [{language: ELanguages.all, text: "St. Gallen / Ostschweiz"}],
        contacts: [
            {
                identifier: "sg-mail",
                type: "email",
                link: "ostschweiz@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "sg-wa",
                type: "whatsapp",
                link: ": https://chat.whatsapp.com/BGlf0dPjAE7HPUFBgjZZha",
                linkName: [{language: ELanguages.all, text: "WhatsApp"}]
            },
            {
                identifier: "sg-wa-rappi",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/KYmhOszMs8WAm1Gug442fK",
                linkName: [{language: ELanguages.all, text: "WhatsApp Rapperswil"}]
            },
            {
                identifier: "sg-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimastreik_ostschweiz",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            }
        ]
    },
    {
        identifier: "sg-wattwil",
        identifiers: ["SG"],
        description: [{language: ELanguages.all, text: "Wattwil"}],
        contacts: [
            {
                identifier: "sg-wattwil-wa",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/DW49KR78rMd4WMY2Hss9an",
                linkName: [{language: ELanguages.all, text: "WhatsApp Wattwil"}]
            },
            {
                identifier: "sg-wattwil-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimagruppe_wattwil/",
                linkName: [{language: ELanguages.all, text: "Instagram Wattwil"}]
            }
        ]
    },
    {
        identifier: "be-thun",
        identifiers: ["BE"],
        description: [{language: ELanguages.all, text: "Thun"}],
        contacts: [
            {
                identifier: "be-thun-mail",
                type: "email",
                link: "thun@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            }
        ]
    },
    {
        identifier: "tg",
        identifiers: ["TG"],
        contacts: [
            {
                identifier: "tg-mail",
                type: "email",
                link: "thurgau@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "tg-wa-1",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/KONwSt9NMn49rYIQtXP0ur",
                linkName: [{language: ELanguages.all, text: "WhatsApp 1"}]
            },
            {
                identifier: "tg-wa-2",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/Is4t2UEjeVkJfvMIiNFW2G",
                linkName: [{language: ELanguages.all, text: "WhatsApp 2"}]
            },
            {
                identifier: "tg-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimastreikthurgau",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            },
            {
                identifier: "tg-fb",
                type: "facebook",
                link: "https://www.facebook.com/klimastreikthurgau/",
                linkName: [{language: ELanguages.all, text: "Facebook"}]
            }
        ]
    },
    {
        identifier: "ti",
        identifiers: ["TI"],
        contacts: [
            {
                identifier: "ti-web",
                type: "website",
                link: "http://ticino.climatestrike.ch/",
                linkName: [{language: ELanguages.all, text: "Website"}]
            },
            {
                identifier: "ti-mail",
                type: "email",
                link: "ticino@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "ti-wa-1",
                type: "whatsapp",
                link: ": https://chat.whatsapp.com/HcrGeVkGQfQK6uqmtC2Npp",
                linkName: [{language: ELanguages.all, text: "WhatsApp 1"}]
            },
            {
                identifier: "ti-wa-2",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/HCrAPZkYBzlGN8KKpwOnDq",
                linkName: [{language: ELanguages.all, text: "WhatsApp 2"}]
            },
            {
                identifier: "ti-wa-3",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/G0K9vtX89APEiE9A8Sj6RA",
                linkName: [{language: ELanguages.all, text: "WhatsApp 3"}]
            },
            {
                identifier: "ti-ig",
                type: "instagram",
                link: "https://www.instagram.com/scioperoperilclimasvizzera/",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            },
            {
                identifier: "ti-fb",
                type: "facebook",
                link: "https://www.facebook.com/scioperoperilclimaCH/",
                linkName: [{language: ELanguages.all, text: "Facebook"}]
            }
        ]
    },
    {
        identifier: "ur",
        identifiers: ["UR"],
        contacts: [
            {
                identifier: "ur-mail",
                type: "email",
                link: "klimastreik.uri@gmail.com",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "ur-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimastreik_ur/",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            }
        ]
    },
    {
        identifier: "vd",
        identifiers: ["VD"],
        contacts: [
            {
                identifier: "vd-web",
                type: "website",
                link: "http://vaud.climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "Website"}]
            },
            {
                identifier: "vd-mail",
                type: "email",
                link: "vaud@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "vd-wa",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/JQPB2PbhwUp2PfLNBmvu8l",
                linkName: [{language: ELanguages.all, text: "WhatsApp"}]
            },
            {
                identifier: "vd-tg",
                type: "telegram",
                link: "https://t.me/greveduclimatvaud",
                linkName: [{language: ELanguages.all, text: "Telegram"}]
            }
        ]
    },
    {
        identifier: "vs",
        identifiers: ["VS"],
        contacts: [
            {
                identifier: "vs-mail",
                type: "email",
                link: "valaiswallis@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            }
        ]
    },
    {
        identifier: "lu-sz-zg-nw",
        identifiers: ["LU", "SZ", "ZG", "NW"],
        description: [{language: ELanguages.all, text: "Zentralschweiz"}],
        contacts: [
            {
                identifier: "zs-mail",
                type: "email",
                link: "zentralschweiz@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "zs-wa-1",
                type: "whatsapp",
                link: "https://tinyurl.com/KlimastreikZentralschweiz",
                linkName: [{language: ELanguages.all, text: "WhatsApp 1"}]
            },
            {
                identifier: "zs-wa-2",
                type: "whatsapp",
                link: "https://tinyurl.com/KlimastreikZentralschweiz2",
                linkName: [{language: ELanguages.all, text: "WhatsApp 2"}]
            },
            {
                identifier: "zs-wa-people",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/HMoGIrJ9ZhwE68729NvuZz",
                linkName: [{language: ELanguages.all, text: "WhatsApp Menschen fürs Klima"}]
            },
            {
                identifier: "zs-wa-parents",
                type: "whatsapp",
                link: "https://tinyurl.com/ElternFuersKlimaLU-News",
                linkName: [{language: ELanguages.all, text: "WhatsApp Eltern fürs Klima Luzern"}]
            },
            {
                identifier: "zs-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimastreikzentralschweiz",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            },
            {
                identifier: "zg-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimastreikzug",
                linkName: [{language: ELanguages.all, text: "Instagram Zug"}]
            }
        ]
    },
    {
        identifier: "zh",
        identifiers: ["ZH"],
        contacts: [
            {
                identifier: "zh-mail",
                type: "email",
                link: "zuerich@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "zh-web",
                type: "website",
                link: "https://zuerich.climatestrike.ch/info/",
                linkName: [{language: ELanguages.all, text: "Website"}]
            },
            {
                identifier: "zh-tg",
                type: "telegram",
                link: "https://t.me/klimastreikzurich",
                linkName: [{language: ELanguages.all, text: "Telegram"}]
            },
            {
                identifier: "zh-tg-info",
                type: "telegram",
                link: "https://t.me/klimastreikzhinfo",
                linkName: [{language: ELanguages.all, text: "Telegram Infokanal"}]
            },
            {
                identifier: "zh-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimastreik_zh/",
                linkName: [{language: ELanguages.all, text: "Instagram"}]
            },
            {
                identifier: "zh-wa-parents",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/GNFQmIGKOkqKSZ5HQlUZHO",
                linkName: [{language: ELanguages.all, text: "WhatsApp Eltern fürs Klima"}]
            },
            {
                identifier: "zh-wa-eth",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/LOB2RnZRzyq9ck8NkQrwiB",
                linkName: [{language: ELanguages.all, text: "WhatsApp ETH"}]
            },
            {
                identifier: "zh-wa-zhaw",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/DUwk7PJzQQ6FrL8CwybiVk",
                linkName: [{language: ELanguages.all, text: "WhatsApp ZHAW"}]
            }
        ]
    },
    {
        identifier: "zh-winti",
        identifiers: ["ZH"],
        description: [{language: ELanguages.all, text: "Winterthur"}],
        contacts: [
            {
                identifier: "zh-winti-mail",
                type: "email",
                link: "winterthur@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "zh-winti-tg",
                type: "telegram",
                link: "https://t.me/klimastreik_winterthur",
                linkName: [{language: ELanguages.all, text: "Telegram"}]
            },
            {
                identifier: "zh-winti-tg-info",
                type: "telegram",
                link: "https://t.me/joinchat/Bp6qvldy7b3Or7H2NJs92w",
                linkName: [{language: ELanguages.all, text: "Telegram Infokanal"}]
            },
            {
                identifier: "zh-winti-ig",
                type: "instagram",
                link: "https://www.instagram.com/klimastreik_winterthur",
                linkName: [{language: ELanguages.all, text: "Instagram Winterthur"}]
            }
        ]
    },
    {
        identifier: "zh-uster",
        identifiers: ["ZH"],
        description: [{language: ELanguages.all, text: "Uster"}],
        contacts: [
            {
                identifier: "zh-uster-mail",
                type: "email",
                link: "uster@climatestrike.ch",
                linkName: [{language: ELanguages.all, text: "E-Mail"}]
            },
            {
                identifier: "zh-uster-web",
                type: "website",
                link: "http://www.klimastreik-uster.ch",
                linkName: [{language: ELanguages.all, text: "Website"}]
            },
            {
                identifier: "zh-uster-wa",
                type: "whatsapp",
                link: "https://chat.whatsapp.com/Jovdmk31Lzb4l8yNNm1rZh",
                linkName: [{language: ELanguages.all, text: "WhatsApp"}]
            }
        ]
    }
];