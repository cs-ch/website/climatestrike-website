import {TStrapiPost} from "../@def";
import {topo} from "./topo";
import {
    EEventFilterType,
    EEventView,
    IHeadlineItemProps,
    ILandingLinkProps,
    IExpandableDescriptionProps,
    IDemandItemProps,
    ITitleProps,
    IIconTitleProps,
    IEventsSettings,
    ITextProps,
    IIframeProps,
    IJoinItemProps,
    IPostSectionConfig,
    INewsletterFormProps,
    IFactItemProps,
    EStyles
} from "../../components/@def";
import {ELanguages, EEventCategory} from "../../core/@def";
import {factsDoYouReallyWantToKnowPost} from "./posts/fact1";
import {IContentDelegation, IStrapiContent} from "../../server/api/@def";


type TStrapiFact = Omit<IFactItemProps, "post"> & {post: TStrapiPost}

// Home

export const homeHeadline: IStrapiContent<IHeadlineItemProps> = {
    identifier: "home-headline",
    sortId: 0,
    data: [{
        __component: "component.headline",
        title: [
            {language: ELanguages.de, text: "Solidarität gegen Corona"},
            {language: ELanguages.fr, text: "Solidarité contre Corona"},
            {language: ELanguages.it, text: "Solidarietà contro Corona"}

        ],
        subtitle: [
            {language: ELanguages.de, text: "#WirHelfen"},
            {language: ELanguages.fr, text: "#Aider"},
            {language: ELanguages.it, text: "#Aiuto"}
        ],
        link: {
            type: "read-more", link: [{language: ELanguages.all, text: "/corona"}]
        },
        text: [
            {language: ELanguages.de, text: "Der Klimastreik existiert, weil die Klimakrise beängstigend ist. Unsere Angst gilt aber nicht dem Klima sondern den von der Klimakrise betroffenen Menschen. Die Corona-Krise bedroht die Gesundheit unzähliger Menschen."},
            {language: ELanguages.fr, text: "La grève du climat existe parce que la crise climatique fait peur. Mais notre crainte n'est pas pour le climat mais pour les personnes touchées par la crise climatique. La crise de la corona menace la santé d'innombrables personnes."},
            {language: ELanguages.it, text: "Lo sciopero per il clima esiste perché la crisi climatica provoca paura. Ma la nostra paura non è per il clima, ma per le persone colpite dalla crisi climatica. La crisi della corona minaccia la salute di innumerevoli persone."}

        ]
    }]
};

export const homeNewsletter: IStrapiContent<INewsletterFormProps> = {
    identifier: "home-newsletter",
    sortId: 1,
    data: [{
        __component: "component.newsletter-form",
        identifier: "home-newsletter-registration",
        submitUrl: "register-newsletter",
        text: {
            title: [
                {language: ELanguages.de, text: "Newsletter abonnieren"},
                {language: ELanguages.fr, text: "S'abonner à la newsletter"},
                {language: ELanguages.it, text: "Iscriviti alla newsletter"}

            ],
            text: [
                {language: ELanguages.de, text: "E-Mail und/oder Handynummer angeben. Du erhälst circa einmal im Monat per Mail einen Newsletter von uns."},
                {language: ELanguages.fr, text: "Entrez votre adresse électronique et/ou votre numéro de téléphone portable. Vous recevrez une lettre d'information de notre part par e-mail environ une fois par mois."},
                {language: ELanguages.it, text: "Inserisci il tuo indirizzo e-mail e/o il tuo numero di cellulare. Riceverai una newsletter da noi via e-mail circa una volta al mese."},

            ]
        }
    }]
};

export const homeLandingLinkWhy: IStrapiContent<ILandingLinkProps> = {
    identifier: "home-why-link",
    sortId: 2,
    grouping: "landing-links-line-1",
    data: [{
        __component: "component.landing-link",
        title: [
            {language: ELanguages.de, text: "Warum wir streiken"},
            {language: ELanguages.fr, text: "Pourquoi nous faisons grève"},
            {language: ELanguages.it, text: "Perché scioperiamo"}

        ],
        text: [
            {language: ELanguages.de, text: "Wir streiken, weil Politik und und Wirtschaft nicht handeln"},
            {language: ELanguages.fr, text: "Nous faisons grève parce que les politiciens et les entreprises n'agissent pas"},
            {language: ELanguages.it, text: "Scioperiamo perché la politica e l’economia non agiscono"}

        ],
        icon: [], // [{language: ELanguages.all, text: "/static/images/why.svg"}],
        link: {type: "movement", link: [{language: ELanguages.all, text: "/movement"}]},
    }]
};

export const homeLandingLinkDemands: IStrapiContent<ILandingLinkProps> = {
    identifier: "home-demands-link",
    sortId: 3,
    grouping: "landing-links-line-1",
    data: [{
        __component: "component.landing-link",
        title: [
            {language: ELanguages.de, text: "Wir fordern"},
            {language: ELanguages.fr, text: "Nous demandons"},
            {language: ELanguages.it, text: "Chiediamo"}
        ],
        text: [
            {language: ELanguages.de, text: "Lese unsere Forderungen an Politik und Wirtschaft"},
            {language: ELanguages.fr, text: "Lire nos revendications en matière de politique et d'économie"},
            {language: ELanguages.it, text: "Leggi le nostre rivendicazioni alla politica ed all’economia"}

        ],
        icon: [], // [{language: ELanguages.all, text: "/static/images/demands.svg"}],
        link: {type: "demands", link: [{language: ELanguages.all, text: "/movement#demands"}]},
    }]
};

export const homeLandingLinkFacts: IStrapiContent<ILandingLinkProps> = {
    identifier: "home-facts-link",
    sortId: 4,
    grouping: "landing-links-line-2",
    data: [{
        __component: "component.landing-link",
        title: [
            {language: ELanguages.de, text: "Fakten"},
            {language: ELanguages.fr, text: "Faits"},
            {language: ELanguages.it, text: "Fatti"}
        ],
        text: [
            {language: ELanguages.de, text: "Erfahre mehr über die Klimakrise und was die Wissenschaft sagt"},
            {language: ELanguages.fr, text: "En savoir plus sur la crise climatique et sur ce qu’en dit la science"},
            {language: ELanguages.it, text: "Per saperne di più sulla crisi climatica e su ciò che dice la scienza"}
        ],
        icon: [], // [{language: ELanguages.all, text: "/static/images/facts.svg"}],
        link: {type: "facts", link: [{language: ELanguages.all, text: "/facts"}]},
    }]
};

export const homeLandingLinkJoin: IStrapiContent<ILandingLinkProps> = {
    identifier: "home-join-link",
    sortId: 5,
    grouping: "landing-links-line-2",
    data: [{
        __component: "component.landing-link",
        title: [
            {language: ELanguages.de, text: "Mitmachen"},
            {language: ELanguages.fr, text: "Participer"},
            {language: ELanguages.it, text: "Partecipare"}
        ],
        text: [
            {language: ELanguages.de, text: "Unsere Bewegung ist offen für jung und alt, mach mit!"},
            {language: ELanguages.fr, text: "Notre mouvement est ouvert aux jeunes et aux moins jeunes, Rejoignez-nous!"},
            {language: ELanguages.it, text: "Il nostro movimento è aperto a giovani e meno giovani, unitevi!"}
        ],
        icon: [], // [{language: ELanguages.all, text: "/static/images/join.svg"}],
        link: {type: "read-more", link: [{language: ELanguages.all, text: "/join"}]},
    }]
};

export const homeNews: IStrapiContent<IContentDelegation<IPostSectionConfig>> = {
    identifier: "home-post-section",
    sortId: 6,
    grouping: "home-post-section",
    data: [{
        __component: "component.post-section",
        childIdentifier: "posts",
        childType: "card",
        config: {title: "news", n: 3}
    }]
};

// Facts

export const factsDoYouReallyWantToKnow: IStrapiContent<TStrapiFact> = {
    identifier: "facts-do-you-really-want-to-know",
    sortId: 0,
    data: [{
        __component: "component.fact-item",
        title: [
            {language: ELanguages.de, text: "Willst du es wirklich wissen?"},
            {language: ELanguages.fr, text: "Voulez-vous vraiment le savoir?"},
            {language: ELanguages.it, text: "Vuoi davvero saperlo?"}

        ],
        lead: [
            {language: ELanguages.de, text: "Die Wissenschaft ist sich einig: Wir gefährden mit unserem Treibhausgas-Ausstoss die menschliche Zivilisation. Zweifel daran werden aber gezielt geschürt."},
            {language: ELanguages.fr, text: "La science est certaine: nous mettons en danger la civilisation humaine avec nos émissions de gaz à effet de serre. Mais les doutes à ce sujet sont délibérément alimentés."},
            {language: ELanguages.it, text: "La scienza ne è certa: stiamo mettendo in pericolo la civiltà umana con le nostre emissioni di gas serra. Ma i dubbi su questo sono volutamente alimentati."}
        ],
        icon: [], // [{language: ELanguages.all, text: "static/images/do-you-really-want-to-know.svg"}],
        post: false ? factsDoYouReallyWantToKnowPost : null // FIXME
    }]
};

export const factsYouAreAffected: IStrapiContent<TStrapiFact> = {
    identifier: "facts-you-are-affected",
    sortId: 1,
    data: [{
        __component: "component.fact-item",
        title: [
            {language: ELanguages.de, text: "Es betrifft auch dich"},
            {language: ELanguages.fr, text: "Elle vous concerne également"},
            {language: ELanguages.it, text: "Colpisce anche te"}
        ],
        lead: [
            {language: ELanguages.de, text: "Die Krise betrifft uns in der Schweiz schon heute und in der nahen Zukunft ganz konkret, vor allem aber auch die junge Generation."},
            {language: ELanguages.fr, text: "En Suisse, la crise nous concerne déjà aujourd'hui de manière concrète, surtout la jeune génération, et cela sera encore le cas dans un avenir proche."},
            {language: ELanguages.it, text: "In Svizzera, la crisi ci colpisce concretamente già oggi e continuerà a farlo in modo sempre maggiore nel futuro prossimo, toccando in particolare le generazioni più giovani."}
        ],
        icon: [], // [{language: ELanguages.all, text: "static/images/you-are-affected.svg"}],
        post: null
    }]
};

export const factsChangeIsPossible: IStrapiContent<TStrapiFact> = {
    identifier: "facts-change-is-possible",
    sortId: 2,
    data: [{
        __component: "component.fact-item",
        title: [
            {language: ELanguages.de, text: "Wir können es ändern"},
            {language: ELanguages.fr, text: "Nous pouvons changer les choses"},
            {language: ELanguages.it, text: "Possiamo cambiare le cose"}
        ],
        lead: [
            {language: ELanguages.de, text: "Wir verursachen in unserer aktuellen gesllschaftlichen Organisationsform die Klimakrise. Wir können sie also auch lösen, es ist eine Frage des Wollens."},
            {language: ELanguages.fr, text: "Si nous sommes à l'origine de la crise climatique, à travers notre forme actuelle d'organisation sociale, nous pouvons aussi bien la résoudre; c'est une question de volonté."},
            {language: ELanguages.it, text: "Così come stiamo causando la crisi attraverso l’attuale organizzazione della società, possiamo anche risolverla: è solamente una questione di volontà."}
        ],
        icon: [], // [{language: ELanguages.all, text: "static/images/change-is-possible.svg"}],
        post: null
    }]
};

export const factsYourInvolvementMatters: IStrapiContent<TStrapiFact> = {
    identifier: "facts-your-involvement-matters",
    sortId: 3,
    data: [{
        __component: "component.fact-item",
        title: [
            {language: ELanguages.de, text: "Du kannst etwas bewirken"},
            {language: ELanguages.fr, text: "Vous pouvez faire la différence"},
            {language: ELanguages.it, text: "Puoi fare la differenza"}
        ],
        lead: [
            {language: ELanguages.de, text: "Die grösste Verantwortung liegt, so wichtig diese sind, nicht in persönlichen Konsumentscheiden, sondern in politischem Engagement: Massenmobilisierungen sind die beste Möglichkeit, diese politische Veränderung zu bewirken."},
            {language: ELanguages.fr, text: "La plus grande responsabilité ne réside pas dans les décisions de consommation personnelles, aussi importantes soient-elles, mais dans l'engagement politique. La mobilisation de masse est le meilleur moyen d’amorcer le changement politique."},
            {language: ELanguages.it, text: "La responsabilità più grande non risiede nelle decisioni di consumo personale, per quanto importanti esse siano, ma piuttosto nell'impegno politico. La mobilitazione di massa è il modo migliore per avviare un cambiamento politico."}

        ],
        icon: [], // [{language: ELanguages.all, text: "static/images/your-involvement-matters.svg"}],
        post: null
    }]
};

// Movement

export const movementDescription: IStrapiContent<IExpandableDescriptionProps> = {
    identifier: "movement-description",
    sortId: 0,
    data: [{
        __component: "component.expandable-description",
        title: [
            {language: ELanguages.de, text: "Wer sind wir?"},
            {language: ELanguages.fr, text: "Qui sommes-nous?"},
            {language: ELanguages.it, text: "Chi siamo?"}
        ],
        lead: [
            {language: ELanguages.de, text: "Wir sind Menschen, die sich um ihre eigene Zukunft, die ihrer Kinder und die des Planeten sorgen. Obwohl die wissenschaftlichen Fakten zur Klimakrise seit Jahrzehnten bekannt sind, weigern sich unsere Entscheidungsträger\*innen entsprechend zu handeln. Darum streiken wir für eine gerechte und nachhaltige Gesellschaft."},
            {language: ELanguages.fr, text: "Nous sommes des citoyens qui se préoccupent de leur propre avenir, de celui de leurs enfants et de celui de la planète. Bien que les faits scientifiques concernant la crise climatique soient connus depuis des décennies, nos décideur·es refusent d'agir en conséquence. C'est pourquoi nous sommes en grève pour une société juste et durable. Notre mouvement est ouvert à toutes. Nous travaillons dans un esprit collectif, de démocratie et de rejet des hiérarchies."},
            {language: ELanguages.it, text: "Siamo persone che si preoccupano per il loro futuro, il futuro dei loro figli/e ed il futuro del pianeta. Sebbene i fatti scientifici sulla crisi climatica siano noti da decenni, i/le nostri/e governanti si rifiutano di agire di conseguenza. Per questo motivo ci stiamo battendo per una società giusta e sostenibile. Il nostro movimento è aperto a tutti/e. Lavoriamo cercando di raggiungere l’unanimità, siamo democrati e rifiutiamo le gerarchie."}
        ],
        text: [
            {language: ELanguages.de, text: "Die Wissenschaft ist sich einig, dass die Klimaerwärmung katastrophale Konsequenzen für Mensch und Natur hat. Dennoch weigern sich unsere Entscheidungsträger\*innen, ihre Verantwortung wahrzunehmen und notwendige Massnahmen zur Abwendung der Klimakrise umzusetzen. \nDarum sorgen wir für den nötigen politischen Druck und das öffentliche Bewusstsein für eine nachhaltige und gerechte Gesellschaft. Wir organisieren uns in Regional - und Arbeitsgruppen und arbeiten basisdemokratisch und konsensorientiert. Die Bewegung wird vor allem von jungen Menschen getragen und geformt. Wir pflegen einen respektvollen und solidarischen Umgang miteinander sowie transparente, hierarchiefreie und partizipative Abläufe und Strukturen.\nWir sind an keine Partei oder Organisation gebunden und positionieren uns nicht im politischen Spektrum. Klimaschutz ist kein Kampf von Links und Rechts, sondern ein Überlebenskampf, der uns alle betrifft. Uns vereint die Sorge um die Zukunft unserer Gesellschaft und die Entschlossenheit für diese einzustehen. Wir sehen uns als Teil der weltweiten FridaysForFuture- Bewegung und sind mit Klimabewegungen in der Schweiz und auf dem ganzen Planeten vernetzt."},
            {language: ELanguages.fr, text: "La science s'accorde à dire que le réchauffement climatique a des conséquences catastrophiques pour tous les animaux, humains compris. Néanmoins, nos gouvernements refusent d'assumer leurs responsabilités et de mettre en œuvre les mesures nécessaires pour combattre la crise climatique. \nC'est pourquoi nous exerçons une pression politique et sensibilisons nos concitoyens pour une société durable et juste. Nous nous organisons en groupes de travail régionaux, travaillons de manière démocratique grâce à des processus et des structures transparents, non hiérarchiques et participatifs. Nous cultivons un climat de respect et d’entraide, le mouvement a été principalement initié par des jeunes, mais tout le monde est bienvenu! \nNous ne sommes liés à aucun parti ou organisation et ne nous positionnons pas dans le spectre politique. La protection du climat n'est pas un combat entre la gauche et la droite, mais une lutte pour la survie qui nous concerne tous. Nous sommes uni·e·s par notre préoccupation pour l'avenir de notre société et notre détermination à le rendre enviable. Nous nous considérons comme faisant partie du mouvement mondial FridaysForFuture et sommes en réseau avec les mouvements climatiques en Suisse et sur l’ensemble de la planète."},
            {language: ELanguages.it, text: "La scienza concorda sul fatto che il riscaldamento globale abbia conseguenze catastrofiche per l'uomo e la natura. Ciononostante, i nostri governi rifiutano di assumersi le proprie responsabilità e di mettere in atto le misure necessarie per evitare la crisi climatica. \nPer questo motivo stiamo esercitando la necessaria pressione politica e sensibilizziamo l’opinione pubblica verso una società sostenibile e giusta. Ci organizziamo in gruppi regionali e di lavoro, lavorando in modo democratico e orientato al raggiungimento dell’unanimità. Il movimento è stato iniziato principalmente da giovani, ma tutto il mondo è il benvenuto!\nColtiviamo un clima rispettoso e solidale tra di noi, nonché processi e strutture trasparenti, non gerarchiche e partecipative.\nNon siamo legati ad alcun partito o organizzazione e non ci posizioniamo nello spettro politico. La protezione del clima non è una lotta tra destra e sinistra, ma una lotta per la sopravvivenza che riguarda tutti/ e noi. Siamo uniti dalla nostra preoccupazione per il futuro della nostra società e dalla nostra determinazione a difenderlo. Ci consideriamo parte del movimento mondiale FridaysForFuture e siamo in collegamento con i movimenti climatici in Svizzera e in tutto il pianeta."}
        ],
        style: EStyles.primary
    }]
};

export const movementDemandsTitle: IStrapiContent<ITitleProps> = {
    identifier: "movement-demands-title",
    grouping: "demands",
    sortId: 1,
    data: [{
        __component: "component.title",
        title: [
            {language: ELanguages.de, text: "Forderungen"},
            {language: ELanguages.fr, text: "Revendications"},
            {language: ELanguages.it, text: "Rivendicazioni"}
        ]
    }]
};

export const movementDemandsEmergency: IStrapiContent<IDemandItemProps> = {
    identifier: "movement-demands-emergency",
    sortId: 2,
    data: [{
        __component: "component.demand-item",
        title: [
            {language: ELanguages.de, text: "Klimanotstand"},
            {language: ELanguages.fr, text: "Urgence climatique"},
            {language: ELanguages.it, text: "Emergenza climatica"}
        ],
        lead: [
            {language: ELanguages.de, text: "Wir fordern, dass die Schweiz den nationalen Klimanotstand ausruft, damit die Schweiz die Klimakrise anerkennt und entsprechend handelt."},
            {language: ELanguages.fr, text: "Nous demandons à la Suisse de déclarer une urgence climatique nationale afin que la Suisse reconnaisse la crise climatique et agisse en conséquence."},
            {language: ELanguages.it, text: "Chiediamo che la Svizzera dichiari l'emergenza climatica nazionale affinché la Svizzera riconosca la crisi climatica e agisca di conseguenza."}
        ],
        text: [
            {
                language: ELanguages.de, text: `*Wir fordern, dass die Schweiz den nationalen Klimanotstand ausruft: “Die Schweiz anerkennt die Klimakatastrophe als zu bewältigende Krise. Sie hat folglich auf diese Krise zu reagieren und die Gesellschaft kompetent darüber zu informieren.”*

Der Notstand ist nicht als juristischer Begriff zu verstehen. Unser Ziel ist eine inhaltliche und zeitliche Priorisierung klimarelevanter Geschäfte, nicht etwa die Einschränkung demokratischer Rechte.
Der Begriff wurde jeweils unterschiedlich interpretiert. Häufig beinhaltet er die Prüfung von Gesetzen und Beschlüssen in Bezug auf ihre Klimaverträglichkeit.
Wissenschaftliche Erkenntnisse zur Dringlichkeit der Lage müssen ernst genommen werden. Die Berichte der UNO, insbesondere der [IPCC Sonderbericht zur 1.5°- Erwärmung (IPCC SR15)](https://ipcc.ch/sr15), der [IPCC Sonderbericht über Klimawandel und Land (IPCC SRCCL)](https://www.ipcc.ch/report/srccl/) und der [IPBES Global Assessment Report on Biodiversity and Ecosystem Services](https://www.ipbes.net/global-assessment-report-biodiversity-ecosystem-services), müssen im politischen Prozess berücksichtigt werden.

Die Bevölkerung muss über die Notlage entsprechend informiert werden. Dazu gehört eine ehrliche und transparente Berichterstattung. Es müssen ausreichend und leicht verständliche Informationen zur Klimakrise öffentlich zugänglich sein. Dadurch kann sich die breite Bevölkerung an der Diskussion über Lösungsansätze zur Bewältigung der Krise beteiligen. 
Der Klimanotstand wurde bereits vereinzelt auf Kantons - und Gemeindeebene [ausgerufen](https://www.google.com/maps/d/u/0/viewer?mid=1EBArWOJhhW869Lx0LDjj34bZKa7zEdO8&ll=47.134177601387385%2C8.057395349999979&z=8).`
            },
            {
                language: ELanguages.fr, text: `*Le gouvernement doit déclarer l’état d’urgence climatique. La Suisse doit reconnaître la catastrophe climatique comme une crise qu’il faut surmonter. Elle doit prendre des mesures concrètes en réaction à cette crise, et correctement en informer la population.*

L'état d'urgence ne doit pas être compris comme un terme juridique. Notre objectif est que les actions liées à la protection du climat soient prises de manière ambitieuse et urgente sans pour autant  restreindre les droits démocratiques.      
L’urgence climatique peut être interprétée de nombreuses manières. Pour nous, elle se traduit par une analyse approfondie de chaque loi et nouvelle réglementation afin de vérifier leur compatibilité avec la protection du climat.
Les conclusions scientifiques sur l'urgence de la situation doivent être prises au sérieux. Les rapports des Nations Unies - en particulier le rapport spécial du [GIEC sur le réchauffement à 1,5° (IPCC SR15)](https://ipcc.ch/sr15), le [rapport spécial du GIEC sur le changement climatique et les terres (IPCC SRCCL)](https://www.ipcc.ch/report/srccl/) et le [rapport d'évaluation mondiale de l'IPBES sur la biodiversité et les services écosystémiques](https://www.ipbes.net/global-assessment-report-biodiversity-ecosystem-services) - doivent être pris en compte dans les processus politiques.

a population doit être correctement informée de la situation d'urgence. Cela inclut des rapports honnêtes et transparents. Des informations suffisantes et facilement compréhensibles sur la crise climatique doivent être mises à la disposition de tous. Cela permettra à l'ensemble de la population de participer à la discussion sur les solutions possibles à la crise. 
L'urgence climatique a déjà été [déclarée](https://www.google.com/maps/d/u/0/viewer?mid=1EBArWOJhhW869Lx0LDjj34bZKa7zEdO8&ll=47.134177601387385%2C8.057395349999979&z=8) dans des cas isolés au niveau cantonal et communal.`},
            {
                language: ELanguages.it, text: `*Esigiamo che la Svizzera dichiari lo stato di emergenza climatica nazionale: "La Svizzera riconosce la catastrofe climatica come una crisi da superare. Deve quindi reagire a questa crisi e informare la società in modo competente".*

lo stato di emergenza non deve essere inteso come un termine legale. Il nostro obiettivo è quello di dare priorità alle attività legate al clima in termini di contenuti e di tempi, non di limitare i diritti democratici.

Il termine è stato interpretato in modo diverso a seconda dei casi. Spesso comprende l'esame delle leggi e delle decisioni relative alla loro compatibilità climatica.

Le scoperte scientifiche sull'urgenza della situazione devono essere prese sul serio. I rapporti dell'ONU, in particolare il [rapporto speciale dell'IPCC sul riscaldamento a 1,5°](https://ipcc.ch/sr15) (IPCC SR15), [il rapporto speciale dell'IPCC sul cambiamento climatico e la terra (IPCC SRCCL)](https://www.ipcc.ch/report/srccl/) e il [rapporto di valutazione globale dell'IPBES sulla biodiversità](https://www.ipbes.net/global-assessment-report-biodiversity-ecosystem-services) e i servizi ecosistemici, devono essere presi in considerazione nel processo politico.

La popolazione deve essere adeguatamente informata sulla situazione di emergenza. Ciò include un reporting onesto e trasparente. Informazioni sufficienti e facilmente comprensibili sulla crisi climatica devono essere disponibili al pubblico. Ciò consentirà alla popolazione di partecipare alla discussione sulle possibili soluzioni alla crisi. 
L'emergenza climatica è già stata [dichiarata](https://www.google.com/maps/d/u/0/viewer?mid=1EBArWOJhhW869Lx0LDjj34bZKa7zEdO8&ll=47.134177601387385%2C8.057395349999979&z=8) in casi isolati a livello cantonale e comunale               
`}
        ],
        icon: [], // [{language: ELanguages.all, text: "static/images/emergency.svg"}]
    }]
};

export const movementDemandsNetZero: IStrapiContent<IDemandItemProps> = {
    identifier: "movement-demands-net-zero",
    sortId: 3,
    data: [{
        __component: "component.demand-item",
        title: [
            {language: ELanguages.de, text: "Netto 0 bis 2030"},
            {language: ELanguages.fr, text: "Emissions neutres d’ici 2030"},
            {language: ELanguages.it, text: "Emissioni nette 0 entro il 2030"}
        ],
        lead: [
            {language: ELanguages.de, text: "Wir fordern netto 0 Treibhausgasemissionen bis 2030, damit die Schweiz nur noch so viel Emissionen ausstösst, wie die Natur aufnehmen kann."},
            {language: ELanguages.fr, text: "Nous exigeons des émissions nettes de gaz à effet de serre neutres d'ici 2030, afin que la Suisse n'émette pas plus que ce que la nature peut absorber."},
            {language: ELanguages.it, text: "Chiediamo emissioni nette zero di gas a effetto serra entro il 2030 in modo che la Svizzera emetta solo le emissioni che la natura può assorbire."}


        ],
        text: [
            {
                language: ELanguages.de, text: `*Wir fordern, dass die Schweiz bis 2030 im Inland netto 0 Treibhausgasemissionen ohne Einplanung von Kompensationstechnologien verursacht. Die netto Treibhausgasemissionen müssen zwischen 1.1.2020 und 1.1.2024 um mindestens 13% pro Jahr sinken, und danach um mindestens 8% pro Jahr sinken bis 1.1.2030. Alle Anteile verstehen sich relativ zu den Emissionen von 2018.*

2015 hat die Schweiz mit einer Vielzahl anderer Staaten das Pariser Abkommen [unterzeichnet](https://unfccc.int/process/conferences/pastconferences/paris-climate-change-conference-november-2015/paris-agreement). Dieses verpflichtet die Vertragspartner\*innen zur Mithilfe, die globale Erwärmung im Vergleich zum vorindustriellen Niveau auf deutlich unter 2°C zu begrenzen, angestrebt wird eine Begrenzung auf unter 1.5°C. 

Eine Begrenzung auf eine Erwärmung von 1.5°C würde laut dem [Bericht des IPCC (Intergovernmental Panel on Climate Change)](https://www.ipcc.ch/site/assets/uploads/2019/03/SR1.5-SPM_de_barrierefrei-2.pdf) zu massiv weniger negativen Folgen führen.

Um dies zu erreichen, muss bei der Hauptursache für die globale Erwärmung angesetzt werden: Den Treibhausgasemissionen (THGE). Der Ausstoss muss ab sofort erheblich gesenkt werden, um schliesslich netto null THGE zu erreichen. Es dürfen also nicht mehr THGE ausgestossen werden, als durch die natürliche Speicherung in Wäldern, Gewässern und Böden wieder nachhaltig kompensiert werden können. Weil Treibhausgase lange in der Atmosphäre verbleiben, ist es wichtig, die THGE möglichst schnell und möglichst stark zu senken. Deshalb fordern wir einen steileren Absenkpfad zu Beginn des Jahrzehnts als in der zweiten Hälfte und netto null THGE bis 2030. Je weniger Treibhausgase wir emittieren, desto höher ist die Wahrscheinlichkeit, die Erwärmung begrenzen zu können und desto weniger stark fallen die Konsequenzen der Klimaerwärmung auf Menschen, Tiere und Pflanzen aus.

Als wohlhabendes und innovatives Land sind die dafür notwendigen Massnahmen für die Schweiz leichter umsetzbar als für die Mehrheit der Länder dieser Erde. Darum haben wir die grosse Chance und die klare Verantwortung, eine Vorreiterrolle in der globalen Klimapolitik einzunehmen. Das von der Schweiz ratifizierte Pariser Abkommen begründet diese Forderung mit dem “Principle of Equity”. Länder mit weniger materiellem Wohlstand gewinnen dadurch mehr Zeit, ihre Emissionen zu reduzieren.
`},
            {
                language: ELanguages.fr, text: `*Nous exigeons que la Suisse prenne des mesures législatives et exécutives visant à atteindre un bilan net d’émissions de gaz à effet de serre neutre d'ici à 2030, sans que cela soit dû au  développement ou à l’implémentation de technologies de compensation, il est nécessaire que ce bilan d’émissions neutre soit atteint grâce à une baisse drastique de nos émissions: les émissions nettes de gaz à effet de serre doivent diminuer d'au moins 13 % par an entre le 1.1.2020 et le 1.1.2024, puis d'au moins 8 % par an jusqu'au 1.1.2030. Toutes les taux sont relatifs  aux niveaux d’émissions de 2018.*
                
En 2015, la Suisse a [signé](https://unfccc.int/process/conferences/pastconferences/paris-climate-change-conference-november-2015/paris-agreement) les Accords de Paris avec un grand nombre d'autres États. Les parties contractantes s'engagent ainsi à contribuer à limiter le réchauffement climatique à un niveau bien inférieur à +2°C par rapport aux niveaux préindustriels, dans le but de le maintenir en dessous de +1,5°C. 

Selon le [rapport du GIEC (Groupe d'experts intergouvernemental sur l'évolution du climat)](https://www.ipcc.ch/site/assets/uploads/2019/03/SR1.5-SPM_de_barrierefrei-2.pdf), limiter le réchauffement climatique à +1,5 °C aurait des conséquences dramatiques sur la vie terrestre mais atteindre les +2ºC, serait bien plus catastrophique.

Pour y parvenir, il faut identifier la principale cause du réchauffement climatique: les émissions de Gaz à Effet de Serre (GES). Les émissions doivent être considérablement réduites de manière immédiate afin d'atteindre à terme un niveau net d’émission de GES neutre. Cela signifie qu'il ne faut pas émettre plus de GES que ce qui peut être compensé durablement par le stockage naturel dans les forêts, les eaux et les sols. Comme les gaz à effet de serre restent longtemps dans l'atmosphère, il est important de réduire leur émission plus rapidement et le plus possible. C'est pourquoi nous exigeons une réduction plus forte au début de la décennie qu'au cours de sa seconde moitié, ainsi qu’un bilan d’émission de GES neutre d'ici 2030. Moins nous émettons de gaz à effet de serre, plus nous avons de chances de limiter le réchauffement et moins les conséquences du réchauffement climatique seront graves pour les hommes, les animaux et les plantes.

En tant que pays prospère et innovant, les mesures nécessaires sont plus faciles à mettre en œuvre pour la Suisse que pour la majorité des pays du monde. Nous avons donc la grande opportunité et la responsabilité évidente de jouer un rôle de pionnier dans la politique climatique mondiale. La Convention de Paris, que la Suisse a ratifiée, justifie cette exigence par le "principe d'équité". Elle donne aux pays dont la prospérité matérielle est moindre plus de temps pour réduire leurs émissions.`
            },
            {
                language: ELanguages.it, text: `*Chiediamo che la Svizzera causi 0 emissioni di gas serra su territorio nazionale entro il 2030 senza pianificare tecnologie di compensazione.  Le emissioni nette di gas a effetto serra devono diminuire di almeno il 13% all'anno tra il 1.1.2020 e il 1.1.2024 e successivamente diminuire di almeno l'8% all'anno fino all'1.1.2030.  Tutte le quote sono relative alle emissioni del 2018.*

Nel 2015 la Svizzera ha [firmato](https://unfccc.int/process/conferences/pastconferences/paris-climate-change-conference-november-2015/paris-agreement) l'accordo di Parigi con un gran numero di altri paesi.  Ciò obbliga le parti contraenti a contribuire a limitare il riscaldamento globale ben al di sotto dei 2 ° C rispetto al livello preindustriale; l'obiettivo è mantenerlo al di sotto di 1,5 ° C.

Limitare la temperatura a 1,5 ° C porterebbe a un numero significativamente inferiore di conseguenze negative, [secondo il rapporto IPCC (Gruppo intergovernativo sui cambiamenti climatici)](https://www.ipcc.ch/site/assets/uploads/2019/03/SR1.5-SPM_de_barrierefrei-2.pdf).`
            }
        ],
        icon: [], // [{language: ELanguages.all, text: "static/images/net-zero.svg"}]
    }]
};

export const movementDemandsJustice: IStrapiContent<IDemandItemProps> = {
    identifier: "movement-demands-justice",
    sortId: 4,
    data: [{
        __component: "component.demand-item",
        title: [
            {language: ELanguages.de, text: "Klimagerechtigkeit"},
            {language: ELanguages.fr, text: "Justice climatique"},
            {language: ELanguages.it, text: "Giustizia climatica"}
        ],
        lead: [
            {language: ELanguages.de, text: "Wir fordern Klimagerechtigkeit, damit materiell, finanziell oder sozial benachteiligte Menschen weder durch die Krise, noch durch Massnahmen gegen sie zusätzlich belastet werden. Offizieller Konsens: Wir fordern Klimagerechtigkeit."},
            {language: ELanguages.fr, text: "Nous exigeons une justice climatique afin que les personnes matériellement, financièrement ou socialment défavorisées ne soient pas davantage accablées par la crise ou par les mesures prises contre celle-ci. Consensus officiel: nous demandons une justice climatique."},
            {language: ELanguages.it, text: "chiediamo giustizia climatica affinché le persone materialmente, finanziariamente o socialmente svantaggiate non siano ulteriormente aggravate dalla crisi o dalle misure contro di essa. Unanimità ufficiale: Chiediamo giustizia climatica"}
        ],
        text: [
            {
                language: ELanguages.de, text: `Wir fordern Klimagerechtigkeit.
                
Die Folgen der Klimaerwärmung treffen die ärmsten Teile der Bevölkerung als erstes und am härtesten - sowohl lokal, wie auch global. Deshalb ist Klimaschutz auch eine Frage der sozialen und globalen Gerechtigkeit. Die schwächsten Gruppen der Gesellschaft sollen nicht durch das Leben der wohlhabenderen Menschen leiden. Ausserdem müssen Massnahmen zum Erreichen der Klimaziele in einer Weise ausgestaltet werden, die materiell und finanziell benachteiligte Menschen nicht zusätzlich belasten.

Wir fordern die Umsetzung des Verursacherprinzips: Diejenigen, welche die Treibhausgasemissionen und die Umweltverschmutzung verursachen und davon profitieren, sollen zur Verantwortung gezogen werden. Sie müssen Schäden vorbeugen, beziehungsweise bereits entstandene Schäden beheben.

Alle genannten Punkte gelten insbesondere generationenübergreifend und global betrachtet. Nachfolgende Generationen sollen nicht durch die Taten der vorherigen beeinträchtigt werden. Ungleichheit soll nicht vergrössert, sondern vermindert werden.
`
            },
            {
                language: ELanguages.fr, text: `Nous demandons une justice climatique.
                
Les conséquences du réchauffement climatique touchent en premier lieu et le plus durement les couches les plus pauvres de la population, tant au niveau local que mondial. Par conséquent, la protection du climat est aussi une question mondiale de justice sociale. Les groupes les plus faibles de la société ne devraient pas souffrir de la vie des personnes les plus riches. En outre, les mesures visant à atteindre les objectifs climatiques doivent être conçues de manière à ce que les personnes matériellement et financièrement défavorisées ne soient pas soumises à des charges supplémentaires qu’elles ne pourraient supporter. 

Nous appelons à la mise en œuvre du principe du pollueur-payeur: ceux qui causent les émissions de gaz à effet de serre et de la pollution doivent être tenus pour responsables. Ils doivent prévenir les dommages ou réparer ceux qui se sont déjà produits. 

Tous les points mentionnés ci-dessus s'appliquent en particulier à travers les générations et au niveau mondial. Les générations suivantes ne devraient pas être affectées par les actions des générations précédentes. L'inégalité ne doit pas être augmentée, mais réduite.
`},
            {
                language: ELanguages.it, text: `Le conseguenze del riscaldamento globale hanno colpito per prime e più duramente le fasce più povere della popolazione, sia a livello locale che globale. Pertanto, la protezione del clima è anche una questione di giustizia sociale e globale. I gruppi più deboli della società non dovrebbero soffrire a causa della vita delle persone più ricche. Inoltre, le misure per raggiungere gli obiettivi climatici devono essere concepite in modo tale che le persone materialmente e finanziariamente svantaggiate non siano soggette a ulteriori oneri. 

Chiediamo l'attuazione del principio "chi inquina paga": chi causa e beneficia delle emissioni di gas serra e dell'inquinamento deve essere ritenuto responsabile. Essi devono prevenire danni o riparare danni già verificatisi. 

Tutti i punti sopra citati valgono in particolare per tutte le generazioni e a livello globale. Le generazioni successive non dovrebbero essere influenzate negativamente dalle azioni delle generazioni precedenti. La disuguaglianza non deve essere aumentata, ma ridotta.
`}
        ],
        icon: [], // [{language: ELanguages.all, text: "static/images/climate-justice.svg"}]
    }]
};

export const movementDemandsFinance: IStrapiContent<IDemandItemProps> = {
    identifier: "movement-demands-finance",
    sortId: 5,
    data: [{
        __component: "component.demand-item",
        title: [
            {language: ELanguages.de, text: "Finanzplatzforderungen"},
            {language: ELanguages.fr, text: "Demandes concernant les centres financiers"},
            {language: ELanguages.it, text: "Richieste concernenti i centri finanziari"}
        ],
        lead: [
            {language: ELanguages.de, text: "Wir fordern, dass Banken, Pensionskassen und Versicherungen nicht mehr in fossile Energien investieren."},
            {language: ELanguages.fr, text: "Nous exigeons que les banques, les fonds de pension et les compagnies d'assurance cessent d'investir dans les combustibles fossiles."},
            {language: ELanguages.it, text: "Chiediamo che le banche, i fondi pensione e le compagnie di assicurazione smettano di investire nei combustibili fossili."}
        ],
        text: [
            {
                language: ELanguages.de, text: `Laut der Klima - Allianz Schweiz ist die grösste Gefahr fürs Klima unser Geld. Denn die Schweiz fördert zwar weder Öl, noch verbrennt sie Kohle, investiert und finanziert aber mit den über 7’000 Milliarden hierzulande verwalteten Schweizer Franken [[1]](https://www.swissbanking.org/finanzplatz-in-zahlen/) in umweltschädlichen Projekte. Schon allein durch die Investitionen durch Schweizer Banken, Versicherungen und Pensionskassen werden mehr als 20-mal so viele Treibhausgasemissionen wie die ganze Schweiz als Land ausgestossen [[2]](https://www.bafu.admin.ch/dam/bafu/de/dokumente/klima/externe-studien-berichte/kohlenstoffrisikenfuerdenfinanzplatzschweiz.pdf.download.pdf/kohlenstoffrisikenfuerdenfinanzplatzschweiz.pdf).


1. Wir fordern von Bundesrat und Parlament klare Vorschriften und Standards bezüglich nachhaltiger Investitionen und Finanzierungen <sup>*</sup> für Schweizer Finanzinstitute (Banken, Schweizerische Nationalbank, Pensionskassen, Versicherungen, unabhängige Vermögensverwaltungen und Stiftungen).
    1. Bis zur Frühlingssession 2020 einen umfassenden Plan des Bundes.
2. Wir fordern transparente Veröffentlichungen der Finanzflüsse (Finanzierungen, Investitionen und Versicherungsdienstleistungen) der Schweizer Finanzinstitute ab Jahr 2020.
3. Wir fordern eine Reduktion der direkten und indirekten Treibhausgasemissionen des Schweizer Finanzplatzes auf Netto 0 bis 2030 insbesondere der Stopp von Finanzierungen, Investitionen und Versicherungsdienstleistungen fossiler Energien.
    1. Ab sofort keine neuen Investitionen, Kredite und Versicherungsdienstleistungen von Projekten und Unternehmen, die in fossile Energien aktiv sind! Das betrifft unter anderem Kohleunternehmen, die Teersandbranche, Erdgas und Öl.
    2. Die Finanzinstitute sollen bis Ende 2020 klare Pläne mit konkreten Zielen und Massnahmen vorlegen, wie sie ihre Finanzflüsse (Kredite, Investitionen und Versicherungsdienstleistungen) bis 2030 auf Netto 0 bringen.

**Bemerkungen**

\\* Für uns bedeutet Nachhaltigkeit, dass nicht nur ökologische Aspekte angeschaut werden, sondern auch soziale und ethische. Profitmaximierung soll nie wichtiger als Leben sein. Dabei muss das gesamte Unternehmen nachhaltig sein, vereinzelte nachhaltige Projekte sind nicht ausreichend.
`},
            {
                language: ELanguages.fr, text: `Selon l'Alliance suisse pour le climat, la plus grande menace pour le climat est notre argent. Bien que la Suisse ne produise pas de pétrole et ne brûle pas de charbon, elle investit les 7 000 milliards de francs suisses [[1]](https://www.swissbanking.org/finanzplatz-in-zahlen/)  gérés dans ce pays en finançant des projets nuisibles à l'environnement. Les investissements des seules banques, compagnies d'assurance et caisses de pension suisses sont à l'origine de plus de 20 fois plus d'émissions de gaz à effet de serre que l'ensemble de la Suisse [[2]](https://www.bafu.admin.ch/dam/bafu/de/dokumente/klima/externe-studien-berichte/kohlenstoffrisikenfuerdenfinanzplatzschweiz.pdf.download.pdf/kohlenstoffrisikenfuerdenfinanzplatzschweiz.pdf). 


1. Nous demandons au Conseil fédéral et au Parlement d'établir des réglementations et des normes claires en matière d'investissement et de financement durables <sup>*</sup> pour les institutions financières suisses (banques, Banque nationale suisse, caisses de pension, compagnies d'assurance, gestionnaires de fortune indépendants et fondations).
    1. Un plan complet du conseil fédéral jusqu’à la session de printemps 2020.
2. Nous exigeons des publications transparentes des flux financiers (financement, investissements et services d'assurance) des institutions financières suisses à partir de 2020.
3. Nous demandons une réduction des émissions directes et indirectes de gaz à effet de serre de la place financière suisse à un niveau net 0 d'ici 2030, en particulier l'arrêt des services de financement, d'investissement et d'assurance des combustibles fossiles.
    1. La Suisse doit interdire de nouveaux investissements, de prêts et de services d'assurance en faveur des projets et des entreprises actives dans les énergies fossiles! Cela concerne, entre autres, les entreprises charbonnières, l'industrie des sables bitumineux, le gaz naturel et le pétrole.
    2. Les institutions financières devront présenter des plans clairs d'ici la fin de 2020, avec des objectifs et des mesures concrètes pour ramener leurs flux financiers (prêts, investissements et services d'assurance) à un niveau net neutre d'ici 2030.*

**Remarques**

\\* Pour nous, la durabilité ne signifie pas seulement prendre en compte les aspects écologiques, mais aussi les aspects sociaux et éthiques. La maximisation du profit ne devrait jamais être plus importante que la vie. En même temps, la société doit être durable dans son ensemble ; des projets durables isolés ne suffisent pas.
`},
            {
                language: ELanguages.it, text: `Secondo l'Alleanza svizzera per il clima, la più grande minaccia per il clima è il nostro denaro. Pur non producendo né petrolio né bruciando carbone, la Svizzera investe e finanzia progetti dannosi per l'ambiente con gli oltre 7’000 miliardi di franchi svizzeri [[1]](https://www.swissbanking.org/finanzplatz-in-zahlen/) gestiti in questo Paese. Gli investimenti delle banche, delle assicurazioni e delle casse pensioni svizzere da sole causano un numero di emissioni di gas serra 20 volte superiore a quello dell'intera Svizzera. [[2]](https://www.bafu.admin.ch/dam/bafu/de/dokumente/klima/externe-studien-berichte/kohlenstoffrisikenfuerdenfinanzplatzschweiz.pdf.download.pdf/kohlenstoffrisikenfuerdenfinanzplatzschweiz.pdf)


1. Chiediamo al Consiglio federale e al Parlamento di stabilire norme e standard chiari per investimenti e finanziamenti sostenibili <sup>*</sup> per gli istituti finanziari svizzeri (banche, Banca nazionale svizzera, casse pensioni, assicurazioni, gestori patrimoniali indipendenti e fondazioni).
    1. Un piano federale completo entro la sessione primaverile del 2020.
2. Richiediamo la pubblicazione trasparente dei flussi finanziari (finanziamenti, investimenti e servizi assicurativi) degli istituti finanziari svizzeri a partire dal 2020.
3. Chiediamo una riduzione delle emissioni dirette e indirette di gas serra della piazza finanziaria svizzera a 0 netto entro il 2030, in particolare la sospensione dei servizi di finanziamento, investimento e assicurazione dei combustibili fossili.
    1. D'ora in poi, niente nuovi investimenti, prestiti e servizi assicurativi in favore di progetti e aziende attive nel settore delle energie fossili! Ciò riguarda, tra l'altro, le aziende del carbone, l'industria delle sabbie bituminose, il gas naturale e il petrolio.
    2. Gli istituti finanziari dovrebbero presentare piani chiari entro la fine del 2020 con obiettivi e misure concrete per portare i loro flussi finanziari (prestiti, investimenti e servizi assicurativi) a zero netto entro il 2030.*

**Osservazioni**

\\* Per noi sostenibilità significa non solo considerare gli aspetti ecologici, ma anche quelli sociali ed etici. La massimizzazione del profitto non dovrebbe mai essere più importante della vita. Allo stesso tempo, l'intera azienda deve essere sostenibile; progetti sostenibili isolati non sono sufficienti.
`}],

        icon: [], // [{language: ELanguages.all, text: "static/images/finance-demands.svg"}]
    }]
};

export const movementDemandsSystemChange: IStrapiContent<IDemandItemProps> = {
    identifier: "movement-demands-system-change",
    sortId: 6,
    data: [{
        __component: "component.demand-item",
        title: [
            {language: ELanguages.de, text: "Systemwandelklausel"},
            {language: ELanguages.fr, text: "Clause de changement de système"},
            {language: ELanguages.it, text: "Clausola di modifica del sistema"}

        ],
        lead: [
            {language: ELanguages.de, text: "Falls unseren Forderungen im aktuellen System nicht nachgekommen werden kann, braucht es einen Systemwandel."},
            {language: ELanguages.fr, text: "Si nos revendications ne peuvent être satisfaites dans le système actuel, un changement de système est nécessaire."},
            {language: ELanguages.it, text: "Se le nostre richieste non potessero essere soddisfatte nel sistema attuale, è necessario un cambiamento di sistema."}
        ],
        text: [
            {
                language: ELanguages.de, text: `Alle Vorschläge, die einen Systemwandel fordern, gehen davon aus, dass einzelne Massnahmen und Veränderungen nicht ausreichen werden, um die Klimakrise abzuwenden. Stattdessen müsse das jetzige System nicht nur teilweise, sondern ganzheitlich verändert werden.
                
Das Eidgenössische Departement für Umwelt, Verkehr, Energie und Kommunikation (UVEK) beschreibt den Begriff «Systemwandel» auf Seite 182 des [«Umweltberichtes 2018»](https://www.bafu.admin.ch/bafu/de/home/dokumentation/umweltbericht/umweltbericht-2018.html) wie folgt:

«Notwendiger Systemwandel: Die Europäische Umweltagentur (EUA) kommt nach einer umfassenden Analyse von Umweltindikatoren und unter Berücksichtigung der Fortschritte bei den politischen Zielen zum Schluss, dass wahrscheinlich weder Umweltmassnahmen allein noch wirtschafts - oder technologiebedingte Effizienzverbesserungen ausreichend sein werden, um die Ziele der Europäischen Union (EU) für das Jahr 2050 zu verwirklichen. Stattdessen seien grundlegende Veränderungen der Produktions - und Verbrauchssysteme nötig (EUA 2015a).“
Sollten unsere Forderungen unter dem aktuellen Zusammenspiel der Verkehrs -, Energie -, Ernährungs -, Wirtschafts -, Finanz -, Bildungs -, Sozial -, Entscheidungsfindungs - und weiteren Systemen nicht umgesetzt werden können, muss sich dieses systemübergreifende Zusammenspiel ändern.
Wie ein Systemwandel oder ein mögliches alternatives System aussehen soll, wurde bewusst offen gelassen. Die Interpretation sollte in einem Prozess gefunden werden, an dem sich die gesamte Gesellschaft beteiligen kann. 
`},
            {
                language: ELanguages.fr, text: `Toutes les propositions appelant à un changement systémique supposent que les mesures et les changements individuels ne suffiront pas à éviter la crise climatique. Au lieu de cela, le système actuel doit être modifié non seulement en partie, mais dans son intégralité. 

Le Département fédéral de l'environnement, des transports, de l'énergie et de la communication (DETEC) décrit comme suit le terme "changement de système" à la page 182 du ["Rapport environnemental 2018"](https://www.bafu.admin.ch/bafu/de/home/dokumentation/umweltbericht/umweltbericht-2018.html):     

"Changement de système nécessaire: Après une analyse complète des indicateurs environnementaux et compte tenu des progrès réalisés en termes d'objectifs politiques, l'Agence européenne pour l'environnement (AEE) conclut que ni les mesures environnementales seules ni les progrès économique ou technologique ne seront probablement suffisants pour atteindre les objectifs de l'Union européenne (UE) pour l'année 2050. Au lieu de cela, des changements fondamentaux dans les systèmes de production et de consommation sont nécessaires (AEE 2015a).”

Si nos demandes ne peuvent être mises en œuvre dans le cadre de l'interaction actuelle des systèmes de transport, d'énergie, d'alimentation, ainsi qu’économique, financier, éducatif, social, décisionnel et autres, cette interaction intersystèmes doit changer. 

La question de savoir à quoi devrait ressembler un changement de système ou un éventuel système alternatif a été délibérément laissée ouverte. Son interprétation doit se trouver dans un processus auquel la société entière peut participer. 
`
            },
            {
                language: ELanguages.it, text: `Tutte le proposte che richiedono un cambiamento di sistema partono dal presupposto che le singole misure e i cambiamenti non saranno sufficienti a scongiurare la crisi climatica. Il sistema attuale deve invece essere modificato non solo in parte, ma nella sua interezza. 

Il Dipartimento federale dell'ambiente, dei trasporti, dell'energia e delle comunicazioni (DATEC) descrive il termine "cambiamento di sistema" a pagina 182 del ["Rapporto ambientale 2018"](https://www.bafu.admin.ch/bafu/de/home/dokumentation/umweltbericht/umweltbericht-2018.html) come segue:     

"Necessario cambiamento di sistema: dopo un'analisi completa degli indicatori ambientali e tenendo conto dei progressi compiuti in termini di obiettivi politici, l'Agenzia europea dell'ambiente (AEA) conclude che né le misure ambientali da sole né i miglioramenti di efficienza economica o tecnologica saranno probabilmente sufficienti per raggiungere gli obiettivi dell'Unione europea (UE) per l'anno 2050. Sono invece necessari cambiamenti fondamentali nei sistemi di produzione e di consumo (EEA 2015a).”


Se le nostre richieste non possono essere attuate nell'ambito dell'attuale interazione tra i sistemi di trasporto, energetico, alimentare, economico, finanziario, educativo, sociale, decisionale e di altro tipo, questa interazione intersistemica deve cambiare. 

Come dovrebbe essere un cambiamento di sistema o un possibile sistema alternativo è stato deliberatamente lasciato aperto. L'interpretazione dovrebbe essere trovata in un processo al quale l'intera società può partecipare. 
`
            }
        ],
        icon: [], // [{language: ELanguages.all, text: "static/images/system-change.svg"}]
    }]
};

export const movementCodex: IStrapiContent<IDemandItemProps> = {
    identifier: "movement-codex",
    sortId: 7,
    data: [{
        __component: "component.demand-item",
        title: [
            {language: ELanguages.de, text: "Aktionskodex Klimastreik"},
            {language: ELanguages.fr, text: "Code d'action"},
            {language: ELanguages.it, text: "Codice di azione"}

        ],
        lead: [
            {language: ELanguages.de, text: "Wir sind eine friedliche Bewegung und unsere Aktionen sind gewaltfrei."},
            {language: ELanguages.fr, text: "Nous sommes un mouvement pacifique et nos actions sont non violentes."},
            {language: ELanguages.it, text: "Siamo un movimento pacifico e le nostre azioni non sono violente."}
        ],
        text: [
            {
                language: ELanguages.de, text: `Alle Aktionen im Namen vom Klimastreik Schweiz richten sich nach diesem Kodex. Dieser Kodex dient als Sammlung von Richtlinien, die auf nationaler Ebene beschlossen wurden. Alles Weitere wird den Regionalen Gruppen überlassen, die eine grosse Autonomie haben sollen.
Wir sind eine friedliche Bewegung. Alle Aktionen sind non - violent. Oberste Priorität hat dabei, keinem Lebewesen zu schaden. Weiterhin sollen Sachbeschädigungen vermieden werden.

Alle Aktionen sollen unsere Forderungen unterstützen / weiterbringen.

Alle Aktionen sollen Vielfältigkeit und Offenheit vermitteln.

Alle Aktionen richten sich nach dem bisherigen nationalen Konsens, sich nur einseitig von Organisationen / Parteien unterstützen zu lassen. Wir wollen nicht, dass Organisationen / Parteien unsere Aktionen für ihre Zwecke nutzen.

#### One Side Support

Wir möchten eine unabhängige Bewegung sein kommunizieren unsere Forderungen in offenen Briefen und Petitionen und weiteren transparenten Wegen, nicht hinter verschlossenen Türen, damit die Forderungen von gewählten Institutionen, Politiker\*innen, Parteien und Unternehmen aufgegriffen werden können. Für ökologische Fragestellungen nehmen wir Hilfe von Expert\*innen in einem beratenden Prozessen an.
`
            },
            {
                language: ELanguages.fr, text: `Toutes les actions au nom de Grève du Climat Suisse sont régies par ce code. Ce code sert de recueil des lignes directrices qui ont été adoptées au niveau national. Tout le reste est laissé aux groupes régionaux, qui sont censés avoir une grande autonomie.

Nous sommes un mouvement pacifique. Toutes les actions sont non violentes. Notre première priorité est de ne pas nuire aux êtres vivants. En outre, les dommages aux biens doivent être évités.

Toutes les actions doivent soutenir nos demandes.

Toutes les actions doivent véhiculer la diversité et l'ouverture.

Toutes les actions sont basées sur le consensus national précédent et ne doivent être soutenues qu'unilatéralement par les organisations/partis. Nous ne voulons pas que les organisations/partis utilisent nos actions à leurs propres fins.

#### Un soutien unilatéral

Nous voulons être un mouvement indépendant nous communiquons nos demandes par des lettres ouvertes, des pétitions et d'autres moyens transparents, et non à huis clos, afin que les demandes puissent être prises en compte par les institutions élues, les politiciens et les politiciennes, les partis et les entreprises. Pour les questions écologiques, nous acceptons l'aide d'experts dans le cadre d'un processus de conseil.
`
            },
            {
                language: ELanguages.it, text: `Tutte le azioni a favore di Sciopero per il Clima Svizzera sono regolate da questo codice. Questo codice serve come una raccolta di linee guida che sono state adottate a livello nazionale. Tutto il resto è lasciato ai gruppi regionali, che dovrebbero avere una grande autonomia.

Siamo un movimento pacifico. Tutte le azioni sono non violente. La nostra prima priorità è di non fare del male a nessun essere vivente. Anche i danni alle cose dovrebbero essere evitati.

Tutte le azioni dovrebbero sostenere le nostre richieste.

Tutte le azioni devono trasmettere diversità e apertura.

Tutte le azioni si basano sulla precedente unanimità nazionale e possono essere sostenute solo unilateralmente dalle organizzazioni/partiti. Non vogliamo che le organizzazioni/partiti utilizzino le nostre azioni per i loro scopi.

#### Supporto unilaterale

Vogliamo essere un movimento indipendente e comunichiamo le nostre richieste tramite lettere aperte, petizioni e in altri modi trasparenti, non a porte chiuse, in modo che le richieste possano essere prese in considerazione da istituzioni elette, politici, partiti e aziende. Per le questioni ecologiche accettiamo l'aiuto di esperti in un processo di consultazione.
`
            }
        ],
        icon: [], // [{language: ELanguages.all, text: "static/images/codex.png"}]
    }]
};

// Join

export const joinThankYou: IStrapiContent<IIconTitleProps> = {
    identifier: "join-thank-you",
    sortId: 0,
    data: [{
        __component: "component.icon-title",
        title: [
            {language: ELanguages.de, text: "Danke"},
            {language: ELanguages.fr, text: "Merci"},
            {language: ELanguages.it, text: "Grazie"}

        ],
        text: [
            {language: ELanguages.de, text: "dass du dich für das Klima einsetzt!"},
            {language: ELanguages.fr, text: "de te battre pour le climat!"},
            {language: ELanguages.it, text: "di star combattendo per il clima!"}
        ],
        icon: [], // [{language: ELanguages.all, text: "static/images/heart.svg"}]
    }]
};

export const joinTalkAboutIt: IStrapiContent<IJoinItemProps> = {
    identifier: "join-talk-about-it",
    sortId: 1,
    data: [{
        __component: "component.join-item",
        title: [
            {language: ELanguages.de, text: "Rede mit Menschen in deinem Umfeld über die Klimakrise"},
            {language: ELanguages.fr, text: "Parlez de la crise climatique à votre entourage"},
            {language: ELanguages.it, text: "Parlate con le persone intorno a voi della crisi climatica"}

        ],
        text: [
            {language: ELanguages.de, text: "Die Klimakrise geht alle etwas an. Je mehr Menschen über die Klimakrise und ihre Auswirkungen Bescheid wissen, desto grösser die Wahrscheinlichkeit dass wir es schaffen diese zu bewältigen."},
            {language: ELanguages.fr, text: "La crise climatique concerne tout le monde. Plus les gens en savent sur la crise climatique et ses effets, plus nous aurons de chances de la gérer."},
            {language: ELanguages.it, text: "La crisi climatica riguarda tutti/e. Più la gente conosce la crisi climatica e i suoi effetti, maggiori sono le probabilità che riusciremo a gestirla."}
        ],
        icon: [], // [{language: ELanguages.all, text: "static/images/talk-about-it-icon.svg"}],
        image: [], // [{language: ELanguages.all, text: "static/images/talk-about-it.png"}],
        link: {link: [{language: ELanguages.all, text: "/facts"}], type: "facts"}
    }]
};

export const joinJoinUs: IStrapiContent<IJoinItemProps> = {
    identifier: "join-join-us",
    sortId: 2,
    data: [{
        __component: "component.join-item",
        title: [
            {language: ELanguages.de, text: "Sei mit dabei"},
            {language: ELanguages.fr, text: "Sois présent"},
            {language: ELanguages.it, text: "Essere lì"}
        ],
        text: [
            {language: ELanguages.de, text: "Komm an unsere Streiks und Demos! Alles sind willkommen, egal ob jung oder alt! Am besten nimmst du gleich deine ganze Familie und alle deine Freund\*innen mit."},
            {language: ELanguages.fr, text: "Viens assister à nos grèves et à nos manifestations! Toutes et tous sont les bienvenus, petit.e.s et grand.e.s! Emmène toute ta famille et tou.te.s tes ami.e.s avec toi."},
            {language: ELanguages.it, text: "Venite ai nostri scioperi e alle nostre manifestazioni! Tutti/e sono i benvenuti/e, giovani e meno giovani! Porta con te tutta la tua famiglia e tutti/e i/le tuoi amici/amiche."}
        ],
        icon: [], // [{language: ELanguages.all, text: "static/images/join-us-icon.svg"}],
        image: [], // [{language: ELanguages.all, text: "static/images/join-us.png"}],
        link: {link: [{language: ELanguages.all, text: "/events"}], type: "events"}
    }]
};

export const joinGetActive: IStrapiContent<IJoinItemProps> = {
    identifier: "join-get-active",
    sortId: 3,
    data: [{
        __component: "component.join-item",
        title: [
            {language: ELanguages.de, text: "Werde aktiv!"},
            {language: ELanguages.fr, text: "Sois actifs.ve!"},
            {language: ELanguages.it, text: "Attivati!"}
        ],
        text: [
            {language: ELanguages.de, text: "Helfe mit im Klimastreik und vernetze dich mit Menschen aus der Region. Das Klima braucht dich!"},
            {language: ELanguages.fr, text: "Aide à la lutte contre le changement climatique et crée un réseau avec les habitants de ta région. Le climat a besoin de vous!"},
            {language: ELanguages.it, text: "Aiuta lo Sciopero per il Clima e crea una rete con le persone della regione. Il clima ha bisogno di te!"}
        ],
        icon: [], // [{language: ELanguages.all, text: "static/images/get-active-icon.svg"}],
        image: [], // [{language: ELanguages.all, text: "static/images/get-active.png"}],
        link: {link: [{language: ELanguages.all, text: "/join#regio"}], type: "join"}
    }]
};

export const joinRegioTitle: IStrapiContent<ITitleProps> = {
    identifier: "join-regio",
    sortId: 5,
    grouping: "regio",
    data: [{
        __component: "component.title",
        title: [
            {language: ELanguages.de, text: "Regionalgruppen"},
            {language: ELanguages.fr, text: "Groupes régionaux"},
            {language: ELanguages.it, text: "Gruppi regionali"}
        ]
    }]
};

export const joinMap: IStrapiContent<IContentDelegation<any>> = {
    identifier: "join-map",
    sortId: 6,
    data: [{
        __component: "component.atlas",
        childIdentifier: "contacts",
        childType: "contact-data",
        config: topo
    }]
};

export const joinWiki: IStrapiContent<ILandingLinkProps> = {
    identifier: "join-wiki",
    sortId: 7,
    data: [{
        __component: "component.landing-link",
        title: [
            {language: ELanguages.de, text: "Klimastreik Wiki"},
            {language: ELanguages.fr, text: "Wiki sur la grève du climat"},
            {language: ELanguages.it, text: "Wiki su Sciopero per il Clima"}
        ],
        text: [
            {language: ELanguages.de, text: "Das Klimastreik Wiki wurde ins Leben gerufen um das schnell wachsende Wissen unserer dezentral organisierten Bewegung an einem Ort zu sammeln. Hier findest du alles Wissenswerte über die Bewegung sowie viele praktische Infos. Jede*r ist eingeladen seine Erfahrung und Wissen im Wiki mit anderen zu teilen."},
            {language: ELanguages.fr, text: "Le Wiki de la grève du climat a été créé pour rassembler en un seul endroit les connaissances en pleine expansion de notre mouvement organisé décentralisé. Tu trouveras ici tout ce qu'il faut savoir sur le mouvement ainsi que de nombreuses informations pratiques. Chacun est invité à partager son expérience et ses connaissances dans le wiki."},
            {language: ELanguages.it, text: "La Wiki su Sciopero per il Clima è stata creata per raccogliere in un unico luogo le informazioni in rapida espansione riguardo il nostro movimento, che è organizzato in modo decentralizzato. Qui troverete tutto ciò che c’è da sapere sul movimento, comprese molte informazioni pratiche. Tutti sono invitati a condividere la propria esperienza e le proprie conoscenze sulla pagina wiki."},
        ],
        icon: [], // [{language: ELanguages.all, text: "static/images/codex.png"}],
        link: {
            link: [
                {language: ELanguages.de, text: "https://de.climatestrike.ch"},
                {language: ELanguages.fr, text: "https://fr.climatestrike.ch"},
                {language: ELanguages.it, text: "https://it.climatestrike.ch"}
            ],
            text: [
                {language: ELanguages.de, text: "Zum Klimastreik Wiki"},
                {language: ELanguages.fr, text: "Au Wiki de la grève du climat"},
                {language: ELanguages.it, text: "Al Wiki di Sciopero per il Clima"}
            ]
        }
    }]
};

// Events

export const eventsDummy: IStrapiContent<IContentDelegation<IEventsSettings>> = {
    identifier: "events",
    sortId: 0,
    data: [{
        __component: "component.events",
        childIdentifier: "events",
        childType: "events",
        config: {
            views: [EEventView.timeline],
            filters: [
                {type: EEventFilterType.zip},
                {type: EEventFilterType.category, defaults: [EEventCategory.digital]}]
        }
    }]
};

// News

export const postsDummy: IStrapiContent<IContentDelegation> = {
    identifier: "posts",
    sortId: 0,
    data: [{
        __component: "component.posts",
        childIdentifier: "posts",
        childType: "posts"
    }]
};

// Donation

export const donationText: IStrapiContent<ITextProps> = {
    identifier: "donation-text",
    grouping: "donation-section",
    sortId: 0,
    data: [{
        __component: "component.text",
        title: [
            {language: ELanguages.de, text: "Spenden"},
            {language: ELanguages.fr, text: "Dons"},
            {language: ELanguages.it, text: "Donare"}
        ],
        text: [
            {
                language: ELanguages.de, text: `Die Welt retten kostet viel. Viel Zeit, viel Energie und leider auch viel Geld.
Deshalb sind wir auf Spenden angewiesen, um Dinge, wie beispielsweise Flyer, Plakate, Bewilligungen für die Demos oder diese Website zu finanzieren. 
Viele von uns sind Schüler\*innen oder Studierende, deshalb ist unser finanzieller Spielraum beschränkt. Für diese Bewegung setzen viele Menschen unglaublich viel von ihrer Freizeit ein, darum freuen wir uns über jede Spende!

**Bank:** Alternativen Bank Schweiz

**IBAN:** CH28 0839 0036 0277 1000 2

**Adresse:** Climatestrike Switzerland Finance, Aarbergergasse 61, 3001 Bern
                `},
            {
                language: ELanguages.fr, text: `Sauver le monde coûte très cher:  beaucoup de temps, beaucoup d'énergie et malheureusement aussi beaucoup d'argent.
C'est pourquoi nous dépendons des dons pour financer des choses comme les prospectus, les affiches, les permis pour les démonstrations ou ce site web. 
Beaucoup d'entre nous sont des élèves* ou des étudiant.e.s, notre marge de manœuvre financière est donc limitée. Pour ce mouvement, de nombreuses personnes investissent une quantité incroyable de leur temps libre, c'est pourquoi nous sommes heureux de chaque don!

**Banque:** Alternativen Bank Schweiz

**IBAN:** CH28 0839 0036 0277 1000 2

**Adresse:** Climatestrike Switzerland Finance, Aarbergergasse 61, 3001 Bern
                `},
            {
                language: ELanguages.it, text: `Salvare il mondo costa molto. Molto tempo, molta energia e purtroppo anche molto denaro.
Pertanto dipendiamo dalle donazioni per finanziare cose come volantini, poster, permessi per le manifestazioni o questo sito web. 
Molti di noi sono alunni/e o studenti/esse, quindi la nostra portata finanziaria è limitata. Per questo movimento molte persone usano una quantità incredibile del loro tempo libero, quindi siamo felici di ogni donazione!

**Banca:** Alternativen Bank Schweiz

**IBAN:** CH28 0839 0036 0277 1000 2

**Indirizzo:** Climatestrike Switzerland Finance, Aarbergergasse 61, 3001 Bern
                `}
        ],
    }]
};

export const donationIframe: IStrapiContent<IIframeProps> = {
    identifier: "donation-iframe",
    grouping: "donation-section",
    sortId: 0,
    data: [{
        __component: "component.iframe",
        src: [{language: ELanguages.all, text: "https://donorbox.org/embed/climatestrike-ch"}]
    }]
};

// Posts

export const postsSection: IStrapiContent<IContentDelegation<IPostSectionConfig>> = {
    identifier: "post-section",
    sortId: 0,
    grouping: "post-section",
    data: [{
        __component: "component.post-section",
        childIdentifier: "posts",
        childType: "card",
        config: {title: "posts", n: 0}
    }]
};

// Media

export const mediaNational: IStrapiContent<ITextProps> = {
    identifier: "media-national",
    sortId: 0,
    data: [{
        __component: "component.text",
        title: [
            {language: ELanguages.de, text: "Nationale Medien"},
            {language: ELanguages.fr, text: "Media national"},
            {language: ELanguages.it, text: "Medie nationali"}
        ],
        text: [
            {
                language: ELanguages.all, text: `**Deutsch:** [medien@climatestrike.ch](mailto:medien@climatestrike.ch)

**Français:** [medias@climatestrike.ch](mailto:medias@climatestrike.ch)

**Telefon:** 052 770 12 24
    `}]
    }]
};

export const mediaRegional: IStrapiContent<ITextProps> = {
    identifier: "media-regional",
    sortId: 1,
    data: [{
        __component: "component.text",
        title: [
            {language: ELanguages.de, text: "Regionale Kontakte"},
            {language: ELanguages.fr, text: "Contacts régionaux"},
            {language: ELanguages.it, text: "Contatti regionali"}

        ],
        text: [
            {
                language: ELanguages.all, text: `** Aargau:** [aargau@climatestrike.ch](mailto:aargau@climatestrike.ch)** 
                
**Basel**: [basel@climatestrike.ch](mailto:basel@climatestrike.ch)

**Bern:** [bern@climatestrike.ch](mailto:bern@climatestrike.ch) / [medien-bern@climatestrike.ch](mailto:medien-bern@climatestrike.ch)

**Biel:** [biel - bienne@climatestrike.ch](mailto:biel-bienne@climatestrike.ch)

**Fribourg:** [fribourg@climatestrike.ch](mailto:fribourg@climatestrike.ch)

**Genève:** [climatestrike.ge@gmail.com](mailto:climatestrike.ge@gmail.com)

**Glarus:** [glarus@climatestrike.ch](mailto:glarus@climatestrike.ch)

**Jura:** [jura@climatestrike.ch](mailto:jura@climatestrike.ch)

**Köniz:** [koeniz@climatstrike.ch](mailto:koeniz@climatstrike.ch)

**Neuchâtel:** [neuchatel@climatestrike.ch](mailto:neuchatel@climatestrike.ch)

**Olten:** [olten@climatestrike.ch](mailto:olten@climatestrike.ch)

**Schaffhausen:** [schaffhausen@climatestrike.ch](mailto:schaffhausen@climatestrike.ch)

**St. Gallen:** [ostschweiz@climatestrike.ch](mailto:ostschweiz@climatestrike.ch)

**Thun:** [thun@climatestrike.ch](mailto:thun@climatestrike.ch)

**Thurgau:** [thurgau@climatestrike.ch](mailto:thurgau@climatestrike.ch)

**Ticino:** [ticino@climatestrike.ch](mailto:ticino@climatestrike.ch)

**Vailais:** [valaiswallis@climatestrike.ch](mailto:valaiswallis@climatestrike.ch)

**Vaud:** [vaud@climatestrike.ch](mailto:vaud@climatestrike.ch)

**Winterthur:** [winterthur@climatestrike.ch](mailto:winterthur@climatestrike.ch)

**Zentralschweiz:** [zentralschweiz@climatestrike.ch](mailto:zentralschweiz@climatestrike.ch)

**Zürich:** [zuerich@climatestrike.ch](mailto:zuerich@climatestrike.ch)
`}],
    }]
};

// Privacy

export const privacyText: IStrapiContent<ITextProps> = {
    identifier: "privacy-text",
    sortId: 0,
    data: [
        {
            __component: "component.text",
            title: [
                {language: ELanguages.de, text: "Datenschutz"},
                {language: ELanguages.fr, text: "Protection des données"},
                {language: ELanguages.it, text: "Informatione sulla Privacy"}
            ],
            text: [
                {
                    language: ELanguages.de, text: `Der verantwortungsvolle Umgang mit personenbezogenen Daten ist Climatestrike Switzerland ein grosses Anliegen. Climatestrike Switzerland hält sich an das Bundesgesetz über den Datenschutz und informiert in dieser Datenschutzrichtlinie – publiziert auf der Website unter „Datenschutz“ – transparent und wahrheitsgemäss über die Sammlung und Nutzung personenbezogener Daten. Änderungen werden hier umgehend kommuniziert.

### Allgemein: Personenbezogene Daten

Damit unsere Angebote zielgerichtet und relevant sind sowie effizient genutzt werden können, ist es erforderlich, bestimmte Daten zu erheben. Personen, die sich für Climatestrike Switzerland interessieren, Informationen oder Produkte bestellen, einen Newsletter abonnieren, sich an einer Petition, Aktion oder Kampagne beteiligen oder Climatestrike Switzerland finanziell unterstützen, übergeben uns personenbezogene Daten. Diese werden mit dem bestehenden Adressstamm abgeglichen und elektronisch gespeichert. Sie werden ausschliesslich für die Zwecke von Climatestrike Switzerland verwendet. Zur Kontaktpflege kann es erforderlich sein, Auszüge dieser Daten an einen von uns beauftragten Dienstleister (z.B.eine Druckerei) weiterzugeben. Diese unterstehen dem Datenschutzgesetz und dürfen gemäss Vereinbarung die Daten weder anderweitig nutzen noch weitergeben. Davon abgesehen werden von Climatestrike Switzerland keine personenbezogenen Daten an Dritte gegeben oder vermarktet.

### Postversand und Telefonanrufe

Wer uns eine Postadresse mitteilt, kann in regelmässigen Abständen per Post über aktuelle Themen informiert werden, ausser dies wird ausdrücklich nicht erwünscht. Die Angabe der Telefonnummer dient Climatestrike Switzerland für allfällige Rückfragen oder Telefonaktionen. Die Altersangabe kann im Zusammenhang mit altersspezifischen Angeboten verwendet werden.

### Elektronischer Versand: E-Mails und Newsletter

Wer uns eine E-Mail-Adresse mitteilt, kann Informationen (wie z.B. Zahlungsbestätigungen, Veranstaltungshinweise) per E-Mail erhalten. Der regelmässige Versand von Informationen, insbesondere des Newsletters, erfolgt jedoch nur mit einer zusätzlichen Einwilligung der interessierten Person (Opt-In). Wer unseren Newsletter nicht mehr erhalten möchte, kann sich einfach und schnell über den Abmeldelink abmelden, der in jedem Newsletter enthalten ist. Climatestrike Switzerland benutzt für den Versand von E-Mail-Newslettern das Produkt ActiveCampaign des amerikanischen Anbieters ActiveCampaign Inc., das Auswertungen zur Optimierung unserer Kommunikation ermöglicht. Die Nutzung unseres Newsletters (Öffnungen, Klicks auf Links) wird gespeichert und mit persönlichen Daten verknüpft. Dies ermöglicht uns, unsere Angebote zu verbessern.

### Zahlungen

Climatestrike Switzerland behandelt Zahlungsdaten jeglicher Art äusserst vertraulich. Wir nehmen keine Spenden oder andere Mittel von Privatpersonen, Firmen oder Institutionen an, welche – soweit dies ersichtlich ist – unsere Unabhängigkeit oder ethische Integrität beeinträchtigen könnten. Die Climatestrike Switzerland anvertrauten Kreditkartendaten werden verschlüsselt an einen externen, von der Kreditkartenindustrie (PCI DSS) zertifizierten Partner übermittelt. Sie werden nicht durch Climatestrike Switzerland selbst gespeichert oder ausgewertet.

### Website

Beim Besuch einer von Climatestrike Switzerland betriebenen Website registriert der Webserver standardmässig unpersönliche Nutzungsdaten. Diese Daten geben Auskunft über aufgerufene Seiten und Dateien, IP-Adresse des anfordernden Computers, Zeitpunkt und Dauer des Besuchs, Betriebssystem und Browsertyp. Besuche der Website können mit personenbezogenen Daten verknüpft werden, falls Climatestrike Switzerland solche mitgeteilt wurden. Dies erlaubt es uns, auf relevante und zielgerichtete Informationen hinzuweisen. Für die Erfassung und Auswertung nutzt Climatestrike Switzerland das Webanalyse-Tool Piwik.

### Cookies

Auf den von Climatestrike Switzerland betriebenen Websites werden Cookies eingesetzt. Dies sind kleine Textdateien, die beim Aufruf auf den benutzten Geräten gespeichert werden, um unpersönliche Nutzungsdaten auszuwerten oder wiederkehrende Besuche zu erkennen (z.B. Sprachauswahl, Warenkorb-Funktion im Online-Shop). Climatestrike Switzerland verwendet sowohl Session-Cookies, die nach Ablauf der Sitzung verfallen, als auch permanente Cookies, die erst nach einer bestimmten Zeitdauer oder bei einer manuellen Löschung verfallen. In den Browser-Einstellungen können Cookies jederzeit gelöscht und deaktiviert werden.

### Externe Links und Funktionen von Drittanbietern

Climatestrike Switzerland hat weder einen Einfluss auf externe Inhalte, auf die wir verweisen, noch auf die Einhaltung von Datenschutzbestimmungen durch Drittanbieter, deren Funktionen auf unseren Websites eingebunden sind. Diesbezüglich möchten wir insbesondere auf die Datenschutzrichtlinien folgender Drittanbieter bzw.ihrer Tochtergesellschaften hinweisen: Facebook Inc.und Instagram Inc., Twitter Inc., ActiveCampaign Inc., RaiseNow, InnoCraft Ltd., Policat.

### Recht auf Auskunft

Wir geben Ihnen gerne schriftlich Auskunft über Ihre gespeicherten Angaben und berichtigen bzw.löschen diese auf Wunsch. Wenden Sie sich dazu per E-Mail an website[at]climatestrike.ch.

### Einverständniserklärung

Durch die Nutzung der zuvor genannten Angebote von Climatestrike Switzerland erklären Sie sich mit der Bearbeitung der über Sie erhobenen Daten in der beschriebenen Art und Weise und zu den benannten Zwecken einverstanden. Für weitere Fragen und Auskünfte zur Datenschutzrichtlinie steht Climatestrike Switzerland gerne zur Verfügung: Climatestrike Switzerland, Hinterdorfstrasse 12, 8635 Dürnten`
                },
                {
                    language: ELanguages.fr, text: `Ce contenu n'est pas disponible en français. Si vous le pouvez, consultez cette page en allemand.`
                },
                {
                    language: ELanguages.it, text: `Sfortunatamente questo contenuto non è disponibile in italiano. Per favore vedi la pagina in tedesco.`
                }]
        }
    ]
};

// Special pages

export const coronaTitle: IStrapiContent<ITextProps> = {
    identifier: "corona-title",
    sortId: 0,
    data: [{
        __component: "component.text",
        title: [
            {language: ELanguages.de, text: "Gesundheit geht vor"},
            {language: ELanguages.fr, text: "La santé d'abord"},
            {language: ELanguages.it, text: "La salute prima di tutto"}
        ],
        text: [
            {language: ELanguages.de, text: "Kontakt mit Menschen aus Risikogruppen ist möglichst zu vermeiden. Wir wollen praktische Hilfe leisten. Wir unterstützen das Umfeld der Betroffenen, übernehmen die Aufgaben von Menschen aus Risikogruppen. Heroische Inszenierungen sind fehl am Platz. Wir leisten keine medizinische Hilfe. Informiere dich in jedem Fall über die Risiken bevor du handelst."},
            {language: ELanguages.fr, text: "Il convient d'éviter au maximum tout contact avec des personnes appartenant à des groupes à risque. Nous voulons apporter une aide pratique. Nous soutenons l'environnement des personnes touchées et prenons en charge les tâches des personnes appartenant à des groupes à risque. Les mises en scène héroïques ne sont pas à leur place. Nous ne fournissons pas d'aide médicale. Dans tous les cas, informez-vous sur les risques avant d'agir."},
            {language: ELanguages.it, text: "Il contatto con persone appartenenti a gruppi a rischio va evitato per quanto possibile. Vogliamo fornire un aiuto pratico. Sosteniamo le condizioni delle persone colpite e ci assumiamo i compiti delle persone appartenenti ai gruppi a rischio. Le messe in scena eroiche sono fuori luogo. Non forniamo assistenza medica. In ogni caso, informatevi sui rischi prima di agire."}
        ],
    }]
};

export const coronaLanding1: IStrapiContent<ILandingLinkProps> = {
    identifier: "corona-landing-link-1",
    sortId: 1,
    grouping: "corona-landing-links",
    data: [{
        __component: "component.landing-link",
        title: [
            {language: ELanguages.de, text: "Weisungen"},
            {language: ELanguages.fr, text: "Directives"},
            {language: ELanguages.it, text: "Direttive"}

        ],
        text: [
            {language: ELanguages.de, text: "Befolge die Weisungen des Bundesamts für Gesundheit"},
            {language: ELanguages.fr, text: "Obéis aux directives de l'Office fédéral de la santé publique"},
            {language: ELanguages.it, text: "Segui le direttive dell'Ufficio federale della sanità pubblica."}
        ],
        icon: [], // [],
        link: {
            type: "read-more", link: [
                {language: ELanguages.de, text: "https://www.bag.admin.ch/bag/de/home.html"},
                {language: ELanguages.fr, text: "https://www.bag.admin.ch/bag/fr/home.html"},
                {language: ELanguages.it, text: "https://www.bag.admin.ch/bag/it/home.html"},
            ]
        }
    }]
};

export const coronaLanding2: IStrapiContent<ILandingLinkProps> = {
    identifier: "corona-landing-link-2",
    sortId: 2,
    grouping: "corona-landing-links",
    data: [{
        __component: "component.landing-link",
        title: [
            {language: ELanguages.de, text: "Was gibt es zu beachten?"},
            {language: ELanguages.fr, text: "Qu'y a-t-il à prendre en compte?"},
            {language: ELanguages.it, text: "Cosa c'è da considerare?"}

        ],
        text: [
            {language: ELanguages.de, text: "Hilf-jetzt hat ein paar Tipps zusammengestellt"},
            {language: ELanguages.fr, text: "Aide-maintenant a rassemblé quelques conseils"},
            {language: ELanguages.it, text: "Aide-maintenant/Hilf-jetzt ha raccolto alcuni suggerimenti (in francese o tedesco)."}
        ],
        icon: [], // [],
        link: {
            type: "read-more", link: [
                {language: ELanguages.de, text: "https://www.hilf-jetzt.ch/blog/an-alle-helfer-innen-was-du-beachten-solltest?categoryId=51284"},
                {language: ELanguages.fr, text: "https://www.aide-maintenant.ch/blog/aux-personnes-qui-aident-ce-a-quoi-vous-devez-faire-attention"},
                {language: ELanguages.it, text: "https://www.aide-maintenant.ch/blog/aux-personnes-qui-aident-ce-a-quoi-vous-devez-faire-attention"}
            ]
        }
    }]
};

export const coronaIframe: IStrapiContent<IIframeProps> = {
    identifier: "corona-iframe",
    sortId: 3,
    data: [{
        __component: "component.iframe",
        src: [{language: ELanguages.all, text: "https://www.youtube-nocookie.com/embed/CSqP-GHsjHI"}]
    }]
};

export const coronaSolidarity: IStrapiContent<any> = {
    identifier: "corona-solidarity",
    sortId: 4,
    data: [{
        __component: "component.text",
        text: [
            {
                language: ELanguages.de, text: `## Corona Solidaritätsnetzwerk
** Die App ‘Five Up’ verbindet Hilfesuchende und Freiwillige. Aufgaben können einfach erfasst werden. Freiwillige können Aufgaben in ihrer Umgebung suchen und annehmen. Die Aufgaben werden also ein einem Ort gesammelt und genau dort verteilt, wo Menschen sie übernehmen können.**

### Der Gruppe beitreten
1. Lade dir die App [‘Five Up‘](https://www.fiveup.org/) auf dein Smartphone. Du findest sie im [App Store](https://apps.apple.com/ch/app/five-up-connect-your-help/id1456339918) oder im [Play Store](https://play.google.com/store/apps/details?id=org.fiveup.fiveup).
2. Registriere dich. Du erhältst eine Mail mit einem Link zugesandt. Öffne diesen auf dem Smartphone.
3. Tritt der Gruppe ‘Corona Solidaritätsnetzwerk’ bei. Entweder unter ‘Gruppen / + / Öffentliche Gruppe suchen’ oder indem du diesen [Link öffnest](https://fiveup.org/group/23e23eed-47e8-4c34-ae76-7d901f60e683).

### Hilfe beanspruchen
1. Tritt der Gruppe wie oben beschrieben bei.
2. Unter ‘Verwalten / + ‘ die Aufgabe erfassen. Trage die nötigen Angaben ein. Setze ein Häckchen bei #WirHelfen.
3. Du wirst benachrichtigt, wenn sich Menschen für die Aufgabe interessieren.
4. Akzeptiere ihre Teilnahme und nimm mit ihnen Kontakt auf. Ihre Mailadressen und Telefonnummern werden für dich sichtbar.

[![Hilfe beanspruchen - Erklärungsvideo](http://img.youtube.com/vi/ld6zyMyD7gw/0.jpg)](http://www.youtube.com/watch?v=ld6zyMyD7gw "Corona Solidaritätsnetzwerk - Hilfe beanspruchen")

### Helfen
1. Tritt der Gruppe wie oben beschrieben bei.
2. Unter ‘Suche / Meine Gruppen’ findest du offene Aufgaben.
3. Mit dem Button unten rechts (dem Trichter-Symbol) kannst du den Radius der Suche ändern. So werden dir nur Aufgaben aus deiner Umgebung angezeigt.
4. Klicke auf die Aufgabe und melde dein Interesse an.
5. Die Person wird sich bei dir melden.


### Zusätzliche Informationen erhalten
1. Lade dir die App Telegram herunter. ([App Store](https://apps.apple.com/ch/app/telegram-messenger/id686449807), [Play Store](https://play.google.com/store/apps/details?id=org.telegram.messenger&hl=de_CH), [F-Droid](https://f-droid.org/en/packages/org.telegram.messenger/))
2. Tritt dem Infokanal bei. Öffne dazu den folgenden Link: [https://t.me/corona_soli](https://t.me/corona_soli).

### Links
* [https://www.hilf-jetzt.ch/](https://www.hilf-jetzt.ch/)
* [Übersicht existierender Gruppen](https://do.knack.com/hilfjetzt)

#### Telegram, WhatsApp oder Facebook Gruppe erstellen
Auf [hilf-jetzt.ch findest du jeweils eine Anleitung](https://www.hilf-jetzt.ch/). In einer Übersicht kannst du deine Gruppen eintragen.

#### Offline informieren
Nicht alle informieren sich via Internet. [Hier](https://www.hilf-jetzt.ch/blog/quartier-brief-zum-ausdrucken-und-aufhangen?categoryId=51284) findest du ein Dokument, dass du ausdrucken und aufhängen kannst.

## Weshalb ausgerechnet der Klimastreik?
Der Klimastreik existiert, weil sich viele um die Klimakrise besorgte Menschen zusammengeschlossen haben. Doch unsere Sorge gilt nicht “dem Klima”, sondern den von der Klimakrise betroffenen Lebewesen. Bei der Corona-Krise ist ebenfalls die Gesundheit und das Leben unzähliger Menschen akut bedroht. Darum ist für uns vom Klimastreik sofort klar, dass wir helfen müssen.

Wir wollen all jenen Eltern helfen, welche dringend jemanden brauchen, die*der auf ihr Kind aufpasst. Wir wollen älteren Menschen bei ihrem Einkauf helfen, da sie besonders gefährdet sind. Wir wollen jenen helfen, welche mit jemandem sprechen möchten, da sie seit Tagen zu Hause sein müssen.

Wir brauchen in Zeiten der Corona- und Klimakrise einen starken Zusammenhalt unter den Menschen vor Ort. Wir brauchen starke Strukturen, welche füreinander sorgen. Wir sehen unseren Beitrag zum Wohle der Menschen als gelebte Klimagerechtigkeit.
`
            },
            {
                language: ELanguages.fr, text: `## Réseau de solidarité Corona
** L'application "Five Up" met en relation les personnes qui cherchent de l'aide et les bénévoles. Les tâches peuvent être facilement saisies. Les volontaires peuvent rechercher et accepter des tâches dans leur région. Les tâches sont donc rassemblées en un seul endroit et réparties exactement là où les gens peuvent les assumer.**

### Rejoindre le groupe
1. télécharge l'application ["Five Up"] (https://www.fiveup.org/) sur ton smartphone. Vous pouvez le trouver dans l'[App Store](https://apps.apple.com/ch/app/five-up-connect-your-help/id1456339918) ou le [Play Store](https://play.google.com/store/apps/details?id=org.fiveup.fiveup).
2. Inscris-toi. Tu recevras un courriel avec un lien. Ouvre-le sur votre Smartphone.
3. rejoins le groupe "Réseau de solidarité Corona Soit sous "Groupes / + / Recherche groupe public" ou en cliquant sur ce [lien ouvert] (https://fiveup.org/group/23e23eed-47e8-4c34-ae76-7d901f60e683).

### Obtenir de l'aide
1. rejoindre le groupe comme décrit ci-dessus
2. entrer la tâche sous "Gérer / +". Saisir les informations nécessaires. Cocher la case #WeHelp.
3. Tu seras informé.e lorsque des personnes seront intéressées par la tâche.
4. accepter leur participation et les contacter. Leurs adresses électroniques et leurs numéros de téléphone te seront visibles.

[ ! [Réclamer de l'aide - vidéo d'explication](http://img.youtube.com/vi/ld6zyMyD7gw/0.jpg)](http://www.youtube.com/watch?v=ld6zyMyD7gw "Corona Solidarity Network - Réclamer de l'aide")

### Aider
1. rejoindre le groupe comme décrit ci-dessus
2. sous "Recherche / Mes groupes", tu trouveras les tâches ouvertes.
3. Tu peux modifier le rayon de la recherche avec le bouton en bas à droite (le symbole de l'entonnoir). Seules les tâches de votre environnement seront affichées.
4. cliquer sur la tâche et enregistrer ton intérêt.
5. la personne te contactera.


### Obtenir des informations complémentaires
1. télécharger le télégramme de l'application ([App Store](https://apps.apple.com/ch/app/telegram-messenger/id686449807), [Play Store](https://play.google.com/store/apps/details?id=org.telegram.messenger&hl=de_CH), [F-Droid](https://f-droid.org/en/packages/org.telegram.messenger/)
2. rejoindre le canal d'information. Ouvrir le lien suivant : [https://t.me/corona_soli](https://t.me/corona_soli).

### Liens
* [https://www.hilf-jetzt.ch/](https://www.hilf-jetzt.ch/)
* [Aperçu des groupes existants](https://do.knack.com/hilfjetzt)

#### Créer un télégramme, un groupe WhatsApp ou Facebook
Sur le site [hilf-jetzt.ch] (https://www.hilf-jetzt.ch/), tu trouveras des instructions pour chacun d'entre eux. Dans un aperçu, tu peux entrer tes groupes.

#### Informer hors ligne
Tout le monde ne reçoit pas d'informations via Internet. Ici](https://www.hilf-jetzt.ch/blog/quartier-brief-zum-ausdrucken-und-aufhangen?categoryId=51284), tu trouveras un document à imprimer et accrocher.

## Pourquoi la grève du climat ?
La grève du climat existe parce que de nombreuses personnes préoccupées par la crise climatique ont uni leurs forces. Mais notre préoccupation n'est pas "le climat", mais les êtres vivants touchés par la crise climatique. La crise du Corona constitue également une menace aiguë pour la santé et la vie d'innombrables personnes. C'est pourquoi il nous semble évident d’apporter notre aide.

Nous voulons aider tous les parents qui ont un besoin urgent de quelqu'un pour s'occuper de leur enfant. Nous voulons aider les personnes âgées à faire leurs courses, car elles sont particulièrement menacées. Nous voulons aider ceux qui veulent parler à quelqu'un, parce qu'ils doivent rester chez eux depuis des jours.

En période de crise du corona et du climat, nous avons besoin d'une forte cohésion entre les populations locales. Nous avons besoin de structures solides qui prennent soin les unes des autres. Nous considérons que notre contribution au bien-être de la population est une justice climatique vécue.
`},
            {
                language: ELanguages.it, text: `## Rete di solidarietà Corona
** L'app 'Five Up' mette in contatto le persone in cerca di aiuto con i volontari. I compiti possono essere facilmente inseriti. I volontari possono cercare e accettare compiti nella loro regione. Così, i compiti sono raccolti in un unico luogo e distribuiti esattamente dove le persone possono prendersene carico.**

### Unirsi al gruppo
1. Scarica l'app ['Five Up'](https://www.fiveup.org/) sul tuo smartphone. Puoi trovarlo nell'[App Store](https://apps.apple.com/ch/app/five-up-connect-your-help/id1456339918) o nel [Play Store](https://play.google.com/store/apps/details?id=org.fiveup.fiveup).
2. Registrati. Riceverai un'e-mail con un link. Aprilo sul tuo Smartphone.
3. Aderisci al gruppo "Rete di solidarietà Corona Sotto 'Gruppi / + / Cerca gruppo pubblico' o cliccando su questo link [apri il link](https://fiveup.org/group/23e23eed-47e8-4c34-ae76-7d901f60e683).

### Chiedere aiuto
1. Entra a far parte del gruppo come spiegato sopra.
2. Inserisci il compito di cui hai bisogno sotto la voce "Gestire / +". Inserisci le informazioni necessarie. Metti un segno di spunta accanto a #WeHelp.
3. Verrai informato/a quando qualcuno si dichiarerà interessato a svolgere il compito.
4. Accetta la loro partecipazione e contattali. I loro indirizzi e-mail e numeri di telefono saranno visibili.

[![Richiesta di aiuto - video esplicativo](http://img.youtube.com/vi/ld6zyMyD7gw/0.jpg)](http://www.youtube.com/watch?v=ld6zyMyD7gw "Corona Solidarity Network - Richiesta di aiuto")

### Aiutare
1. Entra a far parte del gruppo come spiegato sopra.
2. Sotto "Ricerca / I miei gruppi" troverai i compiti pubblicati. 
3. Puoi modificare il raggio della ricerca con il pulsante in basso a destra (il simbolo dell'imbuto). Saranno visualizzati solo i compiti dell'ambiente circostante.
4. Clicca sul compito e registra il tuo interesse.
5. la persona in question ti contatterà.


### Ottenere ulteriori informazioni
1. Scarica l'App Telegram ([App Store](https://apps.apple.com/ch/app/telegram-messenger/id686449807), [Play Store](https://play.google.com/store/apps/details?id=org.telegram.messenger&hl=de_CH), [F-Droid](https://f-droid.org/en/packages/org.telegram.messenger/)
2. Entra nel canale informativo aprendo il seguente link: https://t.me/corona_soli](https://t.me/corona_soli).

### Link
* [https://www.hilf-jetzt.ch/](https://www.hilf-jetzt.ch/)
* [Panoramica dei gruppi esistenti](https://do.knack.com/hilfjetzt)

#### Creare un gruppo Telegram, WhatsApp o Facebook
Sul sito [hilf-jetzt.ch](https://www.hilf-jetzt.ch/) troverai le istruzioni per crearli. In una panoramica puoi inserire i tuoi gruppi.

#### Informare offline
Non tutti ricevono informazioni via Internet. Qui](https://www.hilf-jetzt.ch/blog/quartier-brief-zum-ausdrucken-und-aufhangen?categoryId=51284) troverai un documento che puoi stampare e appendere.

## Perché Sciopero per il Clima?
Sciopero del Clima esiste perché molte persone preoccupate per la crisi climatica hanno unito le loro forze. La nostra preoccupazione non è "il clima" di per sé, quanto gli esseri viventi colpiti dalla crisi climatica. La crisi del coronavirus rappresenta anche una grave minaccia per la salute e la vita di innumerevoli persone. Per questo motivo, a noi di Sciopero per il Clima è apparso subito chiaro che dobbiamo dare il nostro contributo.

Vogliamo aiutare tutti i genitori che hanno urgente bisogno di qualcuno che si prenda cura del loro bambino o della loro bambina. Vogliamo aiutare le persone anziane a fare la spesa, perché sono la fascia della popolazione più a rischio. Vogliamo aiutare chi vuole parlare con qualcuno, perché è obbligato/a stare a casa da solo/a per giorni.

In tempi di crisi legate al coronavirus e al clima, abbiamo bisogno di una forte coesione tra le popolazioni locali. Abbiamo bisogno di strutture forti che si prendano cura l'una dell'altra. Consideriamo il nostro contributo al benessere della gente un’attuazione della giustizia climatica.
`
            }
        ]
    }]
};

export const coronaWebinars: IStrapiContent<ITextProps> = {
    identifier: "corona-webinars",
    grouping: "webinars",
    sortId: 5,
    data: [{
        __component: "component.text",
        title: [
            {language: ELanguages.de, text: "Webinare​"}
        ],
        text: [
            {
                language: ELanguages.de, text: `### In Zeiten von Corona bleiben wir Zuhause. Kein Grund, diese Zeiten nicht sinnvoll zu nutzen.

Um weiterhin vernetzt zu sein und Wissen weiterzugeben, organisieren wir Webinare! Dabei werden Expert\\*innen aus Bereichen der Naturwissenschaften, Ökonomie, Politik etc. online ein Referat halten und Fragen direkt beantworten. Es wird dabei einerseits um Weiterbildung zu Klima-relevanten Themen gehen, andererseits möchten wir auch Wissensweitergabe-Webinare für Aktivist*innen anbieten. 

Zurzeit gilt: Der Livestream ist jeweils auf [unserem YouTube Kanal](https://www.youtube.com/channel/UC_caP9IpnSQ_t6KTAOBVqYA) zu sehen.
Wir müssen uns selbst noch ein wenig einspielen. Es kann also sein, dass sich Informationen ändern.

### Wir sind nicht die einzigen

Deutschland organisiert unter dem Motto #WirBildenZukunft in den kommenden Tagen und Wochen Livestreams zur Klima-Gesellschafts-Krisen-Bildung.

[Hier wird jeweils gestreamt](https://www.youtube.com/channel/UCZwF7J5rbyJXBZMJrE_8XCA) und [hier gehts zum Archiv](https://fridaysforfuture.de/wirbildenzukunft-archiv/).
`
            }
        ]
    }]
};

export const coronaPostsSection: IStrapiContent<IContentDelegation<IPostSectionConfig>> = {
    identifier: "corona-post-section",
    sortId: 6,
    grouping: "post-section",
    data: [{
        __component: "component.post-section",
        childIdentifier: "corona-post-section",
        childType: "card",
        config: {title: "posts", n: 0}
    }]
};

export const financialInstituteHeading: IStrapiContent<ITextProps> = {
    identifier: "financial-institute-heading",
    sortId: 0,
    data: [{
        __component: "component.text",
        title: [
            {language: ELanguages.de, text: "Wie grün ist dein Finanzinstitut?​"},
            {language: ELanguages.fr, text: "Ta banque est- elle verte?​"},
            {language: ELanguages.it, text: "La tua banca, é verde?​"}

        ],
        text: [
            {
                language: ELanguages.de, text: `Finanzinstitute haben durch ihre Finanzierungen, Investitionen und Versicherungen von klimaschädlichen Projekten und Unternehmen einen immensen Einfluss auf die Klimakrise und damit aber auch einen wichtigen Hebel. Jedoch findet dieses Thema in der Gesellschaft und Politik nach wie vor zu wenig Beachtung.

Einzelpersonen haben wegen mangelnder Transparenz oder Greenwashing oftmals nicht die  notwendigen Informationen, unsere Finanzinstitute klimafreundlich zu wählen. Deshalb hat es sich die Klimastreikbewegung zum Ziel gemacht, die grössten Schweizer Banken und Versicherungen anhand ihrerer Klimafreundlichkeit in Listen aufzuteilen. 

Der Orientierung halber verwenden wir vorerst hauptsächlich unterschriebenen Abkommen und Initiativen als Kriterien für diese Listen. Die Kriterien werden aber laufend modifiziert und so können  zum Beispiel auch Abkommen oder Initiativen von den Listen gestrichen werden, wenn sie uns als nicht genügend griffig erscheinen. Finanzinstitute können uns auch sonst glaubhaft darlegen, wie sie ihre direkten und indirekten Finanzströme klimafreundlich bewirtschaften, oder uns auf unterschriebene Abkommen oder weitere Kriterien hinweisen. Diese Kriterien werden laufend angepasst. Bei Fragen oder Anregungen, melde dich bei uns! [financial_centre@climatestrike.ch](mailto:financial_centre@climatestrike.ch)
`},
            {
                language: ELanguages.fr, text: `Les institutions financières ont une immense influence sur la crise climatique par le biais de leurs financements, de leurs investissements et de leur soutien dans des projets et entreprises nuisibles au climat. Elles disposent donc d’un levier de pression important. Cependant, ce sujet reçoit encore trop peu d’attention de la part de la société et du monde politique.

Souvent, les particuliers ne disposent pas des informations nécessaires pour le choix d’une institution financière selon des critères écologique. Ceci est dû à un manque de transparence ou au greenwashing. C’est pourquoi le mouvement de la Grève du climat s’est fixé pour objectif de classer les plus grandes banques et compagnies d’assurance suisses en fonction de leur respect du climat. 

Pour l’instant, nous utilisons principalement des accords et des initiatives signés comme critères pour ces listes. Les paramètres sont constamment modifiés et les accords ou initiatives peuvent être retirés des listes s’ils ne nous semblent pas suffisamment pertinents. Les institutions financières peuvent également nous fournir des informations crédibles sur la manière dont elles gèrent leurs flux financiers directs et indirects dans le respect du climat, ou nous informer sur les accords signés ou autres engagements. Les listes sont constamment adaptées.

Questions? [financial_centre@climatestrike.ch](mailto:financial_centre@climatestrike.ch)
`},
            {
                language: ELanguages.it, text: "Questo contenuto al momento non é disponibile in italiano. Per favore vedi la versione tedesca o francese."
            }
        ]
    }]
};

export const financialInstituteGreen: IStrapiContent<IDemandItemProps> = {
    identifier: "financial-institute-green",
    sortId: 1,
    data: [{
        __component: "component.demand-item",
        icon: [], // [{language: ELanguages.all, text: "/static/images/green-thumb.svg"}],
        title: [
            {language: ELanguages.de, text: "Grüne Liste"},
            {language: ELanguages.fr, text: "Liste verte"}
        ],
        lead: [
            {
                language: ELanguages.de, text: `Auf die Grüne Liste kommen Finanzinstitute, welche unsere Forderungen unterschrieben haben und sich damit öffentlich verpflichten, diese umzusetzen. Für uns ist ein Finanzinstitut klimafreundlich, wenn es unsere Forderungen unterschreibt und sich damit zur Umsetzung dieser bekennt.

### Finanzinstitute
* Basellandschaftliche Kantonalbank
* Forma Futura Invest AG
`},
            {
                language: ELanguages.fr, text: `Les institutions financières qui ont signé nos demandes et s’engagent donc publiquement à les mettre en œuvre sont inscrites sur la liste verte. Pour nous, une institution financière est respectueuse du climat si elle signe nos exigences et s’engage ainsi à les mettre en œuvre.
### Institutions financières
* Basellandschaftliche Kantonalbank
* Forma Futura`}
        ],
        text: []
    }]
};

export const financialInstituteLightGray: IStrapiContent<IDemandItemProps> = {
    identifier: "financial-institute-light-gray",
    sortId: 2,
    data: [{
        __component: "component.demand-item",
        icon: [], // [{language: ELanguages.all, text: "/static/images/light-gray-thumb.svg"}],
        title: [
            {language: ELanguages.de, text: "Hellgraue Liste"},
            {language: ELanguages.fr, text: "Liste gris clair"}
        ],
        lead: [
            {
                language: ELanguages.de, text: `Auf dieser Liste werden Finanzinstitute aufgelistet, welche ihre Finanzflüsse auf Netto 0 bis 2050 ausrichten. Dies können sie einerseits durch das Unterzeichnen von entsprechenden Abkommen, andererseits durch das öffentliche Bekennen und Veröffentlichen von konkreten Massnahmen und Zielen bis Juni 2020.

### Abkommen
* [United Nations-convened Net-Zero Asset Owner Alliance](https://www.unepfi.org/net-zero-alliance/)
* [Science Based Targets Initiative](https://sciencebasedtargets.org/what-is-a-science-based-target/)
* [UN Global Compact Business Ambition for 1.5°C](https://www.unglobalcompact.org/docs/publications/Business-Ambition-for-1.5C-Pledge.pdf)
* [UNEP FI Collective Commitment to climate action](https://www.unepfi.org/banking/bankingprinciples/collective-commitment/)
* [B Corporation certification](https://bcorporation.net/certification)
* [GABV Climate Change commitment](http://www.gabv.org/news/global-banking-leaders-commit-to-align-their-carbon-footprint-with-paris-agreement) 

### Finanzinstitute
* Allianz
* Alternative Bank Schweiz
* Axa
* Generali
* Globalance Bank AG
* Munich Re
* Swiss Re
* Zurich Versicherung
`},
            {
                language: ELanguages.fr, text: `Cette liste énumère les institutions financières qui visent des flux financiers nets de 0 à 2050. Ils peuvent le faire d’une part en signant des accords appropriés, et d’autre part en annonçant publiquement et en publiant des mesures et des objectifs concrets d’ici juin 2020.

### Accords
* [United Nations-convened Net-Zero Asset Owner Alliance](https://www.unepfi.org/net-zero-alliance/)
* [Science Based Targets Initiative](https://sciencebasedtargets.org/what-is-a-science-based-target/)
* [UN Global Compact Business Ambition for 1.5°C](https://www.unglobalcompact.org/docs/publications/Business-Ambition-for-1.5C-Pledge.pdf)
* [UNEP FI Collective Commitment to climate action](https://www.unepfi.org/banking/bankingprinciples/collective-commitment/)
* [B Corporation certification](https://bcorporation.net/certification)
* [GABV Climate Change commitment](http://www.gabv.org/news/global-banking-leaders-commit-to-align-their-carbon-footprint-with-paris-agreement) 

### Institutions financières
* Allianz
* Alternative Bank Schweiz
* Axa
* Generali
* Globalance Bank AG
* Munich Re
* Swiss Re
* Zurich Versicherung`}
        ],
        text: []
    }]
};

export const financialInstituteGray: IStrapiContent<IDemandItemProps> = {
    identifier: "financial-institute-gray",
    sortId: 3,
    data: [{
        __component: "component.demand-item",
        icon: [], // [{language: ELanguages.all, text: "/static/images/gray-thumb.svg"}],
        title: [
            {language: ELanguages.de, text: "Graue Liste"},
            {language: ELanguages.fr, text: "Liste grise"}
        ],
        lead: [
            {
                language: ELanguages.de, text: `Auf dieser Liste werden Finanzinstitute aufgelistet, welche die untenstehenden Initiativen unterzeichnet haben.Diese Initiative versuchen eine Verbesserung in Richtung nachhaltige Investitionen, Kredite oder Versicherungen zu erzielen.Bei diesen wird jedoch nur eine Business - Sparte behandelt wie zum Beispiel nur “verantwortliches Investieren” oder Nachhaltigkeit allgemein.Dies ist aber viel zu vage und breit und reicht bei langem nicht genug.

### Initiativen
* [Principles for Responsible Banking](https://www.unepfi.org/banking/bankingprinciples/)
*[Principles for Sustainable Insurance](https://www.unepfi.org/psi/)
*[Principles for Responsible Investment](https://www.unpri.org/)
*[Carbon Disclosure Project](https://www.cdp.net/en)
*[Climate Action 100+](https://climateaction100.wordpress.com/)

### Finanzinstitute
* Baloise Asset Management
* Bank J.Safra Sarasin Ltd
* Banque Cantonale Vaudoise
* Berner Kantonalbank
* Helvetia Holding
* Julius Bär
* Lombard Odier
* Luzerner Kantonalbank
* Mirabaud & Cie SA
* Pictet Asset Management
* SUVA
* Vaudoise Assurances Holding SA
* Vontobel Holding AG
* Zürcher Kantonalbank
`},
            {
                language: ELanguages.fr, text: `Cette liste contient les institutions financières qui ont signé les initiatives énumérées ci - dessous.Ces initiatives tentent de s’améliorer vers des investissements, des prêts ou des assurances durables.Toutefois, ils ne concernent qu’un seul secteur d’activité, comme “l’investissement responsable” ou la durabilité en général.Ces mesures sont trop vagues ou trop larges et insuffisantes à moyen ou long terme.

### Initiatives
* [Principles for Responsible Banking](https://www.unepfi.org/banking/bankingprinciples/)
* [Principles for Sustainable Insurance](https://www.unepfi.org/psi/)
* [Principles for Responsible Investment](https://www.unpri.org/)
* [Carbon Disclosure Project](https://www.cdp.net/en)
* [Climate Action 100+](https://climateaction100.wordpress.com/)

### Institutions financières
* Baloise Asset Management
* Bank J. Safra Sarasin Ltd
* Banque Cantonale Vaudoise
* Berner Kantonalbank
* Helvetia Holding
* Julius Bär
* Lombard Odier
* Luzerner Kantonalbank
* Mirabaud & Cie SA
* Pictet Asset Management
* SUVA
* Vaudoise Assurances Holding SA
* Vontobel Holding AG
* Zürcher Kantonalbank`}
        ],
        text: []
    }]
};

export const financialInstituteBlack: IStrapiContent<IDemandItemProps> = {
    identifier: "financial-institute-black",
    sortId: 4,
    data: [{
        __component: "component.demand-item",
        icon: [], // [{language: ELanguages.all, text: "/static/images/black-thumb.svg"}],
        title: [
            {language: ELanguages.de, text: "Schwarze Liste"},
            {language: ELanguages.fr, text: "Liste noire"}
        ],
        lead: [
            {
                language: ELanguages.de, text: `Auf der schwarze Liste sind diejenigen Finanzinstitute, welche gar keine Abkommen unterzeichnet haben und sich nicht zur Umsetzung unserer Forderungen bekennen.Da vor allem im Inland tätige Finanzinstitute  internationale Abkommen in der Regel nicht unterzeichnen, wird diesen die Chance gegeben, bis zur Aktualisierung der Liste im Juni 2020 konkrete Massnahmen und Ziele vorzulegen oder uns auf ihre Teilnahme an griffigen Initiativen hinzuweisen.Aus diesem Grund werden in der schwarzen Liste vorläufig nur diejenigen Institute aufgelistet, welche in internationalen Reports als Klimasünder\* innen auftauchen, auch wenn sie eine Initiative unterschrieben haben.

### Reports
* [ShareAction Report](https://shareaction.org/research-resources/point-of-no-returns/)
* [Rainforest Action Network Report](https://www.ran.org/bankingonclimatechange2019/)
* [Bank Track Report](https://www.banktrack.org/download/banking_on_climate_change_2019_fossil_fuel_finance_report_card/banking_on_climate_change_2019.pdf)
* [Report von Artisans dela Transition](https://www.artisansdelatransition.org/assets/swiss-national-bank-investments-artisansdelatransition-report2.pdf)

### Finanzinstitute
* Credit Suisse
* Schweizer Nationalbank
* Swiss Life Asset Managers
* UBS
`},
            {
                language: ELanguages.fr, text: `Sur la liste noire figurent les institutions financières qui n’ont signé aucun accord et ne s’engagent pas à mettre en œuvre nos demandes, comme les institutions financières opérant principalement en Suisse n’ayant pas rejoint d’accords internationaux.Elles auront la possibilité de présenter des mesures et des objectifs concrets d’ici la mise à jour de la liste en juin 2020 ou de nous informer sur la participation à des initiatives efficaces.C’est pourquoi la liste noire ne contient pour l’instant que les institutions qui apparaissent dans les rapports internationaux comme des criminels climatiques, même si elles ont signé une initiative.  

### Reports
* [ShareAction Report](https://shareaction.org/research-resources/point-of-no-returns/)
* [Rainforest Action Network Report](https://www.ran.org/bankingonclimatechange2019/)
* [Bank Track Report](https://www.banktrack.org/download/banking_on_climate_change_2019_fossil_fuel_finance_report_card/banking_on_climate_change_2019.pdf)
* [Report von Artisans dela Transition](https://www.artisansdelatransition.org/assets/swiss-national-bank-investments-artisansdelatransition-report2.pdf)

### Finanzinstitute
* Credit Suisse
* Schweizer Nationalbank
* Swiss Life Asset Managers
* UBS`}
        ],
        text: []
    }]
};

export const financialInstituteBlue: IStrapiContent<IDemandItemProps> = {
    identifier: "financial-institute-blue",
    sortId: 5,
    data: [{
        __component: "component.demand-item",
        icon: [], // [{language: ELanguages.all, text: "/static/images/blue-questionmark.svg"}],
        title: [
            {language: ELanguages.de, text: "Blaue Liste"},
            {language: ELanguages.fr, text: "Liste bleue"}
        ],
        lead: [
            {
                language: ELanguages.de, text: `Da wir von vielen Instituten keine Antwort über Ihre Klimafreundlichkeit erhielten, mit einigen noch Treffen ausstehen, oder sie vor allem im Inland tätig sind und unter anderem deshalb nicht die oben aufgelisteten Initiativen oder Abkommen unterzeichneten, werden wir die Kriterien für die Listen weiter ausarbeiten und auf allfällige Informationen der Finanzinstitute warten, bevor wir die Institute auf der schwarzen Liste vermerken.

* Aargauische Kantonalbank*
* Appenzeller Kantonalbank
* Aspen Re
* Assura SA
* Bank Cler AG
* Banque Bonhôte
* Banque Cantonale de Fribourg
* Banque Cantonale de Genève
* Banque Cantonale du Valais
* Basler Kantonalbank*
* Branchen Versicherung Schweiz
* CAP Rechtsschutz – Versicherungsgesellschaft AG
* Catlin RE Schweiz AG
* CCAP Caisse Cantonale d’Assurance Populaire
* Cembra Money Bank*
* Chubb Versicherungen (Schweiz) AG
* Coface Re SA
* Echo Rückversicherungs-AG
* EFG Bank
* Europäische Reiseversicherungs AG
* First Caution SA
* Gebäudeversicherung Bern
* Glarner Kantonalbank*
* Graubündner Kantonalbank
* Groupe Mutuel*
* GVB Services AG
* HOTELA Assurances SA
* LGT Bank
* Liverty Speciality Markets
* Maerki Baumann & Co AG*
* MS Amlin AG
* Neue Aagauer Bank AG
* Nidwaldner Kantonalbank
* Obwaldner Kantonalbank
* PartnerRE Zurich Branch
* Postfinance AG
* Protekta Rechtsschutz-Versicherung AG – gehört zur Mobiliar AG
* Rahn+Bodmer Co.
* Raiffeisen*
* Rentes Genevoises
* Retraites Populaires
* Schaffhauser Kantonalbank*
* Schweizerische Lebensverischerungs
* Gesellschaft AG
* Schweizerische Mobiliar Versicherungsgesellschaft AG
* Schwyzer Kantonalbank
* SCOR Switzerland AG
* Skandia Leben AG
* Societé Générale
* St Galler Kantonalbank*
* Sympani AG
* SYZ Bank
* Thurgauer Kantonalbank
* TSM Compagnie d’Assurances
* Union Bancaire Privée UBP SA
* Urner Kantonalbank
* Valiant Holding AG*
* Validus Reinsurance (Switzerland)
* Versicherung der Schweizer Ärtze Genossenschaft
* VP Bank AG

(*) Diese Finanzinstitute haben uns zu einem Gespräch eingeladen, noch steht dieses Gespräch allerdings aus
`},
            {
                language: ELanguages.fr, text: `Comme nous n’avons pas reçu de réponse de nombreuses institutions concernant leur respect du climat ou comme elles sont principalement actives au niveau national et n’ont donc pas signé les initiatives ou les accords énumérés ci - dessus, entre autres raisons, nous allons développer davantage les critères pour les listes et attendre les informations des institutions financières avant de les inscrire sur la liste noire.

* Aargauische Kantonalbank*
* Appenzeller Kantonalbank
* Aspen Re
* Assura SA
* Bank Cler AG
* Banque Bonhôte
* Banque Cantonale de Fribourg
* Banque Cantonale de Genève
* Banque Cantonale du Valais
* Basler Kantonalbank*
* Branchen Versicherung Schweiz
* CAP Rechtsschutz – Versicherungsgesellschaft AG
* Catlin RE Schweiz AG
* CCAP Caisse Cantonale d’Assurance Populaire
* Cembra Money Bank*
* Chubb Versicherungen (Schweiz) AG
* Coface Re SA
* Echo Rückversicherungs-AG
* EFG Bank
* Europäische Reiseversicherungs AG
* First Caution SA
* Gebäudeversicherung Bern
* Glarner Kantonalbank*
* Graubündner Kantonalbank
* Groupe Mutuel*
* GVB Services AG
* HOTELA Assurances SA
* LGT Bank
* Liverty Speciality Markets
* Maerki Baumann & Co AG*
* MS Amlin AG
* Neue Aagauer Bank AG
* Nidwaldner Kantonalbank
* Obwaldner Kantonalbank
* PartnerRE Zurich Branch
* Postfinance AG
* Protekta Rechtsschutz-Versicherung AG – gehört zur Mobiliar AG
* Rahn+Bodmer Co.
* Raiffeisen*
* Rentes Genevoises
* Retraites Populaires
* Schaffhauser Kantonalbank*
* Schweizerische Lebensverischerungs
* Gesellschaft AG
* Schweizerische Mobiliar Versicherungsgesellschaft AG
* Schwyzer Kantonalbank
* SCOR Switzerland AG
* Skandia Leben AG
* Societé Générale
* St Galler Kantonalbank*
* Sympani AG
* SYZ Bank
* Thurgauer Kantonalbank
* TSM Compagnie d’Assurances
* Union Bancaire Privée UBP SA
* Urner Kantonalbank
* Valiant Holding AG*
* Validus Reinsurance (Switzerland)
* Versicherung der Schweizer Ärtze Genossenschaft
* VP Bank AG

(*) Ces institutions financières nous ont invités pour un entretien, mais cet entretien est toujours en cours`}
        ],
        text: []
    }]
};
