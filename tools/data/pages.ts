import {forEach} from "lodash";

import {ELinkLocation} from "../../core/@def";
import {IStrapiPage} from "../../server/api/@def";
import {
    homeHeadline,
    homeNewsletter,
    homeLandingLinkWhy,
    homeLandingLinkDemands,
    homeLandingLinkFacts,
    homeLandingLinkJoin,
    homeNews,
    movementDescription,
    movementDemandsTitle,
    movementDemandsEmergency,
    movementDemandsNetZero,
    movementDemandsJustice,
    movementDemandsFinance,
    movementDemandsSystemChange,
    movementCodex,
    joinThankYou,
    joinTalkAboutIt,
    joinJoinUs,
    joinGetActive,
    joinRegioTitle,
    joinMap,
    joinWiki,
    eventsDummy,
    mediaNational,
    mediaRegional,
    donationText,
    donationIframe,
    privacyText,
    postsDummy,
    factsDoYouReallyWantToKnow,
    factsYouAreAffected,
    factsChangeIsPossible,
    factsYourInvolvementMatters,
    postsSection,
    coronaTitle,
    coronaLanding1,
    coronaLanding2,
    coronaIframe,
    coronaSolidarity,
    coronaWebinars,
    coronaPostsSection,
    financialInstituteHeading,
    financialInstituteGreen,
    financialInstituteLightGray,
    financialInstituteGray,
    financialInstituteBlack,
    financialInstituteBlue
} from "./contents";


// Header pages
// const coronaBackgrounds = [
//     {language: ELanguages.de, text: "/static/images/corona-banner-de.svg"},
//     {language: ELanguages.fr, text: "/static/images/corona-banner-fr.svg"},
//     {language: ELanguages.it, text: "/static/images/corona-banner-it.svg"}
// ];

const homePage: IStrapiPage = {
    identifier: "home",
    slug: "",
    sortId: 0,
    image: [], // coronaBackgrounds,
    contents: [],
    config: {
        linkLocations: [ELinkLocation.header],
        dataDependencies: [{identifier: "posts", type: "POST", route: "posts?_sort=date:DESC&category=good-news&category=other-news&category=bad-news"}]
    }
};

homePage.contents.push(homeHeadline, homeNewsletter, homeLandingLinkWhy, homeLandingLinkDemands, homeLandingLinkFacts, homeLandingLinkJoin, homeNews);

const factsPage: IStrapiPage = {
    identifier: "facts", slug: "facts", sortId: 3,
    image: [], contents: [], config: {linkLocations: [ELinkLocation.header]}
};

factsPage.contents.push(factsDoYouReallyWantToKnow, factsYouAreAffected, factsChangeIsPossible, factsYourInvolvementMatters);

const movementPage: IStrapiPage = {
    identifier: "movement", slug: "movement", sortId: 6,
    image: [], // [{language: ELanguages.all, text: "/static/images/movement-background.jpg"}],
    contents: [], config: {linkLocations: [ELinkLocation.header]}
};

movementPage.contents.push(movementDescription, movementDemandsTitle, movementDemandsEmergency, movementDemandsNetZero, movementDemandsJustice, movementDemandsFinance, movementDemandsSystemChange, movementCodex);

const mediaPage: IStrapiPage = {
    identifier: "media", slug: "media", sortId: 9,
    image: [], contents: [], config: {linkLocations: [ELinkLocation.footer]}
};

mediaPage.contents.push(mediaNational, mediaRegional);

const joinPage: IStrapiPage = {
    identifier: "join", slug: "join", sortId: 12,
    image: [], // [{language: ELanguages.all, text: "/static/images/join-background.jpg"}],
    contents: [], config: {linkLocations: [ELinkLocation.header, ELinkLocation.footer], dataDependencies: [{identifier: "contacts", type: "POST", route: "contact-groups?_limit=-1"}]}
};

joinPage.contents.push(joinThankYou, joinTalkAboutIt, joinJoinUs, joinGetActive, joinRegioTitle, joinMap, joinWiki);

const eventsPage: IStrapiPage = {
    identifier: "events", slug: "events", sortId: 15,
    image: [], contents: [], config: {linkLocations: [ELinkLocation.header, ELinkLocation.footer], dataDependencies: [{identifier: "events", type: "POST", route: "event-groups?_limit=-1"}]}
};

eventsPage.contents.push(eventsDummy);

const newsPage: IStrapiPage = {
    identifier: "news", slug: "news", sortId: 18,
    image: [], contents: [],
    config: {linkLocations: [ELinkLocation.header], dataDependencies: [{identifier: "posts", type: "POST", route: "posts?_sort=date:DESC&category=good-news&category=other-news&category=bad-news&_limit=-1"}]}
};

newsPage.contents.push(postsDummy);

const donatePage: IStrapiPage = {
    identifier: "donate", slug: "donate", sortId: 21,
    image: [], contents: [], config: {linkLocations: [ELinkLocation.header, ELinkLocation.footer]}
};

donatePage.contents.push(donationText, donationIframe);

// Footnote pages

const privacyPage: IStrapiPage = {
    identifier: "privacy", slug: "privacy", sortId: 99,
    image: [], contents: [], config: {linkLocations: [ELinkLocation.footnote]}
};

privacyPage.contents.push(privacyText);

const impressumPage: IStrapiPage = {
    identifier: "impressum", slug: "impressum", sortId: 102,
    image: [], contents: [], config: {linkLocations: []}
};

// Wrapper pages

const postsPage: IStrapiPage = {
    identifier: "posts", slug: "posts", sortId: 201,
    image: [], contents: [],
    config: {linkLocations: [], dataDependencies: [{identifier: "posts", type: "POST", route: "posts?_sort=date:DESC&_limit=-1"}]}
};

postsPage.contents.push(postsSection);


// Special pages

const electionsPage: IStrapiPage = {
    identifier: "elections", slug: "elections", sortId: 300,
    image: [], contents: [], config: {linkLocations: []}
};

const coronaPage: IStrapiPage = {
    identifier: "corona", slug: "corona", sortId: 303,
    image: [], // coronaBackgrounds, 
    contents: [], config: {linkLocations: [], dataDependencies: [{identifier: "corona-post-section", route: "posts?_sort=date:DESC&identifier_contains=corona", type: "POST"}]}
};
coronaPage.contents.push(coronaTitle, coronaLanding1, coronaLanding2, coronaIframe, coronaSolidarity, coronaWebinars, coronaPostsSection);

const howGreenIsYourFinancialInstitutionPage: IStrapiPage = {
    identifier: "how-green-is-your-financial-institution", slug: "how-green-is-your-financial-institution", sortId: 305,
    image: [], contents: [], config: {linkLocations: []}
};

const wieGrunIstDeinFinanzinstitutPage: IStrapiPage = {
    identifier: "wie-grun-ist-dein-finanzinstitut", slug: "wie-grun-ist-dein-finanzinstitut", sortId: 305,
    image: [], contents: [], config: {linkLocations: []}
};

const taBanqueEstElleVertePage: IStrapiPage = {
    identifier: "ta-banque-est-elle-verte", slug: "ta-banque-est-elle-verte", sortId: 308,
    image: [], contents: [], config: {linkLocations: []}
};

forEach([howGreenIsYourFinancialInstitutionPage, /* wieGrunIstDeinFinanzinstitutPage, taBanqueEstElleVertePage*/], (page) => {
    // page.image = [{language: ELanguages.all, text: "/static/images/financial-institute-background.jpg"}];
    page.contents.push(financialInstituteHeading, financialInstituteGreen, financialInstituteLightGray, financialInstituteGray, financialInstituteBlack, financialInstituteBlue);
});

export const pages: IStrapiPage[] = [
    homePage, factsPage, movementPage, mediaPage, joinPage,
    eventsPage, newsPage, donatePage, privacyPage, impressumPage, postsPage,
    electionsPage, coronaPage,
    howGreenIsYourFinancialInstitutionPage, wieGrunIstDeinFinanzinstitutPage, taBanqueEstElleVertePage
];
