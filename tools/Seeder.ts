import {find} from "lodash";
import nodefetch from "node-fetch";

import {ISeedModel, IStrapiConfig} from "./@def";
import {IPage, IHttpService} from "../core/@def";
import {HttpService} from "../core/HttpService";
import {log} from "../server/core/Logger";
import {pages} from "./data/pages";
import {contactGroups} from "./data/contactGroups";
import {posts} from "./data/posts/exports";
import {getAllRoute, authenticate} from "./Core";

if (!globalThis.fetch) {
    globalThis.fetch = nodefetch as any;
}

const _model: ISeedModel = {
    pages: pages,
    contactGroups: contactGroups,
    posts: posts
};

interface ISeederContext {
    model: ISeedModel;
    header?: any;
}

interface IHierarchicalItemSeedCtx<T = any> {
    route: string;
    childRoute: string;
    childKey: keyof T;
    parentKey: any;
    items: T[];
}


export class Seeder {
    private $http: IHttpService;

    constructor(private config: IStrapiConfig = {}) {
        const {baseURL = "http://localhost:1337/"} = config;
        this.$http = new HttpService({baseURL});
    }

    public async seed(model: ISeedModel = {}) {
        const context: ISeederContext = {model};
        const {posts, pages, contactGroups, eventGroups} = model;
        await this.authenticate();
        if (posts) {
            log.debug("Posts");
            await this.posts(context);
        }
        if (pages) {
            log.debug("Pages");
            await this.pages(context);
        }
        // await this.content(context);
        if (contactGroups) {
            log.debug("ContactGroups");
            await this.contactGroups(context);
        }
        if (eventGroups) {
            log.debug("EventGroups");
            await this.eventGroups(context);
        }
    }

    private async authenticate() {
        await authenticate(this.$http, this.config);
    }

    private async pages(context: ISeederContext) {
        const model = context.model;
        const route = "pages";
        let dbItems = await this.$http.getd<IPage[]>({url: getAllRoute(route)});
        for (const item of dbItems) {
            await this.$http.delete({url: `${route}/${item.id}`});
        }
        const childRoute = "contents";
        const dbChildren = await this.$http.getd<IPage[]>({url: getAllRoute(childRoute)});
        for (const child of dbChildren) {
            await this.$http.delete({url: `${childRoute}/${child.id}`});
        }
        const items = model.pages;
        for (const item of items) {
            log.debug("Processing page: " + item.identifier);
            const insertItem = {...item};
            delete insertItem.contents;
            const page = await this.$http.postd<IPage>({url: route, body: insertItem});
            for (const child of item.contents) {
                const insertChild = {...child};
                log.debug("Processing child: " + child.identifier);
                (insertChild as any).page = {id: page.id};
                await this.$http.postd({url: childRoute, body: insertChild});
            }
        }
        dbItems = await this.$http.getd({url: route});
        return dbItems;
    }

    private async posts(context: ISeederContext) {
        const route = "posts";
        const items = context.model.posts;
        await this.cleanInsert(route, items);
    }

    private async contactGroups(context: ISeederContext) {
        const {model: {contactGroups: items}} = context;
        const route = "contact-groups";
        const childRoute = "contacts";
        const childKey = "contacts";
        const parentKey = "contact_group";
        await this.hierarchicalItems({route, childKey, childRoute, parentKey, items});
    }

    private async eventGroups(context: ISeederContext) {
        const {model: {eventGroups: items}} = context;
        const route = "event-groups";
        const childRoute = "events";
        const childKey = "events";
        const parentKey = "event_group";
        await this.hierarchicalItems({route, childKey, childRoute, parentKey, items});
    }

    private async hierarchicalItems(ctx: IHierarchicalItemSeedCtx) {
        const {route, childRoute, childKey, parentKey, items} = ctx;
        let dbItems = await this.$http.getd<IPage[]>({url: getAllRoute(route)});
        for (const item of dbItems) {
            await this.$http.delete({url: `${route}/${(item as any).id}`});
        }
        for (const item of items) {
            const dbItem = {...item};
            delete dbItem[childKey];
            log.debug("Processing: " + item.identifier);
            await this.$http.postd({url: route, body: dbItem});
        }
        dbItems = await this.$http.getd({url: getAllRoute(route)});

        const childItems = await this.$http.getd<IPage[]>({url: getAllRoute(childRoute)});
        for (const childItem of childItems) {
            await this.$http.delete({url: `${childRoute}/${(childItem as any).id}`});
        }

        for (const _item of items) {
            const parent = find(dbItems, dbItem => dbItem.identifier === _item.identifier);
            for (const contact of _item[childKey]) {
                (contact as any)[parentKey] = {id: (parent as any).id};
                log.debug("Processing child: " + contact.identifier);
                await this.$http.postd({url: childRoute, body: contact});
            }
        }
        return dbItems;
    }

    private async cleanInsert(route: string, items: any[]) {
        let dbItems = await this.$http.getd<IPage[]>({url: getAllRoute(route)});
        for (const item of dbItems) {
            try {
                await this.$http.delete({url: `${route}/${item.id}`});
            }
            catch (err) {
                log.error(`Error deleting for route ${route}: ${item.id}`);
            }
        }
        for (const item of items) {
            try {
                await this.$http.postd({url: route, body: item});
            }
            catch (err) {
                log.error(`Error inserting for route ${route}: ${item} - Error: ${err}`);
            }
        }
        dbItems = await this.$http.getd({url: route});
        return dbItems;
    }
}

if (require.main === module) {
    const seeder = new Seeder();
    seeder.seed(_model)
        .then(() => log.debug("Done"))
        .catch((err) => log.debug(err));
}