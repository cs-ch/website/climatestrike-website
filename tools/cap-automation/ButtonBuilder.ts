
import {EStyles} from "../../components/@def";
import {ELanguages, ITranslation} from "../../core/@def";
import {IBuilder} from "./@def";


export class ButtonBuilder implements IBuilder {
    private _link: string;
    private _style: EStyles = EStyles.primary;
    private _texts: ITranslation[] = [];
    private _type: string;

    public style(style: EStyles) {
        this._style = style;
        return this;
    }

    public link(link: string) {
        this._link = link;
        return this;
    }

    public text(text: string, language = ELanguages.all) {
        this._texts.push({language, text});
        return this;
    }

    public type(type: string) {
        this._type = type;
        return this;
    }

    public build() {
        const result: any = {
            __component: "config.button",
            style: this._style,
            link: [{language: "all", text: this._link}],
            text: []
        };
        if (this._texts.length) {
            result.text = this._texts;
        }
        if (this._type) {
            result.type = this._type;
        }
        return result;
    }
}