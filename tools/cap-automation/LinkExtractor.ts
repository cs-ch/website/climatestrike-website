import fs from "fs";
import {map, sortBy, uniq} from "lodash";
import {LinkUpdater} from "./LinkUpdater";

const idRewrites = {
    ChapterCrossSectoralPolicies: {path: "/posts/cap-1-cross-sectoral-policies", fragment: null},
    ChapterMobility: {path: "/posts/cap-2-mobility", fragment: null},
    ChapterBuildings: {path: "/posts/cap-3-buildings-and-spatial-development", fragment: null},
    ChapterIndustry: {path: "/posts/cap-4-industry-and-service-sector", fragment: null},
    ChapterEnergy: {path: "/posts/cap-5-energy-supply", fragment: null},
    ChapterAgriculture: {path: "/posts/cap-agriculture-6and-food-system", fragment: null},
    ChapterNegativeEmissions: {path: "/posts/cap-7-negative-emissions", fragment: null},
    ChapterFinancialSector: {path: "/posts/cap-8-financial-sector", fragment: null},
    ChapterEPS: {path: "/posts/cap-9-economic-and-political-structures", fragment: null},
    ChapterInternationalCollaboration: {path: "/posts/cap-10-international-collaboration-and-climate-finance", fragment: null},
    ChapterEducation: {path: "/posts/cap-11-education", fragment: null},
    ChapterAdaptation: {path: "/posts/cap-12-adaptation", fragment: null},
    _Vision: {path: "/posts/cap-3-buildings-and-spatial-development", fragment: "vision"}
};


class LinkExtractor {
    private linkUpdater = new LinkUpdater(idRewrites);

    public run(): void {
        const html = fs.readFileSync("./tools/cap-automation/data/doc.html", "utf-8");
        const linkModel = this.linkUpdater.createLinkModel(html);
        const createFullPath = (path: string, fragment: string) => {
            return path + (fragment ? ("#" + fragment) : "");
        };
        const links = map(linkModel, (value) => createFullPath(value.path, value.fragment));
        fs.writeFileSync("tools/cap-automation/data/links.json", (JSON.stringify(sortBy(uniq(links)), null, 4)));
    }
}

new LinkExtractor().run();