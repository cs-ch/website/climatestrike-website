import {times, clone, map, compact, slice} from "lodash";
import cheerio from "cheerio";

import {EnglishPostBuilder} from "./AllPostBuilder";
import {getNormalizedWhitespaceText} from "./Utils";
import {IDictionary} from "../../core/@def";
import {slugify} from "../../core/Utils";


interface ILinkModelEntry {
    originalId: string;
    postTitle: string;
    headerHierarchy?: string[];
    fragment?: string;
    path?: string;
}

interface ILinkRewrite {
    path?: string;
    fragment?: string;
}

export class LinkUpdater {

    constructor(private idRewrites: IDictionary<ILinkRewrite> = {}) { }

    public updateLinksInHtml(html: string): string {
        const linkModel = this.createLinkModel(html);
        const createFullPath = (path: string, fragment: string) => {
            return path + (fragment ? ("#" + fragment) : "");
        };
        const $ = cheerio.load(html);
        let h1 = null;
        $("a").each((_idx, elt) => {
            const anchor = $(elt);
            const parent = anchor.parent();
            const parentTag = parent.prop("tagName");
            if (parentTag === "H1") {
                h1 = getNormalizedWhitespaceText(parent);
            }
            let href = anchor.attr().href;
            if (href
                && (href.startsWith("https://uzh-my.sharepoint.com/personal/jonas_kampus_uzh_ch/Documents")
                    || href.startsWith("bookmark://")
                    || href.startsWith("file://"))) {
                href = href.substring(href.indexOf("#"));
            }
            if (href && href.startsWith("http")) {
                // NOP
            }
            else if (href && href.startsWith("#")) {
                const ref = linkModel[href];
                if (ref) {
                    const refTitle = ref.postTitle;
                    const fullPath = createFullPath(ref.path, ref.fragment);
                    const isSameSite = refTitle === h1;
                    const link = isSameSite ? "#" + ref.fragment : fullPath;
                    anchor.attr("href", link);
                }
                else {
                    const text = getNormalizedWhitespaceText(anchor);
                    if (text || anchor.children().length) {
                        anchor.replaceWith(`<div>${text}</div>`);
                    }
                    else {
                        anchor.remove();
                    }
                }
            }
            else if (href) {
                console.warn("Weird link: " + href);
            }
            const id = anchor.attr().id;
            if (id) {
                const ref = linkModel["#" + id];
                if (ref && ref.fragment) {
                    if ($("#" + ref.fragment).length) {
                        anchor.remove();
                    }
                    else {
                        anchor.attr("id", ref.fragment);
                        anchor.addClass("fragment-anchor");
                    }
                }
            }
        });
        const result = $.html($("body").children());
        return result;
    }

    public createLinkModel(html: string) {
        const linkModel: IDictionary<ILinkModelEntry> = {};
        const $ = cheerio.load(html);
        const headers: string[] = times(6, () => null);
        $(":header").each((_j, elt) => {
            const header = $(elt);
            const title = getNormalizedWhitespaceText(header);
            let level = parseInt(header.prop("tagName")[1]);
            headers[level - 1] = title;
            while (level < 6) {
                headers[level++] = null;
            }
            const postTitle = headers[0];
            const postContent = $.html(header) + $.html(header.nextUntil(":header"));
            const $content = cheerio.load(postContent);
            $content("a").each((_i, elt) => {
                const anchor = $content(elt);
                const id = anchor.attr().id;
                const headerHierarchy = clone(headers);
                const idRewrite = this.idRewrites[id];
                const postPath = EnglishPostBuilder.createIdentifierFromTitle(postTitle);
                let path = `/posts/${postPath}`;
                let fragment = map(compact(slice(headerHierarchy, 1)), slugify).join("_");
                if (idRewrite) {
                    if (idRewrite.path) {
                        path = idRewrite.path;
                    }
                    if (idRewrite.fragment) {
                        fragment = idRewrite.fragment;
                    }
                }
                if (id) {
                    linkModel["#" + id] = {originalId: id, headerHierarchy, path, fragment, postTitle};
                }
            });
        });

        return linkModel;
    }
}