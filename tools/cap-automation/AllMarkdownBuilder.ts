
import {ELanguages, ITranslation} from "../../core/@def";
import {IBuilder} from "./@def";


export class MarkdownBuilder implements IBuilder {
    private _text: string

    public text(text: string) {
        this._text = text;
        return this;
    }

    public build() {
        return {
            __component: "component.rich-text",
            text: [{language: "all", text: this._text}]
        };
    }
}

export class HTMLBuilder implements IBuilder {
    private _texts: ITranslation[] = [];

    public text(text: string, language: ELanguages = ELanguages.all) {
        this._texts.push({language, text});
        return this;
    }

    public build() {
        return {
            __component: "component.rich-text",
            text: this._texts
        };
    }
}