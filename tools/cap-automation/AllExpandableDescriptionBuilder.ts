import {IBuilder} from "./@def";

export class AllExpendableDescriptionBuilder implements IBuilder {
    private _titleLevel: number = 2;
    private _title: string;
    private _lead: string;
    private _text: string;

    public title(title: string) {
        this._title = title;
        return this;
    }

    public titleLevel(level: number) {
        this._titleLevel = level;
        return this;
    }

    public lead(lead: string) {
        this._lead = lead;
        return this;
    }

    public text(text: string) {
        this._text = text;
        return this;
    }

    public build() {
        return {
            __component: "component.collapsible-text-with-lead",
            title: [{language: "all", text: this._title}],
            titleLevel: this._titleLevel,
            lead: this._lead ? [{language: "all", text: this._lead}] : [],
            text: [{language: "all", text: this._text}],
            style: "secondary"
        };
    }

}