
const dotenv = require("dotenv");
dotenv.config();
import fs from "fs";
import cheerio from "cheerio";
import {concat, filter, findIndex, flatMap, forEach, map, startsWith} from "lodash";
import {EStyles, IExpandableDescriptionProps} from "../../components/@def";

import {StrapiAPI} from "../../server/api/StrapiAPI";
import {log} from "../../server/core/Logger";
import {getNormalizedWhitespaceText} from "./Utils";
import {MarkdownBuilder} from "./AllMarkdownBuilder";
import {ButtonBuilder} from "./ButtonBuilder";



export class Main {
    private strapiApi = new StrapiAPI({url: process.env.BACKEND_URL});


    public async run(): Promise<void> {
        log.info("Starting...");
        const [post] = this.readPostFromFs();
        const policiesToAdd = this.getPoliciesToAdd();
        let content = concat(post.content, policiesToAdd);
        content = this.removeHeaders(content);
        content = this.expandWithButtons(content);
        post.content = content;
        this.strapiApi.createPost(post);
        log.info("Done");
    }

    private readPostFromFs() {
        return JSON.parse(fs.readFileSync("./tools/cap-automation/data/table-of-policies.json", "utf-8"));
    }

    private getPoliciesToAdd() {
        const postModel = JSON.parse(fs.readFileSync("./tools/cap-automation/data/postModel.json", "utf-8"));
        const postContents = flatMap(postModel, model => model.content);
        const policies = filter(postContents, content => content.__component == "component.collapsible-text-with-lead");
        const doneIndex = findIndex(policies, policy => startsWith(policy.title[0].text, "Policy 6.9"));
        const toAdd = policies.slice(doneIndex + 1);
        return toAdd;
    }

    private removeHeaders(contents: IExpandableDescriptionProps[]): IExpandableDescriptionProps[] {
        return map(contents, content => {
            const policyId = this.getPolicyId(content);
            const $ = cheerio.load(content.text[0].text);
            const header = $(":header").first();
            const headerText = getNormalizedWhitespaceText(header);
            if (headerText.startsWith("Policy " + policyId)) {
                header.remove();
            }
            content.text[0].text = $.html($("body").children());
            return content;
        });
    }

    private getPolicyId(content: IExpandableDescriptionProps) {
        const title = content.title[0].text;
        const policyId = title.match(/\d{1,2}\.\d{1,2}/);
        return policyId;
    }

    private expandWithButtons(contents: IExpandableDescriptionProps[]): IExpandableDescriptionProps[] {
        const newContent: IExpandableDescriptionProps[] = [];
        let lastChapter = 0;
        forEach(contents, content => {
            const policyId = this.getPolicyId(content);
            const chapter = parseInt(policyId[0]);
            if (chapter > lastChapter) {
                newContent.push(new MarkdownBuilder().text(`<h2>Chapter ${chapter}</h2><p>Hier geht es zum vollständigen Kapitel</p>`).build() as any);
                newContent.push(new ButtonBuilder().type("read-more").style(EStyles.secondary).link("/posts/cap-TODO").build() as any);
                lastChapter = chapter;
            }
            newContent.push(content);
        });
        return newContent;
    }
}

const main = new Main();
main.run();