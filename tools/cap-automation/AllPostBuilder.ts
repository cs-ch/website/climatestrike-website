import {compact, map} from "lodash";

import {IBuilder} from "./@def";
import {slugify} from "../../core/Utils";
import {ITranslation} from "../../core/@def";
import {titleToChapterMap} from "./Main";


export class EnglishPostBuilder implements IBuilder {
    private _date: string;
    private _author: ITranslation[];
    private _category: string = "unlisted";
    private _title: string;
    private _lead: string;
    private _imageId: number;
    private _content: IBuilder[] = [];
    private _relatedPosts: number[] = [];

    public static createIdentifierFromTitle(title: string) {
        if (!title) return "";
        const chapter = titleToChapterMap[title.trim()];
        return slugify(compact(["cap", chapter, title.trim()]).join("-"));
    }

    public date(date: string) {
        this._date = date;
        return this;
    }

    public category(category: string) {
        this._category = category;
        return this;
    }

    public title(title: string) {
        this._title = title;
        return this;
    }

    public lead(lead: string) {
        this._lead = lead;
        return this;
    }

    public addContent(contents: IBuilder[]) {
        this._content.push(...contents);
        return this;
    }

    public addAuthor(author: ITranslation) {
        this._author.push(author);
        return this;
    }

    public addImage(imageId: number) {
        this._imageId = imageId || null;
        return this;
    }

    public addRelatedPost(id: number) {
        this._relatedPosts.push(id);
        return this;
    }

    public build() {
        return {
            identifier: EnglishPostBuilder.createIdentifierFromTitle(this._title),
            date: this._date,
            category: this._category,
            title: this._title ? [{language: "all", text: this._title}] : [],
            lead: this._lead ? [{language: "all", text: this._lead}] : [],
            link: [],
            content: map(this._content, content => content.build()),
            author: this._author,
            thumbnail: [],
            image: this._imageId ? [{language: "all", image: {id: this._imageId}}] : [],
            relatedPosts: map(this._relatedPosts, id => ({id}))
        };
    }
}