
const dotenv = require("dotenv");
dotenv.config();
import fs from "fs";
import {compact, map} from "lodash";
import {EStyles, IExpandableDescriptionProps, IPostProps} from "../../components/@def";
import {ELanguages} from "../../core/@def";

import {StrapiAPI} from "../../server/api/StrapiAPI";
import {log} from "../../server/core/Logger";
import {HTMLBuilder} from "./AllMarkdownBuilder";
import {ButtonBuilder} from "./ButtonBuilder";



export class Main {
    private strapiApi = new StrapiAPI({url: process.env.BACKEND_URL});

    public async run(): Promise<void> {
        log.info("Starting...");
        const posts: IPostProps[] = this.readPostsFromFs();
        const updatedPosts = compact(map(posts, post => {
            const identifier = post.identifier;
            if (post.identifier.match(/^cap-[1-9]/) && post.identifier.match(/-executive-summary$/)) {
                const postlink = identifier.replace("-executive-summary", "");
                post.content = (this.expandWithButtons(post.content as any, postlink) as any);
                return post;
            }
        }));
        for (const post of updatedPosts) {
            await this.strapiApi.updatePost(post);
        }
        log.info("Done");
    }

    private readPostsFromFs() {
        return JSON.parse(fs.readFileSync("./tools/cap-automation/data/executive-summaries.json", "utf-8"));
    }

    private expandWithButtons(contents: IExpandableDescriptionProps[], link: string): IExpandableDescriptionProps[] {
        if (!link.includes("cap-0")) {
            contents.unshift(this.buildButton(link));
            contents.unshift(this.buildHtml());
        }
        return contents;
    }

    private buildButton(link: string): any {
        return new ButtonBuilder()
            .text("Vollständiges Kapitel", ELanguages.de)
            .text("Chapitre complet", ELanguages.fr)
            .text("Capitolo completo", ELanguages.it)
            .style(EStyles.secondary)
            .link(`/posts/${link}`)
            .build();
    }

    private buildHtml(): any {
        return new HTMLBuilder()
            .text(
                "<p><i>Du liest die Kurzfassung des Klimaaktionsplans. Hier kannst du das Kapitel in voller Länge lesen:</i></p>",
                ELanguages.de)
            .text(
                "<p><i>Vous êtes en train de lire le Sommaire Exécutif du Plan d’Action Climatique. Ici, vous pouvez lire le chapitre complet:</i></p>",
                ELanguages.fr)
            .text(
                "<p><i>Stai leggendo il Riassunto Esecutivo del Piano d’Azione Climatico. Qui, puoi leggere il capitolo completo:</i></p>",
                ELanguages.it)
            .build();
    }
}

const main = new Main();
main.run();