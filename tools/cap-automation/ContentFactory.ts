import {map} from "lodash";
import cheerio from "cheerio";

import {EModelEntryType, IBuilder, IModelEntry} from "./@def";
import {AllExpendableDescriptionBuilder} from "./AllExpandableDescriptionBuilder";
import {MarkdownBuilder} from "./AllMarkdownBuilder";
import {ButtonBuilder} from "./ButtonBuilder";
import {EStyles} from "../../components/@def";
import {slugify} from "../../core/Utils";

export class ContentFactory {
    public static create(model: IModelEntry[]): IBuilder[] {
        return map(model, entry => {
            if (entry.type === EModelEntryType.html) {
                return new MarkdownBuilder().text(entry.html);
            }
            else if (entry.type === EModelEntryType.policy) {
                const $ = cheerio.load(entry.html);
                const header = $(":header").first();
                const tagName = header.prop("tagName");
                const level = parseInt(tagName[1]);
                const title = header.text().replace(/\s+/g, " ");
                header.remove();
                return new AllExpendableDescriptionBuilder()
                    .title(title)
                    .titleLevel(level)
                    .text($.html($("body").children()));
            }
            else if (entry.type === EModelEntryType.execSummaryButton) {
                const {title, chapter} = entry.config;
                const execSummaryLink = `/posts/cap-${chapter}-${slugify(title)}-executive-summary`;
                return new ButtonBuilder()
                    .text("Executive Summary")
                    .style(EStyles.secondary)
                    .link(execSummaryLink);
            }
            else {
                throw new Error("ContentFactory - Don't know how to create " + entry.type);
            }
        });
    }
}