
const dotenv = require("dotenv");
dotenv.config();
import fs from "fs";
import {IExpandableDescriptionProps, IPostProps} from "../../components/@def";
import {ELanguages} from "../../core/@def";

import {StrapiAPI} from "../../server/api/StrapiAPI";
import {log} from "../../server/core/Logger";
import {HTMLBuilder} from "./AllMarkdownBuilder";


export class Main {
    private strapiApi = new StrapiAPI({url: process.env.BACKEND_URL});

    public async run(): Promise<void> {
        log.info("Starting...");
        const [post]: IPostProps[] = this.readPostsFromFs();
        post.content = (this.expandWithButtons(post.content as any) as any);
        await this.strapiApi.updatePost(post);
        log.info("Done");
    }

    private readPostsFromFs() {
        return JSON.parse(fs.readFileSync("./tools/cap-automation/data/cap-table-of-policies-content.json", "utf-8"));
    }

    private expandWithButtons(contents: IExpandableDescriptionProps[]): IExpandableDescriptionProps[] {
        contents.unshift(this.buildHtml());
        return contents;
    }

    private buildHtml(): any {
        return new HTMLBuilder()
            .text(
                "<h2>Header on top</h2>",
                ELanguages.all)
            .build();
    }
}

const main = new Main();
main.run();