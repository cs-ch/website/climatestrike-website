
const dotenv = require("dotenv");
dotenv.config();
import fs from "fs";
import path from "path";
import {filter, forEach, isNil, map} from "lodash";
import cheerio from "cheerio";
import mammoth from "mammoth";

import {DocxParser} from "./DocxParser";
import {EnglishPostBuilder} from "./AllPostBuilder";
import {EModelEntryType, IModelEntry, IPostModel} from "./@def";
import {ContentFactory} from "./ContentFactory";
import {createRequestOptions, getNormalizedWhitespaceText} from "./Utils";
import {StrapiAPI} from "../../server/api/StrapiAPI";
import {log} from "../../server/core/Logger";
import {LinkUpdater} from "./LinkUpdater";
import {ITranslation} from "../../core/@def";

export enum ETocModifyingMode {
    remove = "remove",
    removePageNumbersOnly = "removePageNumbersOnly"
}

let count = 1;

const POST_IMAGE_MAP = {
    Mobility: "./tools/cap-automation/data/images/cap-mobility.jpg"
};

const figuresToReplace = {
    "Figure ‑ Various pathways to net-zero consumption-based": "Figure 0-1: Various pathways to net-zero consumption-based",
    "Figure ‑ Potential and costs of different NETs in": "Figure 0-2: Potential and costs of different NETs in",
    "Figure ‑ Change in per capita CO2 emissions and": "Figure 0-3: Change in per capita CO2 emissions and",
    "Figure ‑ The Matterhorn and a pathway of emissions": "Figure 1-1: The Matterhorn and a pathway of emissions",
    "Figure ‑: The difference in modal choice between": "Figure 2-1: The difference in modal choice between",
    "Figure ‑: The emitted GHG emission per person": "Figure 2-2: The emitted GHG emission per person",
    "Figure ‑: Development of prices for public transport": "Figure 2-3: Development of prices for public transport",
    "Figure ‑: The amount of goods transported on": "Figure 2-4: The amount of goods transported on",
    "Figure ‑: World Seaborne trade carried by container": "Figure 2-5: World Seaborne trade carried by container",
    "Figure ‑: Global warming potential per passenger kilometer": "Figure 2-6: Global warming potential per passenger kilometer",
    "Figure ‑: Growth Flight Passengers (FSO 2019d).": "Figure 2-7: Growth Flight Passengers (FSO 2019d).",
    "Figure ‑: Custom plot for c=60": "Figure 2-8: Custom plot for c=60",
    "Figure ‑ In a condensed perimeter block neighborhood (approx": "Figure 3-1: In a condensed perimeter block neighborhood (approx",
    "Figure ‑ Evolution of land uses 1985-2009 (in": "Figure 3-2: Evolution of land uses 1985-2009 (in",
    "Figure ‑: Development of emissions in the main": "Figure 4-1: Development of emissions in the main",
    "Figure ‑: Visualization of the different policy instruments": "Figure 4-2: Visualization of the different policy instruments",
    "Figure ‑ Development of emissions under source category 2F": "Figure 4-3: Development of emissions under source category 2F",
    "Figure ‑ Various estimates for the potential of photovoltaics": "Figure 5-1: Various estimates for the potential of photovoltaics",
    "Figure ‑: Dispersion of the standardized PV gain": "Figure 5-2: Dispersion of the standardized PV gain",
    "Figure ‑: Distribution of the roof surface area": "Figure 5-3: Distribution of the roof surface area",
    "Figure ‑ Frequency of different annual gains per solar": "Figure 5-4: Frequency of different annual gains per solar",
    "Figure ‑: Distribution of the electricity yield correlating": "Figure 5-5: Distribution of the electricity yield correlating",
    "Figure ‑: PV electricity demand for the year": "Figure 5-6: PV electricity demand for the year",
    "Figure ‑: Proposal for a rapid expansion of": "Figure 5-7: Proposal for a rapid expansion of",
    "Figure ‑: Number of PV specialist planners required": "Figure 5-8: Number of PV specialist planners required",
    "Figure ‑: The development of the installed capacity": "Figure 5-9: The development of the installed capacity",
    "Figure ‑: Investment costs of the demand scenarios until 2035": "Figure 5-10: Investment costs of the demand scenarios until 2035",
    "Figure ‑: Investment costs of the demand scenarios until 2050": "Figure 5-11: Investment costs of the demand scenarios until 2050",
    "Figure ‑ Greenhouse gas emissions of the Swiss agricultural": "Figure 6-1: Greenhouse gas emissions of the Swiss agricultural",
    "Figure ‑ Consumption and greenhouse gas intensities of food": "Figure 6-2: Consumption and greenhouse gas intensities of food",
    "Figure ‑ Climate-Friendly Production System (original graphic in": "Figure 6-3: Climate-Friendly Production System (original graphic in",
    "Figure ‑ Global Negative Emissions used in the 1": "Figure 7-1: Global Negative Emissions used in the 1",
    "Figure ‑ Possible emission pathway for Switzerland with 13": "Figure 7-2: Possible emission pathway for Switzerland with 13",
    "Figure ‑ Schematic representation of CO2 removal from the": "Figure 7-3: Schematic representation of CO2 removal from the",
    "Figure ‑ *in Switzerland 500Mt CO2 in total (dark": "Figure 7-5: *in Switzerland 500Mt CO2 in total (dark",
    "Figure ‑ Time series of energy related emissions from 1990 until 2018 (FOEN 2019d)": "Figure 9-1: Time series of energy related emissions from 1990 until 2018 (FOEN 2019d)",
    "Figure ‑ Time series of energy related emissions from 1990 until 2018 Source: (FOEN 2019d)": "Figure 9-2: Time series of energy related emissions from",
    "Figure ‑ Production vs. consumption-based CO2 Emissions": "Figure 9-3: Production vs. consumption-based CO2 Emissions",
    "Figure ‑ Exemplary pathway towards zero emissions by 2030 (territorial)": "Figure 9-4: Exemplary pathway towards zero emissions by 2030 (territorial)",
    "Figure ‑ Exemplary pathway towards zero emissions by 2030 (consumption-based)": "Figure 9-5: Exemplary pathway towards zero emissions by 2030 (consumption-based)",
    "Figure ‑ Exemplary pathway towards net-zero emissions by 2030 (territorial)": "Figure 9-6: Exemplary pathway towards net-zero emissions by 2030 (territorial)",
    "Figure ‑ Exemplary pathway towards net-zero emissions by 2030 (consumption-based)": "Figure 9-7: Exemplary pathway towards net-zero emissions by 2030 (consumption-based)",
    "Figure ‑ World GDP over the last two millennia": "Figure 9-8: World GDP over the last two millennia",
    "Figure ‑ Annual total CO2 emissions, by world": "Figure 9-9: Annual total CO2 emissions, by world",
    "Figure ‑ Change in CO2 Emissions and GDP per": "Figure 9-10: Change in CO2 Emissions and GDP per",
    "Figure ‑ CO2 reductions through the characteristics catalog Climate": "Figure 9-11: CO2 reductions through the characteristics catalog Climate",
    "Figure ‑ International Climate Finance needs to be mobilized": "Figure 10-1: International Climate Finance needs to be mobilized"
};

const strapiApi = new StrapiAPI({url: process.env.BACKEND_URL});

const strapiImageUploadOptions = {
    convertImage: mammoth.images.imgElement(async (image) => {
        const extension = image.contentType.split("/")[1];
        log.debug("About to upload image number " + count);
        const fileName = `cap-${count++}.${extension}`;
        const buffer = await image.read();
        const requestOptions = createRequestOptions(buffer, fileName);
        try {
            const [uploadResponse] = await strapiApi.uploadImage(requestOptions);
            const url = `${process.env.BACKEND_URL}${uploadResponse.url}`;
            const srcSet = [`${url} ${uploadResponse.width}w`];
            forEach(uploadResponse.formats, (format) => {
                srcSet.push(`${process.env.BACKEND_URL}${(format.url)} ${format.width}w`);
            });
            return {
                src: `${process.env.BACKEND_URL}/uploads/` + uploadResponse.url,
                srcSet: srcSet.join(", ")

            };
        }
        catch (err) {
            log.error("Could not upload image to strapi. Error: " + err);
        }
    })
};

const styleMapOptions = {
    styleMap: [
        "p[style-name='Ü 4 ohne Navi'] => h4:fresh",
        "p[style-name='Ü1 ohne Nummerierung'] => h1:fresh"
    ]
};

const idRewrites = {
    ChapterCrossSectoralPolicies: {path: "/posts/cap-1-cross-sectoral-policies", fragment: null},
    ChapterMobility: {path: "/posts/cap-2-mobility", fragment: null},
    ChapterBuildings: {path: "/posts/cap-3-buildings-and-spatial-development", fragment: null},
    ChapterIndustry: {path: "/posts/cap-4-industry-and-service-sector", fragment: null},
    ChapterEnergy: {path: "/posts/cap-5-energy-supply", fragment: null},
    ChapterAgriculture: {path: "/posts/cap-agriculture-6and-food-system", fragment: null},
    ChapterNegativeEmissions: {path: "/posts/cap-7-negative-emissions", fragment: null},
    ChapterFinancialSector: {path: "/posts/cap-8-financial-sector", fragment: null},
    ChapterEPS: {path: "/posts/cap-9-economic-and-political-structures", fragment: null},
    ChapterInternationalCollaboration: {path: "/posts/cap-10-international-collaboration-and-climate-finance", fragment: null},
    ChapterEducation: {path: "/posts/cap-11-education", fragment: null},
    ChapterAdaptation: {path: "/posts/cap-12-adaptation", fragment: null},
    _Vision: {path: "/posts/cap-3-buildings-and-spatial-development", fragment: "vision"}
};

export const titleToChapterMap = {
    "Cross Sectoral Policies": 1,
    "Mobility": 2,
    "Buildings and Spatial Development": 3,
    "Industry and Service Sector": 4,
    "Energy Supply": 5,
    "Agriculture and Food System": 6,
    "Negative Emissions": 7,
    "Financial Sector": 8,
    "Economic and Political Structures": 9,
    "International Collaboration and Climate Finance": 10,
    "Education": 11,
    "Adaptation": 12
};


export class Main {
    private docxParser = new DocxParser({...strapiImageUploadOptions, ...styleMapOptions});
    private linkUpdater = new LinkUpdater(idRewrites);
    private strapiApi = strapiApi;
    private postTitleToImageId = {};

    public async run(): Promise<void> {
        log.info("Starting...");
        let html: string = await this.getHtml();
        log.debug("Got html and uploaded images...");
        html = this.replaceFigureNumbers(html);
        html = this.linkUpdater.updateLinksInHtml(html);
        const newModel = this.createNewModels(html);
        fs.writeFileSync("./tools/cap-automation/data/new-model.json", JSON.stringify(newModel, null, 4), "utf-8");
        log.debug("Created header model...");
        await this.uploadPostImages(newModel);
        const postModel = this.createPostModel(newModel);
        log.debug("Created post model...");
        fs.writeFileSync("./tools/cap-automation/data/postModel.json", JSON.stringify(postModel, null, 4), "utf-8");
        await this.uploadPostToStrapi(postModel);
        log.info("Done");
    }
    private replaceFigureNumbers(html: string): string {
        forEach(figuresToReplace, (value, key) => {
            forEach([key, key.replace("-", "&#x2011;")], _key => {
                html = html.replace(_key, value);
            });
        });
        return html;
    }


    private createNewModels(html: string) {
        const model = [];
        const $ = cheerio.load(html);
        $("h1").each((_i, elt) => {
            const header = $(elt);
            const postTitle = getNormalizedWhitespaceText(header);
            let postContent = $.html(header.nextUntil("h1"));
            const removePageNumbersOnly = ["Glossary", "Table of Figures", "List of Tables"].includes(postTitle);
            const mode = removePageNumbersOnly ? ETocModifyingMode.removePageNumbersOnly : ETocModifyingMode.remove;
            postContent = this.modifyToc(postContent, mode);
            const $content = cheerio.load(postContent);
            const parts: IModelEntry[] = [];
            if (!isNil(titleToChapterMap[postTitle])) {
                parts.push({type: EModelEntryType.html, html: "<hr><i> You are reading the full version of the Climate Action Plan. Do you just want to read the Executive Summary? Click here for the Executive Summary:</i>"});
                parts.push({type: EModelEntryType.execSummaryButton, config: {title: postTitle, chapter: titleToChapterMap[postTitle]}});
                parts.push({type: EModelEntryType.html, html: "<hr>"});
            }
            let htmlCollector = null;
            const contentHeaders = $content("h2,h3");
            if (contentHeaders.length) {
                contentHeaders.each((j, h2h3) => {
                    const selector = $content(h2h3);
                    if (j === 0) {
                        htmlCollector = $content.html(selector.prevUntil("h2,h3"));
                    }
                    const title = getNormalizedWhitespaceText(selector);
                    const content = $content.html(selector) + $content.html(selector.nextUntil("h2,h3"));
                    const isPolicy = title.match(/policy \d/i);
                    if (isPolicy) {
                        if (htmlCollector) {
                            parts.push({type: EModelEntryType.html, html: htmlCollector});
                            htmlCollector = "";
                        }
                        parts.push({type: EModelEntryType.policy, html: content});
                    }
                    else {
                        htmlCollector += content;
                    }
                });
                if (htmlCollector) {
                    parts.push({type: EModelEntryType.html, html: htmlCollector});
                }
            }
            else if (postContent) {
                parts.push({type: EModelEntryType.html, html: postContent});
            }
            if (parts.length) {
                model.push({title: getNormalizedWhitespaceText(header), parts});
            }
        });
        return model;
    }

    private async uploadPostImages(model: IPostModel[]): Promise<void> {
        for (const entry of model) {
            const filePath = POST_IMAGE_MAP[entry.title];
            if (filePath) {
                try {
                    const basename = path.basename(filePath);
                    const buffer = fs.readFileSync(filePath);
                    const requestOptions = createRequestOptions(buffer, basename);
                    const [response] = await this.strapiApi.uploadImage(requestOptions);
                    if (response && response.id) {
                        this.postTitleToImageId[entry.title] = response.id;
                    }
                }
                catch (err) {
                    log.error("Error uploading post image: " + filePath + " - " + err);
                }
            }
        }
    }

    private createPostModel(model: IPostModel[]) {
        const itemsWithTitle = filter(model, item => !!item.title);
        return map(itemsWithTitle, (item) => {
            const title = item.title;
            if (!title) {return null;}
            return new EnglishPostBuilder()
                .title(title)
                .date("2021-01-08")
                .addContent(ContentFactory.create(item.parts))
                .addImage(this.postTitleToImageId[title])
                .build();
        });
    }

    private async getHtml() {
        let html: string;
        const htmlPath = "./tools/cap-automation/data/doc.html";
        const optimized = false;
        if (optimized) {
            html = fs.readFileSync(htmlPath, "utf-8");
        }
        else {
            html = await this.docxParser.parse("../../../Downloads/Climate Action Plan Kopie 6.docx");
            fs.writeFileSync(htmlPath, html, "utf-8");
        }
        return html;
    }

    private modifyToc(html: string, mode: ETocModifyingMode) {
        const $ = cheerio.load(html);
        $("p a").each((_idx, elt) => {
            const anchor = $(elt);
            const href = anchor.attr().href;
            const linkText = anchor.text();
            const tocRegex = /\t\d{1,3}/;
            const isTableOfContent = href && linkText.match(tocRegex);
            if (isTableOfContent && mode === ETocModifyingMode.removePageNumbersOnly) {
                const textWithoutPageNumber = linkText.replace(tocRegex, "").replace(/\s+/g, " ");
                anchor.text(textWithoutPageNumber);
            }
            else if (isTableOfContent && ETocModifyingMode.remove) {
                anchor.remove();
            }
            else if (linkText.includes("\t")) {
                log.debug("Keeping link:" + href + ", " + linkText);
            }
        });
        $("p").each((_idx, elt: any) => {
            const selector = $(elt);
            const text = getNormalizedWhitespaceText(selector);
            const children = selector.children();
            if (!text && !children.length) {
                $(elt).remove();
            }
        });
        return $.html($("body").children());
    }

    private async uploadPostToStrapi(postModel: {identifier: string; date: string; category: string; title: {language: string; text: string;}[]; lead: {language: string; text: string;}[]; link: any[]; content: any[]; author: ITranslation[]; thumbnail: any[]; image: {language: string; image: {id: number;};}[]; relatedPosts: {id: number;}[];}[]) {
        for (let i = postModel.length - 1; i >= 0; i--) {
            log.debug("Posts left to do: " + (i + 1) + " of " + (postModel.length + 1));
            const post = postModel[i];
            try {
                const response = await this.strapiApi.createPost(post);
                if (i > 0) {
                    (postModel[i - 1] as any).relatedPosts = [{id: response.id}];
                }
            }
            catch (err) {
                log.error("Error occurred for post " + (i + 1) + ": " + err);
            }
        }
    }
}

// const main = new Main();
// main.run();