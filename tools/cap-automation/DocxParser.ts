import mammoth from "mammoth";

export class DocxParser {

    constructor(private options?: any) { }

    public parse(path: string): Promise<string> {

        return mammoth.convertToHtml({path}, this.options)
            .then(result => {
                return result.value; // The generated HTML
            });
    }
}

