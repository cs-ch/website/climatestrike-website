import FormData from "form-data";


export const createRequestOptions = (buffer: Buffer, fileName: string) => {
    const formData = new FormData();
    formData.append("files", buffer, fileName);
    formData.append("fileInfo", `{"alternativeText":"","caption":"","name":"${fileName}"}`);
    const requestOptions = {
        method: "POST",
        body: formData,
        redirect: "follow"
    };
    return requestOptions;
};

export const getNormalizedWhitespaceText = (item: cheerio.Cheerio) => {
    return item.text().replace(/\s+/g, " ");
};