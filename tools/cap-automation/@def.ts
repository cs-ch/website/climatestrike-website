export enum EModelEntryType {
    html = "html",
    policy = "policy",
    execSummaryButton = "execSummaryButton",
}

export interface IModelEntry {
    type: EModelEntryType;
    html?: string;
    config?: any;
}


export interface IPostModel {
    title: string;
    parts: IModelEntry[];
}

export interface IBuilder {
    build(): any;
}