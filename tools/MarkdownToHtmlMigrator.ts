

import dotenv from "dotenv";
dotenv.config();
import {Remarkable} from "remarkable";


import {createConnection, Connection, Column, PrimaryGeneratedColumn, Entity} from "typeorm";
import {ELanguages} from "../core/@def";

@Entity("components_config_long_translations")
class LongTranslation {
    @PrimaryGeneratedColumn()
    public id: number;

    @Column()
    public language: ELanguages;

    @Column()
    public text: string;
}

async function main() {
    const connection: Connection = await createConnection({
        type: "mysql",
        host: process.env.DATABASE_HOST,
        port: parseInt(process.env.DATABASE_PORT),
        username: process.env.DATABASE_USERNAME,
        password: process.env.DATABASE_PASSWORD,
        database: process.env.DATABASE_NAME,
        entities: [LongTranslation]
    });
    const repository = await connection.getRepository(LongTranslation);
    const translations = await repository.find();
    const markdownParser = new Remarkable("full", {
        html: true,
        typographer: true,
    });
    for (const translation of translations) {
        const htmlOutput = markdownParser.render(translation.text);
        translation.text = htmlOutput;
        if (htmlOutput) {
            await repository.save(translation);
        }
        else {
            console.error(`There is no content for id ${translation.id}, initial content was ${translation.text}`);
        }
    }
    await connection.close();
}

main()
    .then(() => console.log("DONE"))
    .catch(console.error);