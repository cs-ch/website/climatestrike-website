import {IContactGroup, IPostProps, IEventGroup} from "../components/@def";
import {IStrapiPage} from "../server/api/@def";


export type TStrapiPost = Omit<IPostProps, "createdAt" | "updatedAt" | "content"> & {id?: number; content: (any & {__component: string})[]};

export interface ISeedModel {
    pages?: IStrapiPage[];
    contactGroups?: IContactGroup[];
    posts?: TStrapiPost[];
    eventGroups?: IEventGroup[];
}

export interface IStrapiConfig {
    baseURL?: string;
    identifier?: string;
    password?: string;
}

export enum EStrapiRoute {
    contacts = "contacts",
    contactGroups = "contact-groups",
    contents = "contents",
    events = "events",
    eventGroups = "event-groups",
    pages = "pages",
    posts = "posts"
}

export interface IMigrationCtx {
    [index: string]: any[]
}

export interface IStrapiAuth {
    identifier?: string;
    password?: string;
}

interface IMigrationModifier {
    type: EStrapiRoute;
    migrate?<T>(ctx: IMigrationCtx, item: T): T;
}

export interface IMigratorModel {
    routes: EStrapiRoute[];
    source: IStrapiConfig;
    target: IStrapiConfig;
    migrators: IMigrationModifier[];
}
