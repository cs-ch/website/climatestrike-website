module.exports = {
    i18n: {
        defaultLocale: "de",
        locales: ["de", "fr", "it"],
    },
};