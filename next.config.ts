// next.config.js 
import path from "path";

import {i18n} from "./next-i18next.config";


export default {
    i18n,
    sassOptions: {
        includePaths: [path.join(__dirname, "styles")],
    },
    webpack(config) {
        return config;
    },
};