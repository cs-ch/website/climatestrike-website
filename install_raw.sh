rm -rf ~/tmp
mkdir ~/tmp
pushd ~/tmp

# Setup
git clone $CI_REPOSITORY_URL
cd $CI_PROJECT_NAME
git checkout $CI_COMMIT_SHA
npm i
npm run build
popd

# Copy to destination
shopt -s dotglob
cp -rf ~/tmp/$CI_PROJECT_NAME/node_modules .
cp -rf ~/tmp/$CI_PROJECT_NAME/public .
cp -rf ~/tmp/$CI_PROJECT_NAME/package.json .
cp -rf ~/tmp/$CI_PROJECT_NAME/next.config.js .
cp -rf ~/tmp/$CI_PROJECT_NAME/build .
shopt -u dotglob

pm2 restart app
