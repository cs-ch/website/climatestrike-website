import * as Yup from "yup";

Yup.addMethod(Yup.string, "phone", function () {
    return (this as Yup.StringSchema).test({
        name: "phone",
        exclusive: true,
        message: "phone-invalid",
        test: (value) => {
            return Yup.string().trim().matches(/[0-9+ \-]{5,20}/).isValidSync(value);
        }
    });
});


interface IValidationFactoryParams {
    required?: boolean;
    input?: any;
}

export class ValidationFactory {
    private static _instance: ValidationFactory;

    public static instance(): ValidationFactory {
        if (!this._instance) {
            ValidationFactory._instance = new ValidationFactory();
        }
        return this._instance;
    }

    public create(type: string, params: IValidationFactoryParams) {
        const {required, input} = params;
        let validation: Yup.AnySchema<any> & {required(message: string): any; nullable(): any} = null;
        switch (type) {
            case "text":
                validation = Yup.string(); break;
            case "email":
                validation = Yup.string().email("email-invalid"); break;
            case "zipch": {
                const message = "zip-invalid";
                validation = Yup.number().typeError(message).integer(message).min(1000, message).max(9999, message); break;
            }
            case "phone":
                validation = (Yup.string() as any).phone(); break;
            case "object":
                validation = Yup.object().shape(input); break;
            case "dropdown":
                return null;
            case "checkboxes":
                return null;
            case "boolean":
                return Yup.boolean();
            case "keepMePosted":
                return Yup.boolean();
            case "acceptPrivacyPolicy":
                return Yup.boolean().oneOf([true], "required-field");
            default:
                throw new Error("Type not implemented: " + type);
        }
        if (required) {
            return validation.required("required-field");
        }
        return validation.nullable();
    }
}