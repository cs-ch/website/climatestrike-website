import React from "react";
import {
    Facebook, Instagram, Twitter, YouTube,
    Language, MailOutline, WhatsApp, Telegram,
    Forward, Menu, Close, ControlPoint, ExpandMore,
    ExpandLess, Check, Clear, DevicesOther,
    WbSunny, Brightness2, Share, ArrowForwardIos,
    ArrowBackIos, VerticalAlignTop, Search, X
} from "@mui/icons-material";

import {SvgIconProps} from "@mui/material/SvgIcon/SvgIcon";

export class IconFactory {
    private static _instance: IconFactory = new IconFactory();
    public static instance() {
        return IconFactory._instance;
    }

    public create(type: string, props?: SvgIconProps) {
        switch (type) {
            case "share": return <Share className={type} {...props} />;
            case "facebook": return <Facebook className={type} {...props} />;
            case "instagram": return <Instagram className={type} {...props} />;
            case "twitter": return <Twitter className={type} {...props} />;
            case "x": return <X className={type} {...props} />;
            case "youtube": return <YouTube className={type} {...props} />;

            case "website": return <Language className={type} {...props} />;
            case "email": return <MailOutline className={type} {...props} />;
            case "whatsapp": return <WhatsApp className={type} {...props} />;
            case "telegram": return <Telegram className={type} {...props} />;
            case "discord": return <img {...props as any} src="/static/images/discord.svg" />;
            case "other": return <Forward className={type} {...props} />;

            case "activity": return <img {...props as any} src="/static/images/activity.svg" />;
            case "meetup": return <img {...props as any} src="/static/images/meetup.svg" />;
            case "strike": return <img {...props as any} src="/static/images/strike.svg" />;
            case "digital": return <DevicesOther className={type} {...props} />;

            case "download-doc-icon": return <img {...props as any} src="/static/images/download-doc-icon.svg" />;
            case "download-other-icon": return <img {...props as any} src="/static/images/download-other-icon.svg" />;
            case "download-xls-icon": return <img {...props as any} src="/static/images/download-xls-icon.svg" />;
            case "download-image-icon": return <img {...props as any} src="/static/images/download-image-icon.svg" />;
            case "download-pdf-icon": return <img {...props as any} src="/static/images/download-pdf-icon.svg" />;
            case "download-zip-icon": return <img {...props as any} src="/static/images/download-zip-icon.svg" />;
            case "download-video-icon": return <img {...props as any} src="/static/images/download-video-icon.svg" />;
            case "download-ppt-icon": return <img {...props as any} src="/static/images/download-ppt-icon.svg" />;

            case "menu": return <Menu className={type} {...props} />;
            case "close": return <Close className={type} {...props} />;
            case "plus": return <ControlPoint className={type} {...props} />;
            case "expand": return <ExpandMore className={type} {...props} />;
            case "collapse": return <ExpandLess className={type} {...props} />;
            case "check": return <Check className={type} {...props} />;
            case "clear": return <Clear className={type} {...props} />;
            case "forward": return <ArrowForwardIos className={type} {...props} />;
            case "backward": return <ArrowBackIos className={type} {...props} />;
            case "to-top": return <VerticalAlignTop className={type} {...props} />;

            case "sun": return <WbSunny className={type} {...props} />;
            case "moon": return <Brightness2 className={type} {...props} />;

            case "search": return <Search className={type} {...props} />;

            default:
                throw new Error("IconFactory - No icon found: " + type);
        }

    }
}