import React from "react";

import {ICON_MENU_ITEM, LANGUAGES} from "../core/Constants";
import {IContribution, IAppContext} from "../components/@def";
import {LanguageSelector} from "../components/Selector";
import {SocialLinks} from "../components/SocialLinks";
import NewsletterForm from "../components/forms/NewsletterForm";
import {IconMenuItem} from "../components/contributions/IconMenuItem";
import {HierarchicalLinks} from "../components/contributions/HierarchicalLinks";
import {Footnotes} from "../components/contributions/Footnotes";
import {MenuItems} from "../components/contributions/MenuItems";
import {DarkLightToggle} from "../components/contributions/DarkLightToggle";


export class ContributionFactory {
    public create(type: string, context: IAppContext, config: any): IContribution {
        switch (type) {
            case LANGUAGES:
                return {
                    identifier: type,
                    provider: () => <LanguageSelector key={type} items={config.languages} />
                };
            case "social":
                return {
                    identifier: type,
                    provider: () => <SocialLinks key={type} {...config} />
                };
            case ICON_MENU_ITEM:
                return {
                    identifier: type,
                    provider: () => <IconMenuItem key={type} {...config} type={type} />
                };
            case "menu-items":
                return {
                    identifier: type,
                    provider: () => <MenuItems
                        key={type}
                        pages={context.pages}
                        activePageIdentifier={context.activePageIdentifier}
                        location={config.location}
                    />
                };
            case "light-dark-toggle":
                return {
                    identifier: type,
                    provider: () => <DarkLightToggle />
                };
            case "newsletter":
                return {
                    identifier: type,
                    provider: () => <NewsletterForm
                        key={type}
                        identifier="newsletter-form"
                        submitUrl="api/register-newsletter"
                        text={null}
                        context={{
                            items: [],
                            itemIdentifier: "newsletter-form",
                            pageIdentifier: "footer",
                            query: null
                        }}
                    />
                };
            case "hierarchical-links":
                return {
                    identifier: type,
                    provider: () => <HierarchicalLinks key={type} {...config} />
                };
            case "footnotes":
                return {
                    identifier: type,
                    provider: () => <Footnotes key={type} {...config} />
                };
            default:
                throw new Error(`ContributionFactory - Cannot find type '${type}'`);
        }
    }
}