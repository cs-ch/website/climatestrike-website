import React from "react";
import dynamic from "next/dynamic";

import {IPageContext, EEventView} from "../components/@def";


export class ContentFactory {
    public create(componentId: string, config: any, context: IPageContext): React.ReactNode {
        switch (componentId) {
            case "headline": {
                const Headline = dynamic(() => import("../components/Headline"), {});
                return <Headline context={context} {...config} />;
            }
            case "audio-player": {
                const AudioPlayer = dynamic(() => import("../components/AudioPlayer"), {});
                return <AudioPlayer context={context} {...config} />;
            }
            case "form": {
                const Form = dynamic(() => import("../components/forms/Form"), {});
                return <Form context={context} {...config} />;
            }
            case "newsletter-form": {
                const NewsletterForm = dynamic(() => import("../components/forms/NewsletterForm"), {});
                return <NewsletterForm context={context} {...config} />;
            }
            case "landing-link": {
                const LandingLink = dynamic(() => import("../components/LandingLink"), {});
                return <LandingLink context={context} {...config} />;
            }
            case "post-section": {
                const PostSection = dynamic(() => import("../components/PostSection"), {});
                return <PostSection context={context} {...config} />;
            }
            case "expandable-description":
            case "collapsible-text-with-lead": {
                const ExpandableDescription = dynamic(() => import("../components/ExpandableDescription"), {});
                return <ExpandableDescription context={context} {...config} />;
            }
            case "title": {
                const Title = dynamic(() => import("../components/Title"), {});
                return <Title context={context} {...config} />;
            }
            case "demand-item": {
                const DemandItem = dynamic(() => import("../components/DemandItem"), {});
                return <DemandItem context={context} {...config} />;
            }
            case "fact-item": {
                const FactItem = dynamic(() => import("../components/FactItem"), {});
                return <FactItem context={context} {...config} />;
            }
            case "icon-title": {
                const IconTitle = dynamic(() => import("../components/IconTitle"), {});
                return <IconTitle context={context} {...config} />;
            }
            case "join-item": {
                const JoinItem = dynamic(() => import("../components/JoinItem"), {});
                return <JoinItem context={context} {...config} />;
            }
            case "image-with-caption":
            case "large-image": {
                const ImageWithCaption = dynamic(() => import("../components/ImageWithCaption"), {});
                return <ImageWithCaption context={context} {...config} />;
            }
            case "atlas": {
                const Map = dynamic(() => import("../components/Map"), {});
                return <Map context={context} {...config} />;
            }
            case "milestones": {
                const Milestones = dynamic(() => import("../components/Milestones"), {});
                return <Milestones context={context} {...config} />;
            }
            case "contact-data": {
                const ContactData = dynamic(() => import("../components/ContactData"), {});
                return <ContactData context={context} {...config} />;
            }
            case "events": {
                const Events = dynamic(() => import("../components/Events"), {});
                return <Events context={context} {...config} />;
            }
            case EEventView.timeline: {
                const Timeline = dynamic(() => import("../components/Timeline"), {});
                return <Timeline context={context} {...config} />;
            }
            case EEventView.createEvent: {
                const CreateEventForm = dynamic(() => import("../components/forms/CreateEventForm"), {});
                return <CreateEventForm context={context} {...config} />;
            }
            case "text":
            case "titled-text": {
                const Text = dynamic(() => import("../components/Text"), {});
                return <Text context={context} {...config} />;
            }
            case "iframe": {
                const Iframe = dynamic(() => import("../components/Iframe"), {});
                return <Iframe context={context} {...config} />;
            }
            case "posts":
            case "news": {
                const News = dynamic(() => import("../components/News"), {});
                return <News context={context} {...config} />;
            }
            case "collapsible-text": {
                const CollapsibleText = dynamic(() => import("../components/CollapsibleText"), {});
                return <CollapsibleText context={context} {...config} />;
            }
            case "markdown": // Should be html, here for legacy support
            case "rich-text": {
                const LongText = dynamic(() => import("../components/HTML"), {});
                return <LongText {...config} />;
            }
            case "long-translation": {
                const LongText = dynamic(() => import("../components/HTML"), {});
                return <LongText text={config} />;
            }
            case "references": {
                const References = dynamic(() => import("../components/References"), {});
                return <References context={context} {...config} />;
            }
            case "raise-now-donation-widget": {
                const RaiseNowDonationWidget = dynamic(() => import("../components/RaiseNowDonationWidget"), {});
                return <RaiseNowDonationWidget context={context} {...config} />;
            }
            case "titled-subtitled-icon-item": {
                const TitledSubtitledIconItem = dynamic(() => import("../components/TitledSubtitledIconItem"), {});
                return <TitledSubtitledIconItem context={context} {...config} />;
            }
            case "link":
            case "button": {
                const LinkButton = dynamic(() => import("../components/Button"), {});
                return <LinkButton {...config} />;
            }
            case "download-section": {
                const DownloadSection = dynamic(() => import("../components/DownloadSection"), {});
                return <DownloadSection context={context} {...config} />;
            }
            case "petition": {
                const Petition = dynamic(() => import("../components/Petition"), {});
                return <Petition context={context} {...config} />;
            }
            case "slides": {
                const Slides = dynamic(() => import("../components/Slides"), {});
                return <Slides context={context} {...config} />;
            }
            default:
                throw new Error(`ContentFactory - Cannot find type '${componentId}'`);
        }
    }
}