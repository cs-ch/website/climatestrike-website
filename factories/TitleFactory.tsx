import React from "react";

import {ETitleLevel} from "../components/@def";
import {H2, H3, H4, H5, H6} from "../components/Typography";


export class TitleFactory {
    private static _instance: TitleFactory = new TitleFactory();
    public static instance() {
        return TitleFactory._instance;
    }

    public create(level: ETitleLevel, text: string, className?: string) {
        if (!text) return null;
        if (!level) return <H2 className={className} text={text} />;
        switch (level) {
            case ETitleLevel.two: return <H2 className={className} text={text} />;
            case ETitleLevel.three: return <H3 className={className} text={text} />;
            case ETitleLevel.four: return <H4 className={className} text={text} />;
            case ETitleLevel.five: return <H5 className={className} text={text} />;
            case ETitleLevel.six: return <H6 className={className} text={text} />;
            default:
                throw new Error("TitleFactory - No icon found: " + level);
        }
    }
}