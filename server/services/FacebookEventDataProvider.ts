import {IFacebookEventData} from "../../core/@def";
import {HttpService} from "../../core/HttpService";


interface IFacebookRawData {
    startDate: string;
    endDate: string;
    name: string;
    location: {
        "@type": "Place",
        name: string;
        address?: {
            "@type"?: "PostalAddress",
            addressCountry?: string;
            addressLocality?: string;
            postalCode?: string;
            streetAddress?: string;
        };
    },
    description: string;
}

export class FacebookEventDataProvider {
    private $http = HttpService.instance();

    public async getData(link: string): Promise<IFacebookEventData> {
        try {
            const eventId = link.match(/(\d)+/)[0];
            const requestLink = `https://www.facebook.com/events/${eventId}/`;
            const response = await this.$http.get<string>({url: requestLink});
            const responseText = await response.text();
            const json = responseText.match(/>{.*type":"Event.*}/gm);
            if (json && json.length) {
                const data = JSON.parse(json[0].substr(1)) as IFacebookRawData;
                const location = data.location;
                const address = location.address || {};
                const locationParts = [];
                if (location.name) {locationParts.push(location.name);}
                if (address.streetAddress) {locationParts.push(address.streetAddress);}
                const result: IFacebookEventData = {
                    title: data.name,
                    description: data.description,
                    startDate: data.startDate,
                    endDate: data.endDate,
                    location: locationParts.join(", "),
                    city: address.addressLocality,
                    zip: address.postalCode
                };
                return Promise.resolve(result);
            }
            else {
                return Promise.reject("Could not find event data");
            }
        }
        catch (err) {
            return Promise.reject("Could not connect");
        }
    }
}