import {IDataServiceOptions} from "./@def";
import {ICache, ICacheOptions} from "../core/@def";
import {Cache} from "../core/Cache";
import {ICreateEventData, IPageModelStrategy, IFormData, IDataDependency} from "../../core/@def";
import {PageModelFactory} from "../core/PageModelFactory";
import {configuration} from "../../core/Configuration";


class DataService implements IPageModelStrategy {
    private delegate: IPageModelStrategy;
    private cache: ICache;
    private static _instance: DataService;

    public static instance(options: IDataServiceOptions): DataService {
        if (!DataService._instance) {
            DataService._instance = new DataService(options);
        }
        return DataService._instance;
    }

    constructor(options: IDataServiceOptions) {
        this.cache = new Cache(options.cache);
    }

    public setDelegate(delegate: IPageModelStrategy) {
        this.delegate = delegate;
    }

    public getPages(skeletonOnly: boolean = false) {
        if (!this.delegate) {
            throw new Error("DataService - No strategy set");
        }
        const provider = () => this.delegate.getPages();
        const cacheKey = "pages" + (skeletonOnly ? "-skeleton" : "");
        return this.cache.get(cacheKey, provider);
    }

    public getContent(id: number, dataDependencies?: IDataDependency[]) {
        if (!this.delegate) {
            throw new Error("DataService - No strategy set");
        }
        const key = ["content", id].join("-");
        const provider = () => this.delegate.getContent(id, dataDependencies);
        return this.cache.get(key, provider);
    }

    public getPosts(skeletonOnly: boolean = false) {
        if (!this.delegate) {
            throw new Error("DataService - No strategy set");
        }
        const provider = () => this.delegate.getPosts(skeletonOnly);
        const cacheKey = "posts" + (skeletonOnly ? "-skeleton" : "");
        return this.cache.get(cacheKey, provider);
    }

    public getPost(id: string) {
        if (!this.delegate) {
            throw new Error("DataService - No strategy set");
        }
        const key = ["post", id].join("-");
        const provider = () => this.delegate.getPost(id);
        return this.cache.get(key, provider);
    }

    public submitForm(data: IFormData) {
        if (!this.delegate) {
            throw new Error("DataService - No strategy set");
        }
        return this.delegate.submitForm(data);
    }

    public getFormSubmissionCount(id: string) {
        if (!this.delegate) {
            throw new Error("DataService - No strategy set");
        }
        return this.delegate.getFormSubmissionCount(id);
    }

    public registerEvent(data: ICreateEventData) {
        if (!this.delegate) {
            throw new Error("DataService - No strategy set");
        }
        return this.delegate.registerEvent(data);
    }

    public clearCache() {
        this.cache.clear();
    }
}

const cacheEnabled = process.env.CACHE_ENABLED == "true";
const cacheOptions: ICacheOptions = {
    enabled: cacheEnabled,
    max: 200,
    maxAge: 2000 // 2 seconds
};

export const dataService = DataService.instance({cache: cacheOptions});
const dataSourceStrategy = new PageModelFactory().create(configuration.dataSources.type, configuration.dataSources.config);
dataService.setDelegate(dataSourceStrategy);