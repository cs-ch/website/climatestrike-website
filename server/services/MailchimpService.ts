import {forEach, toUpper, toLower} from "lodash";

import {IMailchimpService} from "./@def";
import {log} from "../core/Logger";
import {IDictionary, IHttpService} from "../../core/@def";
import {HttpService} from "../../core/HttpService";


export class MailchimpService implements IMailchimpService {
    private $http: IHttpService;
    constructor(private apiKey: string, private idLanguageMapping: IDictionary<string>,
        private instance: string, private mapping: IDictionary<string>) {
        this.$http = HttpService.instance({
            baseURL: `https://${this.instance}.api.mailchimp.com/3.0/`
        });
        this.$http.setAuthHeader("Basic " + Buffer.from("any:" + this.apiKey).toString("base64"));
    }

    public async register(data: any): Promise<void> {
        const language = data.language;
        const formData: any = {};
        forEach(data, (value, key) => {
            const formKey = this.mapping[key];
            if (formKey && value) formData[formKey] = value;
        });
        if (language) formData[this.mapping.language] = toUpper(language);
        const body = {
            "email_address": data.email,
            "status": "pending",
            "merge_fields": formData
        };
        // logic from here: https://www.codementor.io/@mattgoldspink/integrate-mailchimp-with-nodejs-app-du10854xp
        const listUniqueId = this.idLanguageMapping[toLower(language)];
        const url = `lists/${listUniqueId}/members/`;
        const response = await this.$http.post<any>({
            url,
            body
        });
        if (response.status < 300) {
            log.debug("MailchimpService - Registation sucessfull");
            return Promise.resolve();
        }
        else if (response.status === 400) {
            const responseData = await response.json();
            if (responseData.title === "Member Exists") {
                log.debug("MailchimpService - Already registered");
                return Promise.resolve();
            }
            else {
                const message = "MailchimpService - Error during registration: " + responseData;
                log.error(message);
                return Promise.reject(message);
            }
        }
        else {
            return Promise.reject("MailchimpService - Unknown error");
        }
    }
}
