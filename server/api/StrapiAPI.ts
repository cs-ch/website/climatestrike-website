import {map, sortBy, find, keys, forEach, includes, every, isObjectLike, isPlainObject, isString} from "lodash";
import nodefetch from "node-fetch";

import {IImageTranslation, IStrapiConfig, IStrapiContent, IStrapiFile, IStrapiImageFormat} from "./@def";
import {
    ICreateEventData, independentId, IPageModelStrategy,
    IPage, IHttpService, ITranslationDict, ELanguages, ITranslation, IHttpResponse, IFormData, IStrapiResponse, IDataDependency, IAttributeWrapper
} from "../../core/@def";
import {HttpService} from "../../core/HttpService";
import {ILinkProps, IEventGroup, IEvent, IPostProps} from "../../components/@def";
import {isAttributeWrapper, isContentDelegation, isImageTranslations, isStrapiFile, isStrapiResponseWrapper} from "../core/Guards";
import {log} from "../core/Logger";
import {uuid} from "../../core/Utils";
import {DateTime} from "../../core/DateTime";
import {BACKEND_DATE_FORMAT} from "../../core/Constants";

global.fetch = nodefetch as any;

export interface IStrapiImageUploadResponse {
    id: number;
    name: string;
    alternativeText: string;
    caption: string;
    width: number;
    height: number;
    formats: {
        thumbnail: {
            name: string;
            hash: string;
            ext: string;
            mime: string;
            width: number;
            height: number;
            size: number;
            path: string;
            url: string;
        },
        large: {
            name: string;
            hash: string;
            ext: string;
            mime: string;
            width: number;
            height: number;
            size: number;
            path: string;
            url: string;
        },
        medium: {
            name: string;
            hash: string;
            ext: string;
            mime: string;
            width: number;
            height: number;
            size: number;
            path: string;
            url: string;
        },
        small: {
            name: string;
            hash: string;
            ext: string;
            mime: string;
            width: number;
            height: number;
            size: number;
            path: string;
            url: string;
        },
    },
    hash: string;
    ext: string;
    mime: string;
    size: number;
    url: string;
    previewUrl: string;
    provider: string;
    provider_metadata: string;
    created_at: string;
    updated_at: string;
}

export class StrapiAPI implements IPageModelStrategy {
    private baseUrl: string;
    private mediaUrl: string;
    private $http: IHttpService;
    private shouldAuthenticate = process.env.BACKEND_USERNAME && process.env.BACKEND_PASSWORD;
    private isAuthenticated = false;

    constructor(config: IStrapiConfig) {
        this.baseUrl = config.url;
        this.mediaUrl = config.mediaUrl;
        this.$http = new HttpService({baseURL: this.baseUrl});
    }

    public async getPages() {
        const path = "/api/pages";
        const pages = await this.strapiFetchAndAdjust<IPage[]>(() => this.$http.get({url: this.getAllRoute(path)}));
        return pages;
    }


    public async getContent(id: number, dataDependencies?: IDataDependency[]) {
        const depData = [];
        if (dataDependencies) {
            for (const dep of dataDependencies) {
                const data = await this.strapiFetchAndAdjust(() => this.$http.get<any>({url: dep.route}));
                depData.push({identifier: dep.identifier, data});
            }
        }
        const response = await this.strapiFetchAndAdjust<IStrapiContent[]>(() => this.$http.get({url: this.getAllRoute(`/api/contents?filters[page][id][$eq]=${id}`)}));
        const contents = map(response, (content) => {
            const config = content?.data?.[0];
            if (isContentDelegation(config)) {
                log.debug("Delgation found: " + content.identifier);
                const depDp = find(depData, dp => dp.identifier == config.childIdentifier);
                (config as any).data = depDp?.data ?? {};
            }
            return {
                ...content,
                grouping: content.grouping || uuid(),
                type: config.type,
                config: config
            };
        });
        const sorted = sortBy(contents, content => content.sortId);
        return Promise.resolve(sorted);
    }

    public async getPosts(skeletonOnly: boolean) {
        const path = "/api/posts";
        const response = await this.strapiFetchAndAdjust<IPostProps[]>(() => this.$http.get({url: this.getAllRoute(path)}));
        return Promise.resolve(map(response, entry => {
            if (skeletonOnly && entry.content) {
                delete entry.content;
            }
            return entry;
        }));
    }

    public async getPost(id: string): Promise<IPostProps> {
        try {
            const [post] = await this.strapiFetchAndAdjust<IPostProps[]>(() => this.$http.get({url: `/api/posts?filters[identifier][$eq]=${id}&pagination[limit]=1&populate=*`}));
            return Promise.resolve(post);

        }
        catch (err) {
            return Promise.reject(err);
        }
    }

    public async registerEvent(data: ICreateEventData): Promise<void> {
        const linkTranslations = this.mapToTranslations(data.link);
        const link: ILinkProps = {
            type: every(linkTranslations, t => includes(t.text, "facebook.com")) ? "facebook" : "website",
            link: linkTranslations
        };
        let eventGroupId = data.parentId;
        const uid = uuid();
        if (eventGroupId === independentId) {
            const groupCreateArgs: IEventGroup = {
                identifier: `${this.getIdentifier(data.title, uid)} (Event-group)`,
                title: this.mapToTranslations(data.title),
                category: data.category,
                description: this.mapToTranslations(data.description),
                start: DateTime.create(data.startDate).format(BACKEND_DATE_FORMAT),
                end: data.endDate ? DateTime.create(data.endDate).format(BACKEND_DATE_FORMAT) : null,
                events: [],
                multipleChildren: false
            };
            const eventGroup = await this.strapiFetchAndAdjust(() => this.$http.post<IStrapiResponse<{id: number}>>({url: "/api/event-groups", body: {data: groupCreateArgs}}));
            eventGroupId = eventGroup.id;
        }
        const eventArgs: IEvent = {
            identifier: `${this.getIdentifier(data.title, uid)} (Event)`,
            description: this.mapToTranslations(data.description),
            start: data.startDate as any,
            end: data.endDate as any,
            published: false,
            location: this.mapToTranslations(data.location),
            zip: data.zip ? parseInt(data.zip) : null,
            city: this.mapToTranslations(data.city),
            link: linkTranslations.length ? link : null,
            contact: data.contact
        };
        (eventArgs as any).event_group = {id: eventGroupId};
        await this.strapiFetchAndAdjust(() => this.$http.post<IStrapiResponse<{id: number}>>({url: "/api/events", body: {data: eventArgs}}));
    }

    public async submitForm(data: IFormData): Promise<void> {
        const formData = {identifier: data.identifier, data: data};
        delete formData.data.identifier;
        await this.strapiFetchAndAdjust(() => this.$http.post<IStrapiResponse<{id: number}>>({url: "/api/form-data", body: {data: formData}}));
    }

    public async getFormSubmissionCount(formId: string): Promise<number> {
        return this.secureFetch(() => this.$http.get<number>({url: `/api/form-data/count?filters[identifier][$eq]=${formId}`}));
    }

    public async uploadImage(data: any): Promise<IStrapiImageUploadResponse[]> {
        const response = await this.strapiFetchAndAdjust<IStrapiImageUploadResponse[]>(
            () => this.$http.fetch("/api/upload", data));
        return Promise.resolve(response);
    }

    public async createPost(data: any): Promise<{id: number}> {
        return this.strapiFetchAndAdjust(() => this.$http.post({url: "/api/posts", body: data}));
    }

    public async updatePost(data: any): Promise<{id: number}> {
        const id = data.id;
        return this.strapiFetchAndAdjust(() => this.$http.put({url: `/api/posts/${id}`, body: data}));
    }

    private getIdentifier(translations: ITranslationDict, uid: string) {
        const properties = keys(translations);
        let identifier = uid;
        for (const key of properties) {
            const translation = translations[key];
            if (translation) {
                identifier = [translation, uid.substr(0, 8)].join("_");
            }
        }
        return identifier;
    }

    private mapToTranslations(translations: ITranslationDict): ITranslation[] {
        const result: ITranslation[] = [];
        forEach(translations, (text, language: ELanguages) => {
            if (text) {result.push({language, text});}
        });
        return result;
    }

    private async strapiFetchAndAdjust<T>(dispatcher: () => Promise<IHttpResponse<IStrapiResponse<T>>>): Promise<T> {
        const response = await this.secureFetch(dispatcher);
        const responseData = response.data;
        this.unpackData(responseData);
        this.unpackAttributes(responseData);
        this.unpackComponent(responseData);
        this.modifyImageTranslations(responseData);
        this.modifyFileTranslations(responseData);
        return responseData as any;
    }

    private async secureFetch<T>(dispatcher: () => Promise<IHttpResponse<T>>): Promise<T> {
        if (this.shouldAuthenticate && !this.isAuthenticated) {
            await this.authenticate();
        }
        const response = await dispatcher();
        const {ok, status} = response;
        if (ok) {
            return response.json();
        }
        const error = await response.text();
        log.error("StrapiAPI - Request failed with status: " + status + " and error: " + error);
        const is401Error = (_status) => _status === 401;
        if (is401Error(response.status)) {
            log.debug("StrapiAPI - 40x detected. Reauthenticating");
            await this.authenticate();
            log.debug("StrapiAPI - Retrying request");
            // retry with (maybe) new auth token
            const retryResponse = await dispatcher();
            if (response.ok) {
                return retryResponse.json();
            }
            if (retryResponse && is401Error(retryResponse.status)) {
                const errMsg = "StrapiAPI - Persisting auth error";
                log.error(errMsg);
            }
            return Promise.reject(retryResponse.statusText);
        }
        return Promise.reject(`StrapiAPI: Error secureFetching - Status: ${response.status}, Error: ${error}`);
    }

    private getAllRoute(route: string) {
        const addon = (includes(route, "?") ? "&" : "?") + "pagination[limit]=1000&populate=*";
        return route + addon;
    }

    private async authenticate() {
        if (this.shouldAuthenticate) {
            try {
                this.$http.setAuthHeader(null);
                this.isAuthenticated = false;
                const response = await this.$http.postd<{jwt: string;}>({
                    url: "/api/auth/local/", body: {
                        identifier: process.env.BACKEND_USERNAME,
                        password: process.env.BACKEND_PASSWORD
                    }
                });
                log.debug("StrapiAPI - Authentication sucessful");
                this.$http.setAuthHeader(`Bearer ${response.jwt}`);
                this.isAuthenticated = true;
                return Promise.resolve();
            }
            catch (err) {
                log.error("StrapiAPI - Error during authentication:" + err);
            }
        }
    }

    private unpackData(entity: any) {
        const modifier = (item: IStrapiResponse<any>) => {
            return item.data;
        };
        this.modifyDeep(entity, isStrapiResponseWrapper, modifier);
    }


    private unpackAttributes(entity: any) {
        const modifier = (item: IAttributeWrapper<any>) => {
            return {id: item.id, ...item.attributes};
        };
        this.modifyDeep(entity, isAttributeWrapper, modifier);
    }

    private unpackComponent(entity: any) {
        const modifier = (item: any) => {
            const type = (item.__component as string).split(".").slice(1).join(".");
            delete item.__component;
            return {...item, type};
        };
        this.modifyDeep(entity, (entry) => isPlainObject(entry) && isString(entry?.__component), modifier);
    }

    private modifyImageTranslations(entity: any) {
        const modifier = (items: IImageTranslation[]) => {
            return map(items, (item) => {
                const {language, image} = item;
                const {url, formats: rawFormats, caption, alternativeText} = image.data;
                const formats = [
                    ...map(rawFormats, (value) => this.getFormat(value)),
                    this.getFormat(image.data)
                ];
                return {
                    language: language,
                    image: {
                        url: this.getFullUrl(url),
                        formats: sortBy(formats, (f) => f.width),
                        alt: alternativeText,
                        caption
                    }
                };
            });
        };
        return this.modifyDeep(entity, isImageTranslations, modifier);
    }

    private modifyFileTranslations(entity: any) {
        const modifier = (item: IStrapiFile) => {
            return {
                ...item.data,
                url: this.getFullUrl(item.data.url)
            };
        };
        return this.modifyDeep(entity, isStrapiFile, modifier);
    }

    private modifyDeep<T>(entity: any, condition: (value: any) => boolean, modifier: (value: any) => T) {
        if (isObjectLike(entity)) {
            forEach(entity, (value, key) => {
                if (condition(value)) {
                    entity[key] = modifier(value);
                }
                this.modifyDeep(value, condition, modifier);
            });
        }
    }

    private getFormat(imgFormat: IStrapiImageFormat) {
        const url = this.getFullUrl(imgFormat.url);
        const {height, width} = imgFormat;
        return {url, height, width, src: `${url} ${imgFormat.width}w`};
    }

    private getFullUrl(url: string) {
        return (this.mediaUrl ?? this.baseUrl) + url;
    }

}