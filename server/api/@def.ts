import {IPage} from "../../core/@def";

export interface IStrapiConfig {
    url: string;
    mediaUrl: string;
}

export interface IStrapiPage extends IPage {
    contents: IStrapiContent[]
}

export interface IStrapiContent<T = any> {
    id?: number;
    identifier: string;
    sortId: number;
    grouping?: string;
    data: [T & {__component: string}];
}

export interface IContentDelegation<T = any> {
    childIdentifier: string;
    childType: string;
    config?: T;
}

export interface IStrapiImageFormat {
    hash: string; // "thumbnail_new-website-launch-title_(copy)_73c9f028bc",
    ext: string; // ".jpeg",
    mime: string; // "image/jpeg",
    width: number;
    height: number;
    size: number;
    url: string; // "/uploads/thumbnail_new-website-launch-title_(copy)_73c9f028bc.jpeg"
}

export interface IStrapiImageFormats {
    thumbnail: IStrapiImageFormat;
    large: IStrapiImageFormat;
    medium: IStrapiImageFormat;
    small: IStrapiImageFormat;
}

export interface IStrapiFile {
    data: {
        alternativeText: string;
        caption: string;
        createdAt: string;
        ext: string;
        formats: any;
        hash: string;
        height: number;
        id: number;
        mime: string;
        name: string;
        previewUrl: string;
        provider: string;
        provider_metadata: any;
        size: number;
        updatedAt: string;
        url: string;
        width: string;
    }
}

export interface IStrapiImage {
    id: number;
    data: {
        name: string; // "corona-banner-de",
        alternativeText: string; // "",
        caption: string; // "",
        width: number;
        height: number;
        formats?: IStrapiImageFormats;
        hash: string; // "corona-banner-de_13adb8f38e",
        ext: string; // ".svg",
        mime: string; // "image/svg+xml",
        size: number;
        url: string; // "/uploads/corona-banner-de_13adb8f38e.svg",
        previewUrl?: string;
        provider: string; // "local",
        provider_metadata?: any;
        created_at: string; // "2020-05-03T13:47:10.877Z",
        updated_at: string; // "2020-05-03T13:47:10.877Z"
    }
}

export interface IImageTranslation {
    language: string;
    image: IStrapiImage;
}