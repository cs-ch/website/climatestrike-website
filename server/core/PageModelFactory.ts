import {StrapiAPI} from "../api/StrapiAPI";
import {IPageModelStrategy} from "../../core/@def";

export class PageModelFactory {
    public create(type: string, config: any): IPageModelStrategy {
        switch (type) {
            // case "mock":
            //     return new MockAPI();
            case "strapi":
                return new StrapiAPI(config);
            default:
                throw new Error(`PageModelFactory - Cannot create type '${type}'`);
        }
    }
}