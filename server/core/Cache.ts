import {isNil, isBoolean} from "lodash";
import {LRUCache} from "lru-cache";

import {ICache, ICacheOptions} from "./@def";
import {log} from "./Logger";

export class Cache implements ICache {
    private cache: LRUCache<string, any>;
    private logging: boolean = true;

    constructor(opts: ICacheOptions) {
        const {enabled, logging, max, maxAge} = opts;
        if (isNil(enabled) || enabled) {
            this.cache = new LRUCache({max: max, ttl: maxAge});
        }
        if (isBoolean(logging)) {
            this.logging = logging;
        }
    }

    public get<T>(key: string, provider: () => T) {
        if (this.cache) {
            let result = this.cache.get(key);
            if (!result) {
                result = provider();
            }
            else if (this.logging) {
                log.debug(`Cache - Hit key: ${key}`);
            }
            if (result.then) {
                return (result as Promise<any>)
                    .then(response => {
                        this.cache.set(key, response);
                        return Promise.resolve(response);
                    })
                    .catch(err => {
                        log.error("Error fetching response: " + err);
                        return Promise.reject(err);
                    });
            }
            else {
                this.cache.set(key, result);
                return result;
            }
        }
        return provider();
    }

    public clear() {
        if (this.cache) {
            this.cache.clear();
        }
    }
}