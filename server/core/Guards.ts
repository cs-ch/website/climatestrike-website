import {every, includes, intersection, isArray, isNil, isNumber, isPlainObject, isString, keys} from "lodash";
import {IContentDelegation, IImageTranslation, IStrapiFile, IStrapiImage} from "../api/@def";
import {IAttributeWrapper, IStrapiResponse} from "../../core/@def";

export const isContentDelegation = (item: any): item is IContentDelegation => {
    return !!(item && item.childIdentifier && item.childType);
};

export const isStrapiResponseWrapper = (item: any): item is IStrapiResponse<any> => {
    return !!(item && item.data && keys(item).length == 1 && every(item.data, isAttributeWrapper));
};

export const isAttributeWrapper = (item: any): item is IAttributeWrapper<any> => {
    return !!(item && keys(item).length == 2 && isNumber(item.id) && isPlainObject(item.attributes));
};


export const isImageTranslations = (items: any): items is IImageTranslation[] => {
    if (!isArray(items)) {
        return false;
    }
    return every(items, (item) => {
        if (!isPlainObject(item)) return false;
        const {language, image} = item;
        return isString(language) && isStrapiImage(image);
    });
};

export const isStrapiFile = (item: any): item is IStrapiFile => {
    if (!isPlainObject(item)) {
        return false;
    }
    const {data} = item;
    if (!data) {
        return false;
    }
    const candidateKeys = keys(data);
    const expectedKeys = ["alternativeText",
        "caption",
        "createdAt",
        "ext",
        "formats",
        "hash",
        "height",
        "id",
        "mime",
        "name",
        "previewUrl",
        "provider",
        "provider_metadata",
        "size",
        "updatedAt",
        "url",
        "width"];
    return intersection(candidateKeys, expectedKeys).length == expectedKeys.length
};

export const isStrapiImage = (item: any): item is IStrapiImage => {
    if (!isPlainObject(item) || !item.data) return false;
    const {name, width, height, hash, ext, mime, size, url} = item.data;
    const itemKeys = keys(item.data);
    const result = includes(itemKeys, "alternativeText")
        && includes(itemKeys, "caption")
        && every(
            [name, width, height, hash, ext, mime, size, url],
            (entry) => !isNil(entry)
        );
    return result;
};