export interface ICacheOptions {
    enabled?: boolean;
    logging?: boolean;
    max?: number; // Max number of entries
    maxAge?: number; // Maximum age in ms
}

export interface ICache {
    get<T>(key: string, provider: () => T): T;
    clear(): void;
}