FROM node:22
WORKDIR /app
COPY package*.json ./
# FIXME
RUN npm install --legacy-peer-deps 
COPY next.config.ts ./
COPY next-i18next.config.js ./
COPY tsconfig.json ./
COPY components components
COPY core core
COPY factories factories
COPY pages pages
COPY public public
COPY server server
COPY styles styles
RUN npm run build
RUN rm -rf components core factories pages server styles
EXPOSE 3000
CMD ["npm", "start" ]