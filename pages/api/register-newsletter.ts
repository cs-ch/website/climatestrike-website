import {MailchimpService} from "../../server/services/MailchimpService";

const $mc = new MailchimpService(
    process.env.MAILCHIMP_API_KEY,
    JSON.parse(process.env.MAILCHIMP_LIST_ID_LANGUAGE_MAPPING),
    process.env.MAILCHIMP_INSTANCE,
    JSON.parse(process.env.MAILCHIMP_FIELD_MAPPING || "{}")
);

export default (req, res) => {
    $mc.register(req.body)
        .then(() => {
            res.status(200).send({success: true});
        })
        .catch(() => {
            res.status(500).send({success: false});
        });
};