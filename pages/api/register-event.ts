import {dataService} from "../../server/services/DataService";

export default (req, res) => {
    dataService.registerEvent(req.body)
        .then(() => {
            res.status(200).send({success: true});
        })
        .catch(() => {
            res.status(500).send({success: false});
        });
};