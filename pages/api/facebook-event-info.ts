import {log} from "../../server/core/Logger";
import {FacebookEventDataProvider} from "../../server/services/FacebookEventDataProvider";

const $fedp = new FacebookEventDataProvider();

export default (req, res) => {
    $fedp.getData(req.body.link)
        .then((data) => {
            res.status(200).send(data);
        })
        .catch((err) => {
            log.error(err);
            res.status(500).send("Unknown error");
        });
};
