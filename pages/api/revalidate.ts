import {forEach, includes, isNil, some} from "lodash";
import {configuration} from "../../core/Configuration";
import {dataService} from "../../server/services/DataService";
import {log} from "../../server/core/Logger";

interface IStrapiWebhookBodyEntry {
    id: number;
    identifier: string;
    slug?: string;
    page?: {slug?: string}
}

interface IStrapiWebhookBody {
    event: string;
    model: string;
    entry: IStrapiWebhookBodyEntry;
}

const handler = async (req, res) => {
    const body: IStrapiWebhookBody = req.body;
    const model = body.model;
    log.info(`Running revalidation for model=${model} and entry with identifier=${body.entry.identifier}`);
    const pages = await dataService.getPages(true);
    const pagesToRevalidate = [];
    if (model === "content" && !isNil(body.entry?.page?.slug)) {
        pagesToRevalidate.push(body.entry.page.slug);
    }
    if (model === "page") {
        pagesToRevalidate.push(body.entry.slug);
    }
    if (model === "post") {
        pagesToRevalidate.push(`posts/${body.entry.identifier}`);
    }
    const dependenciesToRevalidate = [model, `${model}-group`];
    forEach(pages, (page) => {
        const dataDependencies = page?.config?.dataDependencies;
        forEach(dataDependencies, dataDependency => {
            if (some(dependenciesToRevalidate, (dependency) => includes(`/api/${dataDependency.route}`, dependency))) {
                pagesToRevalidate.push(page.slug);
            }
        });
    });
    const pagesToRevalidateWithLanguage = [];
    forEach(pagesToRevalidate, page => {
        forEach(configuration.languages.languages, language => {
            const path = page ? `/${page}` : "";
            pagesToRevalidateWithLanguage.push(`/${language}${path}`);
        });
    });
    log.info(`About to revalidate the following pages: ${pagesToRevalidateWithLanguage}`);

    let errored = false;
    for (const page of pagesToRevalidateWithLanguage) {
        try {
            log.info(`About to revalidate page=${page}`);
            await res.revalidate(page);
            log.info(`Revalidation done of page=${page}`);
        } catch (err) {
            errored = true;
            log.error(`Error revalidating page=${page}, error=${err}`);
            // If there was an error, Next.js will continue
            // to show the last successfully generated page
        }
    }
    if (errored) {
        log.info("Failed to revalidate all pages");
        return res.status(500).send("Error revalidating");
    }
    log.info("All pages revalidated successfully");
    return res.json({revalidated: true});
};

export default handler;