import {dataService} from "../../server/services/DataService";

export default (req, res) => {
    dataService.getFormSubmissionCount(req.body.formId)
        .then((response) => {
            res.status(200).send({success: true, count: response});
        })
        .catch(() => {
            res.status(500).send({success: false});
        });
};
