import React from "react";
import {AdapterDayjs} from "@mui/x-date-pickers/AdapterDayjs";
import {LocalizationProvider} from "@mui/x-date-pickers/LocalizationProvider";
import {appWithTranslation} from "next-i18next";

import "../styles/styles.scss";

const App = ({Component, pageProps}) => {
    return <LocalizationProvider dateAdapter={AdapterDayjs}>
        <Component {...pageProps} />
    </LocalizationProvider>;
};


export default appWithTranslation(App);