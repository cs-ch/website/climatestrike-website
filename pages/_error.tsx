import {GetStaticProps} from "next";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";

import {dataService} from "../server/services/DataService";
import {configuration} from "../core/Configuration";
import {IPageWrapperProps} from "../components/@def";
import {ErrorPage} from "../components/ErrorPage";


export const getStaticProps: GetStaticProps = async ({locale}) => {
    // From the Next.JS docs (https://nextjs.org/docs/basic-features/data-fetching#getserversideprops-server-side-rendering):
    //Note: You should not use fetch() to call an API route in your application.
    // Instead, directly import the API route and call its function yourself.
    //You may need to slightly refactor your code for this approach.
    const pages = process.env.BACKEND_URL ? await dataService.getPages(true) : [];
    const props: IPageWrapperProps = {
        ...(await serverSideTranslations(locale, ["common"])),
        namespacesRequired: ["common"],
        pageIdentifier: null,
        activePage: null,
        query: {id: null},
        pages: pages,
        menu: configuration.menu,
        submenu: configuration.submenu,
        footer: configuration.footer,
        items: []
    };
    return {props, revalidate: 20};
};

export default ErrorPage;
