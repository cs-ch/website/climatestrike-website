import React from "react";
import {GetStaticPaths, GetStaticProps} from "next";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {find, union, cloneDeep, isEmpty} from "lodash";

import {dataService} from "../../server/services/DataService";
import {configuration} from "../../core/Configuration";
import {AbstractPage} from "../../components/AbstractPage";
import {IPageWrapperProps} from "../../components/@def";
import {EImageRole, ELanguages} from "../../core/@def";

const Page = (props) => {
    return <AbstractPage {...props} />;
};

export const getStaticPaths: GetStaticPaths = async () => {
    return {paths: [], fallback: "blocking"};
};


export const getStaticProps: GetStaticProps = async ({params, locale}) => {
    const slug = "posts";
    const postId = params.id as string;
    const pages = await dataService.getPages(true);
    const activePage = cloneDeep(find(pages, (page) => page.slug === slug));
    const content = await dataService.getPost(postId);
    if (!content) {
        return {notFound: true};
    }
    activePage.title = union(content.title || [], [{language: ELanguages.all, text: "no-content-title"}]);
    activePage.description = union(content.lead || [], [{language: ELanguages.all, text: "no-content-text"}]);
    activePage.image = content.thumbnail || content.image;
    activePage.imageRole = isEmpty(content.image) ? EImageRole.previewOnly : EImageRole.pageAndPreview;
    const props: IPageWrapperProps = {
        ...(await serverSideTranslations(locale, ["common"])),
        namespacesRequired: ["common"],
        pageIdentifier: slug,
        activePage: activePage,
        query: {id: postId},
        pages: pages,
        menu: configuration.menu,
        submenu: configuration.submenu,
        footer: configuration.footer,
        items: content ? [{
            identifier: postId,
            sortId: 0,
            type: "post-section",
            config: {data: [content]}
        }] : null,
    };
    return {props};
};

export default Page;