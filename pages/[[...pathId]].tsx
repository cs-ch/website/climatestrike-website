import React from "react";
import {GetStaticProps, GetStaticPaths} from "next";
import {serverSideTranslations} from "next-i18next/serverSideTranslations";
import {filter, find} from "lodash";

import {dataService} from "../server/services/DataService";
import {configuration} from "../core/Configuration";
import {IPageWrapperProps} from "../components/@def";
import {AbstractPage} from "../components/AbstractPage";


const Page = (props) => {
    return <AbstractPage {...props} />;
};

export const getStaticPaths: GetStaticPaths = async () => {
    return {paths: [], fallback: "blocking"};
};


export const getStaticProps: GetStaticProps = async (ctx) => {
    const {params, locale} = ctx;
    // From the Next.JS docs (https://nextjs.org/docs/basic-features/data-fetching#getserversideprops-server-side-rendering):
    //Note: You should not use fetch() to call an API route in your application.
    // Instead, directly import the API route and call its function yourself.
    //You may need to slightly refactor your code for this approach.
    const pages = await dataService.getPages(true);
    let pathIds = params?.pathId;
    pathIds = filter(pathIds, id => id != locale);
    const [slug, queryId = null] = pathIds;
    const activePage = find(pages, (page) => (slug ? page.slug === slug : page.identifier === "home")) || null;
    if (!activePage) {
        return {notFound: true};
    }
    const activePageIdentifier = activePage?.identifier;
    let content = null;
    if (activePageIdentifier) {
        content = await dataService.getContent(activePage.id, activePage?.config?.dataDependencies);
    }
    const props: IPageWrapperProps = {
        ...(await serverSideTranslations(locale, ["common", "create-event"])),
        namespacesRequired: ["common"],
        pageIdentifier: activePageIdentifier,
        activePage: activePage,
        query: {id: queryId},
        pages: pages,
        menu: configuration.menu,
        submenu: configuration.submenu,
        footer: configuration.footer,
        items: content
    };
    return {props};
};

export default Page;