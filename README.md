# Climatestrike Website
## General information
This project contains the code for the new climatestrike.ch website.
The goal is to implement the design proposed [here][design], the final design elements you can find [here][design-final]. Development is done on the `development` branch.

There was a previous design, which you can find [here][previous-design]. The tag `pre-redesign` represents the last state before starting the redesign.

### Prerequisites
We will assume that you have a current version (e.g. LTS) of `nodejs` installed. Our production instance runs with `v20.9.0`.

### Development setup

For development, run:
```
git clone https://gitlab.com/cs-ch/website/climatestrike-website.git
cd climatestrike-website
cp .env_dist .env
# Modify .env according to your setup
npm i
npm run dev
```
Then open
```
localhost:3000
```
in your browser.

#### Configuration
By default we use `strapi` as a headless CMS. Theoretically we could add support for all kinds of CMS, but that might be done a lot later.

### Production install
For a production installation, use:

```
git clone https://gitlab.com/cs-ch/website/climatestrike-website.git
cd climatestrike-website
npm i
npm run build
```

You could start the app with just `npm start`, we reccomend use a process manager. The following section describes how to set up [`pm2`](https://pm2.keymetrics.io/) to watch the app state and restart.

#### Process manager
If not already, install `pm2`:
```
npm i -g pm2
```
Create a `ecosystem.json` file in the root directory, with the following content
```
{
    "name": "app",
    "script": "build/server/main.js",
    "args": "",
    "autorestart": true,
    "watch": true,
    "watch_delay": 1000,
    "ignore_watch": ["build/log", "node_modules", "package.json", "package-lock.json", "release.zip"],
    "env": {
        "NODE_ENV": "production",
        "PORT": 3000,
        "MAILCHIMP_API_KEY": "<your-mailchimp-api-key>",
        "MAILCHIMP_LIST_ID": "<your-mailchimp-list-id>",
        "MAILCHIMP_INSTANCE": "<your-mailchimp-instance>",
        "MAILCHIMP_MAPPING": "{\"email\": \"<your-mailchimp-email-field>\", \"zipch\": \"<your-mailchimp-zip-field>\", \"phone\": \"<your-mailchimp-phone-field>\", \"language\": \"<your-mailchimp-language-field>\"}",
        "BACKEND_URL": "<your-backend-url>",
        "BACKEND_USERNAME": "<your-backend-username>",
        "BACKEND_PASSWORD": "<your-backend-password>"
    }
}
```
Then start the process with `pm2 start ecosystem.json`.

## I want to help out...

Contributions are most welcome - please read the [contribution guide](./CONTRIBUTING.md). We try to categorize the issues accordingly. Also, if you have a good idea for a new feature, feel free to raise a new issue.
If you are working on a specific issue, let other people know. 

Please test your styles and components cross-browser. Consult [caniuse](https://caniuse.com/) to check if the `CSS` is supported by all major browsers.

We need to support all modern browsers. The page should also work on `IE11` - but we are currently not optimizing all styles for it.

### ...with styling
Once you got your project running, you are most welcome to style components. The goal is that the page will look as designed [here][design] and [here][design-final]. We label the respective issue with *Area::Styling*, i.e. you should be able to find them [here](https://gitlab.com/cs-ch/website/climatestrike-website/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Area%3A%3AStyling).

We use [`.less`](http://lesscss.org/) instead of `.css` files, but if you don't like or know `less` you can simply add `css`, because `less` will handle your `.css` styles.

The styles are located in `./styles`, usually one file per component. If you add a new file, you need to register it in `./styles/styles.less` (just follow the code already in there) for it to get loaded into the browser.

### ...by implementing components
For the next milestone there are some new components to implement. We label the respective issues with *Area::Component*, i.e. you should be able to find them [here](https://gitlab.com/cs-ch/website/climatestrike-website/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Area%3A%3AComponent).

Make sure the component fits into the [current design][design] and use the colors already defined.

If the components needs data from the backend, you can either
* register the component yourself on the [backend][strapi-backend]
* raise an issue for that
* contact one of the maintainers for help

### ...by working on the backend
Technically there are two backends, the server responding to the `HTTP` requests and a CMS providing the content. We label the respective issues with *Area::Backend*, i.e. you should be able to find them [here](https://gitlab.com/cs-ch/website/climatestrike-website/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Area%3A%3ABackend).


#### CMS
Currently we only support a `strapi` integration. If you want to work on the CMS, please go to [the repository][strapi-backend].

If you want to support another kind of CMS, you need to implement the connector yourself. It must implement the `IPageModelStrategy`-interface which you can find [here](./core/@def.ts)

#### HTTP server

We use an `express` server to respond to `HTTP` requests. The code for that is located under [`./server`](./server).

### ...with code review
We are mostly young developers and coding a webapp is not something we do every day. We value every feedback contributing to a better architecture, code quality and/or maintainability. Please raise an issue with your comments or contact one of the maintainers.

### ...with testing
You can always test the current state of development under [https://climate-dev.ch](https://climate-dev.ch), and check if everything works. If you find something that is not working, please raise an issue.

Even better, if you have experience with automated testing, you are free to set up an automated testing environment.

### ...with documentation
Sometimes, a library is only as good as its documentation. If you have questions or something is unclear from the current documentation, raise an issue or contact a maintainer.

We label the respective issue with *Area::Docs*, i.e. you should be able to find them [here](https://gitlab.com/cs-ch/website/climatestrike-website/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Area%3A%3ADocs).

If you find errors, typos or missing parts, feel free to add them yourself.


## Troubleshooting
On Linux, if you get an error like `Watchpack Error (watcher): Error: ENOSPC: System limit for number of file watchers reached, watch <some-dir>`, you can increase your file watchers limit using

```
sudo sysctl fs.inotify.max_user_watches=<some-number>
sudo sysctl -p
```
or to make it permanent
```
echo fs.inotify.max_user_watches=<some-number> | sudo tee -a /etc/sysctl.conf
sudo sysctl -p
```

You can print the current limit with `cat /proc/sys/fs/inotify/max_user_watches`.

## Contributions
See [CONTRIBUTING](CONTRIBUTING.md).

## License
See [LICENSE](LICENSE).

[design]: https://www.figma.com/file/25KpJ6rzDG4KpVguoPm0tl/REDESIGN-3.0?node-id=0%3A1
[design-final]: https://www.figma.com/file/70cISswQ02B9MciG9s5vq6/Design-Elements-Final?node-id=0%3A1
[previous-design]: https://www.figma.com/proto/gS764DdLo3IkDJgHfni6fd/Climatestrike-Website-new-Prototype?node-id=260%3A0&viewport=299%2C362%2C0.019355816766619682&scaling=scale-down
[strapi-backend]: https://gitlab.com/cs-ch/website/climatestrike-backend