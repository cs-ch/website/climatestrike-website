import React from "react";
import classNames from "classnames";
import {default as MuiSnackbar} from "@mui/material/Snackbar";
import {Alert} from "@mui/material";

import {ISnackbarProps} from "./@def";



export const Snackbar = (props: ISnackbarProps) => {
    const {vertical = "bottom", horizontal = "right", autoHideDuration = 10000,
        message, open, setOpen, severity} = props;

    const handleClose = () => {
        setOpen(false);
    };
    const className = classNames("snackbar", severity);
    return (
        <MuiSnackbar
            className={className}
            autoHideDuration={autoHideDuration}
            anchorOrigin={{vertical, horizontal}}
            key={`${vertical},${horizontal}`}
            open={open}
            onClose={handleClose}>
            <Alert onClose={handleClose} severity={severity} variant="standard">
                {message}
            </Alert>
        </MuiSnackbar>
    );
};