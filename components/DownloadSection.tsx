import React from "react";
import {useRouter} from "next/router";
import classNames from "classnames";
import {map, replace, toLower} from "lodash";

import {IPageContextProps, IDownloadSectionProps, IMediaTranslation} from "./@def";
import {languageTextProvider, getLanguageMedium} from "../core/I18n";
import {IconFactory} from "../factories/IconFactory";


const getIconIdFromExtension = (_ext: string) => {
    const ext = toLower(replace(_ext, ".", ""));
    const extToIconId = {
        pdf: "download-pdf-icon",
        doc: "download-doc-icon",
        docx: "download-doc-icon",
        odt: "download-doc-icon",
        xls: "download-xls-icon",
        xlsx: "download-xls-icon",
        ods: "download-xls-icon",
        ppt: "download-ppt-icon",
        pptx: "download-ppt-icon",
        odp: "download-ppt-icon",
        zip: "download-zip-icon",
        rar: "download-zip-icon",
        tar: "download-zip-icon",
        gz: "download-zip-icon",
        png: "download-image-icon",
        svg: "download-image-icon",
        jpg: "download-image-icon",
        jpeg: "download-image-icon",
        gif: "download-image-icon",
        bmp: "download-image-icon",
        avi: "download-video-icon",
        mp4: "download-video-icon",
        mkv: "download-video-icon",
        mov: "download-video-icon",
        mpg: "download-video-icon",
        mpeg: "download-video-icon"
    };
    return extToIconId[ext] || "download-other-icon";
};

const DownloadSection: React.FC<IDownloadSectionProps & IPageContextProps> = (props) => {
    const {context, media} = props;
    return <div className={classNames("download-section", context.itemIdentifier)}>
        <ul className="download-items">
            {map(media, (item, idx) => {
                return <DownloadItem key={idx} {...item} />;
            })}
        </ul>
    </div >;
};

const DownloadItem: React.FC<IMediaTranslation> = (props) => {
    const {locale: language} = useRouter();
    const getLanguageText = languageTextProvider(language);
    const title = getLanguageText(props.title);
    const medium = getLanguageMedium(props.item, language);
    if (!medium) {
        return null;
    }
    const iconName = getIconIdFromExtension(medium.ext);
    const iconFactory = IconFactory.instance();
    return <li className="download-item">
        <a download={medium.name} href={medium.url} target="_blank" rel="noreferrer noopener" >
            <div className="icon">{iconFactory.create(iconName)}</div>
            <div className="title">{title}</div>
        </a>
    </li>;
};

export default DownloadSection;