import React, {useReducer, useState, useEffect, useCallback} from "react";
import {find, map, reject} from "lodash";
import {AppBar, Toolbar} from "@mui/material";
import classNames from "classnames";

import {IContribution, IHeaderProps} from "./@def";
import {Submenu} from "./Submenu";
import {ICON_MENU_ITEM, LANGUAGES} from "../core/Constants";
import {MenuIcon} from "./MenuIcon";


interface ILayoutProps {
    activePageIdentifier: string;
    menuContributions: IContribution[];
    submenuContributions: IContribution[];
    footerContributions: IContribution[];
}

export const Header: React.FC<ILayoutProps> = (props) => {
    return <AppBar className="header" position="fixed" hidden={false}>
        <Toolbar className="toolbar">
            <DesktopHeader {...props} />
            <MobileHeader {...props} />
        </Toolbar>
    </AppBar >;
};

const DesktopHeader = (props: IHeaderProps) => {
    const {menuContributions, submenuContributions} = props;
    const isHidden = useCallback(() => window.scrollY > 0, []);
    const [hideSubmenu, setHideSubmenu] = useState(false);
    const scrollListener = useCallback(() => {
        const hide = isHidden();
        setHideSubmenu(hide);
    }, [isHidden, setHideSubmenu]);
    useEffect(
        () => {
            window.addEventListener("scroll", scrollListener);
            return () => {window.removeEventListener("scroll", scrollListener);};
        },
        [scrollListener]);
    return < div className={classNames("header", "desktop")}>
        <div key="submenu-container" className={classNames("submenu-container", {hidden: hideSubmenu})}>
            <Submenu className={classNames("desktop")} contributions={submenuContributions} />
            <hr className="submenu-separator"></hr>
        </div>
        <nav key="menu-container" className={classNames("menu", "navigation", "desktop")}>
            {map(menuContributions, contribution => contribution.provider())}
        </nav>
    </div >;
};

const MobileHeader = (props: IHeaderProps) => {
    const {menuContributions, submenuContributions} = props;
    const [showMenu, toggleMenu] = useReducer((state) => {return !state;}, false);
    const iconContribution = find(menuContributions, contribution => contribution.identifier === ICON_MENU_ITEM);
    const languageContribution = find(submenuContributions, contribution => contribution.identifier === LANGUAGES);
    const contributions = reject(menuContributions, iconContribution);
    const reducedSubmenuContributions = reject(submenuContributions, languageContribution);
    return <div className={classNames("header", "mobile")}>
        <div className={classNames("menu", "mobile")}>
            {iconContribution && iconContribution.provider()}
            {showMenu && languageContribution && languageContribution.provider()}
            <MenuIcon active={showMenu} onClick={() => toggleMenu()} />
        </div>
        <div className={classNames("menu-content", {hidden: !showMenu})}>
            <nav className={classNames("navigation", "mobile")}>
                {map(contributions, (contribution, idx) =>
                    <div key={idx} onClick={(e) => {e.stopPropagation(); toggleMenu();}}>{contribution.provider()}</div>
                )}
            </nav>
            <Submenu className="mobile" contributions={reducedSubmenuContributions} />
        </div>
    </div>;
};