import React from "react";
import {useRouter} from "next/router";
import classNames from "classnames";

import {IHeadlineItemProps, IPageContextProps} from "./@def";
import {LinkButton} from "./Button";
import {languageTextProvider} from "../core/I18n";
import {H2, H3} from "./Typography";
import {HTML} from "./HTML";


const Headline: React.FC<IHeadlineItemProps & IPageContextProps> = (props) => {
    const {context, link} = props;
    const {locale: language} = useRouter();
    const getLanguageText = languageTextProvider(language);
    const title = getLanguageText(props.title);
    const subtitle = getLanguageText(props.subtitle);
    const text = getLanguageText(props.text);
    if (!title && !subtitle && !text) return null;
    return <div className={classNames("headline", context.itemIdentifier)}>
        <div className="headline-titles">
            <H2 text={title} />
            {subtitle && <H3 text={subtitle} />}
        </div>
        {text && <HTML text={text} />}
        {link && <LinkButton {...link} />}
    </div >;
};

export default Headline;