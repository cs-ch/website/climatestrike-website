import React, {useEffect, useState} from "react";
import {useRouter} from "next/router";
import classNames from "classnames";
import {kebabCase, reduce} from "lodash";
import {useTranslation} from "next-i18next";


import {EShareButtonsVariant, IShareButtonsProps} from "./@def";
import {Link} from "./Core";
import {languageTextProvider} from "../core/I18n";
import {ITranslation} from "../core/@def";
import {IconFactory} from "../factories/IconFactory";


interface IShareButtonProps {
    url: string;
    messageText: string;
}

interface IEMailShareButtonProps extends IShareButtonProps {
    messageSubject: string;
}

export const EmailShareButton = (props: IEMailShareButtonProps) => {
    const {messageText: rawMessageText, messageSubject} = props;
    const messageText = rawMessageText.replace("[url]", props.url);
    return <Link className={classNames("share-button", "email-share-button")}
        href={`mailto:?subject=${encodeURIComponent(messageSubject)}&body=${encodeURIComponent(messageText)}`}>
        {IconFactory.instance().create("email")}
    </Link>;
};

export const WhatsAppShareButton = (props: IShareButtonProps) => {
    const messageText = props.messageText.replace("[url]", props.url);
    return <Link className={classNames("share-button", "whatsapp-share-button")}
        href={`whatsapp://send?text=${encodeURIComponent(messageText)}`}>
        {IconFactory.instance().create("whatsapp")}
    </Link>;
};

export const FacebookShareButton = (props: IShareButtonProps) => {
    return <Link className={classNames("share-button", "facebook-share-button")}
        href={`https://facebook.com/sharer/sharer.php?u=${encodeURIComponent(props.url)}`}>
        {IconFactory.instance().create("facebook")}
    </Link>;
};

export const TwitterShareButton = (props) => {
    const messageText = props.messageText.replace("[url]", props.url);
    return <Link className={classNames("share-button", "facebook-share-button")}
        href={`https://twitter.com/intent/tweet?url=${encodeURIComponent(props.url)}&amp;text=${encodeURIComponent(messageText)}`}>
        {IconFactory.instance().create("twitter")}
    </Link>;
};

const getMessagesFromPropsOrDefaults = (props: any, language, t) => {
    const getLanguageText = languageTextProvider(language);
    const keys: (keyof IShareButtonsProps)[] = ["emailSubject", "emailMessage", "twitterMessage", "whatsappMessage"];
    const result = reduce(keys, (collector, key) => {
        const text = getLanguageText(props[key] as ITranslation[]);
        const fallBack = t("share-" + kebabCase(key));
        collector[key] = text || fallBack;
        return collector;
    }, {} as any);
    return result;
};


export const ShareButtons: React.FC<Partial<IShareButtonsProps>> = (props) => {
    const {locale: language} = useRouter();
    const {t} = useTranslation("common");
    const {enableFacebook = true, urlOverride = "", variant = EShareButtonsVariant.minimal} = props;
    const {emailSubject, emailMessage, whatsappMessage, twitterMessage} = getMessagesFromPropsOrDefaults(props, language, t);
    const [url, setUrl] = useState(urlOverride);
    useEffect(() => {
        if (!url) {
            setUrl(window.location.href);
        }
    }, [url]);
    const className = variant == EShareButtonsVariant.minimal ? "share-buttons-container" : "social-link-container";
    return <div className={className} >
        {(emailSubject || emailMessage) && <EmailShareButton messageSubject={emailSubject} messageText={emailMessage} url={url} />}
        {whatsappMessage && <WhatsAppShareButton messageText={whatsappMessage} url={url} />}
        {twitterMessage && <TwitterShareButton messageText={twitterMessage} url={url} />}
        {enableFacebook && <FacebookShareButton messageText={null} url={url} />}
    </div >;
};
