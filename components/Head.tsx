import React from "react";
import NextHead from "next/head";
import {useRouter} from "next/router";
import {compact, find, map} from "lodash";

import {IHTMLHeadProps} from "./@def";
import {ellipsisTrim} from "../core/Utils";


const getHeadData = (props: IHTMLHeadProps) => {
    const {title, description, url, image} = props;
    // Reference: https://stackoverflow.com/questions/19778620/provide-an-image-for-whatsapp-link-sharing
    const imageFormats = image ? image.formats : null;
    const imgSrc = imageFormats
        ? find(imageFormats, format => format.width >= 300 && format.height >= 200)?.url
        : null;
    const twitterImgSrc = imageFormats
        ? find(imageFormats, format => format.width > 999)?.url
        : imgSrc;
    const metaTags = compact([
        {name: "description", content: ellipsisTrim(description, 155)},
        {property: "og:title", content: ellipsisTrim(title, 35)},
        {property: "og:description", content: ellipsisTrim(description, 65)},
        url && {property: "og:url", content: url},
        imgSrc && {property: "og:image", content: imgSrc},
        // Twitter support, see https://developer.twitter.com/en/docs/twitter-for-websites/cards/overview/summary
        {name: "twitter:card", content: twitterImgSrc === imgSrc ? "summary" : "summary_large_image"},
        {name: "twitter:title", content: title},
        {name: "twitter:description", content: description},
        twitterImgSrc && {name: "twitter:image", content: twitterImgSrc}
    ]);
    return [
        <title key={title}>{ellipsisTrim(title, 65)}</title>,
        ...map(metaTags, ({name, property, content}) => {
            const key = name || property;
            if (name && content) {
                return <meta key={key} name={name} content={content} />;
            }
            if (property && content) {
                return <meta key={key} property={property} content={content} />;
            }
        })
    ];
};

const getFonts = () => {
    const fontSources = [
        "/static/fonts/graph/graph/condensed-bold/graph-condensed-bold-web.ttf",
        "/static/fonts/klima/web/klima-regular-web.ttf"
    ];
    return map(fontSources, src => {
        return <link
            key={src}
            rel="preload"
            href={src}
            as="font"
            crossOrigin=""
        />;
    });
};

export const Head: React.FC<IHTMLHeadProps> = (props) => {
    const {locale: language} = useRouter();
    const data = getHeadData(props);
    const fonts = getFonts();
    return <NextHead>
        <meta key="charset" charSet="utf-8" />
        <meta key="viewport" name="viewport" content="width=device-width,minimum-scale=1,initial-scale=1" />
        <meta key="theme-color" name="theme-color" content="#20B8C1" />
        <link key="favicon" rel="shortcut icon" href="/static/images/favicon.ico" />
        <link key="apple-touch-icon" rel="apple-touch-icon" href="/static/images/apple-touch-icon.png" />
        <link key="manifest" rel="manifest" href={`/static/manifest_${language}.webmanifest`} />
        {fonts}
        {data}
    </NextHead>;
};