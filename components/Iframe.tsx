import React, {CSSProperties, IframeHTMLAttributes} from "react";
import {useRouter} from "next/router";
import {some, includes} from "lodash";
import classNames from "classnames";
import DomPurify from "isomorphic-dompurify";

import {IIframeProps, IPageContextProps} from "./@def";
import {languageTextProvider} from "../core/I18n";


const getIframeStyles = (src) => {
    const isVideoElement = some(["youtube.com", "youtube-nocookie.com", "youtu.be", "vimeo.com"], videoSource => includes(src, videoSource));
    const wrapperStyle: CSSProperties = {};
    const iframeStyle: CSSProperties = {};
    const attributes: IframeHTMLAttributes<any> = {};
    if (isVideoElement) {
        wrapperStyle.position = "relative";
        wrapperStyle.paddingBottom = "56.25%"; // 16:9 
        wrapperStyle.height = 0;
        iframeStyle.position = "absolute";
        iframeStyle.top = "0";
        iframeStyle.left = "0";
        iframeStyle.width = "100%";
        iframeStyle.height = "100%";
        attributes.allow = "fullscreen";
        attributes.title = "Video";
    }
    return {wrapper: wrapperStyle, iframe: iframeStyle, attributes};
};

const Iframe: React.FC<IIframeProps & IPageContextProps> = (props) => {
    const {context} = props;
    const {locale: language} = useRouter();
    const getLanguageText = languageTextProvider(language);
    const html = getLanguageText(props.html);
    const src = getLanguageText(props.src);
    const purified = html ? DomPurify.sanitize(html, {ADD_TAGS: ["iframe"], ADD_ATTR: ["allow", "allowfullscreen", "frameborder", "scrolling"], KEEP_CONTENT: false}) : null;
    const className = classNames("iframe", context.itemIdentifier);
    if (purified) return <div className={className} dangerouslySetInnerHTML={{__html: purified}} />;
    if (src) {
        const {wrapper, iframe, attributes} = getIframeStyles(src);
        return <div className={className} style={wrapper} ><iframe src={src} style={iframe} {...attributes} /></div >;
    }
    return null;
};

export default Iframe;