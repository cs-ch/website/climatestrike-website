import React from "react";
import classNames from "classnames";
import {map} from "lodash";
import {useRouter} from "next/router";
import {useTranslation} from "next-i18next";


import {IPageContextProps, IReferencesProps} from "./@def";
import {languageTextProvider} from "../core/I18n";
import {Link} from "./Core";
import {H2} from "./Typography";
import {HTML} from "./HTML";


const References: React.FC<IReferencesProps & IPageContextProps> = (props) => {
    const {references} = props;
    const {locale: language} = useRouter();
    const {t} = useTranslation("common");
    const getLanguageText = languageTextProvider(language);
    const text = getLanguageText(props.text);
    return <div className={classNames("references-container", props.context.itemIdentifier)}>
        <H2 text={t("references")} />
        {text && <div className="references">
            <HTML text={text} />
        </div>}
        {references.length > 0 && <ol className="references">
            {map(references, (ref, idx) => {
                const description = getLanguageText(ref.description);
                const link = getLanguageText(ref.link);
                return <li key={idx}>
                    {description && <span>{description + (link ? ", " : "")}</span>}
                    {link && <Link href={link} text={link} />}
                </li>;
            })}
        </ol>}
    </div>;
};

export default References;