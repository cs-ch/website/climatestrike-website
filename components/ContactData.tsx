import React from "react";
import {useRouter} from "next/router";
import {useTranslation} from "next-i18next";
import classNames from "classnames";
import {
    map, first, sortBy, startsWith, isEmpty, filter,
    intersection, partition, includes, reduce
} from "lodash";

import {IContactDataProps, IPageContextProps, TContactType, IContact} from "./@def";
import {getLanguageText} from "../core/I18n";
import {H3} from "./Typography";
import {IDictionary} from "../core/@def";
import {ContactLinkItem} from "./ContactLinkItem";


interface IContactProps {
    data: IContact[];
    language: string
    sorting?: string[];
    className?: string;
}

const Contacts: React.FC<IContactProps> = (props) => {
    const {className, data, sorting, language} = props;
    const sortLookup = reduce(sorting, (prev, curr, idx) => {prev[curr] = idx; return prev;}, {} as IDictionary<number>);
    // Pre sort for a consistent display
    let sorted = sortBy(data, dp => dp.identifier);
    sorted = sorting && sorting.length ? sortBy(data, dp => sortLookup[dp.type]) : sortBy(data, dp => dp.type);

    return <div className={classNames("contacts", className)}>
        {map(sorted, (item) => {
            const linkName = getLanguageText(item.linkName, language);
            const href = item.type === "email" && !startsWith(item.link, "mailto:") ? "mailto:" + item.link : item.link;
            return <ContactLinkItem {...item} key={item.identifier} name={linkName} href={href} />;
        })}
    </div>;
};

const ContactData: React.FC<IContactDataProps & IPageContextProps> = (props) => {
    const {context, items, selection} = props;
    const {locale: language} = useRouter();
    const {t} = useTranslation("common");
    const className = classNames("contact-data", context.itemIdentifier);
    const hasSelection = !isEmpty(selection);
    const data = filter(items, item => hasSelection ? !isEmpty(intersection(item.identifiers, selection)) : item.selected);
    return <div className={className} >
        {map(data, (dp) => {
            const infoOrder: TContactType[] = ["website", "email", "instagram", "facebook", "twitter"];
            const [info, chats] = partition(dp.contacts, (dp) => includes(infoOrder, dp.type));
            const showBorder = info.length && chats.length;
            const description = getLanguageText(dp.description, language);
            return <div key={dp.identifier} className={classNames("content-group", ...dp.identifiers)}>
                <H3 key="contact-header" text={description || t(first(selection)) || t(first(dp.identifiers))} />
                <div key={"contact-link-group"} className="contact-link-group">
                    <Contacts className={classNames("info-contacts", showBorder ? "border" : null)}
                        {...{data: info, sorting: infoOrder, language: language}} />
                    <Contacts className="chat-contacts" {...{data: chats, language: language}} />
                </div>
            </div>;
        })}
    </div>;
};

export default ContactData;