import * as React from "react";
import {map} from "lodash";
import {useRouter} from "next/router";
import {useTranslation} from "next-i18next";

import {IUrlObject} from "../core/@def";
import {getLanguageText} from "../core/I18n";
import {IconFactory} from "../factories/IconFactory";


interface ISocialLinksProps {
    social: IUrlObject[];
    description?: boolean;
}

export const SocialLinks: React.FC<ISocialLinksProps> = (props) => {
    const {social, description} = props;
    const {locale: language} = useRouter();
    const {t} = useTranslation("common");
    const factory = IconFactory.instance();
    return <div className="social-links">
        {description && <div className="social-link-description">
            {t("social-description")}
        </div>}
        <div className="social-link-container">
            {map(social, (socialItem) => {
                const {id, url} = socialItem;
                const href = getLanguageText(url, language);
                if (!href) return null;
                return <a key={id} href={href} target="_blank" rel="noopener noreferrer" aria-label={t(id)}>
                    {factory.create(id)}
                </a>;
            })}
        </div>
    </div>;
};