import * as React from "react";
import {Grid} from "@mui/material";
import {map} from "lodash";

import {IFooterProps} from "./@def";

export const Footer: React.FC<IFooterProps> = ({contributions}) => {
    return <footer className="footer">
        <div className="footer-content">
            {map(contributions, contribution => {
                return <Grid
                    key={contribution.identifier}
                    className={contribution.identifier}
                    item>
                    {contribution.provider()}
                </Grid>;
            })}
        </div>
    </footer>;
};