import React, {useEffect, useRef, useState} from "react";
import {map} from "lodash";
import classNames from "classnames";
import {useRouter} from "next/router";
import Slider from "react-slick";
import {useTranslation} from "react-i18next";

import {IPageContextProps, ISlidesProps, ISlide} from "./@def";
import {IconButton} from "./Button";
import {Image} from "./Core";
import {HTML} from "./HTML";
import {getLanguageText} from "../core/I18n";
import {useWindowSize} from "../core/Hooks";
import {IconFactory} from "../factories/IconFactory";


const Slide: React.FC<ISlide> = (props) => {
    const {locale: language} = useRouter();
    const text = getLanguageText(props.text, language);
    return <div className="slide">
        <div className="image-container">
            <Image image={props.image} width={props.width} className="image" />
        </div>
        <HTML text={text} />
    </div>;
};

export const NavigationArrow: React.FC<any> = (props) => {
    const {className, onClick, direction} = props;
    const {t} = useTranslation("common");
    const label = t(direction);
    return (
        <IconButton
            aria-label={label}
            className={className}
            onClick={onClick}
        >
            {IconFactory.instance().create("forward", {
                color: "primary",
                className: classNames("navigation-arrow", direction)
            })}
        </IconButton>
    );
};
export const ForwardArrow: React.FC<any> = (props) => <NavigationArrow {...props} direction="forward" />;
export const BackwardArrow: React.FC<any> = (props) => <NavigationArrow {...props} direction="backward" />;


const Slides: React.FC<ISlidesProps & IPageContextProps> = (props) => {
    const windowSize = useWindowSize();
    const [availableWidth, setAvailableWidth] = useState(null);
    const ref = useRef();
    const {items} = props;
    useEffect(() => {
        if (ref && ref.current) {
            setAvailableWidth((ref.current as any).offsetWidth);
        }
    }, [windowSize.width]);
    const settings = {
        dots: true,
        infinite: false,
        slidesToShow: 1,
        slidesToScroll: 1,
        lazyLoad: true,
        nextArrow: <ForwardArrow />,
        prevArrow: <BackwardArrow />
    };
    return <div className="slides" ref={ref}>
        <Slider {...settings}>
            {map(items, (slide, idx) => <Slide key={"slide" + idx} {...slide} width={availableWidth} />)}
        </Slider>
    </div>;
};

export default Slides;