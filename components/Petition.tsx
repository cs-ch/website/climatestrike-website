import React, {useEffect, useState} from "react";
import classNames from "classnames";
import {cloneDeep, find} from "lodash";
import {useTranslation} from "next-i18next";

import {EShareButtonsVariant, IPageContextProps, IPetitionProps, IShareButtonsProps} from "./@def";
import {HTML} from "./HTML";
import {H2, H3, Paragraph} from "./Typography";
import {ShareButtons} from "./ShareButton";
import {ProgressChart} from "./charts/ProgressChart";
import Form from "./forms/Form";
import {isReverse} from "../core/Utils";
import {HttpService} from "../core/HttpService";
import {languageTextProvider} from "../core/I18n";
import {useRouter} from "next/router";


interface ICountPhraseProps {
    count: number;
    goal: number;
}

const CountPhrase: React.FC<ICountPhraseProps> = (props) => {
    const {count, goal} = props;
    const {t} = useTranslation("common");
    const text = t("petition-count-phrase");
    return <div className="count-phrase">
        <span className="current-count">{count}</span>
        <span>{" " + text.trim().replace("[goal]", "" + goal)}</span>
    </div>;
};


const AfterSignatureView: React.FC<IShareButtonsProps> = (props) => {
    const {t} = useTranslation("common");
    const title = t("petition-after-signature-title");
    const text = t("petition-after-signature-text");
    const shareTitle = t("petition-after-signature-share-title");
    return <div className="after-signature-view">
        <H3>{title}</H3>
        <Paragraph>{text}</Paragraph>
        <H3>{shareTitle}</H3>
        <ShareButtons {...props} />
    </div>;
};

const Petition: React.FC<IPetitionProps & IPageContextProps> = (props) => {
    const {locale: language} = useRouter();

    const {context} = props;
    const countGoals = [10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 500000, 1000000, 2000000, 5000000, 100000000];
    const [count, setCount] = useState(0);
    const [signed, setSigned] = useState(false); // FIXME
    const formId = props.form.identifier;
    useEffect(() => {
        HttpService.instance().postd<{count: number;}>({url: "/api/form-submission-count", body: {formId}})
            .then(res => {setCount(res.count);})
            .catch(err => {console.error("Error fetching count: " + err);});
    }, [formId, signed]);
    const formProps = cloneDeep(props.form);
    formProps.fields.push(
        {key: "keepMePosted", type: "keepMePosted", required: false},
        {key: "acceptPrivacyPolicy", type: "acceptPrivacyPolicy", required: false});
    const getLanguageText = languageTextProvider(language);
    const title = getLanguageText(props.title);
    const text = getLanguageText(props.text);
    const nextGoal = find(countGoals, goal => goal > count);
    const reverse = isReverse(context.itemIdentifier, "petition", context.items);
    return <div className={classNames("petition", props.context.itemIdentifier, {reverse})}>
        <div key="content-container" className="content-container">
            <H2 className="header" text={title} />
            <CountPhrase count={count} goal={nextGoal} />
            <ProgressChart percentage={nextGoal ? count / nextGoal : null} />
            {!signed && <Form {...formProps} onSuccess={() => {setSigned(true);}} />}
            {signed && <AfterSignatureView {...props.shareButtons} variant={EShareButtonsVariant.normal} />}
            <HTML text={text} />
        </div>
    </div>;
};

export default Petition;