import React, {useState, useReducer} from "react";
import {useRouter} from "next/router";
import {useTranslation} from "next-i18next";
import {Grid, Input, Button as MuiButton} from "@mui/material";
import {
    first, map, find, forEach, filter, groupBy, sortBy,
    uniq, compact, intersection, reject, includes, clone, minBy, findIndex, slice, max
} from "lodash";

import {
    IPageContextProps, IEventGroup, IEvent, EEventView,
    EEventFilterType, IEventsSettings, EStyles, IContentDelegationProps
} from "./@def";
import {Button} from "./Button";
import {Selector} from "./Selector";
import {ChipSelector} from "./ChipSelector";
import {EventPlaceholder} from "./EventPlaceholder";
import {EEventCategory} from "../core/@def";
import {getLanguageText} from "../core/I18n";
import {ContentFactory} from "../factories/ContentFactory";
import {IconFactory} from "../factories/IconFactory";
import {DateTime} from "../core/DateTime";
import {BACKEND_DATE_FORMAT} from "../core/Constants";


interface IEventSelectorProps {
    settings: IEventsSettings;
    view: EEventView;
    setView: (view: EEventView) => void;
    setZipFilter: (input: string) => void;
    categories: EEventCategory[];
    categoryFilters: EEventCategory[];
    setCategoryFilter: (category: EEventCategory) => void;
    t: (value: string) => string;
}

const PAST_EVENT_STEP_SIZE = 10;

const isPublished = (event: IEvent) => {
    return event.published;
};

const filterTimelineItems = (items: IEventGroup[], {zipFilter, categories, categoryFilters}, languageId) => {
    let filtered: IEventGroup[] = [];
    forEach(items, (item) => {
        const languageItems = filter(item.events, (event) => {
            return isPublished(event);
        });
        if (languageItems.length) {
            const eventGroup = clone(item);
            eventGroup.events = sortBy(languageItems, item => getLanguageText(item.city, languageId, true));
            filtered.push(eventGroup);
        }
    });
    filtered = sortBy(filtered, item => minBy(item.events, event => event.start).start);
    const filterCategories = !(categories.length === categoryFilters.length || !categoryFilters.length);
    if (!zipFilter && !filterCategories) {
        return filtered;
    }
    if (filterCategories) {
        filtered = filter(filtered, dp => includes(categoryFilters, dp.category));
    }
    if (zipFilter) {
        const chZip = parseInt((`${zipFilter}0000`).substr(0, 4));
        const distanceItems = [];
        forEach(filtered, (eventGroup: IEventGroup) => {
            forEach(eventGroup.events, (event) => {
                distanceItems.push({
                    // Always show events which have no zip (e.g. digital events)
                    distance: event.zip ? Math.abs(event.zip - chZip) : 0,
                    event: event,
                    group: eventGroup
                });
            });
        });
        const inVicinity = filter(distanceItems, (dp) => dp.distance < 500);
        const grouped = groupBy(inVicinity, (dp) => dp.group.id);
        filtered = map(grouped, (dps) => {
            const group = first(dps).group;
            group.events = map(dps, (dp) => dp.event);
            return group;
        });
    }
    return filtered;
};

const EventsSelector: React.FC<IEventSelectorProps> = (props) => {
    const {settings, view, setView, setZipFilter, categories, categoryFilters, setCategoryFilter, t} = props;
    const iconFactory = IconFactory.instance();
    return <Grid key="events-selectors" className="events-selectors" container>
        {settings.views.length > 1 && <Grid item> <Selector
            items={map(settings.views, (id) => ({id, name: t(id)}))}
            selected={[view]}
            childProvider={({id, name}, className) =>
                <div key={id} className={className} onClick={() => setView(id as EEventView)}>{name}</div>}
            className="events-view-selector"
        /></Grid>}
        {find(settings.filters, filter => filter.type === EEventFilterType.zip)
            && <Grid item xs={12} sm={6}><Input className="zip-input" type="number" placeholder={t("zipch")}
                inputProps={{pattern: "[0-9]{4}", min: "1000", max: "9999"}}
                onChange={(e) => {
                    const value = e.target.value;
                    setZipFilter(value);
                }}
            /></Grid>}
        {categories.length > 1 && <Grid key="events-selectors" className="events-selectors" item xs={12} sm-={6}>
            <ChipSelector
                className="event-type-selector"
                items={map(categories, (id) => ({id, name: t(id)}))}
                selected={categoryFilters}
                onClick={(id: EEventCategory) => setCategoryFilter(id)}
                iconProvider={(item) => iconFactory.create(item.id)}
            />
        </Grid>}
    </Grid>;
};

const Events: React.FC<IContentDelegationProps<IEventGroup[], IEventsSettings> & IPageContextProps> = (props) => {
    const {context, data, config} = props;
    const {locale: language} = useRouter();
    const {t} = useTranslation("common");
    const categories = compact(uniq(map(data, item => item.category)));
    const categoryConfig = find(config.filters, filter => filter.type === EEventFilterType.category);
    const categoryDefaults = categoryConfig ? intersection(categoryConfig.defaults, categories) : [];
    const [view, _setView] = useState(EEventView.timeline);
    const setView = (view: EEventView) => {
        if (view === EEventView.createEvent) {
            window.scrollTo({top: 0, behavior: "smooth"});
        }
        _setView(view);
    };
    const [zipFilter, setZipFilter] = useState(null);
    const [categoryFilters, setCategoryFilter] = useReducer(
        (state, category) => {
            if (includes(state, category)) {
                return reject(state, (item) => item === category);
            }
            else {
                return [...state, category];
            }
        },
        categoryDefaults);
    const items = sortBy(
        filterTimelineItems(data, {zipFilter, categories, categoryFilters}, language),
        dp => dp.start);
    const todayIndex = findIndex(items, item => DateTime
        .create(item.start, BACKEND_DATE_FORMAT)
        .isSameOrAfter(DateTime.create(new Date())));
    const [startIndex, setStartIndex] = useState(max([todayIndex - PAST_EVENT_STEP_SIZE, 0]));
    const onLoadMore = () => setStartIndex(max([startIndex - PAST_EVENT_STEP_SIZE, 0]));

    const contentFactory = new ContentFactory();

    const isCreateView = view === EEventView.createEvent;
    const children = isCreateView ?
        < React.Fragment >
            <div key="create-events-toolbar" className="create-events-toolbar">
                <Button
                    className="prevent-stretch"
                    style={EStyles.secondary} type="back-to-events"
                    onClick={() => setView(EEventView.timeline)} />
            </div>
            {contentFactory.create(view, {...props, items}, context)}
        </React.Fragment > :
        <React.Fragment>
            <div key="events-toolbar" className="events-toolbar">
                <EventsSelector {...{settings: config, view, setView, setZipFilter, categories, categoryFilters, setCategoryFilter, t}} />
                <MuiButton className="add-event" startIcon={new IconFactory().create("plus")} onClick={() => setView(EEventView.createEvent)}>
                    {t("add-event")}
                </MuiButton>
            </div>
            {items.length
                ? contentFactory.create(view, {...props, items: slice(items, startIndex), onLoadMore: startIndex > 0 ? onLoadMore : null}, context)
                : <EventPlaceholder />}
        </React.Fragment>;
    return <div className="events">
        {children}
    </div >;
};

export default Events;