import React, {useState} from "react";
import classNames from "classnames";
import {slice, map, find, filter, min, isString, lowerCase, every, includes} from "lodash";
import {useRouter} from "next/router";
import {useTranslation} from "next-i18next";
import {Grid, Input, InputAdornment} from "@mui/material";

import {IContentDelegationProps, IPostSectionConfig, IPageContextProps, IPostProps, EStyles} from "./@def";
import {Button} from "./Button";
import {Card} from "./Card";
import {Post} from "./Post";
import {H2} from "./Typography";
import {languageTextProvider, getLanguageText as getLanguageTextWithFallbackOption} from "../core/I18n";
import {updatePostLink} from "../core/Utils";
import {IconFactory} from "../factories/IconFactory";


interface IOwnProps extends IContentDelegationProps<IPostProps[], IPostSectionConfig> { }


const PostSection: React.FC<IOwnProps & IPageContextProps> = (props) => {
    const {data, context, config = {title: "", n: 0, showSearch: false}} = props;
    const {locale: language} = useRouter();
    const {t} = useTranslation("common");
    const [limit, setLimit] = useState(config.initialLimit ?? 6);
    const [searchQuery, setSearchQuery] = useState("");
    if (context.query.id) {
        const dp = find(data, dp => dp.identifier === context.query.id);
        const origin = context.pageIdentifier;
        return <Post {...dp} origin={origin} context={context} />;
    }
    const getLanguageText = languageTextProvider(language);
    const languagePosts = filter(data, dp => !!(getLanguageText(dp.title) || getLanguageText(dp.lead)));
    if (!languagePosts.length) return null;
    let subset = slice(languagePosts, 0, config.n > 0 ? min([config.n, limit]) : limit);
    let displayLoadMore = limit < min([config.n || languagePosts.length, languagePosts.length]);
    if (searchQuery.trim()) {
        const termsToFind = lowerCase(searchQuery).split(" ");
        subset = filter(languagePosts, (post) => {
            const content = lowerCase(getLanguageText(post.title)) + " " + lowerCase(getLanguageText(post.lead));
            return every(termsToFind, (term) => includes(content, term));
        });
        displayLoadMore = false;
    }
    const title = isString(config.title) ? t(config.title) : getLanguageTextWithFallbackOption(config.title, language, true);
    return <div className={classNames("post-section", "next-post", context.itemIdentifier)}>
        <H2 text={title} />
        {config.showSearch ? <Input className="post-search-query" placeholder={t("post-search-query")}
            endAdornment={
                <InputAdornment position="end">
                    {IconFactory.instance().create("search")}
                </InputAdornment>}
            onChange={(e) => {
                const value = e.target.value;
                setSearchQuery(value);
            }} /> : null}
        <Grid container justifyContent="space-between">
            {map(subset, (dp: IPostProps) => {
                const cardData = updatePostLink(dp);
                return <Grid key={cardData.identifier} item xs={12} md={6} lg={4}>
                    <Card key={cardData.identifier} {...cardData} />
                </Grid>;
            })}
        </Grid>
        {displayLoadMore &&
            <Button className="load-more" style={EStyles.primary} type="load-more" onClick={() => {setLimit(limit + 6);}} />}
    </div>;
};

export default PostSection;