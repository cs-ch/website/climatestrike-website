import {useTranslation} from "next-i18next";
import React from "react";

import {H4, Paragraph} from "./Typography";

export const EventPlaceholder: React.FC = () => {
    const {t} = useTranslation("common");
    return <div className="event-placeholder">
        <div className="text">
            <H4 className="no-events-title">{t("no-events-title")}</H4>
            <Paragraph className="no-events-content"> {t("no-events-content")}</Paragraph>
        </div>
    </div>;
};