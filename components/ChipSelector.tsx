import React from "react";
import {map, includes} from "lodash";
import {Grid, Chip} from "@mui/material";
import classNames from "classnames";

import {IChipSelectorProps} from "./@def";


export const ChipSelector = (props: IChipSelectorProps) => {
    const {items, selected, disabled, onClick, iconProvider} = props;
    const className = classNames("chip-selector", props.className);
    return <div className={className}>
        <Grid container>
            {map(items, item => {
                const id = item.id;
                const isSelected = includes(selected, id);
                const isDisabled = includes(disabled, id);
                return <Chip
                    key={id}
                    className={classNames(id, {selected: isSelected}, "chip")}
                    disabled={isDisabled}
                    label={item.name}
                    icon={iconProvider && iconProvider(item)}
                    onClick={(e) => onClick(id, e)} />;
            })}
        </Grid>
    </div>;
};
