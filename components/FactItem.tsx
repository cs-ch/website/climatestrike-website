import React from "react";
import {useRouter} from "next/router";
import {useTranslation} from "next-i18next";
import classNames from "classnames";
import {map} from "lodash";

import {IPageContextProps, IFactItemProps} from "./@def";
import {Image} from "./Core";
import {languageTextProvider, getLanguageImage} from "../core/I18n";
import {LinkButton} from "./Button";
import {H3, Paragraph} from "./Typography";
import {isReverse} from "../core/Utils";


const FactItem: React.FC<IFactItemProps & IPageContextProps> = (props) => {
    const {context, links} = props;
    const {locale: language} = useRouter();
    const {t} = useTranslation("common");
    const {itemIdentifier} = context;
    const getLanguageText = languageTextProvider(language);
    const title = getLanguageText(props.title);
    const lead = getLanguageText(props.lead);
    const icon = getLanguageImage(props.icon, language);
    if (!title) return null;
    const hasIcon = !!icon;
    const headlineStyle = hasIcon ? {} : {width: "100%"};
    const reverse = isReverse(itemIdentifier, "fact-item", context.items);
    return <div className={classNames("fact-item", itemIdentifier)}>
        <div key="head" className={classNames("head", {reverse})}>
            {hasIcon && <div key="icon" className="icon"> <Image image={props.icon} /></div>}
            <div key="headline" className={"headline"} style={headlineStyle} >
                <div key="content" className="content">
                    <H3 key="title" text={title} />
                    {lead && <Paragraph key="description" text={lead} />}
                    <div className="link-section">
                        {links.length === 0
                            ? <div className="post-will-follow">{t("post-will-follow")}</div>
                            : map(links, (link, idx) => <LinkButton key={`link-${idx}`} {...link} />)
                        }
                    </div>
                </div >
            </div>
        </div>
    </div >;
};

export default FactItem;