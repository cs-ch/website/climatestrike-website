import React from "react";
import {useRouter} from "next/router";
import parse, {attributesToProps, domToReact} from "html-react-parser";

import {IHTMLProps} from "./@def";
import {ITranslation} from "../core/@def";
import {getLanguageText} from "../core/I18n";
import {H1, H2, H3, H4, H5, H6} from "./Typography";


const getOptions = () => {
    const options = {
        replace: (node) => {
            const props = attributesToProps(node.attribs);
            const children = node.children;
            switch (node.name) {
                case "h1": return <H1 {...props}>{domToReact(children, options)}</H1>;
                case "h2": return <H2 {...props}>{domToReact(children, options)}</H2>;
                case "h3": return <H3 {...props}>{domToReact(children, options)}</H3>;
                case "h4": return <H4 {...props}>{domToReact(children, options)}</H4>;
                case "h5": return <H5 {...props}>{domToReact(children, options)}</H5>;
                case "h6": return <H6 {...props}>{domToReact(children, options)}</H6>;
                default:
            }
        }
    };
    return options;
};

export const HTML: React.FC<IHTMLProps> = (props) => {
    if (!props.text) return null;
    const options = getOptions();
    return <div className="markdown">
        {parse(props.text, options)}
    </div>;
};

const LongText: React.FC<{text: ITranslation[]}> = (props) => {
    const {locale: language} = useRouter();
    const text = getLanguageText(props.text, language);
    return <HTML text={text} />;
};

export default LongText;