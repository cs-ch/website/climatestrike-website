import React from "react";
import {isNil} from "lodash";
import classNames from "classnames";

interface ITypographyProps {
    key?: string | number;
    className?: string;
    text?: string;
    gutterBottom?: boolean;
    variant?: string;
    idProvider?: (text: string) => string;
    children?: any;
}

const classNameProvider = (props: ITypographyProps) => {
    const className = classNames(
        props.className,
        props.variant,
        isNil(props.gutterBottom) || props.gutterBottom ? null : "no-gutter"
    );
    return className;
};

export const H1: React.FC<ITypographyProps> = (props) => {
    const className = classNameProvider({...props, variant: props.variant || "h1"});
    const text = props.text || props.children as string;
    return <h1 className={className}>{text}</h1 >;
};

export const H2: React.FC<ITypographyProps> = (props) => {
    const className = classNameProvider({...props, variant: props.variant || "h2"});
    const text = props.text || props.children as string;
    return <h2 className={className}>{text}</h2 >;
};

export const H3: React.FC<ITypographyProps> = (props) => {
    const className = classNameProvider({...props, variant: props.variant || "h3"});
    const text = props.text || props.children as string;
    return <h3 className={className}>{text}</h3>;
};

export const H4: React.FC<ITypographyProps> = (props) => {
    const className = classNameProvider({...props, variant: props.variant || "h4"});
    const text = props.text || props.children as string;
    return <h4 className={className}>{text}</h4 >;
};

export const H5: React.FC<ITypographyProps> = (props) => {
    const className = classNameProvider({...props, variant: props.variant || "h5"});
    const text = props.text || props.children as string;
    return <h5 className={className}>{text}</h5 >;
};

export const H6: React.FC<ITypographyProps> = (props) => {
    const className = classNameProvider({...props, variant: props.variant || "h6"});
    const text = props.text || props.children as string;
    return <h6 className={className}>{text}</h6 >;
};

export const Paragraph: React.FC<ITypographyProps> = (props) => {
    const text = props.text || props.children;
    const className = classNameProvider({...props, variant: props.variant || "p"});
    return <p className={className}> {text}</p >;
};

export const LI: React.FC<ITypographyProps> = (props) => {
    const text = props.text || props.children;
    const className = classNameProvider({...props, variant: props.variant || "li"});
    return <li className={className}> {text}</li >;
};
