import React from "react";
import {useRouter} from "next/router";
import classNames from "classnames";

import {IPageContextProps, IJoinItemProps} from "./@def";
import {Image} from "./Core";
import {languageTextProvider} from "../core/I18n";
import {H3, Paragraph} from "./Typography";
import {LinkButton} from "./Button";
import {isReverse} from "../core/Utils";



const JoinItem: React.FC<IJoinItemProps & IPageContextProps> = (props) => {
    const {context, image, icon} = props;
    const {locale: language} = useRouter();
    const getLanguageText = languageTextProvider(language);
    const title = getLanguageText(props.title);
    const text = getLanguageText(props.text);
    const reverse = isReverse(context.itemIdentifier, "join-item", context.items);
    return <div className={classNames("join-item", props.context.itemIdentifier, {reverse})}>
        <div key="image-container" className="image-container">
            <Image className="image" image={image} />
        </div>
        <div key="content-container" className="content-container">
            <Image className="icon" image={icon} />
            <H3 className="header" text={title} />
            <Paragraph className="text" text={text} />
            <LinkButton {...props.link} ></LinkButton>
        </div>
    </div>;
};

export default JoinItem;