import React from "react";
import {useRouter} from "next/router";
import {useTranslation} from "next-i18next";
import classNames from "classnames";
import {isBoolean} from "lodash";
import {IconButton as MuiIconButton} from "@mui/material";

import {
    EStyles, IButtonProps,
    IMoreLessButtonProps, IIconButtonProps, ILinkButtonProps
} from "./@def";
import {Link} from "./Core";
import {getLanguageText, languageTextProvider} from "../core/I18n";
import {IconFactory} from "../factories/IconFactory";


export const Button: React.FC<IButtonProps> = (props) => {
    const {className, type, href, disabled, onClick} = props;
    const {locale: language} = useRouter();
    const {t} = useTranslation("common");
    const text = getLanguageText(props.text, language) || t(type);
    const buttonType = type == "submit" ? "submit" : "button";
    return <button className={classNames("button", props.style, className)} type={buttonType} disabled={isBoolean(disabled) ? disabled : false} onClick={() => onClick && onClick()}>
        {href && <Link href={href} text={text} />}
        {!href && <div>{text}</div>}
    </button>;
};

export const LinkButton: React.FC<ILinkButtonProps> = (props) => {
    const {locale: language} = useRouter();
    const {t} = useTranslation("common");
    const className = classNames("button", props.style || EStyles.primary);
    const getLanguageText = languageTextProvider(language);
    const href = getLanguageText(props.link) || props.href;
    const text = getLanguageText(props.text) || t(props.type);
    return <Link className={className} href={href} text={text} />;
};

export const ExpandCollapseButton: React.FC<IMoreLessButtonProps> = (props) => {
    const {expanded, onClick} = props;
    const {t} = useTranslation("common");
    const type = expanded ? "less" : "more";
    const iconType = expanded ? "collapse" : "expand";
    const icon = IconFactory.instance().create(iconType);
    const text = t(type);
    return <button
        className={classNames("button", props.style, "more-less-button")}
        type="button"
        onClick={() => onClick && onClick()}>
        <div>
            <span key="text">{text}</span>
            <span key="icon" className="expand-collapse-icon">{icon}</span>
        </div>
    </button>;
};

export const IconButton: React.FC<IIconButtonProps> = (props) =>
    <MuiIconButton {...props} className={classNames(props.className, "icon-button")} />;

export const ScrollToTopButton: React.FC = () => {
    const {t} = useTranslation("common");
    return <IconButton
        aria-label={t("scroll-to-top")}
        className={classNames("button", "scroll-to-top")}
        onClick={() => window.scrollTo({top: 0})}>
        {new IconFactory().create("to-top")}
    </IconButton>;
};

export default LinkButton;