import React, {useRef} from "react";
import {useTranslation} from "next-i18next";
import {useRouter} from "next/router";
import {map, compact, filter, cloneDeep} from "lodash";

import {IPageContextProps, IPostProps, EStyles} from "./@def";
import {Button} from "./Button";
import {H1, Paragraph} from "./Typography";
import ImageWithCaption from "./ImageWithCaption";
import PostSection from "./PostSection";
import {ShareButtons} from "./ShareButton";
import {TableOfContent} from "./TableOfContent";
import {ELanguages} from "../core/@def";
import {DateTime} from "../core/DateTime";
import {languageTextProvider} from "../core/I18n";
import {slugify} from "../core/Utils";
import {ContentFactory} from "../factories/ContentFactory";
import {BACKEND_DATE_FORMAT} from "../core/Constants";


interface IOriginProps {
    origin: string;
}

export const Post: React.FC<IPostProps & IOriginProps & IPageContextProps> = (props) => {
    const {context, date} = props;
    const {locale: language, push} = useRouter();
    const {t} = useTranslation("common");
    const created = DateTime.create(date, BACKEND_DATE_FORMAT).formatDate();
    const getLanguageProps = languageTextProvider(language);
    const author = getLanguageProps(props.author);
    const title = getLanguageProps(props.title);
    const lead = getLanguageProps(props.lead);
    const relatedPosts = filter(props.relatedPosts, post => {
        const relatedPostTitle = getLanguageProps(post.title);
        const relatedPostLead = getLanguageProps(post.lead);
        return !!(relatedPostTitle || relatedPostLead);
    });
    const hasContent = title || lead;
    const titleOrFallback = hasContent ? title : t("no-content-title");
    const leadOrFallback = hasContent ? lead : t("no-content-text");
    const contentFactory = new ContentFactory();
    const ref = useRef(null);
    return <div className="post-container">
        <Button key="back-button"
            style={EStyles.secondary}
            text={[{language: ELanguages.all, text: t("back")}]}
            onClick={() => {
                if (window.history.length > 2) {
                    window.history.back();
                }
                else {
                    push("/");
                }
            }} />
        <div key="post" className="post">
            {author && <Paragraph className="date-author" text={compact([created, author]).join(", ")} />}
            <H1 className="title" text={titleOrFallback} />
            <Paragraph className="lead" text={leadOrFallback} />
            <ShareButtons />
            <ImageWithCaption image={props.image} />
            {hasContent && <div ref={ref} className="post-content">
                {<TableOfContent
                    title={title}
                    reference={ref} />}
                {map(props.content, (content, idx) =>
                    <React.Fragment key={`${slugify(title)}-content-${idx}`}>
                        {contentFactory.create(content.type, content, {...context, itemIdentifier: "" + idx})}
                    </React.Fragment>)
                }
            </div>}
        </div>
        <div id="post-end" />
        {relatedPosts.length > 0 && <PostSection
            context={{...cloneDeep(context), query: {id: null}}}
            childType="card"
            data={relatedPosts}
            config={{title: "related-posts", n: 0}} />}
    </div>;
};