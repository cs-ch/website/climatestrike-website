import React from "react";
import {map, merge, groupBy, find, uniq, round, compact} from "lodash";
import {Grid} from "@mui/material";
import classNames from "classnames";

import {IPageContext, IPageWrapperProps} from "./@def";
import {Layout} from "./Layout";
import {BackgroundImage} from "./BackgroundImage";
import {EImageRole} from "../core/@def";
import {ContentFactory} from "../factories/ContentFactory";
import {ScrollToTopButton} from "./Button";


export const AbstractPage: React.FC<IPageWrapperProps> = (props) => {
    const factory = new ContentFactory();
    const {activePage, pages, items, query} = props;
    const activePageIdentifier = activePage ? activePage.identifier : null;
    const page = find(pages, page => page.identifier === activePageIdentifier);
    const context: IPageContext = {
        pageIdentifier: activePageIdentifier,
        itemIdentifier: null,
        items,
        query
    };
    const showImage = !page.imageRole || (page.imageRole == EImageRole.pageAndPreview);
    const shoudShowScrollToTop = !page?.config?.disableScrollToTop;
    const rowModel = groupBy(items, item => item.grouping);
    return <Layout {...props}>
        <main key={activePageIdentifier} className={classNames("page", activePageIdentifier)} >
            {showImage && <BackgroundImage identifier={activePageIdentifier} image={page.image} />}
            {map(rowModel, (row, group) => {
                const childTypes = uniq(map(row, item => item.type));
                const children = compact(map(row, item => {
                    const child = factory.create(
                        item.type,
                        item.config,
                        merge({}, context, {itemIdentifier: item.identifier})
                    );
                    if (!child) return null;
                    return <Grid
                        key={`${group}-${item.identifier}`}
                        className={classNames("page-content", item.type, item.identifier)}
                        item
                        xs={12}
                        sm={round(12 / row.length) as any}>
                        {child}
                    </Grid>;
                }));
                if (!children.length) return null;
                return <Grid
                    key={group}
                    justifyContent="space-between"
                    className={classNames("page-content-group", group, group.split("."), childTypes)}
                    container
                >
                    <div key={group + "-anchor"} id={group} className="fragment-anchor" />
                    {children}
                </Grid>;
            })}
            {shoudShowScrollToTop ? <ScrollToTopButton /> : null}
        </main >
    </Layout>;
};