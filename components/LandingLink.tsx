import React from "react";
import {useRouter} from "next/router";
import classNames from "classnames";

import {IPageContextProps, ILandingLinkProps} from "./@def";
import {LinkButton} from "./Button";
import {Image} from "./Core";
import Title from "./Title";
import {Paragraph} from "./Typography";
import {languageTextProvider} from "../core/I18n";


const LandingLink: React.FC<ILandingLinkProps & IPageContextProps> = (props) => {
    const {context, title, link, icon} = props;
    const {locale: language} = useRouter();
    const getLanguageText = languageTextProvider(language);
    const titleLevel = props.titleLevel || 2;
    const text = getLanguageText(props.text);
    if (!title.length) return null;
    return <div className={classNames("landing-link", context.itemIdentifier, `level-${titleLevel}`)}>
        {icon && <div className="icon-container">
            <Image className="landing-link-icon" image={icon} />
        </div>}
        <div className="header-container"><Title {...props} /></div>
        {text && <div className="text-container"><Paragraph text={text} /></div>}
        {link && <div className="button-container"><LinkButton {...link} /></div>}
    </div >;
};

export default LandingLink;