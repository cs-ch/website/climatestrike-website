import * as React from "react";
import {useRouter} from "next/router";
import classNames from "classnames";
import {isNil} from "lodash";

import {IExpandableDescriptionProps, IPageContextProps, EStyles} from "./@def";
import {ExpandCollapseButton} from "./Button";
import {HTML} from "./HTML";
import {languageTextProvider} from "../core/I18n";
import {TitleFactory} from "../factories/TitleFactory";
import {EXPANDABLE_DESCRIPTION_CLASSNAME} from "../core/Constants";


const ExpandableDescription: React.FC<IExpandableDescriptionProps & IPageContextProps> = (props) => {
    const [expanded, setExpanded] = React.useState(false);
    const style = props.style;
    const {locale: language} = useRouter();
    const isPrimary = style === EStyles.primary || isNil(style);
    const getLanguageText = languageTextProvider(language);
    const title = getLanguageText(props.title);
    const lead = getLanguageText(props.lead);
    const text = getLanguageText(props.text);
    if (!title && !lead && !text) return null;
    return <div className={classNames(EXPANDABLE_DESCRIPTION_CLASSNAME, style)}>
        {TitleFactory.instance().create(props.titleLevel, title)}
        {lead && <HTML text={lead} />}
        {!isPrimary && <ExpandCollapseButton
            style={EStyles.secondary}
            expanded={expanded}
            onClick={() => setExpanded(!expanded)}
        />}
        <div style={{
            display: expanded ? "inherit" : "none",
            visibility: expanded ? "inherit" : "hidden"
        }}>
            <HTML text={text} />
        </div>
        {isPrimary && <ExpandCollapseButton
            style={EStyles.primary}
            expanded={expanded}
            onClick={() => setExpanded(!expanded)}
        />}
    </div >;
};

export default ExpandableDescription;