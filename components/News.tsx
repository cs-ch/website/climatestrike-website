import React, {useState} from "react";
import {useTranslation} from "next-i18next";
import {useRouter} from "next/router";
import {map, filter, take, includes, slice} from "lodash";
import classNames from "classnames";
import {FormGroup, FormLabel} from "@mui/material";

import {IPageContextProps, IContentDelegationProps, IPostProps, EPostCategory, EStyles} from "./@def";
import {Card} from "./Card";
import {ToggleSwitch} from "./ToggleSwitch";
import {Button} from "./Button";
import {languageTextProvider} from "../core/I18n";
import {updatePostLink} from "../core/Utils";


const News: React.FC<IContentDelegationProps<IPostProps[]> & IPageContextProps> = (props) => {
    const {context} = props;
    const {locale: language} = useRouter();
    const {t} = useTranslation("common");
    const [limit, setLimit] = useState(6);
    const getLanguageText = languageTextProvider(language);
    let languagePosts = filter(props.data, dp => {
        const hasContent = !!(getLanguageText(dp.title) || getLanguageText(dp.lead));
        return hasContent;
    });
    languagePosts = map(languagePosts, dp => updatePostLink(dp));
    const [categorySelection, setCategorySelection] = useState(EPostCategory.otherNews);
    const [checked, setChecked] = React.useState(false);
    const [boostCount, setBoostCount] = React.useState(1);
    const goodNewsSliceSize = 3;

    const toggleChecked = () => {
        setChecked(prev => {
            const next = !prev;
            setCategorySelection(next ? EPostCategory.goodNews : EPostCategory.otherNews);
            return next;
        });
    };
    const slicedPosts = slice(languagePosts, 0, limit);
    let filtered = slicedPosts;
    const allGoodNews = filter(slicedPosts, dp => dp.category == EPostCategory.goodNews);
    let goodNews = [];

    const updateNewsFilter = () => {
        goodNews = take(allGoodNews, goodNewsSliceSize * boostCount);
        filtered = filter(slicedPosts, dp => !includes(goodNews, dp));
    };

    const showMoreGoodNews = () => {
        setBoostCount(prev => prev + 1);
        updateNewsFilter();
    };

    if (categorySelection !== EPostCategory.otherNews) {
        updateNewsFilter();
    }
    const displayLoadMore = limit < languagePosts.length;


    return <div className={classNames("posts", context.itemIdentifier)}>

        <div className={classNames("good-news-container", checked ? "active" : "")}>
            <FormGroup className="good-news-filter">
                <img id="good-news-boost-icon" src="/static/images/rocket.svg" />
                <FormLabel className="label" htmlFor="good-news-boost-toggle" >{t(EPostCategory.goodNews)}</FormLabel>
                <p>{t("good-news-boost-text")}</p>
                <ToggleSwitch id="good-news-boost-toggle" checked={checked} onChange={toggleChecked} />
            </FormGroup>

            {checked &&
                <>
                    <div className="grid">{map(goodNews, (dp) => <Card key={dp.identifier} {...dp} />)}</div>
                    {goodNewsSliceSize * boostCount < allGoodNews.length &&
                        <Button onClick={showMoreGoodNews} style={EStyles.primary} type="more-good-news" />
                    }
                </>
            }
        </div>

        <div className="grid">
            {map(filtered, (dp) => {
                return <Card key={dp.identifier} {...dp} />;
            })}
        </div>

        {displayLoadMore &&
            <Button className="load-more" style={EStyles.primary} type="load-more" onClick={() => {setLimit(limit + 6);}} />}

    </div>;
};

export default News;