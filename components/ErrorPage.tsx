import React from "react";
import {NextPage} from "next";
import {useTranslation} from "next-i18next";
import classNames from "classnames";

import {IPageWrapperProps, EStyles} from "../components/@def";
import {H1, H2} from "../components/Typography";
import {Button} from "../components/Button";
import {Layout} from "./Layout";


export const ErrorPage: NextPage<IPageWrapperProps & {statusCode?: string}> = (props) => {
    const {activePage} = props;
    const {t} = useTranslation("common");
    const statusCode = props.statusCode ?? (!activePage ? "404" : "500");
    return <Layout {...props}>
        <main key="error" className={classNames("page", "error")}>
            <div className="page-content-group">
                <div key="error-navigation" className="error-navigation">
                    <Button style={EStyles.secondary} href="/" type="back-to-home" />
                </div>
                <div key="error-content" className="error-content">
                    <H1 key="error-title" className="error-title">{t(`${statusCode}-title`)}</H1>
                    <H2 key="error-subtitle" className="error-subtitle">{t(`${statusCode}-subtitle`)}</H2>
                    <img className="error-image" src="/static/images/404.svg" />
                </div>
            </div>
        </main>
    </Layout>;
};