import React from "react";
import {useRouter} from "next/router";
import classNames from "classnames";

import {IImageWCaptionProps} from "./@def";
import {Image} from "./Core";
import {getLanguageImage} from "../core/I18n";

const ImageWithCaption: React.FC<IImageWCaptionProps & {className?: string}> = (props) => {
    const {locale: language} = useRouter();
    const {image} = props;
    const img = getLanguageImage(image, language);
    if (!img) return null;
    return <div>
        <Image className={classNames("cover-image")} image={image} />
        {img.caption && <div className={classNames("image-caption")} >{img.caption}</div>}
    </div>;
};

export default ImageWithCaption;