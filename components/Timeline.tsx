import React, {useEffect, useRef, useReducer, useState} from "react";
import {useRouter} from "next/router";
import {useTranslation} from "next-i18next";
import {findIndex, findLastIndex, map, compact, minBy, includes} from "lodash";
import classNames from "classnames";
import {Grid2} from "@mui/material";

import {IEventsProps, IPageContextProps, IEventGroup, IEvent, EStyles} from "./@def";
import {H4, H5, H6, Paragraph} from "./Typography";
import {HTML} from "./HTML";
import {Button, IconButton} from "./Button";
import {ChipSelector} from "./ChipSelector";
import {ContactLinkItem} from "./ContactLinkItem";
import {ITranslation, ETimeUnit, ELanguages} from "../core/@def";
import {getTextSelection} from "../core/Utils";
import {DateTime} from "../core/DateTime";
import {getLanguageText, languageTextProvider} from "../core/I18n";
import {IconFactory} from "../factories/IconFactory";
import {BACKEND_DATE_FORMAT} from "../core/Constants";


interface IEventDetailProps {
    language: string;
    t: (raw: string) => string;
    isSingleEvent: boolean
    description: string;
    eventGroup: IEventGroup;
}

interface ITimelineContentProps {
    item: IEventGroup;
    language: string;
    t: (raw: string) => string;
}

interface IDetailContentProps {
    event: IEvent;
    getLanguageText: (t: ITranslation[]) => string;
    t: (raw: string) => string;
}

const renderTodayCircle = () => {
    const r = 10;
    const d = 2 * r;
    const stroke = 2;
    const pos = r + stroke / 2;
    const height = 100;
    return <svg height={height} width={d + stroke}>
        <line className="today-line" x1={r} y1="0" x2={r} y2={height} />
        <circle className="today-circle" cx={pos} cy={pos} r={r} strokeWidth={stroke} />
    </svg>;
};

const DetailContent: React.FC<IDetailContentProps> = (props) => {
    const {event, t, getLanguageText} = props;
    const link = event.link;
    const city = getLanguageText(event.city);
    const location = getLanguageText(event.location);
    const zip = event.zip;
    const start = DateTime.create(event.start);
    const end = DateTime.create(event.end);
    const details = getLanguageText(event.description);
    return <div key="detail-content" className="detail-content">
        {<div className="event-when">
            <H6>{t("when")}</H6>
            <Paragraph>{start.formatDateTime()
                + (event.end ? ` - ${end.isSame(start, ETimeUnit.day)
                    ? end.formatTime()
                    : end.formatDateTime()}`
                    : "")}
            </Paragraph>
        </div>}
        {(city || location) && <div className="event-where">
            <H6>{t("where")}</H6>
            <Paragraph>{compact([location, compact([zip, city]).join(" ")]).join(", ")}</Paragraph>
        </div>}
        {details && <div className="event-details">
            <H6>{t("event-details")}</H6>
            <HTML text={details} />
        </div>}
        {link && <ContactLinkItem
            identifier="event-link"
            className="event-link"
            type={includes(["website", "facebook"], link.type) ? link.type : "website"}
            href={getLanguageText(link.link)}
            onClick={(e) => e.stopPropagation()} />}
    </div >;
};

const EventDetail: React.FC<IEventDetailProps> = (props) => {
    const {eventGroup, isSingleEvent, language, t} = props;
    const {events} = eventGroup;
    const getLanguageText = languageTextProvider(language, true);
    const [selected, setSelected] = useState("0");
    const event: IEvent = events[selected];

    return <div className="event-detail">
        {isSingleEvent ? null : <ChipSelector
            selected={[selected]}
            className="event-location-selector"
            items={map(events, (event, idx) => ({id: "" + idx, name: getLanguageText(event.city)}))}
            onClick={(idx, e) => {e.stopPropagation(); setSelected(idx);}}
        />}
        <DetailContent {...{event, t, getLanguageText}} />
    </div>;
};

const TimelineContent: React.FC<ITimelineContentProps> = (props) => {
    const {item, t, language} = props;
    const [expanded, toggleExpanded] = useReducer(state => !state, false);
    const description = item.description && getLanguageText(item.description, language, true);
    const iconFactory = IconFactory.instance();
    const isSingleEvent = item.events.length === 1;
    return <div
        className="timeline-content"
        onClick={() => {
            /**
             * Prevent toggle of expanded when text is selected.
             * Maybe the user wants to copy/past the event text,
             * in this case we should not call toggleExpanded().
             * Possible future Optimization: Only do that when
             * the text matches description & title, ...
             */
            const selection = getTextSelection();
            if (!selection) {
                toggleExpanded();
            }
        }} >
        <H5 variant="h4" >
            <div className="header">
                <div className="text">{getLanguageText(item.title, language, true)}</div>
                <IconButton aria-label={expanded ? t("collapse") : t("expand")}>
                    {expanded ? iconFactory.create("collapse") : iconFactory.create("expand")}
                </IconButton>
            </div>
        </H5>
        {(!expanded || isSingleEvent) &&
            <Paragraph className="location" text={map(item.events, (event) => getLanguageText(event.city, language, true)).join(", ")} />}
        {expanded && <EventDetail language={language} t={t} isSingleEvent={isSingleEvent} description={description} eventGroup={item} />}
    </div >;
};

const Timeline: React.FC<IEventsProps & IPageContextProps> = (props) => {
    const {context, items, onLoadMore} = props;
    const {t} = useTranslation("common");
    const {locale: language} = useRouter();
    const ref = useRef<HTMLElement>();
    const className = classNames("timeline", context.itemIdentifier);
    const now = DateTime.create();
    const lastBefore = findLastIndex(items, item => DateTime.create(item.start, BACKEND_DATE_FORMAT).isSameOrBefore(now, ETimeUnit.day));
    let todayIndex = findIndex(items, item => DateTime.create(item.start, BACKEND_DATE_FORMAT).isSame(now, ETimeUnit.day));
    if (!(todayIndex >= 0)) {
        items.splice(lastBefore + 1, 0, {
            identifier: "today",
            title: [{"language": ELanguages.all, "text": t("today")}],
            description: null,
            start: now.format(),
            end: now.format(),
            category: "today" as any,
            multipleChildren: false,
            events: []
        });
        todayIndex = lastBefore + 1;
    }
    // Scroll to today: https://www.robinwieruch.de/react-scroll-to-item
    useEffect(
        () => {
            if (ref.current) {
                /**
                 * Strangely, only "ref.current.scrollIntoView to view does not work
                 * when switching from the "Join" to the "Events" page.
                 * A little hacky, but it seems to work with a timeout.
                 */
                setTimeout(
                    () => ref.current.scrollIntoView({behavior: "smooth", block: "center"}),
                    50);
            }
        });
    const width = "10%";
    const iconFactory = new IconFactory();
    return (
        <div className={className} >
            {onLoadMore && <div className="timeline-load-more">
                <Button style={EStyles.primary} type="load-more" onClick={props.onLoadMore} />
            </div>}
            {map(items, (item, index) => {
                const itemClassname = classNames("timeline-event", item.category);
                if (item.category === "today" as any) {
                    return <Grid2 ref={index == todayIndex ? ref as any : null} key="today" className={classNames("timeline-today", "today")} wrap="nowrap" justifyContent="space-between" alignItems="stretch" container>
                        <div className="timeline-date" style={{width}} />
                        <div className={classNames("timeline-circle-container", "today")}>
                            {renderTodayCircle()}
                        </div>
                        <div className="timeline-content" style={{width: "70%"}}>
                            <Paragraph text={t("today")} />
                        </div>
                    </Grid2>;
                }
                const start = minBy(item.events, event => event.start).start;
                const startMoment = DateTime.create(start);
                const beforeNow = startMoment.isBefore(now);
                const afterNow = startMoment.isAfter(now);
                return <Grid2 ref={index == todayIndex ? ref as any : null} key={index} className={itemClassname} wrap="nowrap" justifyContent="space-between" alignItems="stretch" container >
                    <div className="timeline-date" style={{width}}>
                        <div className="timeline-day">
                            <H4 gutterBottom={false}>
                                <div>{startMoment.format("DD.")}</div>
                                <div>{startMoment.format("MMM")}</div>
                            </H4>
                        </div>
                    </div>
                    <div className={classNames(
                        "timeline-circle-container",
                        item.category,
                        {
                            "before-now": beforeNow,
                            "after-now": afterNow,
                        })
                    }>
                        <div className={classNames("timeline-circle")}>{iconFactory.create(item.category, {})}</div>
                    </div>
                    <div className="timeline-content-container" style={{width: "70%"}}>
                        <TimelineContent item={item} language={language} t={t} />
                    </div>
                </Grid2>;
            })}
        </div >
    );
};

export default Timeline;