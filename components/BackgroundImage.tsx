import React from "react";
import {useRouter} from "next/router";
import classNames from "classnames";

import {IBackgroundImageProps} from "./@def";
import {Image} from "./Core";
import {getLanguageImage} from "../core/I18n";

export const BackgroundImage: React.FC<IBackgroundImageProps> = (props) => {
    const {locale: language} = useRouter();
    const image = getLanguageImage(props.image, language);
    if (!image) return null;
    return <Image className={classNames("background-image", props.identifier)} image={props.image} />;
};