import React from "react";
import classNames from "classnames";
import {HTML} from "./HTML";

import {IPageContextProps, ITextProps} from "./@def";
import {languageTextProvider} from "../core/I18n";
import {H2} from "./Typography";
import {useRouter} from "next/router";


const Text: React.FC<ITextProps & IPageContextProps> = (props) => {
    const {locale: language} = useRouter();
    const getLanguageText = languageTextProvider(language);
    const title = getLanguageText(props.title);
    const text = getLanguageText(props.text);
    if (!(title || text)) return null;
    return <div className={classNames("text", props.context.itemIdentifier)}>
        <H2 text={title} />
        <HTML text={text} />
    </div>;
};

export default Text;