import React, {createRef, useEffect} from "react";
import {useRouter} from "next/router";
import classNames from "classnames";
import {sortBy, map, reverse} from "lodash";

import {IPageContextProps, IMilestonesProps} from "./@def";
import {H3, H4} from "./Typography";
import {languageTextProvider, getLanguageImage} from "../core/I18n";
import {DateTime} from "../core/DateTime";
import {BACKEND_DATE_FORMAT} from "../core/Constants";



const Milestones: React.FC<IMilestonesProps & IPageContextProps> = (props) => {
    const {locale: language} = useRouter();
    const getLanguageText = languageTextProvider(language);
    const title = getLanguageText(props.title);

    const entries = reverse(sortBy(props.entries, entry => entry.date));
    const containerRef = createRef<HTMLDivElement>();
    useEffect(() => {
        const useSmallBackground = window.innerWidth < 500;
        const background = useSmallBackground ? props.backgroundSmall : props.backgroundLarge;
        const img = getLanguageImage(background, language);
        containerRef.current.style.backgroundImage = `url('${img.url}')`;
    });
    return <div className={classNames("milestone", props.context.itemIdentifier)}>
        <H3 className="header" text={title} />
        <div className="milestones-container" ref={containerRef}>
            {map(entries, (entry, idx) => {
                const date = DateTime.create(entry.date, BACKEND_DATE_FORMAT).format("dd. MMMM yyyy");
                return <div key={idx} className="milestone-entry">
                    <H4>{date}</H4>
                    <p>{getLanguageText(entry.text)}</p>
                </div>;
            })}
        </div>
    </div>;
};

export default Milestones;