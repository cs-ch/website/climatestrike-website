import React, {useEffect, useState} from "react";
import {compact, findLast, forEach, get, includes, map, max, min, minBy, replace, times, toLower} from "lodash";
import classNames from "classnames";

import {ProgressCircle} from "./ProgressCircle";
import {slugify} from "../core/Utils";
import {EXPANDABLE_DESCRIPTION_CLASSNAME} from "../core/Constants";


interface ITableOfContentProps {
    title: string;
    reference: React.MutableRefObject<any>;
    levels?: number;
}

interface IHeaderModelEntry {
    level: number;
    element: HTMLHeadingElement;
    text: string;
}

interface IHeaderModelEntryWithIdAndPercentage extends IHeaderModelEntry {
    id: string;
    percentage: number;
}


const getHeaderModelFromElements = (elements: HTMLElementTagNameMap["h2"][]) => {
    const rawModel = compact(map(elements, (element) => {
        const {textContent, tagName} = element;
        const trimmedContent = textContent.trim();
        if (!trimmedContent) {
            return null;
        }
        let level = 0;
        try {
            level = parseInt(replace(toLower(tagName), "h", ""));
        }
        catch (err) {
            console.error(`Error parsing header level for ${trimmedContent}`);
        }
        return {
            level,
            element,
            text: trimmedContent
        };
    }));
    const minLevel = minBy(rawModel, item => item.level)?.level;
    return map(rawModel, element => {
        return {...element, level: element.level - minLevel};
    });
};

const createHeaderSelector = (levels: number) => {
    const clampedLevels = min([5, levels]);
    return times(clampedLevels, (level) => "h" + (level + 2)).join(", ");
};

const createHeaderHierarchy = (model: IHeaderModelEntry[], idx: number) => {
    const start = model[idx];
    const headerHierarchy = [start.text];
    let level = start.level;
    while (--level >= 0) {
        const headerText = findLast(model, entry => entry.level == level, idx);
        headerHierarchy.unshift(headerText?.text);
    }
    return headerHierarchy;
};

const calculatePercentage = (position: number, start: number, end: number) => {
    if (position >= end) return 1;
    if (position <= start) return 0;
    return min([max([(position - start) / (end - start), 0]), 1]);
};

const getHeaderId = (model: IHeaderModelEntry[], idx: number) => {
    const headerHierarchy = createHeaderHierarchy(model, idx);
    let id = map(headerHierarchy, slugify).join("_");
    if (id && includes("0123456789", id[0])) {
        id = "N" + id;
    }
    return id;
};

const assignHeaderIds = (reference: React.MutableRefObject<any>) => {
    if (!(reference && reference.current)) return;
    const headerSelector = createHeaderSelector(5);
    const headers = reference.current.querySelectorAll(headerSelector);
    const model: IHeaderModelEntry[] = getHeaderModelFromElements(headers);
    forEach(model, (h, idx) => {
        const elt = h.element;
        const id = getHeaderId(model, idx);
        if (id && !document.querySelector("#" + id)) {
            const fragementAnchor = document.createElement("div");
            fragementAnchor.id = id;
            fragementAnchor.classList.add("fragment-anchor");
            const firstChild = elt.firstChild;
            elt.insertBefore(fragementAnchor, firstChild);
        }
    });
};

const computeModelWithIdAndPercentage = (reference: React.MutableRefObject<any>, level: number, position: number) => {
    const headerSelector = createHeaderSelector(level);
    const headers = reference.current.querySelectorAll(headerSelector);
    const model: IHeaderModelEntry[] = getHeaderModelFromElements(headers);
    const modelWithPercentage = compact(map(model, (h, idx) => {
        const elt = h.element;
        const nextItem = get(model, idx + 1);
        // FIXME: Hack, which assumes #post-end is always there, i.e. ToC is only used for posts.
        const nextElt = nextItem ? nextItem.element : document.querySelector("#post-end") as HTMLElement;
        if (!(elt && nextElt)) return null;
        const id = getHeaderId(model, idx);
        const headerStart = elt.offsetTop;
        const headerEnd = nextElt.offsetTop;
        const percentage = calculatePercentage(position, headerStart, headerEnd);
        return {...h, id, percentage};
    }));
    return modelWithPercentage;
};

const onHashChange = () => {
    // Scroll to initial hash after render
    const hash = window.location.hash;
    if (hash) {
        setTimeout(() => {
            const elt = document.querySelector(hash);
            if (elt && elt.scrollTop === 0) {
                // Try to expand expandable description
                try {
                    const expandableDescriptionCandidate = elt.parentElement.parentElement.parentElement.parentElement;
                    const isExpandableDescription = expandableDescriptionCandidate.className.includes(EXPANDABLE_DESCRIPTION_CLASSNAME);
                    if (isExpandableDescription) {
                        (expandableDescriptionCandidate.querySelector(".button") as HTMLElement).click();
                    }
                }
                catch (err) {
                    // NOP
                }
            }
            elt?.scrollIntoView();
        }, 5);
    }
};

const remToPx = (rem: number) => {
    return rem * parseFloat(getComputedStyle(document.documentElement).fontSize);
};

export const TableOfContent: React.FC<ITableOfContentProps> = (props) => {
    const {title, reference, levels = 2} = props;
    const [position, setPosition] = useState(typeof window !== "undefined" ? window.scrollY + remToPx(5) : 0);
    const [model, setModel] = useState<IHeaderModelEntryWithIdAndPercentage[]>([]);
    const scrollHandler = () => {
        setPosition(window.scrollY + remToPx(5));
    };
    useEffect(() => {
        window.addEventListener("scroll", scrollHandler);
        window.addEventListener("hashchange", onHashChange);
        onHashChange();
        return () => {
            window.removeEventListener("scroll", scrollHandler);
            window.removeEventListener("hashchange", onHashChange);
        };
    }, []);
    useEffect(() => {
        assignHeaderIds(reference);
    }, [reference]);
    useEffect(() => {
        setModel(computeModelWithIdAndPercentage(reference, levels, position));
    }, [levels, reference, position]);
    useEffect(() => {
        forEach(model, ({id, element}) => {
            if (id && !document.querySelector("#" + id)) {
                const fragementAnchor = document.createElement("div");
                fragementAnchor.id = id;
                fragementAnchor.classList.add("fragment-anchor");
                const firstChild = element?.firstChild;
                if (firstChild) {
                    element.insertBefore(fragementAnchor, firstChild);
                }
                else {
                    element.appendChild(fragementAnchor);
                }
            }
        });
    }, [model]);
    if (!(reference && reference.current)) return null;
    return model && model.length > 1 && <div className="table-of-contents">
        <div className="title">{title}</div>
        <ul className="list">
            {map(model, ({id, level, percentage, text}) => {
                if (level >= levels) return null;
                return <li key={"toc-" + id} className={classNames("link", "level-" + level, percentage > .95 ? "passed" : percentage > 0 && "passing")}>
                    <a href={`#${id}`} style={{opacity: percentage > 0 ? 1 : 0.5}} >
                        <ProgressCircle radius={7} strokeWidth={2} percentage={percentage} />
                        <span className="toc-text">{text}</span>
                    </a>
                </li>;
            })}
        </ul>
    </div >;
};


