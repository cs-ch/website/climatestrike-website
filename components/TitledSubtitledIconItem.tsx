import React from "react";
import {useRouter} from "next/router";
import classNames from "classnames";

import {IPageContextProps, ITitledSubtitledIconItemProps} from "./@def";
import {Image} from "./Core";
import {H2, H3} from "./Typography";
import {HTML} from "./HTML";
import {languageTextProvider} from "../core/I18n";


const TitledSubtitledIconItem: React.FC<ITitledSubtitledIconItemProps & IPageContextProps> = (props) => {
    const router = useRouter();
    const language = router.locale;
    const getLanguageText = languageTextProvider(language);
    const title = getLanguageText(props.title);
    const subtitle = getLanguageText(props.subtitle);
    const text = getLanguageText(props.text);
    return <div className={classNames("titled-subtitled-icon-item", props.context.itemIdentifier)} >
        <div className="icon-container">
            <Image image={props.icon} />
        </div>
        <div className="text-container">
            {title && <H2 text={title} />}
            {subtitle && <H3 text={subtitle} />}
            {text && <HTML text={text} />}
        </div>
    </div >;
};

export default TitledSubtitledIconItem;