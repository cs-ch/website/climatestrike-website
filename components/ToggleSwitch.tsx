import React from "react";

import {IToggleSwitchProps} from "./@def";

export const ToggleSwitch: React.FC<IToggleSwitchProps> = ({id, checked, onChange}) => {
    return (
        <>
            <input
                className="switch-checkbox"
                id={id}
                checked={checked}
                onChange={onChange}
                type="checkbox"
            />
            <label
                className="switch-label"
                htmlFor={id}
            >
                <span className="switch-button" />
            </label>
        </>
    );
};