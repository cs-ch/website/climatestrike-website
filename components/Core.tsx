import React from "react";
import NextLink from "next/link";
import {useRouter} from "next/router";
import {map} from "lodash";

import {ICoreLinkProps, IImageProps} from "./@def";
import {languageImageProvider} from "../core/I18n";


export const Link: React.FC<ICoreLinkProps> = (props) => {
    const {className, href} = props;
    const content = props.text || props.children;
    if (!href) return null;
    if (href.startsWith("http") || href.startsWith("mailto:") || href.startsWith("whatsapp://")) {
        return <a href={href} className={className} target="_blank" rel="noopener noreferrer">{content}</a>;
    }
    return <NextLink href={href} className={className}>{content}</NextLink>;
};

export const Image: React.FC<IImageProps> = (props) => {
    const {className} = props;
    const {locale: language} = useRouter();
    const getImageTranslation = languageImageProvider(language);
    const image = getImageTranslation(props.image);
    if (!image) return null;
    return <img
        className={className}
        src={image.url}
        srcSet={map(image.formats, f => f.src).join(", ")}
        alt={image.alt}
        width={props.width}
    />;
};