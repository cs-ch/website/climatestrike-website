import React from "react";

import {IProgressCircleProps} from "./@def";


export const ProgressCircle: React.FC<IProgressCircleProps> = ({percentage, radius, strokeWidth}) => {
    const paddedRadius = radius + strokeWidth;
    const size = 2 * paddedRadius;
    // see https://stackoverflow.com/questions/5736398/how-to-calculate-the-svg-path-for-an-arc-of-a-circle

    const d = [
        "M", paddedRadius, strokeWidth,
        "A", radius, radius, 0, 1 - Math.round(percentage), 0,
        paddedRadius + radius * Math.cos(Math.PI * 2 * percentage - Math.PI / 2),
        paddedRadius + radius * Math.sin(Math.PI * 2 * percentage - Math.PI / 2)
    ].join(" ");

    return <svg className="progress-circle" height={size} width={size}>
        <circle className="base-circle" cx={paddedRadius} cy={paddedRadius} r={radius} strokeWidth={strokeWidth} fillOpacity={0} />
        <path className="progress-path" d={d} fill="none" strokeWidth={strokeWidth} />
    </svg>;
};