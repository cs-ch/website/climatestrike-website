import React, {useEffect, useState} from "react";
import {useRouter} from "next/router";

import {IAudioPlayerProps} from "./@def";
import {getLanguageMedium} from "../core/I18n";


const AudioPlayer: React.FC<IAudioPlayerProps> = (props) => {
    const {locale: language} = useRouter();
    // Workaround to load proper styles
    const [initialized, setInitialized] = useState(false);
    useEffect(() => {
        setInitialized(true);
    }, []);
    const file = getLanguageMedium(props.items, language);
    if (!(file && initialized)) {
        return null;
    }
    return <div className="audio-player">
        <audio src={file.url} controls />
    </div>;
};

export default AudioPlayer;