import React, {useState} from "react";
import {useRouter} from "next/router";
import {useTranslation} from "next-i18next";
import classNames from "classnames";
import {Collapse, FormControlLabel} from "@mui/material";

import {ICollapsibleTextProps} from "./@def";
import {HTML} from "./HTML";
import {languageTextProvider} from "../core/I18n";
import {IconFactory} from "../factories/IconFactory";
import {TitleFactory} from "../factories/TitleFactory";

const CollapsibleText: React.FC<ICollapsibleTextProps> = (props) => {
    const {locale: language} = useRouter();
    const {t} = useTranslation("common");
    const [open, setOpen] = useState(false);
    const getLanguageText = languageTextProvider(language);
    const title = getLanguageText(props.title);
    const text = getLanguageText(props.text);
    const iconFactory = IconFactory.instance();
    const action = open ? "collapse" : "expand";
    if (!title && !text) return null;
    return <div className={classNames("collapsible-text", {open, closed: !open})}>
        <FormControlLabel
            className="form-control-label"
            control={
                <div onClick={() => setOpen(!open)}>
                    {iconFactory.create(action)}
                    {TitleFactory.instance().create(props.titleLevel, title, "collapsible-title")}
                </div>
            }
            // Necessary for accesibility?
            label={t(action) as string}
        />
        <Collapse in={open} collapsedSize={0} classes={{wrapper: "collapse-wrapper"}}>
            <HTML text={text} />
        </Collapse>
    </div >;
};

export default CollapsibleText;