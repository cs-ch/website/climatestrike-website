import React from "react";
import {useRouter} from "next/router";
import classNames from "classnames";

import {IPageContextProps, ITitleProps} from "./@def";
import {getLanguageText} from "../core/I18n";
import {TitleFactory} from "../factories/TitleFactory";


const Title: React.FC<ITitleProps & IPageContextProps> = (props) => {
    const router = useRouter();
    const title = getLanguageText(props.title, router.locale);
    const className = classNames("title", props.context.itemIdentifier);
    return TitleFactory.instance().create(props.titleLevel, title, className);
};

export default Title;