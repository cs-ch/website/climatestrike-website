import React from "react";
import {useRouter} from "next/router";
import classNames from "classnames";

import {IPageContextProps, IIconTitleProps} from "./@def";
import {Image} from "./Core";
import {languageTextProvider} from "../core/I18n";
import {H2, Paragraph} from "./Typography";


const IconTitle: React.FC<IIconTitleProps & IPageContextProps> = (props) => {
    const {locale: language} = useRouter();
    const getLanguageText = languageTextProvider(language);
    const title = getLanguageText(props.title);
    const text = getLanguageText(props.text);
    return <div className={classNames("icon-title", props.context.itemIdentifier)}>
        <div className="icon-container">
            <Image className="icon" image={props.icon} />
        </div>
        <div className="text-container">
            <H2 text={title} />
            <Paragraph text={text} />
        </div>
    </div >;
};

export default IconTitle;