import React from "react";
import {useRouter} from "next/router";
import {cloneDeep, first, slice} from "lodash";

import {IPostProps} from "./@def";
import {Link, Image} from "./Core";
import {H4, Paragraph} from "./Typography";
import {ELanguages} from "../core/@def";
import {languageTextProvider, languageImageProvider} from "../core/I18n";

export const Card: React.FC<IPostProps> = (props) => {
    const {locale: language} = useRouter();
    const getLanguageText = languageTextProvider(language);
    const getLanguageImage = languageImageProvider(language);
    const title = getLanguageText(props.title);
    const lead = getLanguageText(props.lead);
    const author = getLanguageText(props.author);
    const image = cloneDeep(getLanguageImage(props.thumbnail) || getLanguageImage(props.image));
    const link = getLanguageText(props.link);
    const fallbackImage = ["/static/images/news-placeholder-blue.svg",
        "/static/images/news-placeholder-gray.svg",
        "/static/images/news-placeholder-yellow.svg"][first(title || lead || link || "a").charCodeAt(0) % 3];
    if (image?.formats) {
        // Only allow smaller images for performance reasons
        image.formats = slice(image.formats, 0, 2);
    }
    const cardImage = [{language: ELanguages.all, image: image ? image : {url: fallbackImage}}];
    return <div className="card-container">
        <div className="card">
            <Link key="link" href={link}>
                <div key="card-image" className="relative">
                    <div className="img">
                        <Image
                            className="bgimg"
                            image={cardImage} />
                    </div>
                </div>
                <div key="card-content" className="card-content">
                    <H4 key="title" text={title} />
                    <Paragraph key="lead" className="card-desc" text={lead} />
                    <div key="bottom" className="bottom">
                        <Paragraph key="card-tagline" className="card-tagline" text={author} />
                        <svg
                            key="card-arrow"
                            className="card-arrow"
                            width="28"
                            height="34"
                            viewBox="0 0 14 17"
                            fill="none"
                            xmlns="http://www.w3.org/2000/svg"
                        >
                            <path d="M 1 1 L 11 8.5 L 1 16" />
                        </svg>
                    </div>
                </div>
            </Link>
        </div>
    </div >;
};