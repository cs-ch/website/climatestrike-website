import React from "react";
import classNames from "classnames";

import {IMenuIconProps} from "./@def";


export const MenuIcon: React.FC<IMenuIconProps> = (props) => {
    const {active, onClick} = props;
    return <div
        className={classNames("menu-icon", {active})}
        onClick={() => onClick()}>
        <div className={classNames("line", "line-1")} />
        <div className={classNames("line", "line-2")} />
        <div className={classNames("line", "line-3")} />
    </div>;
};

