import React from "react";
import {map} from "lodash";
import classNames from "classnames";
import {useTranslation} from "next-i18next";

import {IHierarchicalLinksProps} from "./@def";
import {H5} from "../Typography";
import {Translation} from "./Translation";
import {Link} from "../Core";

export const HierarchicalLinks: React.FC<IHierarchicalLinksProps> = (props) => {
    const {links} = props;
    const {t} = useTranslation("common");
    return <div className="hierarchical-links">
        {map(links, link => <div key={link.id} className={classNames("link-container", link.id)}>
            <H5 className="link-title"><Translation translationKey={link.id} /></H5>
            <div className="links">
                {map(link.children, child => <div key={child.id}><Link href={child.href} text={t(child.id)} /></div>)}
            </div>
        </div>)
        }
    </div>;
};