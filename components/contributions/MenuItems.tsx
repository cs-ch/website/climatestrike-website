import React from "react";
import {map, filter, includes, sortBy} from "lodash";
import classNames from "classnames";
import Link from "next/link";

import {IMenuItemsProps} from "./@def";
import {Translation} from "./Translation";
import {IPage, ELinkLocation} from "../../core/@def";



const reducePages = (pages: IPage[], location: ELinkLocation) => {
    const filtered = filter(pages, page => includes(page?.config?.linkLocations, location));
    const sorted = sortBy(filtered, page => page.sortId);
    const mapped = map(sorted, (page) => {
        return {
            id: page.id,
            slug: page.slug,
            identifier: page.identifier,
            menuItem: () => {return <Translation className={page.identifier} translationKey={page.identifier} />;}
        };
    });
    return mapped;
};

export const MenuItems: React.FC<IMenuItemsProps> = (props) => {
    const {location, activePageIdentifier} = props;
    const pages = reducePages(props.pages, location);
    return <React.Fragment>
        {map(pages, (page) => {
            const className = classNames(
                {selected: page.identifier === activePageIdentifier},
                page.slug,
                "menu-item");
            const href = page.slug ? "/" + page.slug : "/";
            return <div key={page.slug} className={className}>
                <Link href={href}>{page.menuItem()}</Link>
            </div>;
        })}
    </React.Fragment>;
};