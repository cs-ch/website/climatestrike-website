import React from "react";
import Link from "next/link";
import {useTranslation} from "next-i18next";

import {IIconMenuItemProps} from "./@def";
import {Translation} from "./Translation";


export const IconMenuItem: React.FC<IIconMenuItemProps> = (props) => {
    const {src, href, type, description} = props;
    const {t} = useTranslation("common");
    return <Link key={type} href={href}>
        <span key="icon-menu-item" className="icon-menu-item">
            <img key="icon" className="layout-home-icon" src={src} alt={`${t("icon-menu-description")} - ${t("home")}`} />
            {description
                && <Translation
                    key="icon-menu-description"
                    className="icon-menu-description"
                    translationKey="icon-menu-description" />
            }
        </span>
    </Link>;
};