import React, {useContext} from "react";
import {useTranslation} from "next-i18next";

import {ThemeContext} from "../Layout";
import {IconFactory} from "../../factories/IconFactory";


export const DarkLightToggle: React.FC = () => {
    const {t} = useTranslation("common");
    const {useDark, setUseDark, theme} = useContext(ThemeContext);
    const iconShape = useDark ? "sun" : "moon";
    const icon = IconFactory.instance().create(iconShape, {titleAccess: t("dark-light-toggle")});
    return <div className="dark-light-toggle" onClick={() => {
        const toDark = !useDark;
        theme.switchTheme(toDark);
        setUseDark(toDark);
    }}>
        {icon}
    </div>;
};