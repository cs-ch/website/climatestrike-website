import React from "react";
import {useTranslation} from "next-i18next";

import {ITranslationProps} from "./@def";

export const Translation: React.FC<ITranslationProps> = (props) => {
    const {translationKey, className, type} = props;
    const {t} = useTranslation("common");
    const translation = t(translationKey);
    if (type && type == "div") {
        return <div className={className}>{translation}</div>;
    }
    return <span className={className}>{translation}</span>;
};