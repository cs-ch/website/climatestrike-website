import React from "react";
import {map} from "lodash";
import classNames from "classnames";

import {IFootnotesProps} from "./@def";
import {Translation} from "./Translation";

export const Footnotes: React.FC<IFootnotesProps> = (props) => {
    const {translationKeys} = props;
    return <div className="footnotes">
        {map(translationKeys, (key, index) => {
            const className = classNames(key, index);
            return <Translation key={key} className={className} translationKey={key} />;
        })}
    </div>;
};