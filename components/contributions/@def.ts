import {IPage, ELinkLocation} from "../../core/@def";

export interface ITranslationProps {
    translationKey: string;
    className?: string;
    type?: "span" | "div"
}

interface IHierarchicalLinkChild {
    id: string;
    href: string;
}

interface IHierarchicalLink {
    id: string;
    children: IHierarchicalLinkChild[];
}

export interface IHierarchicalLinksProps {
    links: IHierarchicalLink[];
}

export interface IIconMenuItemProps {
    src: string;
    href: string;
    type: string;
    description?: boolean;
}

export interface IFootnotesProps {
    translationKeys: string[];
}

export interface IMenuItemsProps {
    pages: IPage[];
    location: ELinkLocation;
    activePageIdentifier: string;
}
