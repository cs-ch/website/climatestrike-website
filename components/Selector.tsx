import React from "react";
import {useRouter} from "next/router";
import Link from "next/link";
import {map, includes} from "lodash";
import classNames from "classnames";

import {TObjectId, INamedObject} from "../core/@def";
import {setCookie} from "../core/Utils";


interface ISelectorProps {
    items: INamedObject[];
    childProvider: (item: INamedObject, className?: string) => React.ReactNode;
    selected?: TObjectId[];
    className?: string;
}

export const Selector: React.FC<ISelectorProps> = ({items, childProvider, selected, className}) => {
    return <div className={classNames("selector", className)}>
        {map(items, (item) => {
            const active = includes(selected, item.id);
            const className = classNames("selector-item", {selected: active});
            return childProvider(item, className);
        })}
    </div >;
};

export const LanguageSelector: React.FC<Omit<ISelectorProps, "childProvider">> = ({items}) => {
    const {locale: language, asPath} = useRouter();
    return <Selector
        className="languages"
        items={items}
        selected={[language]}
        childProvider={(item, className) => {
            return <div
                key={item.id}
                className={className}
                onClick={
                    () => {setCookie("NEXT_LOCALE", item.id);}
                }><Link href={asPath} locale={item.id}>
                    {item.name}</Link>
            </div>;
        }}
    />;
};
