import React, {createContext, useState, useEffect} from "react";
import {useTranslation} from "next-i18next";
import {useRouter} from "next/router";
import {map} from "lodash";
import {ThemeProvider} from "@mui/material/styles";

import {IPageWrapperProps} from "./@def";
import {Head} from "./Head";
import {Header} from "./Header";
import {Footer} from "./Footer";
import {theme} from "../core/Theme";
import {getLanguageText, getLanguageImage} from "../core/I18n";
import {ContributionFactory} from "../factories/ContributionFactory";


export const ThemeContext = createContext({useDark: null, setUseDark: null, theme});

export const Layout: React.FC<IPageWrapperProps> = (props) => {
    const [useDark, setUseDark] = useState(false);
    useEffect(() => {
        const darkPreferred = theme.isDarkPreferred(null);
        setUseDark(darkPreferred);
    }, []);
    useEffect(() => {
        theme.switchTheme(useDark);
    }, [useDark]);
    const {locale: language} = useRouter();
    const {t} = useTranslation("common");
    const {pageIdentifier = "Climatestrike", activePage, pages, menu, submenu, footer, children} = props;
    const appContext = {activePageIdentifier: pageIdentifier, pages};
    const contributionFactory = new ContributionFactory();
    const menuContributions = map(menu.contributions, (contribution) => contributionFactory.create(contribution.type, appContext, contribution.config));
    const submenuContributions = map(submenu.contributions, (contribution) => contributionFactory.create(contribution.type, appContext, contribution.config));
    const footerContributions = map(footer.contributions, (contribution) => contributionFactory.create(contribution.type, appContext, contribution.config));
    const headerProps = {
        activePageIdentifier: pageIdentifier,
        menuContributions: menuContributions,
        submenuContributions: submenuContributions,
        footerContributions: footerContributions,
    };
    const title = activePage && activePage.title ? t(getLanguageText(activePage.title, language)) : t(pageIdentifier);
    const description = activePage ? t(getLanguageText(activePage.description, language)) : null;
    const image = activePage ? getLanguageImage(activePage.image, language) : null;
    const showFooter = (activePage && activePage.config) ? !activePage.config.hideFooter : true;
    return (
        <div className="root">
            <ThemeContext.Provider value={{useDark, setUseDark, theme}}>
                <ThemeProvider theme={theme.getTheme(useDark)}>
                    <Head title={title} description={description} image={image} />
                    <Header {...headerProps} />
                    {children}
                    {showFooter && <Footer contributions={footerContributions} />}
                </ThemeProvider>
            </ThemeContext.Provider>
        </div>
    );
};
