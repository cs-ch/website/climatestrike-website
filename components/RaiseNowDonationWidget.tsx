import React, {useState, useEffect} from "react";
import Head from "next/head";
import {useRouter} from "next/router";
import classNames from "classnames";

import {IPageContextProps, IRaiseNowDonationWidgetProps} from "./@def";
import {H2} from "./Typography";
import {HTML} from "./HTML";
import {languageTextProvider} from "../core/I18n";


const RaiseNowDonationWidget: React.FC<IRaiseNowDonationWidgetProps & IPageContextProps> = (props) => {
    const {context, scriptSrc, config} = props;
    const {locale: language} = useRouter();
    const getLanguageText = languageTextProvider(language);
    const title = getLanguageText(props.title);
    const lead = getLanguageText(props.lead);
    const scriptId = "raise-now-script";
    const widgetClassName = "rnw-widget-container";
    const [onClient, setOnClient] = useState(false);
    const [loaded, setLoaded] = useState(false);
    useEffect(() => {
        setOnClient(true);
    }, []);
    useEffect(
        () => {
            const script = document.getElementById(scriptId);
            const listener = () => setLoaded(true);
            if (script) {
                script.addEventListener("load", listener);
                return () => script.removeEventListener("load", listener);
            }
        },
        [onClient]);
    useEffect(
        () => {
            const rnw = (window as any).rnw;
            if (rnw || loaded) {
                rnw.tamaro.runWidget(
                    `.${widgetClassName}`,
                    {...config, language: language});
            }
        },
        [loaded, language, config]);
    return <div className="raise-now-donation-widget">
        {title && <H2 text={title} />}
        {lead && <HTML key="description" text={lead} />}
        <div className={classNames(widgetClassName, context.itemIdentifier)}>
            <Head>
                <script id={scriptId} src={scriptSrc} />
            </Head>
        </div>
    </div>;
};

export default RaiseNowDonationWidget;