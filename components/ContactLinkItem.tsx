import React from "react";
import classNames from "classnames";
import {upperFirst} from "lodash";

import {IconFactory} from "../factories/IconFactory";


interface IContactLinkItemProps {
    identifier: string;
    className?: string;
    href: string;
    type: string;
    name?: string;
    onClick?: (e: React.MouseEvent) => void;
}

export const ContactLinkItem: React.FC<IContactLinkItemProps> = (props) => {
    const {identifier, className, href, type, name, onClick} = props;
    if (!href) return null;
    const iconFactory = IconFactory.instance();
    return <div key={identifier} className={classNames("contact-link-item", {"with-name": !!name}, type, className)} onClick={onClick}>
        <a href={href} target="_blank" rel="noopener noreferrer" title={`${upperFirst(type)}` + (name ? ` - ${name}` : "")} >
            <div className="icon">{iconFactory.create(type)}</div>
            <div key="link-name" className="link-name">{name}</div>
        </a>
    </div>;
};