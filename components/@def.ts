import {
    TObjectId, IPage, IContentItem, IMenuConfig,
    ISubMenuConfig, IFooterConfig, ITranslation, ILink,
    EEventCategory, INamedObject, IIdentifiable, IStrategy,
    IImageTranslation, ITranslationBase, IImage
} from "../core/@def";
import {ChangeEvent} from "react";

// Core

export enum EEventView {
    timeline = "timeline",
    createEvent = "createEvent"
}

// Page

export interface IPageWrapperProps {
    pages: IPage[];
    pageIdentifier?: string;
    activePage: IPage;
    items: IContentItem[];
    query: IIdentifiable;
    namespacesRequired: string[];
    menu: IMenuConfig;
    submenu: ISubMenuConfig;
    footer: IFooterConfig;
    children?: React.ReactNode;
}

export interface IPageContext {
    itemIdentifier: TObjectId;
    pageIdentifier: string;
    items: IContentItem[];
    query: IIdentifiable;
}

export interface IPageContextProps {
    context: IPageContext;
}

// HTML Head

export interface IHTMLHeadProps {
    title: string;
    description: string;
    url?: string;
    image?: IImage;
}

// Layout

export interface IAppContext {
    activePageIdentifier: TObjectId;
    pages: IPage[];
}

export interface IContribution {
    identifier: TObjectId;
    provider: () => React.ReactNode;
}

export interface IMenuIconProps {
    active: boolean;
    onClick: () => void;
}

export interface ISubMenuProps {
    className: string;
    contributions: IContribution[]
}

export interface IHeaderProps {
    menuContributions: IContribution[];
    submenuContributions: IContribution[];
}

export interface IFooterProps {
    contributions: IContribution[]
}

// Components

// Core

export interface ICoreLinkProps {
    className?: string;
    href: string;
    text?: string;
    children?: any;
}

export interface IImageProps {
    className?: string;
    width?: number;
    image: IImageTranslation[];
}

// - Button
export enum EStyles {
    primary = "primary",
    secondary = "secondary"
}

export interface IButtonProps {
    className?: string;
    style: EStyles;
    type?: string;
    text?: ITranslation[];
    href?: string;
    disabled?: boolean;
    onClick?: () => void;
}

export interface ILinkButtonProps extends Partial<ILinkProps> {
    style?: EStyles;
    href?: string;
}

export interface IMoreLessButtonProps {
    style: EStyles;
    expanded: boolean;
    onClick: () => void;
}

export interface IIconButtonProps {
    "aria-label": string;
    className?: string;
    onClick?: (e?) => void;
    size?: "small" | "large";
    children: any;
}

export interface ILinkProps {
    type?: string;
    link: ITranslation[];
    text?: ITranslation[];
}

// - HeadlineItem

export interface IHeadlineItemProps {
    title: ITranslation[];
    subtitle: ITranslation[];
    text: ITranslation[];
    link: ILink;
}

// - Form

export interface IField {
    key: string;
    type?: string;
    required?: boolean;
    placeholder?: ITranslation[];
    config?: any;
}

export interface IFormProps {
    title?: ITranslation[];
    identifier: string;
    fields: IField[];
    submitUrl?: string;
    submitText: ITranslation[];
    submitTransform?: (values: any) => any;
    onSuccess?: () => void;
    useCustomError?: boolean;
    validate?: (values: any) => any; // input: Form values, output: error object
    validationSchema?: any;
}

// - Download section

export interface IMedium {
    alternativeText: string;
    ext: string;
    name: string;
    size: number; // in KB
    url: string;
}

export interface IMediumItem extends ITranslationBase {
    medium: IMedium;
}

export interface IMediaTranslation {
    title: ITranslation[];
    item: IMediumItem[];
}


export interface IDownloadSectionProps {
    media: IMediaTranslation[];
}

// - Newsletter form

export interface INewsletterFormProps {
    identifier: string;
    submitUrl: string;
    text: ITextProps;
}

// - Landing link

export interface ILandingLinkProps {
    title: ITranslation[];
    titleLevel?: ETitleLevel;
    text: ITranslation[];
    icon: IImageTranslation[];
    link: ILink;
}

// - Image with caption

export interface IImageWCaptionProps {
    image: IImageTranslation[];
}

// - Post section

export interface IPostSectionConfig {
    title: string | ITranslation[];
    n: number;
    initialLimit?: number;
    showSearch?: boolean;
}

export enum EPostCategory {
    goodNews = "good-news",
    badNews = "bad-news",
    otherNews = "other-news",
    fact = "fact"
}

export interface IPostProps {
    identifier: string;
    date: string; // Default date encoding
    createdAt: string; // Default date encoding
    updatedAt: string; // Default date encoding
    title: ITranslation[];
    lead: ITranslation[];
    content: IStrategy[];
    thumbnail?: IImageTranslation[];
    image: IImageTranslation[];
    link?: ITranslation[];
    author: ITranslation[];
    category: EPostCategory;
    relatedPosts?: IPostProps[];
}

// - Expandable description

export interface IExpandableDescriptionProps {
    title: ITranslation[];
    titleLevel?: ETitleLevel;
    lead: ITranslation[];
    text: ITranslation[];
    style: EStyles;
}

// - Demand item

export interface IDemandItemProps {
    title: ITranslation[];
    lead: ITranslation[];
    icon: IImageTranslation[];
    text: ITranslation[];
}

// - Fact item

export interface IFactItemProps {
    title: ITranslation[];
    lead: ITranslation[];
    icon: IImageTranslation[];
    links?: ILink[];
}

// - Title

export enum ETitleLevel {
    two = "2",
    three = "3",
    four = "4",
    five = "5",
    six = "6"
}

export interface ITitleProps {
    title: ITranslation[];
    titleLevel?: ETitleLevel;
}

// - TitledSubtitledIconItem

export interface ITitledSubtitledIconItemProps {
    title: ITranslation[];
    subtitle: ITranslation[];
    text: ITranslation[];
    icon: IImageTranslation[];
}

// - Icon title

export interface IIconTitleProps {
    title: ITranslation[];
    text: ITranslation[];
    icon: IImageTranslation[];
}

// - Content delegation 

export interface IContentDelegationProps<S = any, T = any> {
    childType: string;
    data: S;
    config?: T;
}

// - Contact data

export type TContactType = "website" |
    "email" |
    "telegram" |
    "whatsapp" |
    "facebook" |
    "instagram" |
    "discord" |
    "twitter" |
    "other"

export interface IContact {
    identifier: string;
    type: TContactType;
    link: string;
    linkName: ITranslation[];
}

export interface IContactGroup {
    identifier: string;
    identifiers: string[];
    description?: ITranslation[];
    selected?: boolean;
    contacts: IContact[]
}

export interface IContactDataProps {
    items: IContactGroup[];
    selection: string[];
}

// - Event data

export enum EEventFilterType {
    category = "category",
    zip = "zip"
}

export interface IEvent {
    identifier: string;
    description: ITranslation[];
    start: string;
    end: string;
    zip: number;
    city: ITranslation[];
    location: ITranslation[];
    link: ILink;
    published: boolean;
    contact?: string
}

export interface IEventGroup {
    identifier: string;
    category: EEventCategory;
    title: ITranslation[];
    description: ITranslation[];
    start: string;
    end: string;
    multipleChildren: boolean;
    events: IEvent[];
}

export interface IEventsProps {
    settings: IEventsSettings;
    items: IEventGroup[];
    onLoadMore: () => void;
}

interface IEventFilterConfig<T = any> {
    type: EEventFilterType;
    defaults?: T[];
}

export interface IEventsSettings {
    views: EEventView[];
    filters: IEventFilterConfig[];
}

// - Chip selector

export interface IChipSelectorProps {
    className: string;
    items: INamedObject[];
    selected: string[];
    disabled?: string[];
    onClick: (id: string, e?: React.MouseEvent) => void;
    iconProvider?: (item: INamedObject) => React.ReactElement;
}

// - Create event form

export interface ICreateEventProps {
    items: IEventGroup[];
}

// - Snackbar

export enum ESnackbarSeverity {
    error = "error",
    warning = "warning",
    info = "info",
    success = "success"
}

export interface ISnackbarProps {
    vertical?: "top" | "bottom";
    horizontal?: "left" | "center" | "right";
    open?: boolean;
    setOpen: (open: boolean) => void
    severity: ESnackbarSeverity;
    message: string;
    autoHideDuration?: number;
}

// - Text

export interface ITextProps {
    title: ITranslation[];
    text: ITranslation[];
}

// - Iframe

export interface IIframeProps {
    src?: ITranslation[];
    html?: ITranslation[];
}

// - RaiseNowDonationWidget

export interface IRaiseNowDonationWidgetProps {
    title: ITranslation[];
    lead: ITranslation[];
    scriptSrc: string;
    config: any; // JSON
}

// - HTML

export interface IHTMLProps {
    text: string;
    idProvider?: (text: string) => string;
}

// - BackgroundImage

export interface IBackgroundImageProps {
    identifier?: string;
    image: IImageTranslation[];
}

// - CollapsibleText

export interface ICollapsibleTextProps {
    identifier?: string;
    title: ITranslation[];
    titleLevel?: ETitleLevel;
    text: ITranslation[];
}

// - Join item

export interface IJoinItemProps {
    title: ITranslation[];
    text: ITranslation[];
    icon: IImageTranslation[];
    image: IImageTranslation[];
    link: ILink;
}


// - Milestone

interface IMilestonesEntry {
    date: string;
    text: ITranslation[];
}

export interface IMilestonesProps {
    title: ITranslation[];
    entries: IMilestonesEntry[];
    backgroundSmall: IImageTranslation[];
    backgroundLarge: IImageTranslation[];
}

// - Custom toggle switch

export interface IToggleSwitchProps {
    id: string;
    checked: boolean,
    onChange: (event: ChangeEvent<HTMLInputElement>) => void;
}

// - Sources

interface IReference {
    link: ITranslation[];
    description: ITranslation[];
    index?: number;
}

export interface IReferencesProps {
    references?: IReference[];
    text?: ITranslation[];
}

// - Petition

export interface IPetitionProps {
    title: ITranslation[];
    text: ITranslation[];
    form: IFormProps;
    shareButtons: IShareButtonsProps;
}

// - Slides
export interface ISlide {
    width: number;
    image: IImageTranslation[];
    text: ITranslation[];
}

export interface ISlidesProps {
    items: ISlide[];
}

// - ShareButtons

export enum EShareButtonsVariant {
    normal = "normal",
    minimal = "minimal"
}

export interface IShareButtonsProps {
    variant?: EShareButtonsVariant;
    emailSubject: ITranslation[];
    emailMessage: ITranslation[];
    whatsappMessage: ITranslation[];
    twitterMessage: ITranslation[];
    enableFacebook: boolean;
    urlOverride: string;
}

// - ProgressCircle 

export interface IProgressCircleProps {
    percentage: number;
    radius: number;
    strokeWidth: number;
}

// - AudioPlayer
export interface IAudioPlayerProps {
    items: IMediumItem[];
}