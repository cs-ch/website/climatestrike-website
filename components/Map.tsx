import React, {useRef, useEffect, useReducer} from "react";
import {includes, min, map, first, sortBy, toLower, trim, filter, isEmpty, intersection} from "lodash";
import * as d3 from "d3";
import * as d3g from "d3-geo";
import {GeometryCollection} from "topojson-specification";
import * as topojson from "topojson";
import classNames from "classnames";

import {IContentDelegationProps, IPageContextProps} from "./@def";
import {useWindowSize} from "../core/Hooks";
import {ContentFactory} from "../factories/ContentFactory";
import {TextField, Autocomplete} from "@mui/material";
import {IconFactory} from "../factories/IconFactory";
import {useTranslation} from "next-i18next";


const reducer = (state: string[], id: string) => {
    if (includes(state, id) || !id) {
        return [];
    }
    return [id];
};

// From https://bl.ocks.org/mbostock/4207744
const Map: React.FC<IContentDelegationProps & IPageContextProps> = (props) => {
    const {context, childType, data} = props;
    const {t} = useTranslation("common");
    const d3Container = useRef(null);
    const [selection, setSelection] = useReducer(reducer, []);
    const windowSize = useWindowSize();
    const topo = props.config;
    const featureCollection = topojson.feature(topo, topo.objects.regions as GeometryCollection);

    useEffect(
        () => {
            if (d3Container.current) {
                const width = min([windowSize.width * .85, 960]);
                const height = width / 960 * 600;
                const svg = d3.select(d3Container.current);
                svg
                    .attr("height", height)
                    .attr("width", width);
                const projection = d3g.geoAlbers()
                    .rotate([0, 0])
                    .center([8.3, 46.8])
                    .scale(16000 / 960 * width)
                    .translate([width / 2, height / 2])
                    .precision(.1);

                const path = d3.geoPath()
                    .projection(projection);

                const regions = svg.selectAll("path")
                    .data(featureCollection.features);
                regions.enter()
                    .append("path")
                    .classed("canton", true)
                    .on("click", (_e: d3.ClientPointEvent, d: any) => {
                        setSelection(d.id);
                    });

                const svgCantons = svg.selectAll(".canton");
                svgCantons
                    .attr("class", (d: any) => {
                        return classNames("canton", includes(selection, d.id) ? "selected" : null);
                    });

                svg.selectAll("path")
                    .attr("d", path);
            }
        },
        [selection, featureCollection, windowSize.width]
    );

    const contentFactory = new ContentFactory();
    const hasSelection = !isEmpty(selection);
    const defaults = filter(data, item => hasSelection ? !isEmpty(intersection(item.identifiers, selection)) : item.selected);
    const options = sortBy(map(featureCollection.features, (feature) => feature.id), (id: string) => t(id));
    const className = classNames("map", context.itemIdentifier);
    const optionsFilter = (opt, input) => {
        const inp = toLower(trim(input));
        return includes(toLower(opt), inp) || includes(toLower(t(opt)), inp);
    };

    return (
        <div className={className}>
            <Autocomplete
                id="map-dropdown"
                className={classNames("select-dropdown", "map-dropdown")}
                clearText={t("clear")}
                autoSelect
                noOptionsText={t("no-options")}
                popupIcon={IconFactory.instance().create("expand")}
                filterOptions={(opts, {inputValue}) => filter(opts, opt => optionsFilter(opt, inputValue))}
                options={options}
                getOptionLabel={(selected: string | string[]) => !selected ? t("map-dropdown-default") : t(selected)}
                value={first(selection) || first(defaults).identifier}
                onChange={(_event: any, value: any) => {
                    value ? setSelection(value) : setSelection(null);
                }}
                renderInput={(params) => <TextField {...params} variant="standard" label={t("map-dropdown-label")} margin="normal" />}
            />
            <div key="map" >
                <svg className="map-svg"
                    ref={d3Container}
                />
            </div>
            <div key="contents" className="contents">
                {contentFactory.create(
                    childType,
                    {items: props.data ? props.data : [], selection: selection, defaults},
                    props.context
                )}
            </div>
        </div>
    );
};

export default Map;