import React, {useEffect, useState} from "react";
import {reduce, map, forEach, startsWith, get} from "lodash";
import classNames from "classnames";
import {Formik} from "formik";
import {useRouter} from "next/router";
import {useTranslation} from "next-i18next";

import {IFormProps, EStyles, ESnackbarSeverity, IField} from "../@def";
import {Button} from "../Button";
import {Snackbar} from "../Snackbar";
import {H2} from "../Typography";
import {languageTextProvider} from "../../core/I18n";
import {HttpService} from "../../core/HttpService";
import {ValidationFactory} from "../../factories/ValidationFactory";
import {getValueFromLanguageDict} from "../../core/Utils";
import {FieldFactory} from "./FieldFactory";



const createInitialValues = (fields: IField[], language: string) => {
    return reduce(fields, (prev, curr) => {
        let initialValue: string | string[] | boolean = "";
        if (curr.type === "checkboxes")
            initialValue = [];
        if (curr.type === "boolean")
            initialValue = false;
        if (curr.type === "keepMePosted")
            initialValue = true;
        if (curr.type === "acceptPrivacyPolicy")
            initialValue = false;
        if (curr.type === "dropdown")
            initialValue = get(getValueFromLanguageDict(curr.config || {all: []}, language), 0) || "";
        prev[curr.key] = initialValue;
        return prev;
    }, {});
};

const createValidationSchema = (fields: IField[]) => {
    const validationFactory = ValidationFactory.instance();
    const validationObj = {};
    forEach(fields, field => {
        const {key, type, required} = field;
        validationObj[key] = validationFactory.create(type, {required});
    });
    return validationFactory.create("object", {input: validationObj});
};

const Form: React.FC<IFormProps> = (props) => {
    const {title, fields, useCustomError, submitText, identifier} = props;
    const localStorageKey = `form-values-${identifier}`;
    const {t} = useTranslation("common");
    const router = useRouter();
    const language = router.locale;
    let submitUrl = props.submitUrl || "/api/submit-form";
    if (!startsWith(submitUrl, "/")) submitUrl = "/" + submitUrl;
    const validationSchema = props.validationSchema || createValidationSchema(fields);
    const translationChecker = (s) => t(s) !== s;
    const getLanguageText = languageTextProvider(language);
    const fieldFactory = new FieldFactory(useCustomError, translationChecker, t, language, getLanguageText);
    const [initialValues, setInitialValues] = useState(null);
    useEffect(() => {
        const valuesInLocalStorage = JSON.parse(localStorage.getItem(localStorageKey));
        const rawInitialValues = createInitialValues(fields, language);
        setInitialValues({...rawInitialValues, ...valuesInLocalStorage});
    }, [fields, language, localStorageKey]);
    const [snackbarState, setSnackbarState] = useState({
        open: false,
        severity: null,
        message: null
    });
    if (!initialValues) {return null;}
    // See https://medium.com/codefully-io/react-forms-validation-with-formik-and-material-ui-1adf0c1cae5c
    return (
        <div className={classNames("form", identifier)}>
            <H2 text={getLanguageText(title)} />
            <Formik
                initialValues={initialValues}
                onSubmit={(values, helpers) => {
                    const submitValues = props.submitTransform ? props.submitTransform(values) : values;
                    const $http = HttpService.instance();
                    $http.post({url: submitUrl, body: {identifier: identifier, _meta: {language}, ...submitValues}})
                        .then((response) => {
                            if (response.ok) {
                                localStorage.removeItem(localStorageKey);
                                const freshInitialValues = createInitialValues(fields, language);
                                helpers.resetForm(freshInitialValues);
                                if (props.onSuccess) {
                                    props.onSuccess();
                                }
                                setSnackbarState({open: true, severity: ESnackbarSeverity.success, message: t("form-successful")});
                            }
                            else {
                                setSnackbarState({open: true, severity: ESnackbarSeverity.error, message: t("form-failed")});
                                helpers.setSubmitting(false);
                                console.error("Error submitting form data");
                            }
                        });
                }}
                validationSchema={validationSchema}
            >
                {(props) => {
                    const {
                        isSubmitting,
                        handleChange,
                        handleSubmit,
                        handleBlur
                    } = props;
                    const customHandleChange = (field) => {
                        localStorage.setItem(localStorageKey, JSON.stringify(props.values));
                        return handleChange(field);
                    };
                    const customHandleBlur = (field) => {
                        localStorage.setItem(localStorageKey, JSON.stringify(props.values));
                        return handleBlur(field);
                    };
                    return (<form onSubmit={handleSubmit}>
                        {map(fields, (field) => {
                            return fieldFactory.create(field, {...props, handleChange: customHandleChange, handleBlur: customHandleBlur});
                        })}
                        <Button style={EStyles.primary} className="submit-button"
                            type="submit" disabled={isSubmitting} text={submitText} />
                    </form>);
                }}
            </Formik>
            <Snackbar {...snackbarState} setOpen={(open) => setSnackbarState({...snackbarState, open})} />
        </div >
    );
};

export default Form;