import React from "react";

import {IFormProps} from "./@def";
import {InputGroup} from "./InputGroup";
import {H3} from "../Typography";

export const CrxFormContactSection: React.FC<IFormProps> = (props) => {
    const id = "contact-section";
    return <div key={id} className={id}>
        <H3 text={props.t("contact")} />
        <InputGroup name="contact" {...props} />
    </div>;
};