import React from "react";
import {find, map} from "lodash";
import {TextField} from "@mui/material";
import {MobileDateTimePicker} from "@mui/x-date-pickers/MobileDateTimePicker";

import {IFormProps} from "./@def";
import {getError, getHelperText} from "./InputGroup";
import {LanguageFields} from "./CrxFormLanguageFields";
import {H3} from "../Typography";
import {IconButton} from "../Button";
import {SelectDropdown} from "../SelectDropdown";
import {independentId, EEventCategory} from "../../core/@def";
import {DateTime} from "../../core/DateTime";
import {DATE_TIME_FORMAT, DATE_TIME_MASK_FORMAT} from "../../core/Constants";
import {IconFactory} from "../../factories/IconFactory";


export const CrxFormBasics: React.FC<IFormProps> = (props) => {
    const {parentOptions, touched, values, errors,
        handleChange, handleBlur, setFieldValue, language, t} = props;
    const id = "basics";
    const pickerBaseProps = {
        autoOk: true, showToolbar: false, ampm: false,
        className: "date-picker", variant: "inline" as "inline",
        mask: DATE_TIME_MASK_FORMAT
    };
    const startError = getError({path: "startDate", touched, errors});
    const startHelper = getHelperText({
        name: "startDate",
        value: values.startDate, required: true,
        error: startError, useCustomError: true,
        t
    });
    const endError = getError({path: "endDate", touched, errors});
    const endHelper = getHelperText({
        name: "endDate",
        value: values.endDate, required: true,
        error: endError, useCustomError: true,
        t
    });
    return <div key={id} className={id}>
        <H3 text={t(id)} />
        {
            parentOptions.length > 1 && // has more than "independent-event"
            <SelectDropdown
                id="parent"
                label={t("parent")}
                className="parent-selector"
                key="parent"
                value={values.parentId}
                onChange={(event) => {
                    handleChange("parentId")(event);
                    const parentId = event.target.value;
                    if (parentId !== independentId) {
                        const parent = find(parentOptions, (option) => ("" + option.id) === parentId);
                        setFieldValue("category", parent.category);
                    }
                }}
                options={parentOptions}
            />
        }
        {
            values.parentId === independentId &&
            <SelectDropdown
                id="category"
                label={t("category")}
                className="category-selector"
                key="category"
                value={values.category}
                onChange={(event) => handleChange("category")(event)}
                options={map(EEventCategory, (id) => ({id, name: t(id)}))}
            />
        }

        <LanguageFields currentLanguage={language} {...{...props, language, t}} />
        <H3 text={t("date")} />
        <MobileDateTimePicker
            {...pickerBaseProps}
            label={t("start")}
            value={values.startDate ? DateTime.create(values.startDate).date : null}
            format={DATE_TIME_FORMAT}
            slots={{
                textField: (props) => <TextField
                    {...props}
                    required
                    variant="standard"
                    className="date-picker"
                    placeholder={t("start")}
                    error={!!startError}
                    onBlur={handleBlur("startDate")}
                    helperText={startHelper} />
            }}
            onChange={(e) => {
                handleChange("startDate")(e && DateTime.create(e).format());
            }}
        />
        <div className="clearable-date-picker">
            <MobileDateTimePicker
                {...pickerBaseProps}
                label={t("end")}
                value={values.endDate ? DateTime.create(values.endDate).date : null}
                format={DATE_TIME_FORMAT}
                minDateTime={DateTime.create(values.startDate).date}
                onChange={(e) => {handleChange("endDate")(e && DateTime.create(e).format());}}
                slots={{
                    textField: (props) => <TextField
                        {...props}
                        variant="standard"
                        placeholder={t("end")}
                        className="date-picker"
                        helperText={endHelper}
                        error={!!endError}
                        onBlur={handleBlur("endDate")}
                        InputProps={{
                            endAdornment: (values.endDate &&
                                <IconButton
                                    aria-label={t("clear-end-date")}
                                    onClick={(e: Event) => {
                                        e.stopPropagation();
                                        setFieldValue("endDate", null);
                                    }}>
                                    {IconFactory.instance().create("clear")}
                                </IconButton>)
                        }} />
                }}
            />
        </div>
    </div>;
};