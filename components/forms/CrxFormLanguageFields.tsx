import React from "react";
import {map} from "lodash";

import {ILanguageFieldsProps} from "./@def";
import {H4} from "../Typography";
import {InputGroup} from "./InputGroup";
import {EnhancedLinkInput} from "./EnhancedLinkInput";
import {DESCRIPTION_MAX_LENGTH} from "../../core/Constants";
import {EEventCategory} from "../../core/@def";

export const LanguageFields: React.FC<ILanguageFieldsProps> = (props) => {
    const {language, currentLanguage, t} = props;
    const model = [
        {name: "title", required: true, multiLanguage: true},
        {name: "location", required: language === currentLanguage, multiLanguage: true},
        {name: "city", required: true, multiLanguage: true},
        {name: "description", multiline: true, multiLanguage: true, inputProps: {maxLength: DESCRIPTION_MAX_LENGTH, label: t("characters")}}
    ];
    if (language === currentLanguage) {
        model.splice(3, 0, {
            name: "zip",
            required: props.values.category !== EEventCategory.digital,
            multiLanguage: false
        });
    }
    return <React.Fragment>
        {language !== currentLanguage && <H4 key={`translation${language}`}>{t(`translation-${language}`)}</H4>}
        <EnhancedLinkInput key="link" {...props} />
        {
            map(model, (def) => {
                const {name, multiLanguage} = def;
                const path = multiLanguage ? [name, language].join(".") : name;
                return <InputGroup
                    key={path}
                    {...{
                        ...def,
                        ...props,
                        path: path
                    }} />;
            })
        }
    </React.Fragment >;
};