import React from "react";
import classNames from "classnames";
import * as Yup from "yup";
import {useRouter} from "next/router";
import {useTranslation} from "next-i18next";

import {INewsletterFormProps, IPageContextProps} from "../@def";
import Form from "./Form";
import Text from "../Text";
import {ValidationFactory} from "../../factories/ValidationFactory";
import {ELanguages} from "../../core/@def";


const NewsletterForm: React.FC<INewsletterFormProps & IPageContextProps> = (props) => {
    const {identifier, submitUrl, text, context} = props;
    const {locale: language} = useRouter();
    const {t} = useTranslation("common");
    const validationFactory = new ValidationFactory();
    // The following code can be used if we manage mailchimp to not require the email.
    // const required = "mail-or-phone-required";
    // const requireAorB = (cond, schema) => cond ? schema.nullable() : schema.required(required);
    // const validationSchema = Yup.object().shape({
    //     email: validationFactory.create("email", false, t).when("phone", requireAorB),
    //     zipch: validationFactory.create("zipch", true, t),
    //     phone: validationFactory.create("phone", false, t).when("email", requireAorB)
    // }, [["email", "phone"], ["phone", "email"]]);
    const validationSchema = Yup.object().shape({
        email: validationFactory.create("email", {required: true}),
        zipch: validationFactory.create("zipch", {required: true}),
        phone: validationFactory.create("phone", {required: false})
    });
    // See https://medium.com/codefully-io/react-forms-validation-with-formik-and-material-ui-1adf0c1cae5c
    return (
        <div className={classNames("form", props.identifier)}>
            <Text context={context} {...text} />
            <Form identifier={identifier}
                submitUrl={submitUrl}
                submitText={[{language: language as ELanguages, text: t("newsletter-submit")}]}
                submitTransform={(values) => ({...values, language: language})}
                fields={[
                    {key: "email", required: true},
                    {key: "zipch", required: true},
                    {key: "phone"}
                ]}
                validationSchema={validationSchema}
                useCustomError={true}
            />
        </div >
    );
};

export default NewsletterForm;