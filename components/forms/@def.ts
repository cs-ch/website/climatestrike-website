import {FormikProps, FormikTouched, FormikHandlers, FormikErrors} from "formik";

import {ICreateEventData, INamedObject, EEventCategory, ITranslation} from "../../core/@def";
import {InputBaseComponentProps} from "@mui/material";


type TChangeHandler = FormikHandlers["handleChange"];
type TBlurHandler = FormikHandlers["handleBlur"];

export interface IInputGroupProps<T = any> {
    name: string;
    required?: boolean;
    path?: string;
    multiline?: boolean;
    touched: FormikTouched<T>;
    values: T;
    inputProps?: InputBaseComponentProps;
    errors: FormikErrors<T>;
    useCustomError?: boolean;
    placeholder?: ITranslation[];
    language: string;
    t: (s: string) => string;
    handleChange: TChangeHandler;
    handleBlur: TBlurHandler;
}

export interface ILinkInputProps extends FormikProps<ICreateEventData> {
    t: (value: string) => string;
    language: string;
}


export interface IFormProps extends FormikProps<ICreateEventData> {
    t: (value: string) => string;
    language: string;
    languages: string[];
    parentOptions: (INamedObject & {category: EEventCategory})[];
}

export interface ILanguageFieldsProps extends IFormProps {
    currentLanguage: string;
}