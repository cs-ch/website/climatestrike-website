import React from "react";
import {includes, get, forEach} from "lodash";

import {ILinkInputProps} from "./@def";
import {InputGroup} from "./InputGroup";
import {IFacebookEventData} from "../../core/@def";
import {HttpService} from "../../core/HttpService";


interface IFacebookUpdateModel {
    source: keyof IFacebookEventData;
    target: string;
    transformer?: (value) => any;
}


export const EnhancedLinkInput: React.FC<ILinkInputProps> = (props) => {
    const {language, handleChange, setFieldTouched, setFieldValue, values} = props;
    const name = "link";
    const path = `link.${language}`;
    return <InputGroup
        {...{...props, name, path}}
        required={false}
        handleChange={(e) => {
            handleChange(path)(e);
            const $http = HttpService.instance();
            const link = e.target.value;
            const facebookPartialLink = "facebook.com/events/";
            if (includes(link, facebookPartialLink)) {
                $http.postd<IFacebookEventData>({url: "/api/facebook-event-info", body: {link}})
                    .then((result) => {
                        const updateModel: IFacebookUpdateModel[] = [
                            {source: "title", target: `title.${language}`},
                            {source: "description", target: `description.${language}`},
                            {source: "startDate", target: "startDate"},
                            {source: "endDate", target: "endDate"},
                            {source: "zip", target: "zip"},
                            {source: "city", target: `city.${language}`},
                            {source: "location", target: `location.${language}`}];
                        forEach(updateModel, def => {
                            const {source, target, transformer} = def;
                            if (!get(values, target)) {
                                const value = transformer ? transformer(result[source]) : result[source];
                                setFieldTouched(target, true);
                                setFieldValue(target, value);
                            }
                        });
                    })
                    .catch((err) => {
                        console.error(err);
                    });
            }
        }}
    />;
};