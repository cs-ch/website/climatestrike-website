import React from "react";
import {map, includes, reject, union} from "lodash";

import {IFormProps} from "./@def";
import {LanguageFields} from "./CrxFormLanguageFields";
import {H3, Paragraph} from "../Typography";
import {ChipSelector} from "../ChipSelector";
import {IconFactory} from "../../factories/IconFactory";


export const CrxFormLanguageSection: React.FC<IFormProps> = (props) => {
    const {values, setFieldValue, language, languages, t} = props;
    const iconFactory = IconFactory.instance();
    return <div className="language-section">
        <H3 key="header" text={t("language")} />
        <Paragraph key="paragraph" text={t("language-info")} />
        <ChipSelector
            key="chip-selector"
            className="create-event-selector"
            items={map(languages, id => ({id: id, name: t(id)}))}
            selected={values.languages}
            disabled={[language]}
            iconProvider={(item) => {
                if (item.id === language || includes(values.languages, item.id)) {
                    return iconFactory.create("check", {className: "icon"});
                }
                return null;
            }}
            onClick={(id) => {
                if (id !== language) {
                    const newValues = includes(values.languages, id) ?
                        reject(values.languages, eventLanguage => eventLanguage === id)
                        : union(values.languages, [id]);
                    setFieldValue("languages", newValues);
                }
            }}
        />
        {
            map(reject(values.languages, l => l === language), eventLanguage => {
                return <LanguageFields
                    key={`language-fields-${eventLanguage}`}
                    {...props}
                    language={eventLanguage}
                    currentLanguage={language} />;
            })
        }
    </div>;
};