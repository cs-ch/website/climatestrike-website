import React from "react";
import {includes, map} from "lodash";
import DomPurify from "isomorphic-dompurify";
import {Checkbox, FormControl, FormControlLabel, FormHelperText} from "@mui/material";


import {IField} from "../@def";
import {SelectDropdown} from "../SelectDropdown";
import {InputGroup} from "./InputGroup";
import {HTML} from "../HTML";
import {getValueFromLanguageDict} from "../../core/Utils";


export class FieldFactory {
    constructor(
        private useCustomError: boolean,
        private translationChecker: any,
        private t: any,
        private language: any,
        private getLanguageText: any) { }

    public create(field: IField, formikProps: any) {
        const {touched, values, errors, handleBlur, handleChange} = formikProps;
        if (includes(["boolean", "keepMePosted", "acceptPrivacyPolicy"], field.type)) {
            const label = this.getBooleanLabel(field);
            const error = errors[field.type];
            const hasTouch = touched[field.type];
            return <FormControl error={error} component="fieldset">
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={values[field.key]}
                            onChange={(e) => {
                                e.target.name = field.key;
                                handleChange(e);
                            }}
                            name={field.key}
                            color="primary"
                        />
                    }
                    label={label}
                />
                {error && hasTouch && <FormHelperText>{this.t(error)}</FormHelperText>}
            </FormControl>;
        }
        if (includes(["dropdown", "checkboxes"], field.type)) {
            const allowMultiple = field.type == "checkboxes";
            const rawOptions = getValueFromLanguageDict(field.config, this.language) || (allowMultiple ? [] : "");
            const options = map(rawOptions, (option) => ({id: option, name: option}));
            const fieldValues = values[field.key];
            return <SelectDropdown
                className=""
                multiple={allowMultiple}
                id={field.key}
                value={fieldValues}
                onChange={(e) => {
                    e.target.name = field.key;
                    handleChange(e);
                }}
                options={options}
                label={this.getLanguageText(field.placeholder)}
            />;
        }
        return <InputGroup {...{
            name: field.key, useCustomError: this.useCustomError,
            ...field, touched, values, errors, handleChange, handleBlur,
            t: this.t, translationChecker: this.translationChecker, language: this.language
        }} key={field.key} />;
    }

    private getBooleanLabel(field: IField) {
        switch (field.type) {
            case "keepMePosted":
                return this.t("keep-me-posted");
            case "acceptPrivacyPolicy":
                return <HTML text={DomPurify.sanitize(this.t("accept-privacy-policy"))} />;
            default:
                return this.getLanguageText(field.placeholder);

        }
    }
}
