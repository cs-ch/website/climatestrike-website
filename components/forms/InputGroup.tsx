import React from "react";
import {get} from "lodash";
import classNames from "classnames";
import {FormControl, InputLabel, Input, FormHelperText, InputBaseComponentProps} from "@mui/material";

import {IInputGroupProps} from "./@def";
import {getLanguageText} from "../../core/I18n";


interface IHelperTextProps {
    value: string;
    inputProps?: InputBaseComponentProps;
    helperText?: string;
}

const HelperText: React.FC<IHelperTextProps> = (props) => {
    const {value, helperText, inputProps} = props;
    const maxLength = inputProps && inputProps.maxLength;
    if (!maxLength) return <React.Fragment>{helperText}</React.Fragment>;
    return <span className="max-length-helper">
        {`${value.length}/${maxLength}${inputProps.label ? " " + inputProps.label : ""}`}
    </span>;
};

export const getError = ({path, touched, errors}) => {
    const error = get(errors, path) as string;
    const isTouched = get(touched, path);
    const showError = isTouched && error;
    return showError;
};

export const getHelperText = ({name, value, required, error, useCustomError, t}) => {
    const showError = !!error;
    let helperKey = `${name}-helper`;
    if (required && !value && showError) helperKey = useCustomError ? error : "required-field";
    else if (showError) helperKey = useCustomError ? error : `${name}-invalid`;
    const translated = t(helperKey);
    if (translated != helperKey) {
        return translated;
    }
    return null;
};

export const InputGroup: React.FC<IInputGroupProps> = (props) => {
    const {name, required, multiline, inputProps, useCustomError,
        touched, values, errors, handleChange, handleBlur, t, language} = props;
    const path = props.path || name;
    const value = get(values, path);
    const helperKey = `${name}-helper`;
    const error = getError({path, touched, errors});
    const helperText = getHelperText({name, value, required, error, useCustomError, t});
    const placeholder = props.placeholder ? getLanguageText(props.placeholder, language) : t(name);
    return <FormControl
        key={path}
        error={!!error}
        required={required}
        className={classNames("form-control", `form-control-${name}`, {required, error: !!error})}
    >
        <InputLabel htmlFor={path} className="input-label" variant="standard">{placeholder}</InputLabel>
        <Input id={path} aria-describedby={helperKey} className="input" multiline={multiline} inputProps={inputProps}
            value={value} onChange={handleChange} onBlur={handleBlur}></Input>
        <FormHelperText id={helperKey} variant="standard" className={classNames("helper-text", helperKey)}><HelperText {...{value, inputProps, helperText}} /></FormHelperText>
    </FormControl>;
};