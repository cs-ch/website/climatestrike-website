import React, {useState} from "react";
import {map, reduce, filter, some} from "lodash";
import {Formik} from "formik";
import * as Yup from "yup";
import {useRouter} from "next/router";
import {useTranslation} from "next-i18next";

import {CrxFormBasics} from "./CrxFormBasics";
import {CrxFormLanguageSection} from "./CrxFormLanguageSection";
import {CrxFormContactSection} from "./CrxFormContactSection";
import {ICreateEventProps, ESnackbarSeverity, EStyles} from "../@def";
import {Button} from "../Button";
import {H2, Paragraph} from "../Typography";
import {Snackbar} from "../Snackbar";
import {ICreateEventData, independentId, EEventCategory, ETimeUnit} from "../../core/@def";
import {configuration} from "../../core/Configuration";
import {DateTime} from "../../core/DateTime";
import {languageTextProvider} from "../../core/I18n";
import {HttpService} from "../../core/HttpService";
import {UNKNOWN_ERROR_ID, REQUIRED_ERROR_ID, DESCRIPTION_MAX_LENGTH, DATE_FORMAT, BACKEND_DATE_FORMAT} from "../../core/Constants";
import {ValidationFactory} from "../../factories/ValidationFactory";


const arrayReducer = (list, validator: Yup.AnySchema<any>) => reduce(
    list,
    (prev, current) => {
        prev[current] = validator;
        return prev;
    },
    {}
);

const objectSchema = (list, validator: Yup.AnySchema<any>) => Yup.object().shape(arrayReducer(list, validator));


const CreateEventForm: React.FC<ICreateEventProps> = (props) => {
    const {items} = props;
    const router = useRouter();
    const {t} = useTranslation("create-event");
    const language = router.locale;
    const languages = configuration.languages.languages;
    const getLanguageText = languageTextProvider(language);
    const createSwitch = () => reduce(languages, (prev, curr) => {prev[curr] = ""; return prev;}, {});
    const initialValues: ICreateEventData = {
        languages: [language],
        parentId: independentId,
        link: createSwitch(),
        title: createSwitch(),
        startDate: null,
        endDate: null,
        category: EEventCategory.strike,
        description: createSwitch(),
        zip: "",
        city: createSwitch(),
        location: createSwitch(),
        contact: ""
    };

    const [snackbarState, setSnackbarState] = useState({
        open: false,
        severity: null,
        message: null
    });
    const now = DateTime.create();
    const parentEvents = filter(items, item => item.multipleChildren
        && DateTime.create(item.start, BACKEND_DATE_FORMAT).isSameOrAfter(now));
    const parentOptions = map(parentEvents, (eventGroup) => {
        const dateHint = DateTime
            .create(eventGroup.start, BACKEND_DATE_FORMAT)
            .format(DATE_FORMAT);
        return {
            id: (eventGroup as any).id,
            name: `${getLanguageText(eventGroup.title)} (${dateHint})`,
            category: eventGroup.category
        };
    });
    parentOptions.unshift({id: independentId, name: t(independentId), category: null});

    const requiredString = Yup.string().required("required-field");
    const validationSchema = Yup.object().shape(
        {
            languages: Yup.array()
                .min(1, UNKNOWN_ERROR_ID)
                .max(languages.length, UNKNOWN_ERROR_ID)
                .required(),
            parentId: Yup.mixed().required(REQUIRED_ERROR_ID),
            link: Yup.object().when("languages", (languages) => objectSchema(languages, Yup.string().url().nullable())),
            title: Yup.object().when("languages", (languages) => objectSchema(languages, requiredString)),
            description: Yup.object().when("languages", (languages) => objectSchema(languages, Yup.string().max(DESCRIPTION_MAX_LENGTH).nullable())),
            startDate: Yup.date().required("required-field"),
            endDate: Yup.date().when("startDate", (startDate, schema: Yup.AnySchema) => {
                return startDate ?
                    schema.test("after-start", "must-be-after-start", value => !value || DateTime.create(startDate[0]).isBefore(DateTime.create(value), ETimeUnit.minute)).nullable()
                    : schema.nullable();
            }),
            zip: Yup.mixed().when("category", (category) =>
                ValidationFactory.instance().create("zipch", {required: category[0] !== EEventCategory.digital})),
            city: Yup.object().when("languages", (languages) => objectSchema(languages, requiredString)),
            location: Yup.object().when("languages", (languages) => objectSchema(languages, requiredString)),
            category: Yup.string().required().oneOf(map(EEventCategory)),
            contact: Yup.string().nullable().test(
                "contact",
                "invalid-contact",
                (value) => {
                    return some(
                        [
                            Yup.string().email(),
                            ValidationFactory.instance().create("phone", {required: true}),
                            Yup.string().matches(/^@[^\s]+/)
                        ],
                        test => test.isValidSync(value));
                }
            )
        });
    return <div className="create-event-form-container">
        <H2 text={t("header")} />
        <Paragraph text={t("info")} />
        <Formik<ICreateEventData>
            initialValues={initialValues}
            onSubmit={(values, helpers) => {
                const $http = HttpService.instance();
                const onSuccess = () => {
                    setSnackbarState({open: true, severity: ESnackbarSeverity.success, message: t("form-successful")});
                    helpers.resetForm({values: initialValues});
                };
                const onError = (err) => {
                    setSnackbarState({open: true, severity: ESnackbarSeverity.error, message: t("form-failed")});
                    helpers.setSubmitting(false);
                    console.error(err);
                };
                $http.post({url: "/api/register-event", body: values})
                    .then((response) => {
                        response.json()
                            .then((content: {success: boolean}) => {
                                if (content.success) {
                                    onSuccess();
                                }
                                else {
                                    onError("Request failed. Check server logs for further information");
                                }
                            })
                            .catch((err) => {
                                onError(`Received non-json response: ${err}`);
                            });

                    })
                    .catch(onError);
            }}
            validationSchema={validationSchema}
        >
            {(props) => {
                const otherProps = {parentOptions, language, languages, t};
                return <form className="create-event-form" onSubmit={props.handleSubmit}>
                    <CrxFormBasics {...props} {...otherProps} />
                    <CrxFormLanguageSection {...props} {...otherProps} />
                    <CrxFormContactSection {...props} {...otherProps} />
                    <Button style={EStyles.primary} type="submit" disabled={props.isSubmitting} text={[{language: language as any, text: t("submit")}]} />
                </form>;
            }}
        </Formik>
        <Snackbar {...snackbarState} setOpen={(open) => setSnackbarState({...snackbarState, open})} />
    </div >;
};

export default CreateEventForm;