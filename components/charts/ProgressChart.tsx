import {min} from "lodash";
import React from "react";

import {turquoise} from "../../core/Theme";



interface IProgressChart {
    percentage: number;
    color?: string;
    height?: number;
}

export const ProgressChart = (props: IProgressChart) => {
    const {percentage, color = turquoise, height = 16} = props;
    return <div className="progress-chart" style={{width: "100%", height: height, borderRadius: height / 2}}>
        <div className="bar" style={{height: height, width: (min([percentage, 1]) * 100) + "%", backgroundColor: color, borderRadius: height / 2}} />
    </div >;
};