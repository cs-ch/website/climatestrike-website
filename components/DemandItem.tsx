import React, {useState} from "react";
import {useRouter} from "next/router";
import classNames from "classnames";

import {IPageContextProps, IDemandItemProps, EStyles} from "./@def";
import {Image} from "./Core";
import {languageTextProvider, getLanguageImage} from "../core/I18n";
import {ExpandCollapseButton} from "./Button";
import {HTML} from "./HTML";
import {H3} from "./Typography";
import {isReverse} from "../core/Utils";


const ExpandButton = ({expanded, setExpanded}) =>
    <ExpandCollapseButton
        style={EStyles.primary}
        expanded={expanded}
        onClick={() => setExpanded(!expanded)}
    />;

const DemandItem: React.FC<IDemandItemProps & IPageContextProps> = (props) => {
    const {context} = props;
    const {locale: language} = useRouter();
    const getLanguageText = languageTextProvider(language);
    const title = getLanguageText(props.title);
    const lead = getLanguageText(props.lead);
    const text = getLanguageText(props.text);
    const icon = getLanguageImage(props.icon, language);
    const [expanded, setExpanded] = useState(false);
    if (!title) return null;
    const expandButtonProps = {expanded, setExpanded};
    const reverse = isReverse(context.itemIdentifier, "demand-item", context.items);
    return <div className={classNames("demand-item", context.itemIdentifier)}>
        <div key="head" className={classNames("head", {reverse})}>
            <div key="icon" className="icon">{icon && <Image image={props.icon} />}</div>
            <div key="headline" className={"headline"} >
                <div key="content" className="content">
                    <H3 key="title" text={title} />
                    {lead && <HTML key="description" text={lead} />}
                </div >
                {text && !expanded && <ExpandButton {...expandButtonProps} />}
            </div>
        </div>
        {expanded && <div key="text" className="text">
            <HTML text={text} />
        </div>}
        {text && expanded && <ExpandButton {...expandButtonProps} />}
    </div >;
};

export default DemandItem;