import React from "react";
import {isArray, map} from "lodash";
import classNames from "classnames";
import {Checkbox, FormControl, InputLabel, ListItemText, MenuItem, Select} from "@mui/material";

import {INamedObject} from "../core/@def";
import {IconFactory} from "../factories/IconFactory";


interface ISelectDropdownProps {
    className: string;
    id: string;
    label: string;
    value: any;
    onChange: (event: React.ChangeEvent<any>) => void;
    options: INamedObject[];
    multiple?: boolean;
}

export const SelectDropdown: React.FC<ISelectDropdownProps> = (props) => {
    const {className, id, label, value, onChange, multiple, options} = props;
    return <FormControl className={classNames(id, "select-dropdown", "form-control", className)} variant="outlined">
        <InputLabel variant="standard" htmlFor={id}>{label}</InputLabel>
        <Select
            className="select"
            native={!multiple}
            value={value}
            onChange={onChange}
            inputProps={{
                id: id,
                name: label
            }}
            variant="standard"
            renderValue={(selected) => {return isArray(selected) ? selected.join(", ") : selected;}}
            IconComponent={() => IconFactory.instance().create("expand")}
            multiple={multiple}
        >
            {multiple
                ? map(options, (option) => {
                    return < MenuItem key={"" + option.id} value={"" + option.id} >
                        <Checkbox checked={value.indexOf(option.id) > -1} />
                        <ListItemText primary={option.name} />
                    </MenuItem>;
                })
                : map(options, (option) => <option className="option" key={option.id} value={option.id}>{option.name}</option>)}
        </Select>
    </FormControl >;
};