import {find, first} from "lodash";

import {ITranslationBase, ELanguages, ITranslation, IImageTranslation, IImage} from "./@def";
import {IMedium, IMediumItem} from "../components/@def";


export const languageTextProvider = (languageId: string, fallback: boolean = false) => (translations: ITranslation[]) => getLanguageText(translations, languageId, fallback);

export const getLanguageText = (translations: ITranslation[], languageId: string, fallback: boolean = false) => {
    return getLanguageItem(translations, languageId, "text", fallback);
};

export const languageImageProvider = languageId => (translations: IImageTranslation[]) => getLanguageImage(translations, languageId);

export const getLanguageImage = (translations: IImageTranslation[], languageId: string) => {
    return getLanguageItem(translations, languageId, "image") as IImage;
};

export const getLanguageMedium = (translations: IMediumItem[], languageId: string) => {
    return getLanguageItem(translations, languageId, "medium") as IMedium;
};

const getLanguageItem = <T extends ITranslationBase>(translations: T[], languageId: string, key: Exclude<keyof T, "language">, fallback: boolean = false) => {
    let translation = find(translations, item => item.language === languageId);
    if (!translation) {
        translation = find(translations, item => item.language === ELanguages.all);
    }
    if (fallback && !translation) {
        translation = first(translations);
    }
    return translation ? translation[key] : null;
};
