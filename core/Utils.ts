import {v4} from "uuid";
import {filter, findIndex, get, isEmpty, kebabCase} from "lodash";

import {TObjectId, IContentItem, ELanguages} from "./@def";


export const uuid = v4;

export const slugify = (input: string) => kebabCase(input);

export const isReverse = (itemIdentifier: TObjectId, type: string, items: IContentItem[]) => {
    const enumItems = filter(items, item => item.type == type);
    const itemIndex = findIndex(enumItems, item => item.identifier == itemIdentifier);
    return !((itemIndex + 1) % 2);
};

export const getTextSelection = () => {
    if (window.getSelection) {
        return window.getSelection().toString();
    }
    else if (document.getSelection) {
        return document.getSelection().toString();
    }
    return null;
};

export const ellipsisTrim = (d: string, n: number) => {
    if (!d || (d.length <= n)) return d;
    const result = d.substr(0, n - 1).trim() + "…";
    return result;
};

export const updatePostLink = (dp) => {
    const cardData = {...dp};
    if (isEmpty(cardData.link)) {
        cardData.link = [{language: ELanguages.all, text: "/posts/" + dp.identifier}];
    }
    return cardData;
};

export const getValueFromLanguageDict = (dict, language) => {
    return get(dict, language) || get(dict, "all");
};

export const setCookie = (name: string, value: string, expireAfterDays: number = 10000) => {
    const d = new Date();
    d.setTime(d.getTime() + (expireAfterDays * 24 * 60 * 60 * 1000));
    const expires = "expires=" + d.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
};