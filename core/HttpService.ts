import {IHttpService, IHttpConfig, IHttpRequestConfig, IHttpResponse} from "../core/@def";
import {log} from "../server/core/Logger";

export class HttpService implements IHttpService {
    private baseURL: string;
    private authHeader: string;

    constructor(config: IHttpConfig = {}) {
        this.baseURL = config.baseURL;
    }

    public static instance(config?: IHttpConfig): IHttpService {
        return new HttpService(config);
    }

    public setAuthHeader(value?: string) {
        this.authHeader = value;
    }

    public getAuthHeader() {
        return this.authHeader;
    }

    public async request<T>(config: IHttpRequestConfig): Promise<IHttpResponse<T>> {
        const url = this.getUrl(config);
        const init = this.getRequestInit(config);
        log.debug(`HttpService - fetching (${init.method}): ${url}`);
        const result = await fetch(url, init);
        return Promise.resolve(result);
    }

    public async fetch<T>(url: string, init: any): Promise<T> {
        init.headers = {...init.headers, Authorization: this.authHeader};
        return fetch(this.getUrl({url}), init) as any;
    }

    public async requestd<T>(config: IHttpRequestConfig): Promise<T> {
        const response = await this.request<T>(config);
        if (response.ok) {
            const json = await response.json();
            return Promise.resolve(json);
        }
        const content = await response.text();
        log.error(`Failed request with status ${response.status} and message ${content}`);
        return Promise.reject(response.statusText);
    }

    public get<T>(config: IHttpRequestConfig): Promise<IHttpResponse<T>> {
        return this.request<T>({...config, method: "GET"});
    }

    public getd<T>(config: IHttpRequestConfig): Promise<T> {
        return this.requestd({...config, method: "GET"});
    }

    public post<T>(config: IHttpRequestConfig): Promise<IHttpResponse<T>> {
        return this.request({...config, method: "POST"});
    }

    public postd<T>(config: IHttpRequestConfig): Promise<T> {
        return this.requestd({...config, method: "POST"});
    }

    public put<T>(config: IHttpRequestConfig): Promise<IHttpResponse<T>> {
        return this.request({...config, method: "PUT"});
    }

    public putd<T>(config: IHttpRequestConfig): Promise<T> {
        return this.requestd({...config, method: "PUT"});
    }

    public delete<T>(config: IHttpRequestConfig): Promise<T> {
        return this.requestd({...config, method: "DELETE"});
    }

    private getUrl(config: IHttpRequestConfig): string {
        let url = config.url;
        const baseURL = config.baseURL || this.baseURL;
        if (baseURL) {
            url = new URL(url, baseURL).toString();
        }
        return url;
    }

    private getRequestInit(config: IHttpRequestConfig): RequestInit {
        const headers = this.getRequestHeaders(config);
        const init: RequestInit = {
            headers,
            method: config.method,
            body: JSON.stringify(config.body)
        };
        return init;
    }

    private getRequestHeaders(config: IHttpRequestConfig) {
        const headers: any = {...config};
        headers["Content-Type"] = "application/json";
        headers["credentials"] = "same-origin";
        if (this.authHeader) {
            headers["Authorization"] = this.authHeader;
        }
        return headers;
    }
}