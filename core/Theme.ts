import {createTheme as createMuiTheme} from "@mui/material/styles";
import {isNull} from "lodash";

const orange = "#FF7235";
const yellow = "#FFAB2D";
const blue = "#258EBB";
export const turquoise = "#20B8C1";
const lightThemePrimary = "#000000";
const darkThemePrimary = "#F1F1F1";

// Not an official color
// const green = "#A7D12F";

const createTheme = (config: {mode: "light" | "dark", primaryTextColor: string}) => {
    const {mode, primaryTextColor} = config;
    const theme = createMuiTheme({
        palette: {
            mode: mode,
            primary: {main: turquoise},
            secondary: {main: blue},
            error: {main: orange},
            warning: {main: yellow},
            info: {main: blue},
            success: {main: turquoise},
            text: {primary: primaryTextColor}
        },
        breakpoints: {
            values: {
                xs: 0,
                sm: 600,
                md: 960,
                lg: 1280,
                xl: 1920
            }
        }
    });

    theme.typography.fontFamily = "Klima";
    for (const header of ["h1", "h2", "h3", "h3", "h4", "h5", "h6"]) {
        theme.typography[header].fontFamily = "Graph Condensed, Impact, Arial, sans-serif";
        theme.typography[header].letterSpacing = "0.015em";
        theme.typography[header].marginBottom = "0.2em";

    }
    for (const content of ["body1", "body2", "button", "caption", "overline"]) {
        theme.typography[content].fontFamily = "Klima, Georgia, Arial, sans-serif";
        theme.typography[content].letterSpacing = "0.046em";
        theme.typography[content].lineHeight = "140%";
    }
    return theme;
};

// Inspired by https://stackoverflow.com/questions/56300132/how-to-override-css-prefers-color-scheme-setting
class Theme {
    private light = createTheme({mode: "light", primaryTextColor: lightThemePrimary});
    private dark = createTheme({mode: "dark", primaryTextColor: darkThemePrimary});

    public getTheme(override: boolean = null) {
        const darkPreferred = this.isDarkPreferred(override);
        const theme = darkPreferred ? this.dark : this.light;
        return theme;
    }

    public isDarkPreferred(override: boolean = null): boolean {
        let darkPreferred = false;
        if (!isNull(override)) {
            return override;
        }
        const isClient = typeof window === "object";

        try {
            if (isClient) {
                if (localStorage.getItem("theme")) {
                    if (localStorage.getItem("theme") == "dark") {
                        darkPreferred = true;
                    }
                }
                else if (!window.matchMedia) {
                    return darkPreferred;
                }
                else if (window.matchMedia("(prefers-color-scheme: dark)").matches) {
                    darkPreferred = true;
                }
            }
        }
        catch (err) {
            // Not on the client
        }
        return darkPreferred;
    }

    public switchTheme(toDark: boolean) {
        const theme = toDark ? "dark" : "light";
        localStorage.setItem("theme", theme);
        if (toDark) {
            document.documentElement.setAttribute("data-theme", "dark");
        }
        else {
            document.documentElement.setAttribute("data-theme", "light");
        }
    }
}

export const theme = new Theme();