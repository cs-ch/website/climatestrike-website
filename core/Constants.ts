export const ICON_MENU_ITEM = "icon-menu-item";
export const LANGUAGES = "languages";

export const BACKEND_DATE_FORMAT = "YYYY-MM-DD";
export const DATE_FORMAT = "DD.MM.YYYY";
export const TIME_FORMAT = "HH:mm";
export const DATE_TIME_FORMAT = `${DATE_FORMAT}, ${TIME_FORMAT}`;
export const DATE_TIME_MASK_FORMAT = DATE_TIME_FORMAT.replace(/[dhmy]/gi, "_");
export const UNKNOWN_ERROR_ID = "unknown-error";
export const REQUIRED_ERROR_ID = "required-error";
export const DESCRIPTION_MAX_LENGTH = 600;

export const EXPANDABLE_DESCRIPTION_CLASSNAME = "expandable-description";