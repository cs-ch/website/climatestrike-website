import {IConfiguration, ELinkLocation, ELanguages} from "../core/@def";
import {ICON_MENU_ITEM} from "./Constants";
import {merge} from "lodash";


const socialConfig = [
    {
        id: "instagram",
        url: [
            {language: ELanguages.de, text: "https://www.instagram.com/klimastreikschweiz/"},
            {language: ELanguages.fr, text: "https://www.instagram.com/climatestrike_suisse/"},
            {language: ELanguages.it, text: "https://www.instagram.com/scioperoperilclima"}
        ]
    },
    {
        id: "facebook",
        url: [
            {language: ELanguages.de, text: "https://www.facebook.com/klimastreikschweiz/"},
            {language: ELanguages.fr, text: "https://www.facebook.com/GreveDuClimatSuisse/"},
            {language: ELanguages.it, text: "https://www.facebook.com/scioperoperilclima/"},
        ]
    },
    {
        id: "x",
        url: [
            {language: ELanguages.de, text: "https://x.com/klimastreik"},
            {language: ELanguages.fr, text: "https://x.com/greveduclimat"}
        ]
    },
    {
        id: "youtube",
        url: [{language: ELanguages.all, text: "https://www.youtube.com/channel/UC_caP9IpnSQ_t6KTAOBVqYA"}]
    }
];

const iconMenuContribution = {
    type: ICON_MENU_ITEM,
    config: {
        src: "/static/images/logo-new.svg",
        href: "/",
    }
};

export const configuration: IConfiguration = {
    dataSources: {
        type: "strapi",
        config: {
            url: process.env.BACKEND_URL,
            mediaUrl: process.env.MEDIA_BACKEND_URL
        }
    },
    menu: {
        contributions: [
            {
                type: ICON_MENU_ITEM,
                config: {
                    src: "/static/images/logo-new.svg",
                    href: "/",
                }
            },
            {
                type: "menu-items",
                config: {
                    location: ELinkLocation.header
                }
            }
        ]
    },
    submenu: {
        contributions: [
            {
                type: "social",
                config: {
                    social: socialConfig
                }
            },
            {
                type: "languages",
                config: {
                    languages: [
                        {id: "de", name: "DE"},
                        {id: "fr", name: "FR"},
                        {id: "it", name: "IT"}
                    ]
                }
            },
            {
                type: "light-dark-toggle",
                config: {}
            }
        ]
    },
    footer: {
        contributions: [
            // FIXME: Make configurable
            merge({}, iconMenuContribution, {config: {description: true}}),
            {
                type: "newsletter",
                config: {}
            },
            {
                type: "hierarchical-links",
                config: {
                    links: [
                        {
                            id: "climate-crisis",
                            children: [
                                {id: "facts", href: "/crisis#facts"},
                                {id: "solutions", href: "/crisis#solutions"}
                            ]
                        },
                        {
                            id: "support-us",
                            children: [
                                {id: "join", href: "/join"},
                                {id: "donate", href: "/donate"}
                            ]
                        },
                        {
                            id: "movement",
                            children: [
                                {id: "join", href: "/join"},
                                {id: "regio", href: "/join#regio"},
                                {id: "events", href: "/events"},
                                {id: "wiki", href: "https://de.climatestrike.ch"}, // FIXME Use correct language
                                {id: "legal", href: "/antirep"},
                            ]
                        },
                        {
                            id: "press",
                            children: [
                                {id: "contacts", href: "/media"}]
                        },
                    ]
                }
            },
            {
                type: "social",
                config: {
                    description: true,
                    social: socialConfig
                }
            },
            {
                type: "menu-items",
                config: {
                    nColumns: 1,
                    location: ELinkLocation.footnote
                }
            },
            {
                type: "footnotes",
                config: {
                    translationKeys: ["copyright", "icon-source"]
                }
            }
        ]
    },
    languages: {
        languages: ["de", "fr", "it"],
        default: "de"
    }
};