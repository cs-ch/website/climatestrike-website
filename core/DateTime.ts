import dayjs, {Dayjs} from "dayjs";

import {ETimeUnit, IDateTime} from "./@def";
import {DATE_FORMAT, TIME_FORMAT, DATE_TIME_FORMAT} from "./Constants";



export class DateTime implements IDateTime {
    public date: Dayjs;

    constructor(d?: string | Date | Dayjs, f?: string) {
        this.date = (d && f) ? dayjs(d, f) : d ? dayjs(d) : dayjs();
    }

    public static create(d?: string | Date | Dayjs, f?: string): IDateTime {
        return new DateTime(d, f);
    }

    public isSame(other: IDateTime, unit: ETimeUnit = ETimeUnit.day): boolean {
        return this.date.isSame(other.date, unit);
    }

    public isBefore(other: IDateTime, unit?: ETimeUnit): boolean {
        return this.date.isBefore(other.date) && !this.isSame(other, unit);
    }

    public isSameOrBefore(other: IDateTime, unit?: ETimeUnit): boolean {
        return this.isSame(other, unit) || this.isBefore(other, unit);
    }

    public isAfter(other: IDateTime, unit?: ETimeUnit): boolean {
        return this.date.isAfter(other.date, unit) && !this.isSame(other, unit);
    }

    public isSameOrAfter(other: IDateTime, unit?: ETimeUnit): boolean {
        return this.isSame(other, unit) || this.isAfter(other, unit);
    }

    public format(f?: string) {
        return f ? this.date.format(f) : this.date.format();
    }

    public formatDate() {
        return this.format(DATE_FORMAT);
    }

    public formatTime() {
        return this.format(TIME_FORMAT);
    }

    public formatDateTime() {
        return this.format(DATE_TIME_FORMAT);
    }
}
