// Core

import {Dayjs} from "dayjs";
import {IPostProps} from "../components/@def";

export interface IDictionary<T> {
    [index: string]: T;
}

export type TObjectId = string;

export interface IIdentifiable {
    id: TObjectId;
}

export interface INamedObject extends IIdentifiable {
    name: string;
}

export interface IUrlObject extends IIdentifiable {
    url: ITranslation[];
}

export interface IStrategy<T = any> {
    type: string;
    config: T;
}

// Services

export interface IHttpConfig {
    baseURL?: string
}

export interface IHttpRequestConfig {
    url?: string;
    method?: "GET" | "POST" | "PUT" | "DELETE";
    baseURL?: string;
    headers?: any;
    body?: any;
}

export interface IHttpResponse<T> {
    json(): Promise<T>;
    text(): Promise<string>;
    ok: boolean;
    status: number;
    statusText: string;
}

export interface IHttpService {
    getAuthHeader(): string;
    setAuthHeader(value: string): void;
    fetch<T>(url: string, init: any): Promise<T>;
    request<T>(config: IHttpRequestConfig): Promise<IHttpResponse<T>>;
    requestd<T>(config: IHttpRequestConfig): Promise<T>;
    get<T>(config: IHttpRequestConfig): Promise<IHttpResponse<T>>;
    getd<T>(config: IHttpRequestConfig): Promise<T>;
    post<T>(config: IHttpRequestConfig): Promise<IHttpResponse<T>>;
    postd<T>(config: IHttpRequestConfig): Promise<T>;
    put<T>(config: IHttpRequestConfig): Promise<IHttpResponse<T>>;
    putd<T>(config: IHttpRequestConfig): Promise<T>;
    delete<T>(config: IHttpRequestConfig): Promise<T>;
}

// Config

interface ILanguageConfig {
    languages: TObjectId[];
    default: TObjectId;
    paths?: IDictionary<string>;
}

export interface IMenuConfig {
    contributions: IStrategy[];
}

export interface ISubMenuConfig {
    contributions: IStrategy[];
}

export interface IFooterConfig {
    contributions: IStrategy[];
}

export interface IConfiguration {
    dataSources: IStrategy;
    menu: IMenuConfig;
    submenu: ISubMenuConfig;
    footer: IFooterConfig;
    languages: ILanguageConfig;
}

// Content

// - Page & API

export interface IContentItem<T = any> {
    id?: number;
    identifier: TObjectId;
    sortId: number;
    grouping?: string;
    type: string;
    config: T;
}

export enum ELinkLocation {
    header = "header",
    footer = "footer",
    footnote = "footnote"
}

export interface IDataDependency {
    identifier: string;
    type: "POST";
    route: string;
}

interface IPageConfig {
    linkLocations?: ELinkLocation[]; // e.g. ["menu", "footer"]
    hideFooter?: boolean;
    dataDependencies?: IDataDependency[];
    disableScrollToTop?: boolean;

}

export enum EImageRole {
    pageAndPreview = "PageAndPreview",
    previewOnly = "PreviewOnly"
}

export interface IAttributeWrapper<T> {
    id: number;
    attributes: T;
}

interface IStrapiMetaResponse {
    pagination: {
        page: number;
        pageSize: number;
        pageCount: number;
        total: number;
    }
}

export interface IStrapiResponse<T> {
    data: IAttributeWrapper<T>[];
    meta: IStrapiMetaResponse;
}

export interface IPage {
    id: number;
    identifier: string; // should differ from slug only for home page
    slug: string;
    sortId: number;
    imageRole?: EImageRole;
    title?: ITranslation[]; // only used to correctly render the Head for posts
    description?: ITranslation[];
    image?: IImageTranslation[];
    config: IPageConfig;
}

export interface IPageModelStrategy {
    getPages(): Promise<IPage[]>;
    getContent(id: number, dataDependencies?: IDataDependency[]): Promise<IContentItem[]>;
    getPosts(skeletonOnly?: boolean): Promise<IPostProps[]>;
    getPost(id: string): Promise<IPostProps>;
    submitForm(data: IFormData): Promise<void>;
    getFormSubmissionCount(id: string): Promise<number>;
    registerEvent(data: ICreateEventData): Promise<void>;
}

// - Core

export enum ELanguages {
    all = "all",
    de = "de",
    fr = "fr",
    it = "it"
}

// -- DateTime

export enum ETimeUnit {
    day = "day",
    minute = "minute"
}

export interface IDateTime {
    date: Dayjs;
    isSame(d: IDateTime, unit: ETimeUnit): boolean;
    isBefore(d: IDateTime, unit?: ETimeUnit): boolean;
    isSameOrBefore(d: IDateTime, unit?: ETimeUnit): boolean;
    isAfter(d: IDateTime, unit?: ETimeUnit): boolean;
    isSameOrAfter(d: IDateTime, unit?: ETimeUnit): boolean;

    format(f?: string): string;
    formatDate(): string;
    formatTime(): string;
    formatDateTime(): string;
}

export interface ITranslationBase {
    language: ELanguages;
}

export interface ITranslation extends ITranslationBase {
    text: string;
}

interface IImageFormat {
    url: string;
    src: string; // usable for srcset property
    height: number;
    width: number;
}

export interface IImage {
    url: string;
    formats?: IImageFormat[];
    alt?: string;
    caption?: string;
}

export interface IImageTranslation extends ITranslationBase {
    image: IImage;
}

export interface IAudioTranslation extends ITranslationBase {
    file: {src: string;};
}

export interface ILink {
    type?: string;
    link: ITranslation[];
    text?: ITranslation[];
}

export enum EEventCategory {
    strike = "strike",
    meetup = "meetup",
    activity = "activity",
    digital = "digital"
}

export interface IFormData {
    identifier: string;
    [index: string]: any;
}

export const independentId = "independent-event";

export interface ITranslationDict extends IDictionary<string> { }

export interface ICreateEventData {
    languages: string[];
    parentId: string | number;
    link: ITranslationDict;
    title: ITranslationDict;
    description: ITranslationDict;
    startDate: any;
    endDate?: any;
    zip: string;
    city: ITranslationDict;
    location: ITranslationDict;
    category: EEventCategory;
    contact: string;
}

export interface IFacebookEventData {
    title: string;
    description: string;
    startDate: string;
    endDate?: string;
    zip: string;
    city: string;
    location: string;
}